﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_DemoLeaderboard : MonoBehaviour
{

  
    [SerializeField] GameObject leaderBoardPanel;
    [SerializeField] VerticalLayoutGroup layoutGroup;
    [SerializeField] GameObject entryPrefab;

    [SerializeField] DemoScriptUpload useR_Name;
    [Space (10)]
    [SerializeField] Text rankText;

  
    // Start is called before the first frame update
   
    private void OnEnable() {
        
        if(LB_Controller.instance != null) {
             //pass the username as string
            LB_Controller.OnUpdatedScores += OnLeaderboardUpdated;
            LB_Controller.instance.ReloadLeaderboard();
        }
    }

    public void BackbutttonTouched() {
        leaderBoardPanel.SetActive(false);
        Time.timeScale = 1;

    }

    private void OnDisable() {
        RemoveAllUIEntries();
        leaderBoardPanel.SetActive(false);
       
    }


    private void RemoveAllUIEntries() {
        foreach (Transform child in layoutGroup.transform) {
            if (child.gameObject.tag == "LeaderBoardRowEntry") {
                Destroy(child.gameObject);
            }
        }
    }

    private void OnLeaderboardUpdated(LB_Entry[] entries) {
        if (entries != null && entries.Length > 0) {
            RemoveAllUIEntries();
            foreach (LB_Entry entry in entries) {
                GameObject entryRow = Instantiate(entryPrefab);
                entryRow.transform.SetParent(layoutGroup.transform);
                UIDemoLeaderboardEntry rowEntry = entryRow.GetComponent<UIDemoLeaderboardEntry>();
                rowEntry.Setup(entry);
            }
        } else if (entries == null) {
            Debug.Log("ups something went wrong");
        }

        SetupRank();
    }

    private void SetupRank() {
        int rank = LB_Controller.instance.GetRankForUser(useR_Name.USERNAME());
        Debug.Log($"{useR_Name.USERNAME()}");
        if (rank == 0) {
            //rank is unknown
            rankText.text = "Sorry, can´t find a rank!";
        } else {
            rankText.text = "You are Rank #" + rank;
        }
        
    }
}
