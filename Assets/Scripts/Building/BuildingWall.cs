﻿using FileSO.Barricade;
using Interface;
using Manager_Scripts.OtherManager;
using UnityEngine;
using UnityEngine.UI;

namespace Building
{
    public class BuildingWall : MonoBehaviour,IDamage
    {
        //BaseSetting
        [SerializeField] private float hp;
        [SerializeField] private Slider hpbar;
        private int _selectType;
        [SerializeField] private BarricadeFileSo[] barricadeSo;
        private bool _isSelectType;
        private int _mBitmaskPlayer;
        [SerializeField] private Animator animate;
        
        //MiniGunSetting
        private int _mBitmaskFire;
        private RaycastHit2D _mHit;
        private RaycastHit2D _mHitCheck;
        private float _visionFire = 0;
        [SerializeField]private int miniGunDamage = 10;
        private float _time;
        [SerializeField] private float fireRate;
        [SerializeField] private GameObject uiSelected; 
        [SerializeField] private GameObject uiHp;
        
        //Material
        private string mtName1;
        private int mt1;
        private string mtName2;
        private int mt2;
        private string mtName3;
        private int mt3;

        private void Start()
        {
            hpbar.maxValue = hp;
            hpbar.value = hp;
            _selectType = 0;
            uiSelected.SetActive(false);
            uiHp.SetActive(false);
            hp = barricadeSo[_selectType].hp;
        }
        private void Update()
        {
            if (GameManager.Instance.gameEnd)
            {
                Start();
                SetBuildingSo();
            }
            hpbar.value = hp;
            if (_isSelectType)
            {
                SetBuildingSo();
                _isSelectType = false;
            }
            if (_selectType==2)
            {
                _time += Time.deltaTime;
                Fire();
            }
            else
            {
                animate.SetBool("Fire",false);
            }
            if (_selectType==0)
            {
                SelectType0();
            }
            if (_isSelectType==false)
            {
                CheckPlayer();
            }
        }
        public void Damage(int otherDamage)
        {
            hp -= otherDamage;
            if (hp<=0)
            {
                _selectType = 0;
            }
        }
        private void SetBuildingSo()
        {
            hp = barricadeSo[_selectType].hp;
            hpbar.maxValue = hp;
            hpbar.value = hp;
            mt1 = barricadeSo[_selectType].mt1;
            mt2 = barricadeSo[_selectType].mt2;
            mt3 = barricadeSo[_selectType].mt3;
            mtName1 = barricadeSo[_selectType].mtName1;
            mtName2 = barricadeSo[_selectType].mtName2;
            mtName3 = barricadeSo[_selectType].mtName3;
            animate.SetInteger("SO",_selectType);
        }

        private void SetMtSo(int other)
        {
            mt1 = barricadeSo[other].mt1;
            mt2 = barricadeSo[other].mt2;
            mt3 = barricadeSo[other].mt3;
            mtName1 = barricadeSo[other].mtName1;
            mtName2 = barricadeSo[other].mtName2;
            mtName3 = barricadeSo[other].mtName3;
        }
        private void Fire()
        {
            _mBitmaskFire = ~(1 << 9) & ~(1 << 10) & ~(1<<8) & ~(1<<5) & ~(1<<0) & ~(1<<15);
            var position = transform.position;
            Debug.DrawRay(position,transform.TransformDirection(5,0,0),Color.red);
            _mHit = Physics2D.Raycast(position,transform.right,_visionFire,_mBitmaskFire);
            if (_mHit)
            {
                Debug.Log(_mHit.transform.name);
                IDamage doDamage = _mHit.transform.GetComponent<IDamage>();
                if (doDamage!=null && _time>=fireRate)
                {
                    doDamage.Damage(miniGunDamage);
                    animate.SetBool("Fire",true);
                    _time = 0;
                }
                else
                {
                    animate.SetBool("Fire",false);
                }
            }
            else
            {
                animate.SetBool("Fire",false);
            }
        }

        private void CheckPlayer()
        {
            _mBitmaskPlayer = ~(1 << 9) & ~(1 << 10) & ~(1<<12) & ~(1<<5) & ~(1<<0)& ~(1<<7);
            var position = transform.position;
            _mHitCheck = Physics2D.CircleCast(position, 0.5f, position,0.5f,_mBitmaskPlayer);
            if (_mHitCheck)
            {
                ISPlayerDoWork doWork = _mHitCheck.transform.GetComponent<ISPlayerDoWork>();
                if (doWork!=null && doWork.IsDoingWork())
                {
                    uiSelected.SetActive(true);
                }
            }
            else
            {
                uiSelected.SetActive(false);
            }
        }
        public void SelectType1()
        {
            SetMtSo(1);
            var checkMaterial = InventoryManager.Instance.inventory;
            if (checkMaterial.ContainsKey(mtName1) & checkMaterial.ContainsKey(mtName2))
            {
                if (checkMaterial[mtName1]>=mt1 & checkMaterial[mtName2]>=mt2)
                {
                    checkMaterial[mtName1] -= mt1;
                    checkMaterial[mtName2] -= mt2;
                    uiHp.SetActive(true);
                    _selectType = 1;
                    uiSelected.SetActive(false);
                    _isSelectType = true;
                    gameObject.layer = 10;
                }
                else
                {
                    Debug.Log("You don't have Food more");
                }
            }
            else
            {
                Debug.Log("You don't have this material");
            }
            
        }
        public void SelectType2()
        {
            SetMtSo(2);
            var checkMaterial = InventoryManager.Instance.inventory;
            if (checkMaterial.ContainsKey(mtName1) & checkMaterial.ContainsKey(mtName2) & checkMaterial.ContainsKey(mtName3))
            {
                if (checkMaterial[mtName1]>=mt1 & checkMaterial[mtName2]>=mt2 & checkMaterial[mtName3]>=mt3)
                {
                    checkMaterial[mtName1] -= mt1;
                    checkMaterial[mtName2] -= mt2;
                    checkMaterial[mtName3] -= mt3;
                    uiHp.SetActive(true);
                    _selectType = 2;
                    uiSelected.SetActive(false);
                    _isSelectType = true;
                    gameObject.layer = 10;
                }
            }
            else
            {
                Debug.Log("You don't have this material");
            }
        }

        private void SelectType0()
        {
            gameObject.layer = 0;
            _selectType = 0;
            hp = barricadeSo[_selectType].hp;
        }
    }
}
