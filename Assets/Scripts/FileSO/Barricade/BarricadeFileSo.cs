using UnityEngine;

namespace FileSO.Barricade
{
        [CreateAssetMenu(fileName = "Barricade", menuName = "Barricade")]
        public class BarricadeFileSo : ScriptableObject
        {
                public float hp;
                public Sprite barricadeSprite;
                public string mtName1;
                public int mt1;
                public string mtName2;
                public int mt2;
                public string mtName3;
                public int mt3;
        }
}
