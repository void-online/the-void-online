﻿using UnityEngine;
namespace FileSO.Material
{
    [CreateAssetMenu(fileName = "Material", menuName = "Material")]
    public class MaterialSo : ScriptableObject
    {
        public float keepTime;
        public string name;
        public Sprite spriteMaterial;
    }
}