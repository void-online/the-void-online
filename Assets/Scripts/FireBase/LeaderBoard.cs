using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Firebase
{
    public class LeaderBoard : MonoBehaviour

    {
        public PlayerData playerData;
        [Space] [Header("UI")] public Text rankText;
        public Text playerNameText;
        public Text scoreText;

        public void UpdateData()
        {
            playerNameText.text = playerData.playerName;
            rankText.text = playerData.rankNumber.ToString();
            scoreText.text = playerData.playerScore.ToString();
        }
    }
    
    //[Header("Data")]
    [System.Serializable]
    public class PlayerData
    {
        public string playerName;
        public int rankNumber;
        public int playerScore;
        public PlayerData(string playerNamee,int rankNumberr, int playerScoree)
        {
            this.playerName = playerNamee;
            this.rankNumber = rankNumberr;
            this.playerScore = playerScoree;
        }
    }
}


