using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Firebase;
using UnityEngine;

public class RankManger : MonoBehaviour
{
    public GameObject rankData;
    public Transform rankPanal;
    
    // Start is called before the first frame update
    void Start()
    {
        CreatRankData();
    }
    public List<PlayerData> PlayerDatas;

    public void CreatRankData()
    {
        for (int i = 0; i < PlayerDatas.Count; i++)
        {
            GameObject rankObject = Instantiate(this.rankData, rankPanal) as GameObject;
            LeaderBoard rankData = rankObject.GetComponent<LeaderBoard>();
            rankData.playerData = new PlayerData(PlayerDatas[i].playerName,PlayerDatas[i].rankNumber,  PlayerDatas[i].playerScore);
            rankData.UpdateData();
        }
    }
}

