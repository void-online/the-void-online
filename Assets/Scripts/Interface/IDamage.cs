﻿namespace Interface
{
    public interface IDamage
    {
        void Damage(int otherDamage);
    }
}