﻿namespace Interface
{
    public interface ISPlayerDoWork
    {
        public bool IsDoingWork();
        public bool IsDoingWorkSet(bool setBool);

        public bool IsGenerate();
        public bool IsGenerateSet(bool setBool);
       
    }
}