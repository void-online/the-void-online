﻿using Interface;

using UnityEngine;

namespace Scripe
{  
    
    [CreateAssetMenu(fileName = "Enemy",menuName = "EnemyClass")]
    public class EnemyFrom : ScriptableObject
    {
       public float   bite;
       public float   lifePoints;
       public int     attackDamage;
       public float   moveSpeed ;
       public float   attackSpeed;
       public float   time;
      
    }
    
  
}
