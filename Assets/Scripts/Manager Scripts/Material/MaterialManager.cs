﻿using System;
using FileSO.Material;
using Interface;
using Manager_Scripts.OtherManager;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Manager_Scripts.Material
{
    public class MaterialManager : MonoBehaviour,IDamage,INameMaterial
    {
        [SerializeField] private float keepTime;
        [SerializeField] private float keeping;
        private SpriteRenderer _sprite;
        [SerializeField]private int spriteType;
        [SerializeField] private MaterialSo[] materialSo;
        private int _mBitmaskPlayer;
        private RaycastHit2D _mHitCheck;
        private bool _isKeep;

        [SerializeField]private GameObject keepUI;
        [SerializeField] private GameObject cancelUI;
        [SerializeField] private GameObject keepBarUI;

        [SerializeField] private Slider _slider;
        private ISPlayerDoWork doWork;
        private bool isDoDamage;
        private void Start()
        {
            _sprite = GetComponent<SpriteRenderer>();
            MaterialSetSo();
            keepUI.SetActive(false);
            cancelUI.SetActive(false);
            keepBarUI.SetActive(false);
            _slider.maxValue = keepTime;
            _slider.value = keeping;
        }

        private void Update()
        {
            CheckPlayer();
            if (isDoDamage)
            {
                keeping += Time.deltaTime;
            }
            _slider.value = keeping;
        }

       
        private void CheckPlayer()
        {
            _mBitmaskPlayer = ~(1 << 9) & ~(1 << 10) & ~(1<<12) & ~(1<<5) & ~(1<<0) & ~(1<<7);
            var position = transform.position;
            _mHitCheck = Physics2D.CircleCast(position, 0.1f, position,0.1f,_mBitmaskPlayer);
            if (_mHitCheck)
            {
                Debug.Log(_mHitCheck.transform.name);
                doWork = _mHitCheck.transform.GetComponent<ISPlayerDoWork>();
                if (doWork!=null & _isKeep == false)
                {
                    keepUI.SetActive(true);
                }
            }
            else
            {
                keepUI.SetActive(false);
                ClickCancel();
            }
        }
        public void ClickKeep()
        {
            if (_mHitCheck)
            {
                doWork.IsDoingWorkSet(true);
            }
            _isKeep = true;
            keepUI.SetActive(false);
            cancelUI.SetActive(true);
            keepBarUI.SetActive(true);
        }
        public void ClickCancel()
        {
            if (_mHitCheck)
            {
                doWork.IsDoingWorkSet(false);
            }
            _isKeep = false;
            keepTime = materialSo[spriteType].keepTime;
            cancelUI.SetActive(false);
            keeping = 0;
            keepBarUI.SetActive(false);
        }
        
        public bool IKeep()
        {
            return _isKeep;
        }
        private void MaterialSetSo()
        {
            keepTime = materialSo[spriteType].keepTime;
            name = materialSo[spriteType].name;
            _sprite.sprite = materialSo[spriteType].spriteMaterial;
        }
        public void Damage(int otherDamage)
        {
            isDoDamage = true;
            if (keeping >= keepTime)
            {
                if (_mHitCheck)
                {
                    doWork.IsDoingWorkSet(false);
                }

                isDoDamage = false;
                InventoryManager.Instance.ChekAndAdd(name,1);
                Destroy(gameObject);
            }
        }
    }
}