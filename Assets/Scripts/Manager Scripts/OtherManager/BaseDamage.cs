using Interface;
using UnityEngine;

namespace Manager_Scripts.OtherManager
{
    public class BaseDamage : MonoBehaviour,IDamage
    {
        [SerializeField]private BaseManager baseMain;

        public void Damage(int otherDamage)
        {
            baseMain.hp -= otherDamage;
            if (baseMain.hp <= 0)
            {
                GameManager.Instance.gameEnd = true;
                GameManager.Instance.GameLose();
            }
        }
    }
}
