using Interface;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Manager_Scripts.OtherManager
{
    public class BaseManager : MonoBehaviour,IDamage
    {
        [SerializeField] private Slider hpBar;
        [SerializeField] private float maxHp;
        [SerializeField] private float fixHp;
        private RaycastHit2D _mHitCheck;
        [SerializeField] internal float hp;
        private int _difHp;
        [SerializeField] private bool isClick;
        [SerializeField] private bool isPay;
        [SerializeField] private bool isFix;
        [SerializeField] private GameObject ui;
        [SerializeField] private GameObject fixUi;
        [SerializeField] private GameObject cancelUi;
        [SerializeField] private float time;
        [SerializeField] private float fixTime;
        [SerializeField] private string mt1Name;
        [SerializeField] private string mt2Name;
        [SerializeField] private Text mt1Text;
        [SerializeField] private Text mt2Text;
        void Start()
        {
            isFix = false;
            time = 0;
            isPay = false;
            isClick = false;
            _difHp = 0;
            ui.SetActive(false);
            fixUi.SetActive(false);
            cancelUi.SetActive(false);
            hpBar.maxValue = maxHp;
            hpBar.value = hp;
            hp = maxHp;
        }
        void Update()
        {
            hpBar.value = hp;
            CheckPlayer();
            if (isFix&isClick)
            {
                time += Time.deltaTime;
            }
            _difHp = (int)maxHp - (int)hp;
            if (hp<maxHp-5)
            {
                mt1Text.text = $"{_difHp/5}";
                mt2Text.text = $"{_difHp/5}";
            }
            else
            {
                mt1Text.text = "Null";
                mt2Text.text = "Null";
            }
            if (GameManager.Instance.gameEnd)
            {
                Destroy(gameObject);
            }
        }

        private void CheckPlayer()
        {
            var _mBitmaskPlayer = ~(1 << 9) & ~(1 << 10) & ~(1<<12) & ~(1<<5) & ~(1<<0)& ~(1<<7);
            var position = transform.position;
            _mHitCheck = Physics2D.CircleCast(position, 0.5f, position,0.5f,_mBitmaskPlayer);
            if (_mHitCheck)
            {
                ISPlayerDoWork doWork = _mHitCheck.transform.GetComponent<ISPlayerDoWork>();
                if (doWork!=null && doWork.IsDoingWork())
                {
                    SetText();
                    ui.SetActive(true);
                    if (isPay)
                    {
                        FixBase();
                    }
                }
                else
                {
                    ui.SetActive(false);
                }
            }
            else
            {
                ui.SetActive(false);
            }
        }
        public void Damage(int otherDamage)
        {
            
        }

        public void ClickFix()
        {
            if (isPay)
            {
                isClick = true;
            }
            var inventory = InventoryManager.Instance.inventory;
            if (hp<maxHp-5)
            {
                if (inventory.ContainsKey(mt1Name) & inventory.ContainsKey(mt2Name) & isPay==false)
                {
                    if (inventory[mt1Name]>=_difHp/5 & inventory[mt2Name]>=_difHp/5)
                    {
                        inventory[mt1Name] -= _difHp/5;
                        inventory[mt2Name] -= _difHp/5;
                        isClick = true;
                        isPay = true;
                    }
                    else
                    {
                        Debug.Log("You don't have any Wood");
                    }
                }
                else
                {
                    Debug.Log("You don't have any Material");
                }
            }
            else
            {
                Debug.Log("You have full hp");
            }
            
        }

        public void ClickCancel()
        {
            isClick = false;
        }

        void SetText()
        {
            if (isClick)
            {
                fixUi.SetActive(false);
                cancelUi.SetActive(true);
            }
            else
            {
                fixUi.SetActive(true);
                cancelUi.SetActive(false);
            }
        }
        
        
        void FixBase()
        {
            if (isPay & isClick)
            {
                isFix = true;
                if (time>=fixTime)
                {
                    hp += fixHp;
                    time = 0;
                }
                if (hp>=maxHp)
                {
                    hp = maxHp;
                    isPay = false;
                    isClick = false;
                    isFix = false;
                    ui.SetActive(false);
                }
            }
        }
    }
}
