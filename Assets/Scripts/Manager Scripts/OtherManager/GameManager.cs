﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;


namespace Manager_Scripts.OtherManager
{
   public class GameManager : MonoSingleton<GameManager>
   {
      //////////////// Player
      public Transform playerBornPoint1;
      public Transform playerBornPoint2;
      [SerializeField] private Transform basePoint;
      [SerializeField]private GameObject player;
      [SerializeField]private GameObject basePrefab;
      private GameObject clonePlayer;
      private GameObject cloneBase;
      private int listPlayer;
      public Dictionary<string,GameObject>playerList = new Dictionary<string, GameObject>();
      
      //////////////// MaterialSpawn
      [SerializeField] private Transform materialSpawnPoint;
      [SerializeField] private GameObject materialPrefab;
      private GameObject cloneMaterial;
      
      //////////////// Game
      [SerializeField]private int winDay;
      public bool gameEnd;
      public bool gameStart;
      private int score;

      private void Start()
      {
         gameStart = true;
      }

      private void Update()
      {
         if (Input.GetKeyDown(KeyCode.P))
         {
            SpawnPlayer(playerBornPoint1.transform.position);
         }
         if (UIManager.Instance.showday>=winDay)
         {
            gameEnd = true;
            GameWin();
         }
         
         if (gameEnd)
         {
            listPlayer = 0;
            LightManager.Instance.SetLight();
         }

      }
      
      //////////////// Player Void
      public void SpawnPlayer(Vector3 spawnPoint)
      {
         listPlayer += 1;
         clonePlayer = Instantiate(player,spawnPoint,Quaternion.identity);
      }

      public void SpawnPlayerStart()
      {
         listPlayer += 1;
         clonePlayer = Instantiate(player,playerBornPoint1.transform.position,Quaternion.identity);
      }

      public void SetNameAndAdd(string name)
      {
         clonePlayer.name = name + listPlayer;
         playerList.Add(clonePlayer.name,clonePlayer);
      }
      public int ListThePlayer()
      {
         return listPlayer;
      }
      public void DestroyPlayer(string key)
      {
         if (playerList.ContainsKey(key))
         {
            playerList.Remove(key);
         }
      }
      

      public void SpawnBase()
      {
         cloneBase = Instantiate(basePrefab, basePoint.transform.position, Quaternion.identity);
      }

      public void GameWin()
      {
         UIManager.Instance.GameWin();
         gameStart = true;
         playerList.Clear();
      }

      public void GameLose()
      {
         gameEnd = true;
         UIManager.Instance.GameLose();
         gameStart = true;
         playerList.Clear();
      }

      public void Score(int other)
      {
         score += other;
      }

      public void SetScore(int other)
      {
         score = other;
      }

      public int ShowScore()
      {
         return score;
      }

      public void GameExit()
      {
         Application.Quit();
         
      }
      //////////////// Material Void
      
   }
}
