﻿using System;
using Interface;
using UnityEngine;
using UnityEngine.UI;

namespace Manager_Scripts.OtherManager
{
    public class HealStation : MonoBehaviour
    {
        private RaycastHit2D _hit;
        private bool clickSelect;
        private bool clickStart;
        private int healer;
        private float plusHpRate;
        [SerializeField]private float maxRate;
        [SerializeField] private int plusHp;
        // Ui
        [SerializeField] private Slider slider;
        [SerializeField] private GameObject uiHeal;
        [SerializeField] private GameObject uiStart;
        [SerializeField] private GameObject uiCancel;

        private void Start()
        {
            plusHpRate = 0.0f;
            slider.maxValue = maxRate;
            slider.value = plusHpRate;
            uiHeal.SetActive(false);
            uiCancel.SetActive(false);
        }
        private void Update()
        {
            if (clickStart)
            {
                plusHpRate += Time.deltaTime;
                slider.value = plusHpRate;
            }
            CheckPlayer();
        }

        void CheckPlayer()
        {
            int bitMask = ~(1 << 9) & ~(1 << 10) & ~(1<<12) & ~(1<<5) & ~(1<<0)& ~(1<<7);
            var position = transform.position;
            _hit = Physics2D.CircleCast(position, 0.5f, position, 0.5f, bitMask);
            if (_hit)
            {
                ISPlayerDoWork player = _hit.transform.GetComponent<ISPlayerDoWork>();
                IHungry playerHp = _hit.transform.GetComponent<IHungry>();
                if (player!=null & playerHp!=null)
                {
                    if (player.IsDoingWork())
                    {
                        uiHeal.SetActive(true);
                    }
                    if (player.IsGenerate() == false & clickSelect & healer < 1)
                    {
                        player.IsGenerateSet(true);
                        clickSelect = false;
                        healer += 1;
                    }
                    if (player.IsGenerate() & clickStart & plusHpRate>=maxRate)
                    {
                        playerHp.HpPlus(plusHp);
                        plusHpRate = 0.0f;
                    }
                }
            }
        }
        private void OnTriggerExit2D(Collider2D other)
        {
            ISPlayerDoWork player = other.transform.GetComponent<ISPlayerDoWork>();
            if (player.IsGenerate())
            {
                uiHeal.SetActive(false);
                player.IsGenerateSet(false);
                ClickCancel();
                healer -= 1;
            }
            if (healer<=0)
            {
                uiHeal.SetActive(false);
                ClickCancel();
            }
        }
        public void ClickSelect()
        {
            clickSelect = true;
        }

        public void ClickStart()
        {
            uiStart.SetActive(false);
            uiCancel.SetActive(true);
            clickStart = true;
        }

        public void ClickCancel()
        {
            plusHpRate = 0.0f;
            slider.value = plusHpRate;
            uiStart.SetActive(true);
            uiCancel.SetActive(false);
            clickStart = false;
        }
        void SetUi(bool setBool)
        {
            uiHeal.SetActive(setBool);
            uiStart.SetActive(setBool);
            uiCancel.SetActive(setBool);
        }

       
    }
}