﻿using System;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Serialization;

namespace Manager_Scripts.OtherManager
{
    public class InventoryManager : MonoSingleton<InventoryManager>
    {
        //////////////// Inventory
        public Dictionary<string,int> inventory = new Dictionary<string,int>();
        private string name="";

        private void Update()
        {
            if (GameManager.Instance.gameEnd)
            {
                inventory.Clear();
            }
        }

        public void ChekAndAdd(string otherMaterialName,int quantity)
        {
            if (inventory.ContainsKey(otherMaterialName))
            {
                inventory[otherMaterialName] += quantity;
            }
            else
            {
                inventory.Add(otherMaterialName,+quantity);
            }
            CheckAndRemove();
        }

        public void RemoveMaterial(string materialName)
        {
            if (inventory.ContainsKey(materialName))
            {
                if (inventory[materialName]<=0)
                {
                    inventory.Remove(materialName);
                }
            }
        }

        public void CheckAndRemove()
        {
            foreach (var VARIABLE in inventory)
            {
                Debug.Log($"{VARIABLE.Key} , {VARIABLE.Value}");
                if (inventory[VARIABLE.Key]<=0)
                {
                    name = VARIABLE.Key;
                }
            }
            RemoveMaterial(name);
        }
        
    }
}