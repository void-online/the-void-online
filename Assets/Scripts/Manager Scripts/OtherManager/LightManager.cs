using Sound;
using UnityEngine;
using UnityEngine.Rendering;

namespace Manager_Scripts.OtherManager
{
    public class LightManager : MonoSingleton<LightManager>
    {
        public Volume ppv;
        public bool activateLights;
        public GameObject[] lights;

       [SerializeField] GameObject setDay;
       [SerializeField] GameObject setNight;
        void Start()
        {
            setDay.SetActive(true);
            setNight.SetActive(false);
            ppv = gameObject.GetComponent<Volume>();
        }
    
        // Update is called once per frame
        void Update()
        {
            if (UIManager.Instance.time >= 810 && UIManager.Instance.time < 870)
            {
                ppv.weight = 1 - ((870 - UIManager.Instance.time) / 60);
                setDay.SetActive(false);
                setNight.SetActive(true);
            }
            if (UIManager.Instance.time >= 1170 && UIManager.Instance.time < 1230)
            {
                ppv.weight = ((1230 - UIManager.Instance.time) / 60) ;
              
                setDay.SetActive(true);
                setNight.SetActive(false);


            }

            if (ppv.weight < 0.7)
            {
                for (int i = 0; i < lights.Length; i++)
                {
                    lights[i].SetActive(false);
                }
                activateLights = false;
            }
            if (ppv.weight > 0.7)
            {
                for (int i = 0; i < lights.Length; i++)
                {
                    lights[i].SetActive(true);
                }
                activateLights = true;
            }
        }
        
        public void SetLight()
        {
            ppv.weight = 0 ;
        }
    }
}