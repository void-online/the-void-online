using Interface;
using UnityEngine;


namespace Manager_Scripts
{
    public class PlatfromManager : MonoBehaviour,IBoolCheck
    {
       private PlatformEffector2D m_Platfrom;
       [SerializeField]private Collider2D _collider;
        void Start()
        {
            m_Platfrom = GetComponent<PlatformEffector2D>();
        }

        public void IsRotate180()
        {
            m_Platfrom.rotationalOffset = 180;
        }

        public void IsRotate0()
        {
            m_Platfrom.rotationalOffset = 0;
        }
        
    }
}
