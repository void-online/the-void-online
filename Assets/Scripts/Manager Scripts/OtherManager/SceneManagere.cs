using System.Collections;
using System.Collections.Generic;
using Manager_Scripts;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SceneManagere : MonoBehaviour
{
    
    public string sceneName;
    
    
    public void nextScene()
    {
        SceneManager.LoadScene($"{sceneName}");
    }
}
