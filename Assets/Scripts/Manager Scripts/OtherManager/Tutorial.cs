using System;
using UnityEngine;

namespace Manager_Scripts.OtherManager
{
    public class Tutorial : MonoBehaviour
    {
        [SerializeField] private GameObject text;

        private void Start()
        {
            text.SetActive(false);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                text.SetActive(true);
            }
        }
        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                text.SetActive(false);
            }
        }
    }
}
