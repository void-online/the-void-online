using UnityEngine;

namespace Manager_Scripts.OtherManager
{
    public class TutorialUIManager : MonoSingleton<TutorialUIManager>
    {
        [SerializeField] private GameObject uiElement;
        public bool isTutorial;
        void Start()
        {
            uiElement.SetActive(true);
            GameManager.Instance.SpawnPlayerStart();
            Time.timeScale = 1;
            //InventoryManager.Instance.ChekAndAdd("Food",10000);
           // InventoryManager.Instance.ChekAndAdd("Wood",10000);
            //InventoryManager.Instance.ChekAndAdd("Scrap",10000);
           // InventoryManager.Instance.ChekAndAdd("Electronic",10000);
        }

        
        void Update()
        {
            
        }
    }
}
