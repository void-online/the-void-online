﻿using System;
using Manager_Scripts.OtherManager;
using Sound;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Manager_Scripts
{
    public class UIManager : MonoSingleton<UIManager>
    {
        [SerializeField] private GameObject UIPlay;
        [SerializeField] private GameObject UIScore;
        [SerializeField] private GameObject UiLogin;
        [SerializeField] private GameObject UiWin;
        [SerializeField] private GameObject UiLose;
        [SerializeField] private GameObject Clock;
        [SerializeField] private GameObject warning1;
        [SerializeField] private GameObject warning2;
        [SerializeField] private GameObject warning3;
        [SerializeField]internal float time;
        [SerializeField] private float timeSpeed;

        [SerializeField] private int[] waveDay;
        //Text
        [SerializeField] private Text Day;
        [SerializeField] private Text foodText;
        [SerializeField] private Text woodText;
        [SerializeField] private Text electText;
        [SerializeField] private Text scrapText;
        [SerializeField] private Text player;
        
        //Bool
        private bool day1;
        private bool day2;
        private bool day3;
        [SerializeField] private Text score;
        //
        public float timeWarning;
        private bool isWarning;
        [SerializeField] private GameObject uiOther;
        public int _dayCount;
        public Text selectPlayerName;
        internal string Name = "Non";
        private bool _setActive;
        private bool _setActiveUiScore;
        public bool isClickQuit;
        public bool isClickPlay;
        private bool isClickOther;
        public int showday;
        public bool isTutorial;

        int    scoree;
        private string namee;

       
        
        public GameObject uiElement, pauseUi;

        public static bool GamePauses = false;

        [SerializeField] private DemoScriptUpload Set;

        private void Start()
        {
            if (isTutorial==false)
            {
                day1 = true;
                day2 = false;
                day3 = false;
                isWarning = false;
                timeWarning = 0.0f;
                isClickQuit = false;
                isClickOther = false;
                Clock.gameObject.transform.rotation = Quaternion.Euler(Vector3.zero);
                Time.timeScale = 0f;
                time = 540;
                UiLogin.SetActive(true);
                showday = 1;
                _dayCount = 0;
                UiWin.SetActive(false);
                UiLose.SetActive(false);
                UIPlay.SetActive(false);
                UIScore.SetActive(false);
                Debug.Log(_dayCount); 
                uiElement.SetActive(false);
                pauseUi.SetActive(false);
                uiOther.SetActive(false);
                warning1.SetActive(false);
                warning2.SetActive(false);
                warning3.SetActive(false);
                CheckAndShowText();
            }
        }
        
        
        private void Update()
        {
            if (isWarning)
            {
                timeWarning += Time.deltaTime;
            }
            CheckBigWaveDay();
            CheckAndShowText();
            if (GameManager.Instance.gameEnd)
            {
                Clock.gameObject.transform.rotation = Quaternion.Euler(Vector3.zero);
                Time.timeScale = 0f;
                time = 540;
                showday = 1;
                _dayCount = 0;
                
            }
            time += Time.deltaTime*timeSpeed;
            if (time>=1260)
            {
                time = 540;
                showday += 1;
                _dayCount += 1;
                Debug.Log(_dayCount);
            }
            Clock.gameObject.transform.rotation = Quaternion.Euler(Vector3.back*time);
            selectPlayerName.text = $"Select Hero: {Name}";
            Day.text = $"Day : {showday}";
            if (_setActive)
            {
                UIPlay.SetActive(true);
            }
            else
            {
                UIPlay.SetActive(false);
            }
            if (_setActiveUiScore)
            {
                UIScore.SetActive(true);
            }
            else
            {
                UIScore.SetActive(false);
            }
            
        }

        public void ClickOther()
        {
            isClickOther = !isClickOther;
            uiOther.SetActive(isClickOther);
        }

        public void OnClick()
        {
            _setActive = !_setActive;
        }

        public void OnClickUIPlay()
        {
            _setActiveUiScore = !_setActiveUiScore;
        }

        public void Pause()
        {
            pauseUi.SetActive(true);
            Time.timeScale = 0f;
            GamePauses = true;
        }
        public  void Continue()
        {
            pauseUi.SetActive(false);
            Time.timeScale = 1f;
            GamePauses = false;
        }

        public void ClickPlay()
        {
            InventoryManager.Instance.ChekAndAdd("Food",20);
            InventoryManager.Instance.ChekAndAdd("Wood",5);
            InventoryManager.Instance.ChekAndAdd("Scrap",1);
            InventoryManager.Instance.ChekAndAdd("Electronic",1);
            GameManager.Instance.SpawnBase();
            GameManager.Instance.SpawnPlayerStart();
            GameManager.Instance.gameStart = false;
            isClickPlay = true;
            Time.timeScale = 1f;
            UiLogin.SetActive(false);
            UiLose.SetActive(false);
            UiWin.SetActive(false);
            uiElement.SetActive(true);
            warning1.SetActive(false);
            warning2.SetActive(false);
            warning3.SetActive(false);
            GameManager.Instance.gameEnd = false;
            scoree = GameManager.Instance.ShowScore();
            Set.SetSSScore(scoree,"Player");
            day1 = true;
            day2 = false;
            day3 = false;
        }

        public void ClickQuit()
        {
            isClickQuit = true;
            isClickPlay = false;
            Debug.Log("Click Quit");
            UiLogin.SetActive(true);
            uiElement.SetActive(false);
            UiWin.SetActive(false);
            UiLose.SetActive(false);
            pauseUi.SetActive(false);
            warning1.SetActive(false);
            warning2.SetActive(false);
            warning3.SetActive(false);
            GameManager.Instance.gameStart = true;
            GameManager.Instance.gameEnd = true;
            Time.timeScale = 1f;
            scoree = GameManager.Instance.ShowScore();
            Set.SetSSScore(scoree,"Player");
            LB_Controller.instance.StoreScore(scoree, $"{namee}");
            day1 = true;
            day2 = false;
            day3 = false;
        }

        public void GameWin()
        {
            Time.timeScale = 0f;
            UiWin.SetActive(true);
            warning1.SetActive(false);
            warning2.SetActive(false);
            warning3.SetActive(false);
        }

        public void GameLose()
        {
            Time.timeScale = 0f;
            UiLose.SetActive(true);
            warning1.SetActive(false);
            warning2.SetActive(false);
            warning3.SetActive(false);
            isClickQuit = true;
        }

        public void GameQuit()
        {
            Set.SetSSScore(scoree,namee);
            Application.Quit();
        }

        void CheckAndShowText()
        {
            var inventory = InventoryManager.Instance.inventory;
            foodText.text = inventory.ContainsKey("Food") ? $"{inventory["Food"]}" : "0";
            woodText.text = inventory.ContainsKey("Wood") ? $"{inventory["Wood"]}" : "0";
            electText.text = inventory.ContainsKey("Electronic") ? $"{inventory["Electronic"]}" : "0";
            scrapText.text = inventory.ContainsKey("Scrap") ? $"{inventory["Scrap"]}" : "0";
            player.text = $"{GameManager.Instance.playerList.Count}";
            score.text = $"Score : {GameManager.Instance.ShowScore()}";
        }
        
        void CheckBigWaveDay()
        {
            foreach (var day in waveDay)
            {
                if (day == showday & timeWarning<5 & day3)
                {
                    warning1.SetActive(true);
                    isWarning = true;
                    day1 = true;
                    day2 = false;
                    day3 = false;
                }
                if (showday == day-1 & timeWarning<5 & day2)
                {
                    warning2.SetActive(true);
                    isWarning = true;
                    day1 = false;
                    day2 = false;
                    day3 = true;
                }
                if (showday == day-2 & timeWarning<5 & day1)
                {
                    warning3.SetActive(true);
                    isWarning = true;
                    day1 = false;
                    day2 = true;
                    day3 = false;
                }
                if (timeWarning>=5)
                {
                    warning1.SetActive(false);
                    warning2.SetActive(false);
                    warning3.SetActive(false);
                    isWarning = false;
                    timeWarning = 0;
                }
            }
        }
    }
}