using System;
using System.Collections.Generic;
using Manager_Scripts.OtherManager;
using UnityEngine;

namespace Manager_Scripts.Player
{
    public class Direction : MonoBehaviour
    {
        [SerializeField] private GameObject directionUI;
        public List<GameObject> direcUIList = new List<GameObject>();
        internal void OnInstant(Vector3 target)
        {
             direcUIList.Add(Instantiate(directionUI, target, Quaternion.identity)) ;
        }

        private void Update()
        {
            if (direcUIList.Count>1)
            {
                Destroy(direcUIList[0]);
                direcUIList.RemoveAt(0);
            }
            
        }

        internal void DestroyDirecUI()
        {
            if (direcUIList.Count>=0)
            {
                foreach (var list in direcUIList)
                {
                    Destroy(list);
                }
                direcUIList.Clear();
            }
        }
        
    }
}
