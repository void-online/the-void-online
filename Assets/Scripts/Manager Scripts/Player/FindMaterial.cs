﻿using UnityEngine;
using Interface;

namespace Manager_Scripts.Player
{
    public class FindMaterial : MonoBehaviour
    {
        [SerializeField] private PlayerManager player;
        private RaycastHit2D m_Hit;
        [SerializeField] private float visionAttack;
        private int m_Bitmask;
        private float time;
        private bool isSee;
        private void Update()
        {
            if (isSee)
            {
                time += Time.deltaTime;
            }
            Find();
        }


        void Find()
        {
            m_Bitmask = ~(1 << 0) & ~(1 << 8) & ~(1 << 5) & ~(1 << 9) & ~(1 << 10) & ~(1 << 12) & ~(1 << 14)& ~(1<<15);
            var position = player.shootPoint.position;
            Debug.DrawRay(position,player.transform.TransformDirection(visionAttack,0,0),Color.red);
            m_Hit = Physics2D.Raycast(position,player.transform.right,visionAttack,m_Bitmask);
            if (m_Hit)
            {
                IDamage doDamage = m_Hit.transform.GetComponent<IDamage>();
                INameMaterial isKeep = m_Hit.transform.GetComponent<INameMaterial>();
                isSee = true;
                if (doDamage!=null && time>=player.KeepTime && isKeep.IKeep())
                {
                    Debug.Log("Is Keep");
                    doDamage.Damage(1);
                    time = 0;
                    isSee = false;
                }
            }
        }
    }
}