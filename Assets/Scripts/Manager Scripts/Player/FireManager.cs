﻿using System;
using Interface;
using UnityEngine;

namespace Manager_Scripts.Player
{
    public class FireManager : MonoBehaviour
    {
        [SerializeField] private PlayerManager player;
        [SerializeField] private CharacterMove playerM;
        [SerializeField] private Transform flamePoint;
        [SerializeField] private GameObject flame;
        private GameObject clonner;
        private RaycastHit2D m_Hit;
        private RaycastHit2D[] m_Hit2;
        [SerializeField] private float visionAttack;
        private int m_Bitmask;
        private float time;
        private bool isShootFlame;
        private void Update()
        {
            if (time<=7.0f)
            {
                time += Time.deltaTime;
            }
            if (player.isGenerate == false)
            {
                if (player.playerLv==4)
                {
                    ShootThru();
                }
                else
                {
                    Shoot1Enemy();
                }
            }
            if (isShootFlame & player.playerLv==4)
            {
                clonner = Instantiate(flame, flamePoint.position, transform.rotation);
            }
            else if(isShootFlame==false)
            {
                
                Destroy(clonner,0.5f);
            }
        }


        void Shoot1Enemy()
        {
            m_Bitmask = ~(1 << 9) & ~(1 << 10) & ~(1<<8) & ~(1<<7) & ~(1<<0) & ~(1<<15) & ~(1<<11) & ~(1<<13) & ~(1<<14);
            var position = player.shootPoint.position;
            Debug.DrawRay(position,player.transform.TransformDirection(player.vision,0,0),Color.red);
            m_Hit = Physics2D.CircleCast(position,player.vision,position,player.vision,m_Bitmask);
            if (m_Hit)
            {
                Debug.Log(m_Hit.transform.name);
                IDamage doDamage = m_Hit.transform.GetComponent<IDamage>();
                if (doDamage!=null && time>=player.FireRate)
                {
                    
                    player.animWalk.SetBool($"Attack{player.playerLv}",true);
                    player.animWalk.SetBool($"Idle{player.playerLv}",false);
                    player.Shot();
                    playerM.Rotation(m_Hit.transform.position);
                    doDamage.Damage(player.damage);
                    time = 0;
                }
                else
                {
                    
                    player.animWalk.SetBool($"Attack{player.playerLv}",false);
                    player.animWalk.SetBool($"Idle{player.playerLv}",true);
                    
                }
            }
            else
            {
                player.animWalk.SetBool($"Attack{player.playerLv}",false);
                player.animWalk.SetBool($"Idle{player.playerLv}",true);
            }
        }
        void ShootThru()
        {
            m_Bitmask = ~(1 << 9) & ~(1 << 10) & ~(1<<8) & ~(1<<7) & ~(1<<0) & ~(1<<15) & ~(1<<11) & ~(1<<13) & ~(1<<14);
            var position = player.shootPoint.position;
            Debug.DrawRay(position,player.transform.TransformDirection(visionAttack,0,0),Color.red);
            m_Hit2 = Physics2D.CircleCastAll(position,player.vision,position,player.vision,m_Bitmask);
            if (time>=player.FireRate)
            {
                foreach (var isHit in m_Hit2)
                {
                    IDamage damage = isHit.transform.GetComponent<IDamage>();
                    if (damage!=null)
                    {
                        
                        player.animWalk.SetBool($"Attack{player.playerLv}",true);
                        player.animWalk.SetBool($"Idle{player.playerLv}",false);
                        player.Shot();
                        playerM.Rotation(isHit.transform.position);
                        isShootFlame = true;
                        damage.Damage(player.damage);
                    }
                    else
                    {
                        player.animWalk.SetBool($"Attack{player.playerLv}",false);
                        player.animWalk.SetBool($"Walk{player.playerLv}",true);
                        isShootFlame = false;
                    }
                }
                time = 0;
            }
            else
            {
                player.animWalk.SetBool($"Attack{player.playerLv}",false);
                player.animWalk.SetBool($"Idle{player.playerLv}",true);
                isShootFlame = false;
            }
        }
    }
}