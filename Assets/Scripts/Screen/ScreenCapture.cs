using System;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.UI;


    public class ScreenCapture : MonoBehaviour
    {
        public int captureWidth = 1920;
        public int captureHeight = 1080;


        public enum Format { Raw, JPG, PNG, PPM };
        public Format format = Format.JPG;


        private string outputFolder;


        private Rect rect;
        private RenderTexture _renderTexture;
        private Texture2D screenShot;
        private byte[] currenText;
        public Image showImage;

        public bool isProcess;
        private bool isTakeScreen;

        private float time;
    
    
        void Start()
        {
            outputFolder = Application.persistentDataPath + "/ScreenPhoto";
            if (!Directory.Exists(outputFolder))
            {
                Directory.CreateDirectory(outputFolder);
                Debug.Log("Save at : "+outputFolder);
            }
            showImage.gameObject.SetActive(false);
        }

        private string CreatFileName(int width, int height)
        {
            string timestamp = DateTime.Now.ToString("yyyyMMddTHHmmss");
            var fileName = string.Format("{0}/screen_{1}x{2}_{3}.{4}", outputFolder, width, height, timestamp,
                format.ToString().ToLower());
            return fileName;
        }

        private void CaptureScreen()
        {
            isProcess = true;
            if (_renderTexture == null)
            {
                rect = new Rect(0,0,captureWidth,captureHeight);
                _renderTexture = new RenderTexture(captureWidth,captureHeight,24);
                screenShot = new Texture2D(captureWidth,captureHeight,TextureFormat.RGB24,false);
            }
            Camera camera = Camera.main;
            camera.targetTexture = _renderTexture;
            camera.Render();

            RenderTexture.active = _renderTexture;
            screenShot.ReadPixels(rect,0,0);
        
            //Reset Texture , Remove The Render
            camera.targetTexture = null;
            RenderTexture.active = null;

            // get our filename
            string filename = CreatFileName((int) rect.width, (int) rect.height);
        
            //Get Data Byte
            byte[] fileHeader = null;
            byte[] fileData = null;


            if (format == Format.Raw)
            {
                fileData = screenShot.GetRawTextureData();
            }
            else if (format == Format.JPG)
            {
                fileData = screenShot.EncodeToJPG();
            }
            else if (format == Format.PNG)
            {
                fileData = screenShot.EncodeToPNG();
            }
            else
            {
                string headerStr = string.Format("P6\n{0} {1} \n255\n",rect.width,rect.height);
                fileHeader = System.Text.Encoding.ASCII.GetBytes(headerStr);
                fileData = screenShot.GetRawTextureData();
            }

            currenText = fileData;
            new System.Threading.Thread(() =>
            {
                var file = System.IO.File.Create(filename);
                if (fileHeader != null)
                {
                    file.Write(fileHeader,0,fileHeader.Length);
                }
                file.Write(fileData,0,fileData.Length);
                file.Close();
                Debug.Log(string.Format("Screenshot Saved {0}, size {1}", filename, fileData.Length));
                isProcess = false;
            }).Start();
            StartCoroutine(ShowImage(filename));
            Destroy(_renderTexture);
            _renderTexture = null;
            screenShot = null;
        }

        public IEnumerator ShowImage(string fileName)
        {
            yield return new WaitForEndOfFrame();
            showImage.material.mainTexture = null;
            Texture2D tex;
            tex = new  Texture2D(captureWidth,captureHeight,TextureFormat.RGB24,false);
            Debug.Log(fileName);
            yield return fileName;
            WWW www = new WWW(fileName);
            yield return www;

            tex.LoadImage(currenText);
            showImage.material.mainTexture = tex;
            isTakeScreen = true;
            showImage.gameObject.SetActive(true);
        }
        public void TakeScreen()
        {
            if (!isProcess)
            {
                CaptureScreen();
            }
            else
            {
                Debug.Log("Now Process");
            }
        }

        private void Update()
        {
            if (isTakeScreen)
            {
                time += Time.deltaTime;
                if (time>=2)
                {
                    showImage.gameObject.SetActive(false);
                    time = 0;
                    isTakeScreen = false;
                }
            }
        }

        // Update is called once per frame
    
    }

