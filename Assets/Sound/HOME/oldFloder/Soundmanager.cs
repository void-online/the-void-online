using System;
using Manager_Scripts;
using UnityEngine;

namespace Sound
{
    public class Soundmanager : MonoBehaviour
    {
       

        [SerializeField] private GameObject radio;
        [SerializeField] private GameObject bed;
        [SerializeField] private GameObject friged;
        [SerializeField] private GameObject craft;
        [SerializeField] private GameObject lv_Up;

        private float time = 0 ;
        private float Stime = 0 ;

        private int x;
        public void Update()
        {
            if (x==1)
            {
                 time += Time.deltaTime;
                 Stime = time;
                 
                 Debug.Log("________"+Stime+"_________");
            }

            if (time>1.1f)
            {
                friged.SetActive(false);
                radio.SetActive(false);
                craft.SetActive(false);
                bed.SetActive(false);
                lv_Up.SetActive(false);
                x = 0;
                time = 0;
            }
        }


        public void Frige()
        {
            friged.SetActive(true);
            x = 1;
            Debug.Log(x);
        }
        public void BED()
        {
            bed.SetActive(true);
            time += Time.deltaTime;
            x = 1;
        } 
        public void Radio()
        {
            radio.SetActive(true);
            time += Time.deltaTime;
            x = 1;
        } 
        public void Craft()
        {
            craft.SetActive(true);
            time += Time.deltaTime;
            x = 1;
        }
        public void LV_UP()
        {
            lv_Up.SetActive(true);
            time += Time.deltaTime;
            x = 1;
        }
        
        
        
        
    }
}