using Manager_Scripts;
using UnityEngine;

namespace Sound
{
    public class Sound : MonoBehaviour
    {
        public AudioSource source;

        public AudioClip soundDay;
        public AudioClip soundNight;

        [SerializeField] UIManager uiManager;
        // Start is called before the first frame update
        

        // Update is called once per frame
        void Update()
        {
            if (uiManager.time == 800)
            {
                source.PlayOneShot(soundDay);
                if (uiManager.time == 1199)
                {
                    source.Stop();
                }
            }
            

            if (uiManager.time == 1200)
            {
                source.PlayOneShot(soundNight);
                if (uiManager.time == 799)
                {
                    source.Stop();
                }
            }
            
        }
    }
}