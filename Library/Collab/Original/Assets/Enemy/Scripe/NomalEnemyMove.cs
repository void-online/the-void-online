﻿using System;
using Manager_Scripts;
using UnityEngine;
using UnityEngine.Serialization;

namespace Scripe
{
    public class NomalEnemyMove : Enemy
    { 
        //variables
        [SerializeField] int   moveSpeed;
        [SerializeField] int   attackDamage;
        [SerializeField] int   lifePoints;
        [SerializeField] float attackRadius;
        private float time;

        //movement
        public float followRadius;
        //end
        [SerializeField] Base       baseTransform;
        [SerializeField] Animator   enemyAnim;
        SpriteRenderer              _enemyflip;
        private bool                isEnemyEnter;
        
        //AUTO Gennarate
        private static readonly int AttackA = Animator.StringToHash("AttackA");
        private static readonly int Walk = Animator.StringToHash("Walk");
        
        void Start()
        {
            enemyAnim = gameObject.GetComponent<Animator>();
            _enemyflip = GetComponent<SpriteRenderer>();
            SetMoveSpeed   (moveSpeed);
            SetAttackDamage(attackDamage);
            SetLifePoints  (lifePoints);
            SetAttackRadius(attackRadius);
            SetFollowRadius(followRadius);
            BuildingManager.Instance.DamageEnemy = attackDamage;
            CharacterManager.Instance.GetPropertyEnemy(attackDamage);
        }
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Player")) 
            {
                isEnemyEnter = true;
                CharacterManager.Instance.IsHitPlayer = true;
            }
            if  (other.gameObject.CompareTag("Building")) 
            {
                isEnemyEnter = true;
            }

            if (other.gameObject.CompareTag("Bullet"))
            {
                CharacterManager.Instance.IsHitBullet = true;
            }

            if (other.gameObject.CompareTag("Bullet"))
            {
                Destroy(other.gameObject);
            }
        }
        private void OnTriggerStay2D(Collider2D other)
        {

            if (other.gameObject.CompareTag("Player")) 
            {
                isEnemyEnter = true;
                CharacterManager.Instance.IsHitPlayer = true;
            }
            if  (other.gameObject.CompareTag("Building")) 
            {
                isEnemyEnter = true;
            }
        }
        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.gameObject.CompareTag("Player")) 
            {
                isEnemyEnter = false;
            }
            if  (other.gameObject.CompareTag("Building")) 
            {
                isEnemyEnter = false;
            }
        }
        // Update is called once per frame
        void Update()
        {
            if (baseTransform.transform.position.x < transform.position.x)
            {
                

                    if (isEnemyEnter )
                    {
                        //for attack animation
                        enemyAnim.SetBool(AttackA, true);
                        enemyAnim.SetBool(Walk, false);
                        transform.position += new Vector3(0, 0f, 0f);
                    }
                    else
                    {
                        transform.position += new Vector3(-GETMoveSpeed() * Time.deltaTime, 0f, 0f);
                        //for attack animation
                        enemyAnim.SetBool(AttackA, false);
                        //walk
                        enemyAnim.SetBool(Walk, true);
                        _enemyflip.flipX = false;
                    }
                   
            
            }
            if (baseTransform.transform.position.x > transform.position.x)
            {
                if (isEnemyEnter)
                    {
                        //for attack animation
                        enemyAnim.SetBool(AttackA, true);
                        enemyAnim.SetBool(Walk, false);
                        transform.position += new Vector3(0, 0f, 0f);
                    }
                    else
                    {
                        transform.position += new Vector3(GETMoveSpeed() * Time.deltaTime, 0f, 0f);
                        //for attack animation
                        enemyAnim.SetBool(AttackA, false);
                        //walk
                        enemyAnim.SetBool(Walk, true);
                        _enemyflip.flipX = true;
                    }
            }
            
            else
            {
                
                enemyAnim.SetBool(AttackA, false);
                transform.position += new Vector3(0, 0f, 0f);
            }

            if (lifePoints<=0)
            {
                gameObject.SetActive(false);
            }

            if (CharacterManager.Instance.IsHitBullet)
            {
                lifePoints -= CharacterManager.Instance.OnHitEnemy();
                CharacterManager.Instance.IsHitBullet = false;
            }
        }
 
    }
}
