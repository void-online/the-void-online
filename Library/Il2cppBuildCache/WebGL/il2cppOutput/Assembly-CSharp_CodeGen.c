﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void DemoScriptUpload::Start()
extern void DemoScriptUpload_Start_mEA0B6F65135C7190FA7CF15E2FDD0B601219FD97 (void);
// 0x00000002 System.Void DemoScriptUpload::SetSSScore(System.Int32,System.String)
extern void DemoScriptUpload_SetSSScore_m38CB33DC46259C1C7B17049939FDBABFE4D53DF9 (void);
// 0x00000003 System.Collections.IEnumerator DemoScriptUpload::DownloadScores()
extern void DemoScriptUpload_DownloadScores_m4DCE1F5EC0F4880AAA8AD5BC57F42E08A54A3B88 (void);
// 0x00000004 System.Void DemoScriptUpload::OnUsernameChaged(LB_ChangeUsernameResult,LB_Entry[])
extern void DemoScriptUpload_OnUsernameChaged_m6E1BC99BAB8530F37B95C4558804EE5856FE91A0 (void);
// 0x00000005 System.Void DemoScriptUpload::OnLeaderboardUpdated(LB_Entry[])
extern void DemoScriptUpload_OnLeaderboardUpdated_mD9D4380D0CFAF6ECADFC9BF315A4C7E40B74BA62 (void);
// 0x00000006 System.Void DemoScriptUpload::OkButtonTouched()
extern void DemoScriptUpload_OkButtonTouched_mF7A02AEF54563644C5A81B0D47586D93E0E5EDC5 (void);
// 0x00000007 System.Void DemoScriptUpload::OnDestroy()
extern void DemoScriptUpload_OnDestroy_mC08808A3709B44CC61F2C7B7FD5E19BC28C0B670 (void);
// 0x00000008 System.String DemoScriptUpload::USERNAME()
extern void DemoScriptUpload_USERNAME_mDB4B629ECC5D00A4B905A347688F46EC3B8800E2 (void);
// 0x00000009 System.Void DemoScriptUpload::.ctor()
extern void DemoScriptUpload__ctor_mF21648CBC6C501CB6B8C43ECF49B1C1936171AD1 (void);
// 0x0000000A System.Void DemoScriptUpload/<DownloadScores>d__6::.ctor(System.Int32)
extern void U3CDownloadScoresU3Ed__6__ctor_m458B4DBDDBAEB5065623F0C54F9EF89B8F020E62 (void);
// 0x0000000B System.Void DemoScriptUpload/<DownloadScores>d__6::System.IDisposable.Dispose()
extern void U3CDownloadScoresU3Ed__6_System_IDisposable_Dispose_m8A74ABA9B83DA29A1360699BBDF8DB1121D2AC6E (void);
// 0x0000000C System.Boolean DemoScriptUpload/<DownloadScores>d__6::MoveNext()
extern void U3CDownloadScoresU3Ed__6_MoveNext_m76311778CF8934A78555B6FF9ACB85EE691750DF (void);
// 0x0000000D System.Object DemoScriptUpload/<DownloadScores>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadScoresU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF1616BD5BE626F4FB23ACF7427B45246F98982FE (void);
// 0x0000000E System.Void DemoScriptUpload/<DownloadScores>d__6::System.Collections.IEnumerator.Reset()
extern void U3CDownloadScoresU3Ed__6_System_Collections_IEnumerator_Reset_m33DB0A4DEB52998E9A02C5461074496DE74116CD (void);
// 0x0000000F System.Object DemoScriptUpload/<DownloadScores>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadScoresU3Ed__6_System_Collections_IEnumerator_get_Current_m77E46FD77E265BFDB234B1ACFBFF25935F8A41CE (void);
// 0x00000010 System.Void LeaderBoardEntrySizeFitter::Start()
extern void LeaderBoardEntrySizeFitter_Start_m773E209A1FA87ED6580D6A4ADCA26AC1B823E513 (void);
// 0x00000011 System.Void LeaderBoardEntrySizeFitter::OnRectTransformDimensionsChange()
extern void LeaderBoardEntrySizeFitter_OnRectTransformDimensionsChange_m78FF00DFD8E28D715AEED1713E713B35964E6589 (void);
// 0x00000012 System.Void LeaderBoardEntrySizeFitter::Update()
extern void LeaderBoardEntrySizeFitter_Update_mDB5CF453EF87999C91159C368A2DB99AAA97F5CB (void);
// 0x00000013 System.Void LeaderBoardEntrySizeFitter::UpdateWidth()
extern void LeaderBoardEntrySizeFitter_UpdateWidth_m25BC960E5AE9DBB5BC33A56DA22DA07B79F579AF (void);
// 0x00000014 System.Void LeaderBoardEntrySizeFitter::.ctor()
extern void LeaderBoardEntrySizeFitter__ctor_mFC64905BCF9E16DA766D014AB128877B345F13FB (void);
// 0x00000015 System.Void UIDemoLeaderboardEntry::Start()
extern void UIDemoLeaderboardEntry_Start_m859529BD9C882A8D2C79F3B126221183A6A05147 (void);
// 0x00000016 System.Void UIDemoLeaderboardEntry::Setup(LB_Entry)
extern void UIDemoLeaderboardEntry_Setup_m677B2F556C06F065FDE7CDD3D855E8771813EA19 (void);
// 0x00000017 System.Collections.IEnumerator UIDemoLeaderboardEntry::SetupLayout()
extern void UIDemoLeaderboardEntry_SetupLayout_mCEF15D13A90378EB65FBB01ED65E4A42992E2759 (void);
// 0x00000018 System.Void UIDemoLeaderboardEntry::.ctor()
extern void UIDemoLeaderboardEntry__ctor_m26A64DD8D4EC07872E596247D1FA9D32DA753DFF (void);
// 0x00000019 System.Void UIDemoLeaderboardEntry/<SetupLayout>d__6::.ctor(System.Int32)
extern void U3CSetupLayoutU3Ed__6__ctor_mD78FF193B3045CEE87B93F49B5C9BF7B3E9C4C16 (void);
// 0x0000001A System.Void UIDemoLeaderboardEntry/<SetupLayout>d__6::System.IDisposable.Dispose()
extern void U3CSetupLayoutU3Ed__6_System_IDisposable_Dispose_m3E4CB9082D3557CF3B6DDC4F85D90A5E7E4A4679 (void);
// 0x0000001B System.Boolean UIDemoLeaderboardEntry/<SetupLayout>d__6::MoveNext()
extern void U3CSetupLayoutU3Ed__6_MoveNext_m2184DA523B03F8562B7D626151BAFB369CF0397B (void);
// 0x0000001C System.Object UIDemoLeaderboardEntry/<SetupLayout>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSetupLayoutU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA57BB452742FD0576D602BF4F1B61442AA6D29C1 (void);
// 0x0000001D System.Void UIDemoLeaderboardEntry/<SetupLayout>d__6::System.Collections.IEnumerator.Reset()
extern void U3CSetupLayoutU3Ed__6_System_Collections_IEnumerator_Reset_mEB9B0829A05E26C36FAABCCB2CA48934B53B989F (void);
// 0x0000001E System.Object UIDemoLeaderboardEntry/<SetupLayout>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CSetupLayoutU3Ed__6_System_Collections_IEnumerator_get_Current_mC290A24F1EFCF9F793DE4E9CB92B74C1B13F16B4 (void);
// 0x0000001F System.Void UI_DemoLeaderboard::OnEnable()
extern void UI_DemoLeaderboard_OnEnable_mBF732224E4001D6CE288ACB720F3FA62969CBD9A (void);
// 0x00000020 System.Void UI_DemoLeaderboard::BackbutttonTouched()
extern void UI_DemoLeaderboard_BackbutttonTouched_m7281FC30CF0EB760C02F6AD9702944CE998DD7FF (void);
// 0x00000021 System.Void UI_DemoLeaderboard::OnDisable()
extern void UI_DemoLeaderboard_OnDisable_mA19A7691256994B39A1FC6025424229D85E1AF86 (void);
// 0x00000022 System.Void UI_DemoLeaderboard::RemoveAllUIEntries()
extern void UI_DemoLeaderboard_RemoveAllUIEntries_mC8BD26AA5C8694C08B34F3DCC73AF1AC45BA5EFF (void);
// 0x00000023 System.Void UI_DemoLeaderboard::OnLeaderboardUpdated(LB_Entry[])
extern void UI_DemoLeaderboard_OnLeaderboardUpdated_m2C9479A3484AFC01DAEF6E89775ABE07D62DE6B9 (void);
// 0x00000024 System.Void UI_DemoLeaderboard::SetupRank()
extern void UI_DemoLeaderboard_SetupRank_mDD168BDF24ED66BC9FD9DA78709AA8F82A863A84 (void);
// 0x00000025 System.Void UI_DemoLeaderboard::.ctor()
extern void UI_DemoLeaderboard__ctor_m482BD3932FF81D180E8A1935721B053106C9D9AC (void);
// 0x00000026 System.Void LB_ChangeUsername::Awake()
extern void LB_ChangeUsername_Awake_mA61790D7B93F799604D685476909DBCFD91816C6 (void);
// 0x00000027 System.Void LB_ChangeUsername::ChangeUsername(System.String,System.String,System.String,System.Int32)
extern void LB_ChangeUsername_ChangeUsername_m4CF717A903A65E8EE94D96F6413E72CC215658F6 (void);
// 0x00000028 System.Collections.IEnumerator LB_ChangeUsername::WaitForRequest()
extern void LB_ChangeUsername_WaitForRequest_m3892185548E537D0ACE6C7AFB7245DC7676324D2 (void);
// 0x00000029 System.Collections.IEnumerator LB_ChangeUsername::Queue()
extern void LB_ChangeUsername_Queue_mB548F5326B96D3307CC0D184F7DF8D236F3E26C0 (void);
// 0x0000002A System.Void LB_ChangeUsername::.ctor()
extern void LB_ChangeUsername__ctor_mBCE805B655C310CB2BF2B515F142ED22A2FA330B (void);
// 0x0000002B System.Void LB_ChangeUsername/OnChangeUsernameFinishedDelegate::.ctor(System.Object,System.IntPtr)
extern void OnChangeUsernameFinishedDelegate__ctor_m321935EA9898632CC84FE823E3F19B741FF586C4 (void);
// 0x0000002C System.Void LB_ChangeUsername/OnChangeUsernameFinishedDelegate::Invoke(LB_ChangeUsernameResult,LB_Entry[])
extern void OnChangeUsernameFinishedDelegate_Invoke_m7053FF7EA4E3E1DD5EFE252236EA1C0FF52D8E17 (void);
// 0x0000002D System.IAsyncResult LB_ChangeUsername/OnChangeUsernameFinishedDelegate::BeginInvoke(LB_ChangeUsernameResult,LB_Entry[],System.AsyncCallback,System.Object)
extern void OnChangeUsernameFinishedDelegate_BeginInvoke_m1BF4B394E834836650E7C7FD0A47E985B0E63066 (void);
// 0x0000002E System.Void LB_ChangeUsername/OnChangeUsernameFinishedDelegate::EndInvoke(System.IAsyncResult)
extern void OnChangeUsernameFinishedDelegate_EndInvoke_m57C17139125B84F1B190F89169D7D4377F45E719 (void);
// 0x0000002F System.Void LB_ChangeUsername/<WaitForRequest>d__8::.ctor(System.Int32)
extern void U3CWaitForRequestU3Ed__8__ctor_mC00B9BF46A96F886C582B70A8DD1B565DB61A516 (void);
// 0x00000030 System.Void LB_ChangeUsername/<WaitForRequest>d__8::System.IDisposable.Dispose()
extern void U3CWaitForRequestU3Ed__8_System_IDisposable_Dispose_mFE69ADB3A97C8493CB73F87FDAC3380347DA52A2 (void);
// 0x00000031 System.Boolean LB_ChangeUsername/<WaitForRequest>d__8::MoveNext()
extern void U3CWaitForRequestU3Ed__8_MoveNext_m6CA3F1D353718CF5D1F4A3DE04D7B026A9BC133F (void);
// 0x00000032 System.Object LB_ChangeUsername/<WaitForRequest>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForRequestU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m29C97B6571BF10FE1B6D5627EF92BDC0E437DEB3 (void);
// 0x00000033 System.Void LB_ChangeUsername/<WaitForRequest>d__8::System.Collections.IEnumerator.Reset()
extern void U3CWaitForRequestU3Ed__8_System_Collections_IEnumerator_Reset_mB2A138469B68697C56C0BB7963D0789E51C07E6A (void);
// 0x00000034 System.Object LB_ChangeUsername/<WaitForRequest>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForRequestU3Ed__8_System_Collections_IEnumerator_get_Current_m7188E281D35498BE04AE17D62C000FC55C59DB48 (void);
// 0x00000035 System.Void LB_ChangeUsername/<Queue>d__9::.ctor(System.Int32)
extern void U3CQueueU3Ed__9__ctor_m8AAF21ADBBFA6EF0231E388774456F2093A1D2B1 (void);
// 0x00000036 System.Void LB_ChangeUsername/<Queue>d__9::System.IDisposable.Dispose()
extern void U3CQueueU3Ed__9_System_IDisposable_Dispose_m70CCC49884F45C7E412E5ACAC7E67CD6DC95CD2F (void);
// 0x00000037 System.Boolean LB_ChangeUsername/<Queue>d__9::MoveNext()
extern void U3CQueueU3Ed__9_MoveNext_m052D2C2A82F20D41BCB1914D7171D94607A2A4D5 (void);
// 0x00000038 System.Object LB_ChangeUsername/<Queue>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CQueueU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6FDE843FE9C4F7718E1F56B0F45DD72A49BDEC7D (void);
// 0x00000039 System.Void LB_ChangeUsername/<Queue>d__9::System.Collections.IEnumerator.Reset()
extern void U3CQueueU3Ed__9_System_Collections_IEnumerator_Reset_mE4A8DCB3E020101E430C43AD68A26999C8AD9DFE (void);
// 0x0000003A System.Object LB_ChangeUsername/<Queue>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CQueueU3Ed__9_System_Collections_IEnumerator_get_Current_mD835F4F4F7170E1807829EA642A169C3AE0A3974 (void);
// 0x0000003B System.Void LB_Controller::Awake()
extern void LB_Controller_Awake_m357722092F27230636F3209D48D966BCF8A9584D (void);
// 0x0000003C System.Void LB_Controller::Start()
extern void LB_Controller_Start_mC89F9407D6FCF7EF7AC29720CA5419461713377F (void);
// 0x0000003D System.Void LB_Controller::StoreScore(System.Single,System.String)
extern void LB_Controller_StoreScore_m08384F34856FABD7CBC2127B575E54391D0C35F8 (void);
// 0x0000003E System.Void LB_Controller::ChangeUsername(System.String,System.String)
extern void LB_Controller_ChangeUsername_m19EABE0E568E6540C749CA9A3432A30F3A81A83E (void);
// 0x0000003F System.Void LB_Controller::OnChangeUsernameFinished(LB_ChangeUsernameResult,LB_Entry[])
extern void LB_Controller_OnChangeUsernameFinished_m9AA6FEDD4ACD23D8008F0C1A092938EEEF8BECF9 (void);
// 0x00000040 System.Void LB_Controller::ReloadLeaderboard()
extern void LB_Controller_ReloadLeaderboard_mB971EA354F6A01006E275DE507C173FE8DD43FB2 (void);
// 0x00000041 System.Void LB_Controller::OnRequestFinished(LB_Entry[])
extern void LB_Controller_OnRequestFinished_m09C76FB9B7652D67438A79E214ACFC264328748D (void);
// 0x00000042 System.Int32 LB_Controller::GetRankForUser(System.String)
extern void LB_Controller_GetRankForUser_mC4481C593C1433AF9BB848046274A12B60579F11 (void);
// 0x00000043 LB_Entry[] LB_Controller::Entries()
extern void LB_Controller_Entries_m08C48EB98BA13A6293CF6908CE0CCA85A739AA73 (void);
// 0x00000044 System.Void LB_Controller::OnDestroy()
extern void LB_Controller_OnDestroy_m76FDCBEA9E2A889E0C0E70B1E61E20DB79E001DD (void);
// 0x00000045 System.Void LB_Controller::.ctor()
extern void LB_Controller__ctor_mFF8FF45A36B9AB0D70EFB9C34E3C8793DB2DDFD2 (void);
// 0x00000046 System.Void LB_Controller/OnAllScoresUpdated::.ctor(System.Object,System.IntPtr)
extern void OnAllScoresUpdated__ctor_m241AA2B78CDC9863E57393622AF1FD5A5383F760 (void);
// 0x00000047 System.Void LB_Controller/OnAllScoresUpdated::Invoke(LB_Entry[])
extern void OnAllScoresUpdated_Invoke_m6C67385D54030029DB5054800F3C5D012181539A (void);
// 0x00000048 System.IAsyncResult LB_Controller/OnAllScoresUpdated::BeginInvoke(LB_Entry[],System.AsyncCallback,System.Object)
extern void OnAllScoresUpdated_BeginInvoke_m1EFF01980AC81F8C72181199A833E8AA7748EB76 (void);
// 0x00000049 System.Void LB_Controller/OnAllScoresUpdated::EndInvoke(System.IAsyncResult)
extern void OnAllScoresUpdated_EndInvoke_mBCA1CC8E757C612B5EE3FDCBD9F3CB322216C832 (void);
// 0x0000004A System.Void LB_Controller/OnUsernameChanged::.ctor(System.Object,System.IntPtr)
extern void OnUsernameChanged__ctor_m449ACFA6CAB11172F0967BAAB39C2A49D19394C6 (void);
// 0x0000004B System.Void LB_Controller/OnUsernameChanged::Invoke(LB_ChangeUsernameResult,LB_Entry[])
extern void OnUsernameChanged_Invoke_mF48A1C0EDD88DC1C8FD19146114D2A8E1D9E6FDD (void);
// 0x0000004C System.IAsyncResult LB_Controller/OnUsernameChanged::BeginInvoke(LB_ChangeUsernameResult,LB_Entry[],System.AsyncCallback,System.Object)
extern void OnUsernameChanged_BeginInvoke_m17B348337FCCAFE7D8C49B2E1735FF73E3F9DBC2 (void);
// 0x0000004D System.Void LB_Controller/OnUsernameChanged::EndInvoke(System.IAsyncResult)
extern void OnUsernameChanged_EndInvoke_mE4F5B23728C224683BAA3270DFE72D8C4D7BB79A (void);
// 0x0000004E System.Void Status::.ctor()
extern void Status__ctor_m0FC8D4BFEED8FB685CC8024A574EB1CCAB4F415E (void);
// 0x0000004F System.Void RequestStatus::.ctor()
extern void RequestStatus__ctor_m924EEBC8D13A01BC270C34D3B4E1A31FF0241F03 (void);
// 0x00000050 System.Void LB_Entry::.ctor()
extern void LB_Entry__ctor_mC7B35C70ED11911969BDF78757D6DA38DDF59B32 (void);
// 0x00000051 System.Void LeaderboardResult::.ctor()
extern void LeaderboardResult__ctor_m64DE2CA9B65BF635684B3C92E27631B9468A9A2E (void);
// 0x00000052 System.Void LB_GetAllScores::Awake()
extern void LB_GetAllScores_Awake_m896166A9C96587431EB6EEA4576B35B3BAA357D0 (void);
// 0x00000053 System.Void LB_GetAllScores::GetAllScores(System.Int32,System.String)
extern void LB_GetAllScores_GetAllScores_m4B44D580958771F4B6267A686ABF40CA173EDA75 (void);
// 0x00000054 System.Collections.IEnumerator LB_GetAllScores::WaitForRequest(System.Int32)
extern void LB_GetAllScores_WaitForRequest_m0D5EF9D675EE95A203DA964047FA41839FAEE6AB (void);
// 0x00000055 System.Collections.IEnumerator LB_GetAllScores::Queue()
extern void LB_GetAllScores_Queue_m4A5049608C05575844BC43966116496547349A5D (void);
// 0x00000056 System.Void LB_GetAllScores::.ctor()
extern void LB_GetAllScores__ctor_mADC5CD83C0E28C0DFB4A9082B9AA1D7D8F224DCC (void);
// 0x00000057 System.Void LB_GetAllScores/OnGetAllScoresFinishedDelegate::.ctor(System.Object,System.IntPtr)
extern void OnGetAllScoresFinishedDelegate__ctor_m9519F4612572A54A9E707B89546D8B52EF0768BE (void);
// 0x00000058 System.Void LB_GetAllScores/OnGetAllScoresFinishedDelegate::Invoke(LB_Entry[])
extern void OnGetAllScoresFinishedDelegate_Invoke_m29B4448B8EA0C36CCC86C410630966A182C4DE75 (void);
// 0x00000059 System.IAsyncResult LB_GetAllScores/OnGetAllScoresFinishedDelegate::BeginInvoke(LB_Entry[],System.AsyncCallback,System.Object)
extern void OnGetAllScoresFinishedDelegate_BeginInvoke_m086D2E2D98632432267CBDF219081A1FE445469C (void);
// 0x0000005A System.Void LB_GetAllScores/OnGetAllScoresFinishedDelegate::EndInvoke(System.IAsyncResult)
extern void OnGetAllScoresFinishedDelegate_EndInvoke_mC829E772F3DFB0C33A0497DB807C1DC14E1022C0 (void);
// 0x0000005B System.Void LB_GetAllScores/<WaitForRequest>d__6::.ctor(System.Int32)
extern void U3CWaitForRequestU3Ed__6__ctor_m97BB7708C2EB3A70F5C94182372AF9E32FC7CD47 (void);
// 0x0000005C System.Void LB_GetAllScores/<WaitForRequest>d__6::System.IDisposable.Dispose()
extern void U3CWaitForRequestU3Ed__6_System_IDisposable_Dispose_m7EE7540A570B1E2434619CDA4358814C30FB86A4 (void);
// 0x0000005D System.Boolean LB_GetAllScores/<WaitForRequest>d__6::MoveNext()
extern void U3CWaitForRequestU3Ed__6_MoveNext_m49F31127E708112D8AEEC026608B46AF6CD0E84E (void);
// 0x0000005E System.Object LB_GetAllScores/<WaitForRequest>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForRequestU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF12E25E1BEE99CAF799E12150915679EA217C41B (void);
// 0x0000005F System.Void LB_GetAllScores/<WaitForRequest>d__6::System.Collections.IEnumerator.Reset()
extern void U3CWaitForRequestU3Ed__6_System_Collections_IEnumerator_Reset_mD25372C19CEFCCB9AA996890BBD729FBB9540D9E (void);
// 0x00000060 System.Object LB_GetAllScores/<WaitForRequest>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForRequestU3Ed__6_System_Collections_IEnumerator_get_Current_m9C7C1B996CB4FA711EAF40E109A2514D210DE5B3 (void);
// 0x00000061 System.Void LB_GetAllScores/<Queue>d__7::.ctor(System.Int32)
extern void U3CQueueU3Ed__7__ctor_m61B5858BFB2402452D6C13C8E84B4D85D88AFEA3 (void);
// 0x00000062 System.Void LB_GetAllScores/<Queue>d__7::System.IDisposable.Dispose()
extern void U3CQueueU3Ed__7_System_IDisposable_Dispose_m4F87B63F9383B2160E165D1FC8836C0AA460099D (void);
// 0x00000063 System.Boolean LB_GetAllScores/<Queue>d__7::MoveNext()
extern void U3CQueueU3Ed__7_MoveNext_m77D9B22933517A79019F7068E4E6FC65835F1546 (void);
// 0x00000064 System.Object LB_GetAllScores/<Queue>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CQueueU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC868CCF7E7C3E8225901D74C22CBF6F5CB55FC7C (void);
// 0x00000065 System.Void LB_GetAllScores/<Queue>d__7::System.Collections.IEnumerator.Reset()
extern void U3CQueueU3Ed__7_System_Collections_IEnumerator_Reset_mD1460AC868EBBC5EF1151FAE7B95C77AB7711F69 (void);
// 0x00000066 System.Object LB_GetAllScores/<Queue>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CQueueU3Ed__7_System_Collections_IEnumerator_get_Current_m799D1D4AFD057C4A1D779F6891F5DAD2104225A1 (void);
// 0x00000067 System.Void LB_StoreScore::Awake()
extern void LB_StoreScore_Awake_m955A76945B2FD44355B68DACA00252BFD7182A3E (void);
// 0x00000068 System.Void LB_StoreScore::StoreScore(System.Single,System.String,System.Int32,System.String)
extern void LB_StoreScore_StoreScore_m241D6C1F495DD3B2236001278901C3DC8846183B (void);
// 0x00000069 System.Collections.IEnumerator LB_StoreScore::WaitForRequest(System.Single,System.String,System.Int32)
extern void LB_StoreScore_WaitForRequest_m960777347A3E737EB979B99E98792C91F19C6B0D (void);
// 0x0000006A System.Collections.IEnumerator LB_StoreScore::Queue()
extern void LB_StoreScore_Queue_m43344EBE7F6233651E8F82495D1C195773F9ED70 (void);
// 0x0000006B System.Void LB_StoreScore::.ctor()
extern void LB_StoreScore__ctor_mC0D86244E986EC67F432EA0177B07229D5E6C076 (void);
// 0x0000006C System.Void LB_StoreScore/<WaitForRequest>d__6::.ctor(System.Int32)
extern void U3CWaitForRequestU3Ed__6__ctor_mF766FA099094CD3144F44A84514A1200680C3AFA (void);
// 0x0000006D System.Void LB_StoreScore/<WaitForRequest>d__6::System.IDisposable.Dispose()
extern void U3CWaitForRequestU3Ed__6_System_IDisposable_Dispose_mC5111309F5B49F24C7C35D0CBDB7940979057C41 (void);
// 0x0000006E System.Boolean LB_StoreScore/<WaitForRequest>d__6::MoveNext()
extern void U3CWaitForRequestU3Ed__6_MoveNext_m96A93D387A6BBC777125737DEA2EDEC154D948F4 (void);
// 0x0000006F System.Object LB_StoreScore/<WaitForRequest>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForRequestU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE6ACA16FE5EB0E47CB665A9A42D8D4AF9ABDBED1 (void);
// 0x00000070 System.Void LB_StoreScore/<WaitForRequest>d__6::System.Collections.IEnumerator.Reset()
extern void U3CWaitForRequestU3Ed__6_System_Collections_IEnumerator_Reset_mAD39B16DB0BEAC45E03C87EE94FE87EE0414331A (void);
// 0x00000071 System.Object LB_StoreScore/<WaitForRequest>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForRequestU3Ed__6_System_Collections_IEnumerator_get_Current_m440BAE92717F6F07FD2056DE956D6A0C76F68261 (void);
// 0x00000072 System.Void LB_StoreScore/<Queue>d__7::.ctor(System.Int32)
extern void U3CQueueU3Ed__7__ctor_m46A68EFAF2FEE8EFC70B275F322103151F6C0129 (void);
// 0x00000073 System.Void LB_StoreScore/<Queue>d__7::System.IDisposable.Dispose()
extern void U3CQueueU3Ed__7_System_IDisposable_Dispose_mDADD194443E921593D77B71AAEDFE003D0A0BF38 (void);
// 0x00000074 System.Boolean LB_StoreScore/<Queue>d__7::MoveNext()
extern void U3CQueueU3Ed__7_MoveNext_mE15D5F8AEF59D919C469D337C305D9AAFD594CCC (void);
// 0x00000075 System.Object LB_StoreScore/<Queue>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CQueueU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2A7707DE7731C252B928CC1C3CD2295F60F1A978 (void);
// 0x00000076 System.Void LB_StoreScore/<Queue>d__7::System.Collections.IEnumerator.Reset()
extern void U3CQueueU3Ed__7_System_Collections_IEnumerator_Reset_m99C2E6B33286E36FE375FFC48B3CAADF620744EE (void);
// 0x00000077 System.Object LB_StoreScore/<Queue>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CQueueU3Ed__7_System_Collections_IEnumerator_get_Current_m1DBEF398E21DCFD6B46024014F6CEA2D23CC0E0D (void);
// 0x00000078 System.Void MainScript::LogMessage(System.String,System.String)
extern void MainScript_LogMessage_m7953CB447ABFC4D0E286219425CF26684DB6F6B0 (void);
// 0x00000079 System.Void MainScript::Get()
extern void MainScript_Get_m3BD5249433D12EE1562CB113FF95FD302207B2A0 (void);
// 0x0000007A System.Void MainScript::Post()
extern void MainScript_Post_mF7C21819A39C80CBEF1E5471FF7A505A6CF9B687 (void);
// 0x0000007B System.Void MainScript::Put()
extern void MainScript_Put_m2E077AE7D135014788A9F219D4FA32B618BED552 (void);
// 0x0000007C System.Void MainScript::Delete()
extern void MainScript_Delete_m3CEEF47B740A70E766D3CF74E6C1D37E13C0A8C8 (void);
// 0x0000007D System.Void MainScript::AbortRequest()
extern void MainScript_AbortRequest_mDCEC8008FD946FB21C66D8DE04AA652FD091AC5E (void);
// 0x0000007E System.Void MainScript::DownloadFile()
extern void MainScript_DownloadFile_mF54C07D06969AD450838E27A885FB450BDA6971C (void);
// 0x0000007F System.Void MainScript::.ctor()
extern void MainScript__ctor_mDE68DE7EC111F10F50287CE15AC2100BAC26DF23 (void);
// 0x00000080 System.Void MainScript::<Post>b__4_0(Models.Post)
extern void MainScript_U3CPostU3Eb__4_0_m8B0202AA78218D3787871313668C76F260A52D42 (void);
// 0x00000081 System.Void MainScript::<Post>b__4_1(System.Exception)
extern void MainScript_U3CPostU3Eb__4_1_m2C8C662A8A4EE9A219C715C38E88E9D156CC0777 (void);
// 0x00000082 System.Void MainScript::<Put>b__5_0(Proyecto26.RequestException,Proyecto26.ResponseHelper,Models.Post)
extern void MainScript_U3CPutU3Eb__5_0_m556845DFA7BB10FCFF4A3A04B17EF6BC8AA77CAE (void);
// 0x00000083 System.Void MainScript::<Delete>b__6_0(Proyecto26.RequestException,Proyecto26.ResponseHelper)
extern void MainScript_U3CDeleteU3Eb__6_0_m3092FBC0D31C4B83C0C7E537BFD404FD28F0BB8E (void);
// 0x00000084 System.Void MainScript::<DownloadFile>b__8_0(Proyecto26.ResponseHelper)
extern void MainScript_U3CDownloadFileU3Eb__8_0_m8D61A6C39FFBB97884CF0935DA9BF297B5DE8393 (void);
// 0x00000085 System.Void MainScript::<DownloadFile>b__8_1(System.Exception)
extern void MainScript_U3CDownloadFileU3Eb__8_1_m99B4D340A42D8D8B784669EDB99B721CAD853FB6 (void);
// 0x00000086 System.Void MainScript/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m46C50E0A5C12FA151CC55E0B928E27838925B4FC (void);
// 0x00000087 RSG.IPromise`1<Models.Todo[]> MainScript/<>c__DisplayClass3_0::<Get>b__0(Models.Post[])
extern void U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__0_m1E95C1F0B2D8E5BC302A5ADCA1ABAE0293AEA82D (void);
// 0x00000088 RSG.IPromise`1<Models.User[]> MainScript/<>c__DisplayClass3_0::<Get>b__1(Models.Todo[])
extern void U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__1_mB5A9CA352F06FB80FDC47E6FE114495BA4687AC1 (void);
// 0x00000089 RSG.IPromise`1<Models.Photo[]> MainScript/<>c__DisplayClass3_0::<Get>b__2(Models.User[])
extern void U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__2_mB93ACE707409C85F0542FD7665B0BF499473D98D (void);
// 0x0000008A System.Void MainScript/<>c__DisplayClass3_0::<Get>b__3(Models.Photo[])
extern void U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__3_m381C9E851E8FB988B02A1076B749138ABC954BDB (void);
// 0x0000008B System.Void MainScript/<>c__DisplayClass3_0::<Get>b__4(System.Exception)
extern void U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__4_m44FBEB10E4BAEB006FC92154DB40AB3CB7BC4DE0 (void);
// 0x0000008C System.Void MainScript/<>c::.cctor()
extern void U3CU3Ec__cctor_m4FA3DF2408BEA55F36632D2A36DDEA9E1475BBA3 (void);
// 0x0000008D System.Void MainScript/<>c::.ctor()
extern void U3CU3Ec__ctor_m6B5FCC705564205B21810FFBA043CB8DA659F8E1 (void);
// 0x0000008E System.Void MainScript/<>c::<Put>b__5_1(Proyecto26.RequestException,System.Int32)
extern void U3CU3Ec_U3CPutU3Eb__5_1_mC12F48C710EDD364D74E0411BCE9CB0F55326D2F (void);
// 0x0000008F System.Void AdsManager::Start()
extern void AdsManager_Start_m561A6D94B810C2CBBAD6235533FEF81D3FCB93BA (void);
// 0x00000090 System.Void AdsManager::Update()
extern void AdsManager_Update_m84B68116B4140F28E1729396720F8FCD9F790646 (void);
// 0x00000091 System.Void AdsManager::ShowAd()
extern void AdsManager_ShowAd_m37205D6D03ADA57C5DC2B7890B09CDDCFB87C6DC (void);
// 0x00000092 System.Void AdsManager::ShowAds(System.String)
extern void AdsManager_ShowAds_m8F51286E3D51611E15459A5635394278B8DB192D (void);
// 0x00000093 System.Void AdsManager::OnUnityAdsReady(System.String)
extern void AdsManager_OnUnityAdsReady_mA76941ECD72FD15E03D0B7DBF10365BF5AD72764 (void);
// 0x00000094 System.Void AdsManager::OnUnityAdsDidError(System.String)
extern void AdsManager_OnUnityAdsDidError_m1908BC190CA0D84743BF3480F0D10B4202F9E207 (void);
// 0x00000095 System.Void AdsManager::OnUnityAdsDidStart(System.String)
extern void AdsManager_OnUnityAdsDidStart_mF0AB782E7E55745E4D5A5A1EDFDB13CF64F73B15 (void);
// 0x00000096 System.Void AdsManager::OnUnityAdsDidFinish(System.String,UnityEngine.Advertisements.ShowResult)
extern void AdsManager_OnUnityAdsDidFinish_m73319AF105DBF4B961201FC670FDE62BE190E01F (void);
// 0x00000097 System.Void AdsManager::.ctor()
extern void AdsManager__ctor_m1324EE3D9841045940C44D5E0379750DBE0C0A53 (void);
// 0x00000098 System.Void FirebaseLeaderBoard::Start()
extern void FirebaseLeaderBoard_Start_m2A8606E3CB9A1B2C1679AB3CEA2ABBAE82EDAF19 (void);
// 0x00000099 System.Void FirebaseLeaderBoard::Getdata()
extern void FirebaseLeaderBoard_Getdata_m4C48E7B72E7E7C7D2120D93037E2EB619D94E7DF (void);
// 0x0000009A System.Void FirebaseLeaderBoard::SetData()
extern void FirebaseLeaderBoard_SetData_mD4B3213998D0FB7115813C65D30AA885CE07785B (void);
// 0x0000009B System.Void FirebaseLeaderBoard::.ctor()
extern void FirebaseLeaderBoard__ctor_m1FAF10D9488E553684210B591B25772D8E144EF6 (void);
// 0x0000009C System.Void FirebaseLeaderBoard/User::.ctor()
extern void User__ctor_mADEC54352092BAE9268BB4C08BDFA1044CFA7B5F (void);
// 0x0000009D System.Void FirebaseLeaderBoard/User/UserDataaa::.ctor(System.String,System.Int32,System.Int32)
extern void UserDataaa__ctor_m9E49CA15AB81912B51F77B0354896A50F74495A4 (void);
// 0x0000009E System.Void FirebaseLeaderBoard/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_mCC77BBBA53D919BDE33512665E573EF1DB596230 (void);
// 0x0000009F System.Void FirebaseLeaderBoard/<>c__DisplayClass5_0::<Getdata>b__0(Proyecto26.ResponseHelper)
extern void U3CU3Ec__DisplayClass5_0_U3CGetdataU3Eb__0_mE072A040B7AB8851F8813648BA075A4FF0BCB9EE (void);
// 0x000000A0 System.Void FirebaseLeaderBoard/<>c::.cctor()
extern void U3CU3Ec__cctor_mE98FA4BC4AF7D98FDEF99BCB24D6A5AAB2E4B5D2 (void);
// 0x000000A1 System.Void FirebaseLeaderBoard/<>c::.ctor()
extern void U3CU3Ec__ctor_m1EA501D17E3E06CDD519EF188BDF8290240D7E44 (void);
// 0x000000A2 System.Void FirebaseLeaderBoard/<>c::<Getdata>b__5_1(System.Exception)
extern void U3CU3Ec_U3CGetdataU3Eb__5_1_mA3A399D9EB7F52C3B85CC5AAB12B4241CDE01885 (void);
// 0x000000A3 System.Void FirebaseLeaderBoard/<>c::<SetData>b__6_2(FirebaseLeaderBoard/User)
extern void U3CU3Ec_U3CSetDataU3Eb__6_2_mDE271C3889E38C35DD4E13884087AB860F477075 (void);
// 0x000000A4 System.Void FirebaseLeaderBoard/<>c::<SetData>b__6_3(System.Exception)
extern void U3CU3Ec_U3CSetDataU3Eb__6_3_m3A3BBB790869403499086B12DDBFE46EEB57E118 (void);
// 0x000000A5 System.Void FirebaseLeaderBoard/<>c::<SetData>b__6_1(System.Exception)
extern void U3CU3Ec_U3CSetDataU3Eb__6_1_mB823AA97FFC1975F9E35B3FEFE4CB2B2C1AB49D1 (void);
// 0x000000A6 System.Void FirebaseLeaderBoard/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m67BFE4E5C73A36DD5EB78B620CB1A626133396D6 (void);
// 0x000000A7 System.Void FirebaseLeaderBoard/<>c__DisplayClass6_0::<SetData>b__0(Proyecto26.ResponseHelper)
extern void U3CU3Ec__DisplayClass6_0_U3CSetDataU3Eb__0_m1EF9F29E4C954A82EA8A318ABF931A34E2CFC786 (void);
// 0x000000A8 System.Void RankManger::Start()
extern void RankManger_Start_mC0E8A40BDD26934BA9F22FD2DF1B2D0A5217433A (void);
// 0x000000A9 System.Void RankManger::CreatRankData()
extern void RankManger_CreatRankData_mB9223C0DC71C1F401546AF3110A8893E72A008E8 (void);
// 0x000000AA System.Void RankManger::.ctor()
extern void RankManger__ctor_m277636F4F694280DC5E884A5A40D2B80A90BE665 (void);
// 0x000000AB System.Void SceneManagere::nextScene()
extern void SceneManagere_nextScene_mC0A2B5AE808B0DDA8257E6012639B94F02CFB0E8 (void);
// 0x000000AC System.Void SceneManagere::.ctor()
extern void SceneManagere__ctor_m9EE09D20C8AEF419239B33A2B635CCFA60092E0F (void);
// 0x000000AD System.Void SafeAreaSetter::Start()
extern void SafeAreaSetter_Start_m83780A11588277507500E26788A2EA69E09FF392 (void);
// 0x000000AE System.Void SafeAreaSetter::ApplySafeArea()
extern void SafeAreaSetter_ApplySafeArea_m4F4F38103E2C3689B92117BF87F0E7559B48CB25 (void);
// 0x000000AF System.Void SafeAreaSetter::Update()
extern void SafeAreaSetter_Update_mF64C75E3889BDD924F5886A3AFF934BC395A5E01 (void);
// 0x000000B0 System.Void SafeAreaSetter::.ctor()
extern void SafeAreaSetter__ctor_mA93E55DED3B5E17188E4D3CC093573F5E1BC696B (void);
// 0x000000B1 System.Void ScreenCapture::Start()
extern void ScreenCapture_Start_mE754AB7DB07B1B52F9528011C8394B374ACF6CE5 (void);
// 0x000000B2 System.String ScreenCapture::CreatFileName(System.Int32,System.Int32)
extern void ScreenCapture_CreatFileName_mBC0FF46D1C6D2C3167E22CAB2F5D4F863CA559FB (void);
// 0x000000B3 System.Void ScreenCapture::CaptureScreen()
extern void ScreenCapture_CaptureScreen_m075969365E6FBE7A5902917C1CE1BA98BE589632 (void);
// 0x000000B4 System.Collections.IEnumerator ScreenCapture::ShowImage(System.String)
extern void ScreenCapture_ShowImage_m9219EEEC540FBD07E04C20A067BAD7F855423823 (void);
// 0x000000B5 System.Void ScreenCapture::TakeScreen()
extern void ScreenCapture_TakeScreen_m46600B2F2EEFBB4002BE5326D6511884E56AF201 (void);
// 0x000000B6 System.Void ScreenCapture::Update()
extern void ScreenCapture_Update_m1F10F112088C1AD9570A869177A6D9AD5FD2F4F5 (void);
// 0x000000B7 System.Void ScreenCapture::.ctor()
extern void ScreenCapture__ctor_mF1A1D405C141990DEFD5166FA5370FD91EE8F6D4 (void);
// 0x000000B8 System.Void ScreenCapture/<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_mFAB3648391D7F7415C8099820E6687D05184B688 (void);
// 0x000000B9 System.Void ScreenCapture/<>c__DisplayClass15_0::<CaptureScreen>b__0()
extern void U3CU3Ec__DisplayClass15_0_U3CCaptureScreenU3Eb__0_mFFF45C5919C398F496FF3917DF70929D77D5EEEF (void);
// 0x000000BA System.Void ScreenCapture/<ShowImage>d__16::.ctor(System.Int32)
extern void U3CShowImageU3Ed__16__ctor_mCC0CEF23681A7B9048820FBE923839D9B68B87AD (void);
// 0x000000BB System.Void ScreenCapture/<ShowImage>d__16::System.IDisposable.Dispose()
extern void U3CShowImageU3Ed__16_System_IDisposable_Dispose_m87BB0FC2D94FEFAFF97161D322FBB0CE5898DC65 (void);
// 0x000000BC System.Boolean ScreenCapture/<ShowImage>d__16::MoveNext()
extern void U3CShowImageU3Ed__16_MoveNext_m7DDEBE1874282C4F66CAD5BAF0ADD1798C33E7D0 (void);
// 0x000000BD System.Object ScreenCapture/<ShowImage>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShowImageU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m598FC1EC637E2B1CFAEF2530048EDF317FC78FF7 (void);
// 0x000000BE System.Void ScreenCapture/<ShowImage>d__16::System.Collections.IEnumerator.Reset()
extern void U3CShowImageU3Ed__16_System_Collections_IEnumerator_Reset_mEE4AEDDD2B6E7A84E2AE04E3B8EA2DEFC538C3AE (void);
// 0x000000BF System.Object ScreenCapture/<ShowImage>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CShowImageU3Ed__16_System_Collections_IEnumerator_get_Current_m762F9432D7D55A302AD5EB8D65BF6C58167D4262 (void);
// 0x000000C0 System.Void ChatController::OnEnable()
extern void ChatController_OnEnable_mBC3BFC05A1D47069DFA58A4F0E39CFF8FAD44FF2 (void);
// 0x000000C1 System.Void ChatController::OnDisable()
extern void ChatController_OnDisable_m268A7488A3FDEB850FC3BC91A0BBDA767D42876B (void);
// 0x000000C2 System.Void ChatController::AddToChatOutput(System.String)
extern void ChatController_AddToChatOutput_m43856EEA133E04C24701A2616E7F10D7FFA68371 (void);
// 0x000000C3 System.Void ChatController::.ctor()
extern void ChatController__ctor_m3B66A5F749B457D865E8BDA1DE481C8CF1158026 (void);
// 0x000000C4 System.Void DropdownSample::OnButtonClick()
extern void DropdownSample_OnButtonClick_m97641451A2EAC4FC6682897A948A37FDD6BF9EA6 (void);
// 0x000000C5 System.Void DropdownSample::.ctor()
extern void DropdownSample__ctor_m87664E3DC75A1A9AA19FEE6ED6B993AB1E50E2B9 (void);
// 0x000000C6 System.Void EnvMapAnimator::Awake()
extern void EnvMapAnimator_Awake_mFFC04BC5320F83CA8C45FF36A11BF43AC17C6B93 (void);
// 0x000000C7 System.Collections.IEnumerator EnvMapAnimator::Start()
extern void EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE (void);
// 0x000000C8 System.Void EnvMapAnimator::.ctor()
extern void EnvMapAnimator__ctor_mC151D673A394E2E7CEA8774C4004985028C5C3EC (void);
// 0x000000C9 System.Void EnvMapAnimator/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23 (void);
// 0x000000CA System.Void EnvMapAnimator/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B (void);
// 0x000000CB System.Boolean EnvMapAnimator/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m2F1A8053A32AD86DA80F86391EC32EDC1C396AEE (void);
// 0x000000CC System.Object EnvMapAnimator/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E (void);
// 0x000000CD System.Void EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB (void);
// 0x000000CE System.Object EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF (void);
// 0x000000CF System.Char TMPro.TMP_DigitValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_DigitValidator_Validate_m5D303EB8CD6E9E7D526D633BA1021884051FE226 (void);
// 0x000000D0 System.Void TMPro.TMP_DigitValidator::.ctor()
extern void TMP_DigitValidator__ctor_m1E838567EF38170662F1BAF52A847CC2C258333E (void);
// 0x000000D1 System.Char TMPro.TMP_PhoneNumberValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_PhoneNumberValidator_Validate_mF0E90A277E9E91BC213DD02DC60088D03C9436B1 (void);
// 0x000000D2 System.Void TMPro.TMP_PhoneNumberValidator::.ctor()
extern void TMP_PhoneNumberValidator__ctor_mB3C36CAAE3B52554C44A1D19194F0176B5A8EED3 (void);
// 0x000000D3 TMPro.TMP_TextEventHandler/CharacterSelectionEvent TMPro.TMP_TextEventHandler::get_onCharacterSelection()
extern void TMP_TextEventHandler_get_onCharacterSelection_m90C39320C726E8E542D91F4BBD690697349F5385 (void);
// 0x000000D4 System.Void TMPro.TMP_TextEventHandler::set_onCharacterSelection(TMPro.TMP_TextEventHandler/CharacterSelectionEvent)
extern void TMP_TextEventHandler_set_onCharacterSelection_m5ED7658EB101C6740A921FA150DE18C443BDA0C4 (void);
// 0x000000D5 TMPro.TMP_TextEventHandler/SpriteSelectionEvent TMPro.TMP_TextEventHandler::get_onSpriteSelection()
extern void TMP_TextEventHandler_get_onSpriteSelection_m0E645AE1DFE19B011A3319474D0CF0DA612C8B7B (void);
// 0x000000D6 System.Void TMPro.TMP_TextEventHandler::set_onSpriteSelection(TMPro.TMP_TextEventHandler/SpriteSelectionEvent)
extern void TMP_TextEventHandler_set_onSpriteSelection_m4AFC6772C3357218956A5D33B3CD19F3AAF39788 (void);
// 0x000000D7 TMPro.TMP_TextEventHandler/WordSelectionEvent TMPro.TMP_TextEventHandler::get_onWordSelection()
extern void TMP_TextEventHandler_get_onWordSelection_mA42B89A37810FB659FCFA8539339A3BB8037203A (void);
// 0x000000D8 System.Void TMPro.TMP_TextEventHandler::set_onWordSelection(TMPro.TMP_TextEventHandler/WordSelectionEvent)
extern void TMP_TextEventHandler_set_onWordSelection_m4A839FAFFABAFECD073B82BA8826E1CD033C0076 (void);
// 0x000000D9 TMPro.TMP_TextEventHandler/LineSelectionEvent TMPro.TMP_TextEventHandler::get_onLineSelection()
extern void TMP_TextEventHandler_get_onLineSelection_mB701B6C713AD4EFC61E1B30A564EE54ADE31F58D (void);
// 0x000000DA System.Void TMPro.TMP_TextEventHandler::set_onLineSelection(TMPro.TMP_TextEventHandler/LineSelectionEvent)
extern void TMP_TextEventHandler_set_onLineSelection_m0B2337598350E51D0A17B8FCB3AAA533F312F3DA (void);
// 0x000000DB TMPro.TMP_TextEventHandler/LinkSelectionEvent TMPro.TMP_TextEventHandler::get_onLinkSelection()
extern void TMP_TextEventHandler_get_onLinkSelection_mF5C3875D661F5B1E3712566FE16D332EA37D15BB (void);
// 0x000000DC System.Void TMPro.TMP_TextEventHandler::set_onLinkSelection(TMPro.TMP_TextEventHandler/LinkSelectionEvent)
extern void TMP_TextEventHandler_set_onLinkSelection_m200566EDEB2C9299647F3EEAC588B51818D360A5 (void);
// 0x000000DD System.Void TMPro.TMP_TextEventHandler::Awake()
extern void TMP_TextEventHandler_Awake_m43EB03A4A6776A624F79457EC49E78E7B5BA1C70 (void);
// 0x000000DE System.Void TMPro.TMP_TextEventHandler::LateUpdate()
extern void TMP_TextEventHandler_LateUpdate_mE1D989C40DA8E54E116E3C60217DFCAADD6FDE11 (void);
// 0x000000DF System.Void TMPro.TMP_TextEventHandler::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerEnter_m8FC88F25858B24CE68BE80C727A3F0227A8EE5AC (void);
// 0x000000E0 System.Void TMPro.TMP_TextEventHandler::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerExit_mBB2A74D55741F631A678A5D6997D40247FE75D44 (void);
// 0x000000E1 System.Void TMPro.TMP_TextEventHandler::SendOnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnCharacterSelection_m78983D3590F1B0C242BEAB0A11FDBDABDD1814EC (void);
// 0x000000E2 System.Void TMPro.TMP_TextEventHandler::SendOnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnSpriteSelection_m10C257A74F121B95E7077F7E488FBC52380A6C53 (void);
// 0x000000E3 System.Void TMPro.TMP_TextEventHandler::SendOnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnWordSelection_m95A4E5A00E339E5B8BA9AA63B98DB3E81C8B8F66 (void);
// 0x000000E4 System.Void TMPro.TMP_TextEventHandler::SendOnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnLineSelection_mE85BA6ECE1188B666FD36839B8970C3E0130BC43 (void);
// 0x000000E5 System.Void TMPro.TMP_TextEventHandler::SendOnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventHandler_SendOnLinkSelection_m299E6620DFD835C8258A3F48B4EA076C304E9B77 (void);
// 0x000000E6 System.Void TMPro.TMP_TextEventHandler::.ctor()
extern void TMP_TextEventHandler__ctor_m6345DC1CEA2E4209E928AD1E61C3ACA4227DD9B8 (void);
// 0x000000E7 System.Void TMPro.TMP_TextEventHandler/CharacterSelectionEvent::.ctor()
extern void CharacterSelectionEvent__ctor_m06BC183AF31BA4A2055A44514BC3FF0539DD04C7 (void);
// 0x000000E8 System.Void TMPro.TMP_TextEventHandler/SpriteSelectionEvent::.ctor()
extern void SpriteSelectionEvent__ctor_m0F760052E9A5AF44A7AF7AC006CB4B24809590F2 (void);
// 0x000000E9 System.Void TMPro.TMP_TextEventHandler/WordSelectionEvent::.ctor()
extern void WordSelectionEvent__ctor_m106CDEB17C520C9D20CE7120DE6BBBDEDB48886C (void);
// 0x000000EA System.Void TMPro.TMP_TextEventHandler/LineSelectionEvent::.ctor()
extern void LineSelectionEvent__ctor_m5D735FDA9B71B9147C6F791B331498F145D75018 (void);
// 0x000000EB System.Void TMPro.TMP_TextEventHandler/LinkSelectionEvent::.ctor()
extern void LinkSelectionEvent__ctor_m7AB7977D0D0F8883C5A98DD6BB2D390BC3CAB8E0 (void);
// 0x000000EC System.Collections.IEnumerator TMPro.Examples.Benchmark01::Start()
extern void Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C (void);
// 0x000000ED System.Void TMPro.Examples.Benchmark01::.ctor()
extern void Benchmark01__ctor_mB92568AA7A9E13B92315B6270FCA23584A7D0F7F (void);
// 0x000000EE System.Void TMPro.Examples.Benchmark01/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB (void);
// 0x000000EF System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73 (void);
// 0x000000F0 System.Boolean TMPro.Examples.Benchmark01/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mF3B1D38CD37FD5187C3192141DB380747C66AD3C (void);
// 0x000000F1 System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591 (void);
// 0x000000F2 System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B (void);
// 0x000000F3 System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E (void);
// 0x000000F4 System.Collections.IEnumerator TMPro.Examples.Benchmark01_UGUI::Start()
extern void Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4 (void);
// 0x000000F5 System.Void TMPro.Examples.Benchmark01_UGUI::.ctor()
extern void Benchmark01_UGUI__ctor_mACFE8D997EAB50FDBD7F671C1A25A892D9F78376 (void);
// 0x000000F6 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651 (void);
// 0x000000F7 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561 (void);
// 0x000000F8 System.Boolean TMPro.Examples.Benchmark01_UGUI/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mF9B2C8D290035BC9A79C4347772B5B7B9D404DE1 (void);
// 0x000000F9 System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD (void);
// 0x000000FA System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA (void);
// 0x000000FB System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2 (void);
// 0x000000FC System.Void TMPro.Examples.Benchmark02::Start()
extern void Benchmark02_Start_m315C0DF3A9AC66B4F5802FDAED056E7E5F3D2046 (void);
// 0x000000FD System.Void TMPro.Examples.Benchmark02::.ctor()
extern void Benchmark02__ctor_m526B38D3B7E75905208E4E57B168D8AC1900F4E5 (void);
// 0x000000FE System.Void TMPro.Examples.Benchmark03::Awake()
extern void Benchmark03_Awake_mD23429C1EE428EEF4ED9A753385BF194BAA41A77 (void);
// 0x000000FF System.Void TMPro.Examples.Benchmark03::Start()
extern void Benchmark03_Start_m19E638DED29CB7F96362F3C346DC47217C4AF516 (void);
// 0x00000100 System.Void TMPro.Examples.Benchmark03::.ctor()
extern void Benchmark03__ctor_m89F9102259BE1694EC418E9023DC563F3999B0BE (void);
// 0x00000101 System.Void TMPro.Examples.Benchmark04::Start()
extern void Benchmark04_Start_mEABD467C12547066E33359FDC433E8B5BAD43DB9 (void);
// 0x00000102 System.Void TMPro.Examples.Benchmark04::.ctor()
extern void Benchmark04__ctor_m5015375E0C8D19060CB5DE14CBF4AC02DDD70E7C (void);
// 0x00000103 System.Void TMPro.Examples.CameraController::Awake()
extern void CameraController_Awake_m420393892377B9703EC97764B34E32890FC5283E (void);
// 0x00000104 System.Void TMPro.Examples.CameraController::Start()
extern void CameraController_Start_m32D90EE7232BE6DE08D224F824F8EB9571655610 (void);
// 0x00000105 System.Void TMPro.Examples.CameraController::LateUpdate()
extern void CameraController_LateUpdate_m6D81DEBA4E8B443CF2AD8288F0E76E3A6B3B5373 (void);
// 0x00000106 System.Void TMPro.Examples.CameraController::GetPlayerInput()
extern void CameraController_GetPlayerInput_m6695FE20CFC691585A6AC279EDB338EC9DD13FE3 (void);
// 0x00000107 System.Void TMPro.Examples.CameraController::.ctor()
extern void CameraController__ctor_m09187FB27B590118043D4DC7B89B93164124C124 (void);
// 0x00000108 System.Void TMPro.Examples.ObjectSpin::Awake()
extern void ObjectSpin_Awake_mC1EB9630B6D3BAE645D3DD79C264F71F7B18A1AA (void);
// 0x00000109 System.Void TMPro.Examples.ObjectSpin::Update()
extern void ObjectSpin_Update_mD39DCBA0789DC0116037C442F0BA1EE6752E36D3 (void);
// 0x0000010A System.Void TMPro.Examples.ObjectSpin::.ctor()
extern void ObjectSpin__ctor_m1827B9648659746252026432DFED907AEC6007FC (void);
// 0x0000010B System.Void TMPro.Examples.ShaderPropAnimator::Awake()
extern void ShaderPropAnimator_Awake_mE04C66A41CA53AB73733E7D2CCD21B30A360C5A8 (void);
// 0x0000010C System.Void TMPro.Examples.ShaderPropAnimator::Start()
extern void ShaderPropAnimator_Start_mC5AC59C59AF2F71E0CF65F11ACD787488140EDD2 (void);
// 0x0000010D System.Collections.IEnumerator TMPro.Examples.ShaderPropAnimator::AnimateProperties()
extern void ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469 (void);
// 0x0000010E System.Void TMPro.Examples.ShaderPropAnimator::.ctor()
extern void ShaderPropAnimator__ctor_mAA626BC8AEEB00C5AE362FE8690D3F2200CE6E64 (void);
// 0x0000010F System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::.ctor(System.Int32)
extern void U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A (void);
// 0x00000110 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.IDisposable.Dispose()
extern void U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B (void);
// 0x00000111 System.Boolean TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::MoveNext()
extern void U3CAnimatePropertiesU3Ed__6_MoveNext_m8A58CCFDAE59F55AB1BB2C103800886E62C807CF (void);
// 0x00000112 System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390 (void);
// 0x00000113 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B (void);
// 0x00000114 System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362 (void);
// 0x00000115 System.Void TMPro.Examples.SimpleScript::Start()
extern void SimpleScript_Start_m75CD9CEDCAFA9A991753478D7C28407C7329FB8F (void);
// 0x00000116 System.Void TMPro.Examples.SimpleScript::Update()
extern void SimpleScript_Update_mE8C3930DCC1767C2DF59B622FABD188E6FB91BE5 (void);
// 0x00000117 System.Void TMPro.Examples.SimpleScript::.ctor()
extern void SimpleScript__ctor_mEB370A69544227EF04C48C604A28502ADAA73697 (void);
// 0x00000118 System.Void TMPro.Examples.SkewTextExample::Awake()
extern void SkewTextExample_Awake_m6FBA7E7DC9AFDD3099F0D0B9CDE91574551105DB (void);
// 0x00000119 System.Void TMPro.Examples.SkewTextExample::Start()
extern void SkewTextExample_Start_m536F82F3A5D229332694688C59F396E07F88153F (void);
// 0x0000011A UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void SkewTextExample_CopyAnimationCurve_m555177255F5828DBC7E677ED06F7EFFC052886B3 (void);
// 0x0000011B System.Collections.IEnumerator TMPro.Examples.SkewTextExample::WarpText()
extern void SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3 (void);
// 0x0000011C System.Void TMPro.Examples.SkewTextExample::.ctor()
extern void SkewTextExample__ctor_m945059906734DD38CF860CD32B3E07635D0E1F86 (void);
// 0x0000011D System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F (void);
// 0x0000011E System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF (void);
// 0x0000011F System.Boolean TMPro.Examples.SkewTextExample/<WarpText>d__7::MoveNext()
extern void U3CWarpTextU3Ed__7_MoveNext_mB832E4A3DFDDFECC460A510BBC664F218B3D7FCF (void);
// 0x00000120 System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1 (void);
// 0x00000121 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F (void);
// 0x00000122 System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A (void);
// 0x00000123 System.Void TMPro.Examples.TMP_ExampleScript_01::Awake()
extern void TMP_ExampleScript_01_Awake_m381EF5B50E1D012B8CA1883DEFF604EEAE6D95F4 (void);
// 0x00000124 System.Void TMPro.Examples.TMP_ExampleScript_01::Update()
extern void TMP_ExampleScript_01_Update_m31611F22A608784F164A24444195B33B714567FB (void);
// 0x00000125 System.Void TMPro.Examples.TMP_ExampleScript_01::.ctor()
extern void TMP_ExampleScript_01__ctor_m3641F2E0D25B6666CE77773A4A493C4C4D3D3A12 (void);
// 0x00000126 System.Void TMPro.Examples.TMP_FrameRateCounter::Awake()
extern void TMP_FrameRateCounter_Awake_m958B668086DB4A38D9C56E3F8C2DCCB6FF11FAA3 (void);
// 0x00000127 System.Void TMPro.Examples.TMP_FrameRateCounter::Start()
extern void TMP_FrameRateCounter_Start_mC642CA714D0FB8482D2AC6192D976DA179EE3720 (void);
// 0x00000128 System.Void TMPro.Examples.TMP_FrameRateCounter::Update()
extern void TMP_FrameRateCounter_Update_m5E41B55573D86C3D345BFE11C489B00229BCEE21 (void);
// 0x00000129 System.Void TMPro.Examples.TMP_FrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_FrameRateCounter_Set_FrameCounter_Position_mA72ECDE464E3290E4F533491FC8113B8D4068BE1 (void);
// 0x0000012A System.Void TMPro.Examples.TMP_FrameRateCounter::.ctor()
extern void TMP_FrameRateCounter__ctor_mC79D5BF3FAE2BB7D22D9955A75E3483725BA619C (void);
// 0x0000012B System.Void TMPro.Examples.TMP_TextEventCheck::OnEnable()
extern void TMP_TextEventCheck_OnEnable_mE6D5125EEE0720E6F414978A496066E7CF1592DF (void);
// 0x0000012C System.Void TMPro.Examples.TMP_TextEventCheck::OnDisable()
extern void TMP_TextEventCheck_OnDisable_m94F087C259890C48D4F5464ABA4CD1D0E5863A9A (void);
// 0x0000012D System.Void TMPro.Examples.TMP_TextEventCheck::OnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnCharacterSelection_mDA044F0808D61A99CDA374075943CEB1C92C253D (void);
// 0x0000012E System.Void TMPro.Examples.TMP_TextEventCheck::OnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnSpriteSelection_mE8FFA550F38F5447CA37205698A30A41ADE901E0 (void);
// 0x0000012F System.Void TMPro.Examples.TMP_TextEventCheck::OnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnWordSelection_m6037166D18A938678A2B06F28A5DCB3E09FAC61B (void);
// 0x00000130 System.Void TMPro.Examples.TMP_TextEventCheck::OnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnLineSelection_mD2BAA6C8ABD61F0442A40C5448FA7774D782C363 (void);
// 0x00000131 System.Void TMPro.Examples.TMP_TextEventCheck::OnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventCheck_OnLinkSelection_m184B11D5E35AC4EA7CDCE3340AB9FEE3C14BF41C (void);
// 0x00000132 System.Void TMPro.Examples.TMP_TextEventCheck::.ctor()
extern void TMP_TextEventCheck__ctor_mA5E82EE7CE8F8836FC6CF3823CBC7356005B898B (void);
// 0x00000133 System.Void TMPro.Examples.TMP_TextInfoDebugTool::.ctor()
extern void TMP_TextInfoDebugTool__ctor_mF6AA30660FBD4CE708B6147833853498993CB9EE (void);
// 0x00000134 System.Void TMPro.Examples.TMP_TextSelector_A::Awake()
extern void TMP_TextSelector_A_Awake_mD7252A5075E30E3BF9BEF4353F7AA2A9203FC943 (void);
// 0x00000135 System.Void TMPro.Examples.TMP_TextSelector_A::LateUpdate()
extern void TMP_TextSelector_A_LateUpdate_mDBBC09726332EDDEAF7C30AB6C08FB33261F79FD (void);
// 0x00000136 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerEnter_mC19B85FB5B4E6EB47832C39F0115A513B85060D0 (void);
// 0x00000137 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerExit_mE51C850F54B18A5C96E3BE015DFBB0F079E5AE66 (void);
// 0x00000138 System.Void TMPro.Examples.TMP_TextSelector_A::.ctor()
extern void TMP_TextSelector_A__ctor_mEB83D3952B32CEE9A871EC60E3AE9B79048302BF (void);
// 0x00000139 System.Void TMPro.Examples.TMP_TextSelector_B::Awake()
extern void TMP_TextSelector_B_Awake_m7E413A43C54DF9E0FE70C4E69FC682391B47205A (void);
// 0x0000013A System.Void TMPro.Examples.TMP_TextSelector_B::OnEnable()
extern void TMP_TextSelector_B_OnEnable_m0828D13E2D407B90038442228D54FB0D7B3D29FB (void);
// 0x0000013B System.Void TMPro.Examples.TMP_TextSelector_B::OnDisable()
extern void TMP_TextSelector_B_OnDisable_m2B559A85B52C8CAFC7350CC7B4F8E5BC773EF781 (void);
// 0x0000013C System.Void TMPro.Examples.TMP_TextSelector_B::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TMP_TextSelector_B_ON_TEXT_CHANGED_m1597DBE7C7EBE7CFA4DC395897A4779387B59910 (void);
// 0x0000013D System.Void TMPro.Examples.TMP_TextSelector_B::LateUpdate()
extern void TMP_TextSelector_B_LateUpdate_m8BA10E368C7F3483E9EC05589BBE45D7EFC75691 (void);
// 0x0000013E System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerEnter_m0A0064632E4C0E0ADCD4358AD5BC168BFB74AC4D (void);
// 0x0000013F System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerExit_m667659102B5B17FBAF56593B7034E5EC1C48D43F (void);
// 0x00000140 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerClick_m8DB2D22EE2F4D3965115791C81F985D82021469F (void);
// 0x00000141 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerUp_m303500BE309B56F3ADCE8B461CEC50C8D5ED70BC (void);
// 0x00000142 System.Void TMPro.Examples.TMP_TextSelector_B::RestoreCachedVertexAttributes(System.Int32)
extern void TMP_TextSelector_B_RestoreCachedVertexAttributes_m3D637CF2C6CA663922EBE56FFD672BE582E9F1F2 (void);
// 0x00000143 System.Void TMPro.Examples.TMP_TextSelector_B::.ctor()
extern void TMP_TextSelector_B__ctor_mE654F9F1570570C4BACDA79640B8DB3033D91C33 (void);
// 0x00000144 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Awake()
extern void TMP_UiFrameRateCounter_Awake_m83DBE22B6CC551BE5E385048B92067679716179E (void);
// 0x00000145 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Start()
extern void TMP_UiFrameRateCounter_Start_m59DA342A492C9EF8AD5C4512753211BF3796C944 (void);
// 0x00000146 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Update()
extern void TMP_UiFrameRateCounter_Update_m28FC233C475AA15A3BE399BF9345977089997749 (void);
// 0x00000147 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_UiFrameRateCounter_Set_FrameCounter_Position_m2025933902F312C0EC4B6A864196A8BA8D545647 (void);
// 0x00000148 System.Void TMPro.Examples.TMP_UiFrameRateCounter::.ctor()
extern void TMP_UiFrameRateCounter__ctor_m5774BF4B9389770FC34991095557B24417600F84 (void);
// 0x00000149 System.Void TMPro.Examples.TMPro_InstructionOverlay::Awake()
extern void TMPro_InstructionOverlay_Awake_m2444EA8749D72BCEA4DC30A328E3745AB19062EC (void);
// 0x0000014A System.Void TMPro.Examples.TMPro_InstructionOverlay::Set_FrameCounter_Position(TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions)
extern void TMPro_InstructionOverlay_Set_FrameCounter_Position_m62B897E4DB2D6B1936833EC54D9C246D68D50EEB (void);
// 0x0000014B System.Void TMPro.Examples.TMPro_InstructionOverlay::.ctor()
extern void TMPro_InstructionOverlay__ctor_m7AB5B851B4BFB07547E460D6B7B9D969DEB4A7CF (void);
// 0x0000014C System.Void TMPro.Examples.TeleType::Awake()
extern void TeleType_Awake_m099FCB202F2A8299B133DC56ECB9D01A16C08DFE (void);
// 0x0000014D System.Collections.IEnumerator TMPro.Examples.TeleType::Start()
extern void TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7 (void);
// 0x0000014E System.Void TMPro.Examples.TeleType::.ctor()
extern void TeleType__ctor_m12A79B34F66CBFDCA8F582F0689D1731586B90AF (void);
// 0x0000014F System.Void TMPro.Examples.TeleType/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629 (void);
// 0x00000150 System.Void TMPro.Examples.TeleType/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9 (void);
// 0x00000151 System.Boolean TMPro.Examples.TeleType/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mEF7A3215376BDFB52C3DA9D26FF0459074E89715 (void);
// 0x00000152 System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B (void);
// 0x00000153 System.Void TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8 (void);
// 0x00000154 System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261 (void);
// 0x00000155 System.Void TMPro.Examples.TextConsoleSimulator::Awake()
extern void TextConsoleSimulator_Awake_mA51EE182A528CCA5CAEA8DBE8AD30E43FDFAE7B0 (void);
// 0x00000156 System.Void TMPro.Examples.TextConsoleSimulator::Start()
extern void TextConsoleSimulator_Start_m429701BE4C9A52AA6B257172A4D85D58E091E7DA (void);
// 0x00000157 System.Void TMPro.Examples.TextConsoleSimulator::OnEnable()
extern void TextConsoleSimulator_OnEnable_m3397FBCDA8D9DA264D2149288BE4DCF63368AB50 (void);
// 0x00000158 System.Void TMPro.Examples.TextConsoleSimulator::OnDisable()
extern void TextConsoleSimulator_OnDisable_m43DF869E864789E215873192ECFCDC51ABA79712 (void);
// 0x00000159 System.Void TMPro.Examples.TextConsoleSimulator::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TextConsoleSimulator_ON_TEXT_CHANGED_m51F06EE5DD9B32FEFB529743F209C6E6C41BE3B8 (void);
// 0x0000015A System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealCharacters(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534 (void);
// 0x0000015B System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealWords(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4 (void);
// 0x0000015C System.Void TMPro.Examples.TextConsoleSimulator::.ctor()
extern void TextConsoleSimulator__ctor_m4719FB9D4D89F37234757D93876DF0193E8E2848 (void);
// 0x0000015D System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::.ctor(System.Int32)
extern void U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D (void);
// 0x0000015E System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.IDisposable.Dispose()
extern void U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C (void);
// 0x0000015F System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::MoveNext()
extern void U3CRevealCharactersU3Ed__7_MoveNext_mA9A6555E50889A7AA73F20749F25989165023DE3 (void);
// 0x00000160 System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83 (void);
// 0x00000161 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691 (void);
// 0x00000162 System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F (void);
// 0x00000163 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::.ctor(System.Int32)
extern void U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0 (void);
// 0x00000164 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.IDisposable.Dispose()
extern void U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8 (void);
// 0x00000165 System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::MoveNext()
extern void U3CRevealWordsU3Ed__8_MoveNext_m3811753E2384D4CBBAD6BD712EBA4FAF00D73210 (void);
// 0x00000166 System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB (void);
// 0x00000167 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.Reset()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F (void);
// 0x00000168 System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27 (void);
// 0x00000169 System.Void TMPro.Examples.TextMeshProFloatingText::Awake()
extern void TextMeshProFloatingText_Awake_m679E597FF18E192C1FBD263D0E5ECEA392412046 (void);
// 0x0000016A System.Void TMPro.Examples.TextMeshProFloatingText::Start()
extern void TextMeshProFloatingText_Start_m68E511DEEDA883FE0F0999B329451F3A8A7269B1 (void);
// 0x0000016B System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshProFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7 (void);
// 0x0000016C System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF (void);
// 0x0000016D System.Void TMPro.Examples.TextMeshProFloatingText::.ctor()
extern void TextMeshProFloatingText__ctor_m968E2691E21A93C010CF8205BC3666ADF712457E (void);
// 0x0000016E System.Void TMPro.Examples.TextMeshProFloatingText::.cctor()
extern void TextMeshProFloatingText__cctor_m5FF3949FBDB0FD13908FB7349F64DDB3012512B8 (void);
// 0x0000016F System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::.ctor(System.Int32)
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_m3F7F9561735FF8403090A93E708E781B3997B81B (void);
// 0x00000170 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m6F24E1DC5777D4682375D8742BC9272EE6D4EA2C (void);
// 0x00000171 System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::MoveNext()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m04B1BB9074CBA76BA7F63849721F4CAEA1045A28 (void);
// 0x00000172 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14A7C9DEBD0FF0C7A6979AFEC78B0BEE9E4C3963 (void);
// 0x00000173 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mC0D5548A92F57550F60EE76E17B58125B57B94A8 (void);
// 0x00000174 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mD3CBFD6F8558EF09F2B4A3D8D1B76BD7FDB3A98B (void);
// 0x00000175 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::.ctor(System.Int32)
extern void U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m0466586E44A01852547C6CECCA91C14673B98C20 (void);
// 0x00000176 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m8730A3A2EFBF759A51F0E5330C3BBF8EFC1736A3 (void);
// 0x00000177 System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::MoveNext()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m93E19CF96C4EBC57CC4E03E2D3D0B56B3E92A36A (void);
// 0x00000178 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4076BE73343A473EFE46E73ABA5DE66CC797598 (void);
// 0x00000179 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_mC33B355BB392C5A52DCA0AAF2588E1E78AF6D3A5 (void);
// 0x0000017A System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m28CCE2E51295AD5F368C504BADDDFF008C2CC0A6 (void);
// 0x0000017B System.Void TMPro.Examples.TextMeshSpawner::Awake()
extern void TextMeshSpawner_Awake_mDF8CCC9C6B7380D6FA027263CDC3BC00EF75AEFB (void);
// 0x0000017C System.Void TMPro.Examples.TextMeshSpawner::Start()
extern void TextMeshSpawner_Start_m7CC21883CA786A846A44921D2E37C201C25439BA (void);
// 0x0000017D System.Void TMPro.Examples.TextMeshSpawner::.ctor()
extern void TextMeshSpawner__ctor_m1255777673BE591F62B4FC55EB999DBD7A7CB5A1 (void);
// 0x0000017E System.Void TMPro.Examples.VertexColorCycler::Awake()
extern void VertexColorCycler_Awake_m84B4548078500DA811F3ADFF66186373BE8EDABC (void);
// 0x0000017F System.Void TMPro.Examples.VertexColorCycler::Start()
extern void VertexColorCycler_Start_mC3FF90808EDD6A02F08375E909254778D5268B66 (void);
// 0x00000180 System.Collections.IEnumerator TMPro.Examples.VertexColorCycler::AnimateVertexColors()
extern void VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42 (void);
// 0x00000181 System.Void TMPro.Examples.VertexColorCycler::.ctor()
extern void VertexColorCycler__ctor_m09990A8066C8A957A96CDBEBDA980399283B45E9 (void);
// 0x00000182 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F (void);
// 0x00000183 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007 (void);
// 0x00000184 System.Boolean TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__3_MoveNext_m2842AF5B12AFC17112D1AE75E46AB1B12776D2A6 (void);
// 0x00000185 System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51 (void);
// 0x00000186 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932 (void);
// 0x00000187 System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52 (void);
// 0x00000188 System.Void TMPro.Examples.VertexJitter::Awake()
extern void VertexJitter_Awake_m9C5586A35BD9C928455D6479C93C1CE095447D8F (void);
// 0x00000189 System.Void TMPro.Examples.VertexJitter::OnEnable()
extern void VertexJitter_OnEnable_m97ED60DBD350C72D1436ADFB8009A6F33A78C825 (void);
// 0x0000018A System.Void TMPro.Examples.VertexJitter::OnDisable()
extern void VertexJitter_OnDisable_mF8D32D6E02E41A73C3E958FB6EE5D1D659D2A846 (void);
// 0x0000018B System.Void TMPro.Examples.VertexJitter::Start()
extern void VertexJitter_Start_m8A0FED7ED16F90DBF287E09BFCBD7B26E07DBF97 (void);
// 0x0000018C System.Void TMPro.Examples.VertexJitter::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexJitter_ON_TEXT_CHANGED_mC7AB0113F6823D4FF415A096314B55D9C1BE9549 (void);
// 0x0000018D System.Collections.IEnumerator TMPro.Examples.VertexJitter::AnimateVertexColors()
extern void VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16 (void);
// 0x0000018E System.Void TMPro.Examples.VertexJitter::.ctor()
extern void VertexJitter__ctor_m550C9169D6FCD6F60D6AABCB8B4955DF58A12DCE (void);
// 0x0000018F System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8 (void);
// 0x00000190 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099 (void);
// 0x00000191 System.Boolean TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_m169A75C7E2147372CB933520B670AF77907C1C6B (void);
// 0x00000192 System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479 (void);
// 0x00000193 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D (void);
// 0x00000194 System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF (void);
// 0x00000195 System.Void TMPro.Examples.VertexShakeA::Awake()
extern void VertexShakeA_Awake_m005C6F9EE8A8FD816CD3A736D6AF199CD2B615A2 (void);
// 0x00000196 System.Void TMPro.Examples.VertexShakeA::OnEnable()
extern void VertexShakeA_OnEnable_m9F60F3951A7D0DF4201A0409F0ADC05243D324EA (void);
// 0x00000197 System.Void TMPro.Examples.VertexShakeA::OnDisable()
extern void VertexShakeA_OnDisable_m59B86895C03B278B2E634A7C68CC942344A8D2D2 (void);
// 0x00000198 System.Void TMPro.Examples.VertexShakeA::Start()
extern void VertexShakeA_Start_m3E623C28F873F54F4AC7579433C7C50B2A3319DE (void);
// 0x00000199 System.Void TMPro.Examples.VertexShakeA::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeA_ON_TEXT_CHANGED_m7AD31F3239351E0916E4D02148F46A80DC148D7E (void);
// 0x0000019A System.Collections.IEnumerator TMPro.Examples.VertexShakeA::AnimateVertexColors()
extern void VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599 (void);
// 0x0000019B System.Void TMPro.Examples.VertexShakeA::.ctor()
extern void VertexShakeA__ctor_m7E6FD1700BD08616532AF22B3B489A18B88DFB62 (void);
// 0x0000019C System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2 (void);
// 0x0000019D System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81 (void);
// 0x0000019E System.Boolean TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mCEEDE03667D329386BB4AE7B8252B7A9B54F443F (void);
// 0x0000019F System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE (void);
// 0x000001A0 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA (void);
// 0x000001A1 System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E (void);
// 0x000001A2 System.Void TMPro.Examples.VertexShakeB::Awake()
extern void VertexShakeB_Awake_m99C9A0474DBFE462DC88C898F958C1805376F9D5 (void);
// 0x000001A3 System.Void TMPro.Examples.VertexShakeB::OnEnable()
extern void VertexShakeB_OnEnable_m64299258797D745CDFE7F3CBEC4708BDBC7C3971 (void);
// 0x000001A4 System.Void TMPro.Examples.VertexShakeB::OnDisable()
extern void VertexShakeB_OnDisable_m07D520A8D7BCD8D188CE9F5CC7845F47D5AD6EF4 (void);
// 0x000001A5 System.Void TMPro.Examples.VertexShakeB::Start()
extern void VertexShakeB_Start_m9642281210FA5F701A324B78850331E4638B2DD1 (void);
// 0x000001A6 System.Void TMPro.Examples.VertexShakeB::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeB_ON_TEXT_CHANGED_m5650D69D258D0EEF35AF390D6D5086B327B308A5 (void);
// 0x000001A7 System.Collections.IEnumerator TMPro.Examples.VertexShakeB::AnimateVertexColors()
extern void VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87 (void);
// 0x000001A8 System.Void TMPro.Examples.VertexShakeB::.ctor()
extern void VertexShakeB__ctor_m605A778C36B506B5763A1AE17B01F8DCCBCD51EC (void);
// 0x000001A9 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760 (void);
// 0x000001AA System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC (void);
// 0x000001AB System.Boolean TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m0534E436514E663BF024364E524EE5716FF15C8E (void);
// 0x000001AC System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D (void);
// 0x000001AD System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB (void);
// 0x000001AE System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD (void);
// 0x000001AF System.Void TMPro.Examples.VertexZoom::Awake()
extern void VertexZoom_Awake_m1B5D386B98CF2EB05A8155B238D6F6E8275D181C (void);
// 0x000001B0 System.Void TMPro.Examples.VertexZoom::OnEnable()
extern void VertexZoom_OnEnable_m7F980FC038FC2534C428A5FD33E3E13AEAEB4EEC (void);
// 0x000001B1 System.Void TMPro.Examples.VertexZoom::OnDisable()
extern void VertexZoom_OnDisable_m880C38B62B4BB05AF49F12F75B83FFA2F0519884 (void);
// 0x000001B2 System.Void TMPro.Examples.VertexZoom::Start()
extern void VertexZoom_Start_m10A182DCEF8D430DADAFBFFA3F04171F8E60C84C (void);
// 0x000001B3 System.Void TMPro.Examples.VertexZoom::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexZoom_ON_TEXT_CHANGED_mB696E443C61D2212AC93A7615AA58106DD25250F (void);
// 0x000001B4 System.Collections.IEnumerator TMPro.Examples.VertexZoom::AnimateVertexColors()
extern void VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED (void);
// 0x000001B5 System.Void TMPro.Examples.VertexZoom::.ctor()
extern void VertexZoom__ctor_mE7B36F2D3EC8EF397AB0AD7A19E988C06927A3B2 (void);
// 0x000001B6 System.Void TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m4EC5F8042B5AC645EA7D0913E50FD22144DBD348 (void);
// 0x000001B7 System.Int32 TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::<AnimateVertexColors>b__0(System.Int32,System.Int32)
extern void U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m959A7E1D6162B5AAF28B953AFB04D7943BCAB107 (void);
// 0x000001B8 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2 (void);
// 0x000001B9 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F (void);
// 0x000001BA System.Boolean TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m8340EFEB2B2CF6EA1545CFB6967968CA171FEE66 (void);
// 0x000001BB System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9 (void);
// 0x000001BC System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C (void);
// 0x000001BD System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F (void);
// 0x000001BE System.Void TMPro.Examples.WarpTextExample::Awake()
extern void WarpTextExample_Awake_mEDB072A485F3F37270FC736E1A201624D696B4B0 (void);
// 0x000001BF System.Void TMPro.Examples.WarpTextExample::Start()
extern void WarpTextExample_Start_m36448AEA494658D7C129C3B71BF3A64CC4DFEDC4 (void);
// 0x000001C0 UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void WarpTextExample_CopyAnimationCurve_m744026E638662DA48AC81ED21E0589E3E4E05FAB (void);
// 0x000001C1 System.Collections.IEnumerator TMPro.Examples.WarpTextExample::WarpText()
extern void WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2 (void);
// 0x000001C2 System.Void TMPro.Examples.WarpTextExample::.ctor()
extern void WarpTextExample__ctor_m62299DFDB704027267321007E16DB87D93D0F099 (void);
// 0x000001C3 System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49 (void);
// 0x000001C4 System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60 (void);
// 0x000001C5 System.Boolean TMPro.Examples.WarpTextExample/<WarpText>d__8::MoveNext()
extern void U3CWarpTextU3Ed__8_MoveNext_m9C83C8455FF8ADFBAA8D05958C3048612A96EB84 (void);
// 0x000001C6 System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1 (void);
// 0x000001C7 System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535 (void);
// 0x000001C8 System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25 (void);
// 0x000001C9 System.Void Sound.Soundmanager::Update()
extern void Soundmanager_Update_m74DF9C012C81AA801A44F4FDC8AC74DE9EC297AC (void);
// 0x000001CA System.Void Sound.Soundmanager::Frige()
extern void Soundmanager_Frige_m96C7E9177EAD0F9F183D6A409EA8D2B9D9BEF817 (void);
// 0x000001CB System.Void Sound.Soundmanager::BED()
extern void Soundmanager_BED_mF1315719DC7550F3F765551DE7E1718D31ED34F1 (void);
// 0x000001CC System.Void Sound.Soundmanager::Radio()
extern void Soundmanager_Radio_m189672A2157ACE9F2F835040E6708EF4339AD68E (void);
// 0x000001CD System.Void Sound.Soundmanager::Craft()
extern void Soundmanager_Craft_m5DFB6F8FB5415A24BF4240E63BA36A0D87F9087C (void);
// 0x000001CE System.Void Sound.Soundmanager::LV_UP()
extern void Soundmanager_LV_UP_m5721FA3F9529787E6706A8AAB92BD85E50B657A2 (void);
// 0x000001CF System.Void Sound.Soundmanager::.ctor()
extern void Soundmanager__ctor_m5551364FC678ADE0EA31E2BB68C8F60F29F1A2C9 (void);
// 0x000001D0 System.Void Sound.Sound::Update()
extern void Sound_Update_mE12B8CD3A20B004DC36BC45E9BC9C25E645E6D61 (void);
// 0x000001D1 System.Void Sound.Sound::.ctor()
extern void Sound__ctor_mC4DC1A033BFAF868C25B9BE0C5EF88746FC409E5 (void);
// 0x000001D2 System.Void Manager_Scripts.BuildingManager::.ctor()
extern void BuildingManager__ctor_m7E966CE862DD79F8D88433097A7037EE516521C0 (void);
// 0x000001D3 T Manager_Scripts.MonoSingleton`1::get_Instance()
// 0x000001D4 System.Void Manager_Scripts.MonoSingleton`1::Awake()
// 0x000001D5 System.Void Manager_Scripts.MonoSingleton`1::OnApplicationQuit()
// 0x000001D6 System.Void Manager_Scripts.MonoSingleton`1::OnDestroy()
// 0x000001D7 System.Void Manager_Scripts.MonoSingleton`1::.ctor()
// 0x000001D8 System.Void Manager_Scripts.MonoSingleton`1::.cctor()
// 0x000001D9 System.Void Manager_Scripts.PlatfromManager::Start()
extern void PlatfromManager_Start_m8FE9C105B2EA16FE51891CA6B5B078E93281BF08 (void);
// 0x000001DA System.Void Manager_Scripts.PlatfromManager::IsRotate180()
extern void PlatfromManager_IsRotate180_mF43E0B55F21A7B5CB11D28FAF6287271029E70EF (void);
// 0x000001DB System.Void Manager_Scripts.PlatfromManager::IsRotate0()
extern void PlatfromManager_IsRotate0_m20F1688A43BB64494A487B84BB80AFBEBAC36536 (void);
// 0x000001DC System.Void Manager_Scripts.PlatfromManager::.ctor()
extern void PlatfromManager__ctor_mD28BEC470527210C7F9F68690AABF0CA70B7A9E1 (void);
// 0x000001DD System.Void Manager_Scripts.SpawnManager::SetSo(System.Int32)
extern void SpawnManager_SetSo_m8EC7ED1F7FD378F297290C75BC2A30FACF6D5E1B (void);
// 0x000001DE System.Void Manager_Scripts.SpawnManager::SetCAndF()
extern void SpawnManager_SetCAndF_m788401AA369A7B68BEA6BF12417DA251370FDD05 (void);
// 0x000001DF System.Void Manager_Scripts.SpawnManager::Start()
extern void SpawnManager_Start_m2914D0E400BF8251790B98C978F1892EC16D5D5F (void);
// 0x000001E0 System.Void Manager_Scripts.SpawnManager::SetDo(System.Boolean)
extern void SpawnManager_SetDo_mD3290533A25967749AEE5E58E8F3B7AA677BB3FD (void);
// 0x000001E1 System.Void Manager_Scripts.SpawnManager::Spawn()
extern void SpawnManager_Spawn_m17DF3984B2CB551FBF02408BDB46185BBB63867C (void);
// 0x000001E2 System.Void Manager_Scripts.SpawnManager::SetSpawn(System.Int32[],System.Int32[])
extern void SpawnManager_SetSpawn_mAAEBA95EABD7CC29B5BE361B29F8A213467F3987 (void);
// 0x000001E3 System.Void Manager_Scripts.SpawnManager::Spawner(System.Int32)
extern void SpawnManager_Spawner_m7B820BDCD1BB3955B04D1F27E5AFB6CFAB502812 (void);
// 0x000001E4 System.Void Manager_Scripts.SpawnManager::Update()
extern void SpawnManager_Update_mA35DCE24CE5FCEFEEF0910123C5664BABD2240F9 (void);
// 0x000001E5 System.Void Manager_Scripts.SpawnManager::.ctor()
extern void SpawnManager__ctor_mE5AB370502C4322E2E8AC79E6052CB98068D6A49 (void);
// 0x000001E6 System.Void Manager_Scripts.UIManager::Start()
extern void UIManager_Start_m30D1D9B7AE6D78B5422B9F91B698F76AF09A743B (void);
// 0x000001E7 System.Void Manager_Scripts.UIManager::Update()
extern void UIManager_Update_mD58EC6793FABAFC42ECABA3EAE82FE1836D6EF5B (void);
// 0x000001E8 System.Void Manager_Scripts.UIManager::ClickOther()
extern void UIManager_ClickOther_mF6E3D08C01D79CFCF9B1E9895D9839E08731555B (void);
// 0x000001E9 System.Void Manager_Scripts.UIManager::OnClick()
extern void UIManager_OnClick_m8FC4AD0B85F91D0458887FEA359F86DED6492581 (void);
// 0x000001EA System.Void Manager_Scripts.UIManager::OnClickUIPlay()
extern void UIManager_OnClickUIPlay_m96BACFBAA2726D19573D833B882001AC877D9B7F (void);
// 0x000001EB System.Void Manager_Scripts.UIManager::Pause()
extern void UIManager_Pause_m2D383414219C4487F2791FC5ABC3BEBC5DBEB082 (void);
// 0x000001EC System.Void Manager_Scripts.UIManager::Continue()
extern void UIManager_Continue_m69402401EDC6567B9BEFCB9901CB32D949BB00FC (void);
// 0x000001ED System.Void Manager_Scripts.UIManager::ClickPlay()
extern void UIManager_ClickPlay_m9ACBFAD72FA41316EFED8DB4C034D55ADEB049C5 (void);
// 0x000001EE System.Void Manager_Scripts.UIManager::ClickQuit()
extern void UIManager_ClickQuit_mF285C011A906DE445B31B991031C5C176FF67198 (void);
// 0x000001EF System.Void Manager_Scripts.UIManager::GameWin()
extern void UIManager_GameWin_mF2AB94D893A28F3983AB149BB9EBBE939065ADBE (void);
// 0x000001F0 System.Void Manager_Scripts.UIManager::GameLose()
extern void UIManager_GameLose_m5288C91533E2354065396B2985E4F32AD86CAC40 (void);
// 0x000001F1 System.Void Manager_Scripts.UIManager::GameQuit()
extern void UIManager_GameQuit_mF590928DD6B3D2D344C2349BD8789CAADDE18D5E (void);
// 0x000001F2 System.Void Manager_Scripts.UIManager::CheckAndShowText()
extern void UIManager_CheckAndShowText_mA26C841C7A8B02AD3F0E2FCD860B5BCBD74610FF (void);
// 0x000001F3 System.Void Manager_Scripts.UIManager::CheckBigWaveDay()
extern void UIManager_CheckBigWaveDay_m429C6672B6233081A96D796A4A60EC26757BC65C (void);
// 0x000001F4 System.Void Manager_Scripts.UIManager::.ctor()
extern void UIManager__ctor_m8ECCADCA9247F60A1D29333FBADC9494CCCB9312 (void);
// 0x000001F5 System.Void Manager_Scripts.UIManager::.cctor()
extern void UIManager__cctor_m14BF6A4B635F3569EFA4C551189459DA4F19A393 (void);
// 0x000001F6 System.Void Manager_Scripts.Player.CharacterMove::Start()
extern void CharacterMove_Start_m257966B887DD11B33BDF48671CC666080BAB1FD8 (void);
// 0x000001F7 System.Void Manager_Scripts.Player.CharacterMove::Update()
extern void CharacterMove_Update_m8B3187A4F87D10523AF6B106A039B9A24E291BC3 (void);
// 0x000001F8 System.Void Manager_Scripts.Player.CharacterMove::FixedUpdate()
extern void CharacterMove_FixedUpdate_m2D6D6D80D202FF6167F0F277929735CEC79EFED3 (void);
// 0x000001F9 System.Void Manager_Scripts.Player.CharacterMove::Selection()
extern void CharacterMove_Selection_m4B218325D01D3B356362FEAE7B76221B2FF3DE0E (void);
// 0x000001FA System.Void Manager_Scripts.Player.CharacterMove::Move()
extern void CharacterMove_Move_m4CE32DFD9EF7D2FAE06446121B01E1EF42C012AE (void);
// 0x000001FB System.Void Manager_Scripts.Player.CharacterMove::SetPosition()
extern void CharacterMove_SetPosition_m61F6019D17C2B5529AF270243F3B682757892B01 (void);
// 0x000001FC System.Void Manager_Scripts.Player.CharacterMove::Rotation(UnityEngine.Vector3)
extern void CharacterMove_Rotation_mFED0F8E423A048BA0EF6F5F4F5564E6C95C5B340 (void);
// 0x000001FD System.Void Manager_Scripts.Player.CharacterMove::Climbing()
extern void CharacterMove_Climbing_m82C4900C2A36EBC624755883C3AA23DC1E0B1A64 (void);
// 0x000001FE System.Void Manager_Scripts.Player.CharacterMove::OnTriggerStay2D(UnityEngine.Collider2D)
extern void CharacterMove_OnTriggerStay2D_m644439F00D9100C83ED94748620755B3D6C4CCBB (void);
// 0x000001FF System.Void Manager_Scripts.Player.CharacterMove::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void CharacterMove_OnTriggerEnter2D_m4632E9E168E170350B961DA639C83216E3522A9E (void);
// 0x00000200 System.Void Manager_Scripts.Player.CharacterMove::OnTriggerExit2D(UnityEngine.Collider2D)
extern void CharacterMove_OnTriggerExit2D_mF85BA188A925E601FB5EE8A42FE7EEB04C514E06 (void);
// 0x00000201 System.Void Manager_Scripts.Player.CharacterMove::FindLadderRight()
extern void CharacterMove_FindLadderRight_m1EF56287B091B70D96202218663CAF720AFAD812 (void);
// 0x00000202 System.Void Manager_Scripts.Player.CharacterMove::FindLadderLeft()
extern void CharacterMove_FindLadderLeft_mA650CACBFFAE61A93F30EDCEFFC1A97F9ECE4170 (void);
// 0x00000203 System.Void Manager_Scripts.Player.CharacterMove::CheckToLadder()
extern void CharacterMove_CheckToLadder_m00725F34E489F87D20EC59EE7FD8C329D90CF523 (void);
// 0x00000204 System.Void Manager_Scripts.Player.CharacterMove::MoveToLadder()
extern void CharacterMove_MoveToLadder_m817AE798764995AB84F82547B04A1C7531367145 (void);
// 0x00000205 System.Void Manager_Scripts.Player.CharacterMove::.ctor()
extern void CharacterMove__ctor_mCE2847ADCE505966C47F1AC3F79577F74EC21592 (void);
// 0x00000206 System.Void Manager_Scripts.Player.Direction::OnInstant(UnityEngine.Vector3)
extern void Direction_OnInstant_m6CF5592DFC35586EC1B1F1542465FE11BD6135EF (void);
// 0x00000207 System.Void Manager_Scripts.Player.Direction::Update()
extern void Direction_Update_m6B21A4EE3452A662C24CFD59AAAA80F32B8C5230 (void);
// 0x00000208 System.Void Manager_Scripts.Player.Direction::DestroyDirecUI()
extern void Direction_DestroyDirecUI_mDBABA1FEFB3E2925EBD49F38416D0D1DF38317FD (void);
// 0x00000209 System.Void Manager_Scripts.Player.Direction::.ctor()
extern void Direction__ctor_m43FB67A7571F853129C6513828383B7486DF7B2F (void);
// 0x0000020A System.Void Manager_Scripts.Player.FindMaterial::Update()
extern void FindMaterial_Update_mDAF5067442621C41B7545C79C5846D6CFD3FC801 (void);
// 0x0000020B System.Void Manager_Scripts.Player.FindMaterial::Find()
extern void FindMaterial_Find_mFB51ECD5976932B95B1959794082078FF499F2DD (void);
// 0x0000020C System.Void Manager_Scripts.Player.FindMaterial::.ctor()
extern void FindMaterial__ctor_mB886E1806B1D378E6DCCDCD96D2BBCD9B9156AE1 (void);
// 0x0000020D System.Void Manager_Scripts.Player.FireManager::Update()
extern void FireManager_Update_m2D43DAA0463D61E8B1286BF788BE6BB204D1570F (void);
// 0x0000020E System.Void Manager_Scripts.Player.FireManager::Shoot1Enemy()
extern void FireManager_Shoot1Enemy_mADC43B16FB7289CD4E4AB64D9302E18D788A813C (void);
// 0x0000020F System.Void Manager_Scripts.Player.FireManager::ShootThru()
extern void FireManager_ShootThru_m1298F8F648155BCDDC4229E468E5D951D825D48C (void);
// 0x00000210 System.Void Manager_Scripts.Player.FireManager::.ctor()
extern void FireManager__ctor_m03A6E01A8F00323994D01B0F8B5979089A0CF141 (void);
// 0x00000211 System.Void Manager_Scripts.Player.PlayerManager::Start()
extern void PlayerManager_Start_mEAE82B2C4E0B77F8ECEE948BCAD04F6D77E3FDB1 (void);
// 0x00000212 System.Void Manager_Scripts.Player.PlayerManager::Update()
extern void PlayerManager_Update_mADC80049A80052CD88405D1B7AF948060D04795F (void);
// 0x00000213 System.Void Manager_Scripts.Player.PlayerManager::SetSlider()
extern void PlayerManager_SetSlider_mB61C81B0ABD3C0D6FF8C745F97B9DAC844AB783E (void);
// 0x00000214 System.Void Manager_Scripts.Player.PlayerManager::HungryReduce(System.Single)
extern void PlayerManager_HungryReduce_m120A192980D51E77F3B69769048BDB0F4747900B (void);
// 0x00000215 System.Void Manager_Scripts.Player.PlayerManager::HungryPlus(System.Single)
extern void PlayerManager_HungryPlus_m0B962F1072BA7AC0522BE88BB86C2478CC77D7B5 (void);
// 0x00000216 System.Void Manager_Scripts.Player.PlayerManager::HpPlus(System.Int32)
extern void PlayerManager_HpPlus_m19F7F266EF1245E1E3FBFC6B4FD491CD9D8B5C0E (void);
// 0x00000217 System.Void Manager_Scripts.Player.PlayerManager::Damage(System.Int32)
extern void PlayerManager_Damage_mD1B821A822502051EFBEC098179AF8DD34109D89 (void);
// 0x00000218 System.Void Manager_Scripts.Player.PlayerManager::SetPlayerSo()
extern void PlayerManager_SetPlayerSo_mA923DBC7D40253550FBF92F1BA125EA99EEACFBD (void);
// 0x00000219 System.Boolean Manager_Scripts.Player.PlayerManager::IsDoingWork()
extern void PlayerManager_IsDoingWork_m9593617A98175F4F06D9A8A1FCB8FCE62204B311 (void);
// 0x0000021A System.Boolean Manager_Scripts.Player.PlayerManager::IsDoingWorkSet(System.Boolean)
extern void PlayerManager_IsDoingWorkSet_m59EED3EBDB1CB6CE814644A675C5A0801211D8F1 (void);
// 0x0000021B System.Boolean Manager_Scripts.Player.PlayerManager::IsGenerate()
extern void PlayerManager_IsGenerate_m6A68B960D78F1D6E43D8E9B482FACB9A52B35ACE (void);
// 0x0000021C System.Boolean Manager_Scripts.Player.PlayerManager::IsGenerateSet(System.Boolean)
extern void PlayerManager_IsGenerateSet_m13AD1480C2DD02B3AA73AE4E02A2FA3CAEAE45A4 (void);
// 0x0000021D System.Void Manager_Scripts.Player.PlayerManager::SetPlayerLv(System.Int32)
extern void PlayerManager_SetPlayerLv_mA70DF5F7E92687CBFAD0719326AC90D36A377541 (void);
// 0x0000021E System.Int32 Manager_Scripts.Player.PlayerManager::PlayerLv()
extern void PlayerManager_PlayerLv_m257275C2189D591A977B140947C4DC7158EDCC47 (void);
// 0x0000021F System.Void Manager_Scripts.Player.PlayerManager::Shot()
extern void PlayerManager_Shot_m8745F07EC76BAC781C5AB1D1475FA49C2735388D (void);
// 0x00000220 System.Void Manager_Scripts.Player.PlayerManager::.ctor()
extern void PlayerManager__ctor_mF0285F069DD6A97E5F8A83E9640C66B7B547F5C8 (void);
// 0x00000221 System.Void Manager_Scripts.OtherManager.BaseDamage::Damage(System.Int32)
extern void BaseDamage_Damage_mCE308A5737937691F2E37FAB572C867943E4F955 (void);
// 0x00000222 System.Void Manager_Scripts.OtherManager.BaseDamage::.ctor()
extern void BaseDamage__ctor_m1CB85B501E977C53BFFE2BAC9CFB5FA6490A0A0D (void);
// 0x00000223 System.Void Manager_Scripts.OtherManager.BaseManager::Start()
extern void BaseManager_Start_m405C00D0986B0FC9FDB6BB7B9C9FE8411053D05D (void);
// 0x00000224 System.Void Manager_Scripts.OtherManager.BaseManager::Update()
extern void BaseManager_Update_m6CE8D6CE1B813015EBA57179B3DB452134B8D741 (void);
// 0x00000225 System.Void Manager_Scripts.OtherManager.BaseManager::CheckPlayer()
extern void BaseManager_CheckPlayer_mEF5A4B2D8B3C3BB496A345DE687157ABDEA5E268 (void);
// 0x00000226 System.Void Manager_Scripts.OtherManager.BaseManager::Damage(System.Int32)
extern void BaseManager_Damage_mFE5B61A207017F62528FE738F379F95646B1EA7D (void);
// 0x00000227 System.Void Manager_Scripts.OtherManager.BaseManager::ClickFix()
extern void BaseManager_ClickFix_m04AB72EA57E4C8080D2B169E98A58D0634D39EC0 (void);
// 0x00000228 System.Void Manager_Scripts.OtherManager.BaseManager::ClickCancel()
extern void BaseManager_ClickCancel_m95135BFFEB2494D36FA6B0C30CE7AA0835F96932 (void);
// 0x00000229 System.Void Manager_Scripts.OtherManager.BaseManager::SetText()
extern void BaseManager_SetText_mB8EA1C8CE7DC41611D1A23A601921666644E7F81 (void);
// 0x0000022A System.Void Manager_Scripts.OtherManager.BaseManager::FixBase()
extern void BaseManager_FixBase_m51722C365224C79E334804B0A99929076C42601B (void);
// 0x0000022B System.Void Manager_Scripts.OtherManager.BaseManager::.ctor()
extern void BaseManager__ctor_m3EE267AD02BF980FE1881223D0E8D04CC6FA5C6B (void);
// 0x0000022C System.Void Manager_Scripts.OtherManager.CheckPlayer::Update()
extern void CheckPlayer_Update_mF44444F1811599ECA58A4A8FDE1854CE59189B86 (void);
// 0x0000022D System.Void Manager_Scripts.OtherManager.CheckPlayer::CheckPlayerInScene()
extern void CheckPlayer_CheckPlayerInScene_mF43A9C299A4E174E946021EC0C8843F8F1BCF760 (void);
// 0x0000022E System.Void Manager_Scripts.OtherManager.CheckPlayer::.ctor()
extern void CheckPlayer__ctor_m34EC14FD61F650BB671A7BE314C636937C602179 (void);
// 0x0000022F System.Void Manager_Scripts.OtherManager.FactoryManager::Start()
extern void FactoryManager_Start_m50DBD9106DCC92BDC55908992BCA6722E28D1D2C (void);
// 0x00000230 System.Void Manager_Scripts.OtherManager.FactoryManager::Update()
extern void FactoryManager_Update_mC4B194FE946C209877469FBFA5DA677D23FB5B19 (void);
// 0x00000231 System.Void Manager_Scripts.OtherManager.FactoryManager::SetUIAll(System.Boolean)
extern void FactoryManager_SetUIAll_m679C98D812EE509A4A85C56AE7400A38EC653A7B (void);
// 0x00000232 System.Void Manager_Scripts.OtherManager.FactoryManager::GenerateMaterial(System.String,System.Int32,System.Single)
extern void FactoryManager_GenerateMaterial_m5F2202E6790EB864087C21A1FA3A10BD8D001759 (void);
// 0x00000233 System.Void Manager_Scripts.OtherManager.FactoryManager::SelectFactory(System.Int32)
extern void FactoryManager_SelectFactory_mB444C11EB33B91D2564BA5102582A20F975CFBAA (void);
// 0x00000234 System.Void Manager_Scripts.OtherManager.FactoryManager::SetSo()
extern void FactoryManager_SetSo_m62C2C8D42B30D452E7BE522A866493C6C86B2853 (void);
// 0x00000235 System.Void Manager_Scripts.OtherManager.FactoryManager::SetMaterial()
extern void FactoryManager_SetMaterial_m5208FC67DDDCBBBAB28838943FA312FCB8B105E9 (void);
// 0x00000236 System.Void Manager_Scripts.OtherManager.FactoryManager::ClickGenerate()
extern void FactoryManager_ClickGenerate_mB810E824C10B7ECCF0E2759AE5F287EC04296212 (void);
// 0x00000237 System.Void Manager_Scripts.OtherManager.FactoryManager::ClickCancelGenerate()
extern void FactoryManager_ClickCancelGenerate_m23DA6EF240B21976DFFB341D546F47CBC7542592 (void);
// 0x00000238 System.Void Manager_Scripts.OtherManager.FactoryManager::ClickSelectPlayerToFactory()
extern void FactoryManager_ClickSelectPlayerToFactory_mEC96C811A4EDCFC74BCB4810A84969AF93463510 (void);
// 0x00000239 System.Void Manager_Scripts.OtherManager.FactoryManager::CheckPlayer()
extern void FactoryManager_CheckPlayer_mA1A8E2F57F1785A48E250C3EE3B5267B7AE73B8F (void);
// 0x0000023A System.Void Manager_Scripts.OtherManager.FactoryManager::OnTriggerExit2D(UnityEngine.Collider2D)
extern void FactoryManager_OnTriggerExit2D_mF0D1C08AA05E4B4A65DFA27ACC59A8D8EA4E9ADA (void);
// 0x0000023B System.Void Manager_Scripts.OtherManager.FactoryManager::.ctor()
extern void FactoryManager__ctor_m9C5FFFF05A299EA7D5FE98C30D177DA6114F8555 (void);
// 0x0000023C System.Void Manager_Scripts.OtherManager.FoodSpotManager::Start()
extern void FoodSpotManager_Start_m485950875F8CF186646AF14E6EC55906D9E3F235 (void);
// 0x0000023D System.Void Manager_Scripts.OtherManager.FoodSpotManager::Update()
extern void FoodSpotManager_Update_m20645A54F4518FCAF727A20FB11AE677D0ACB88B (void);
// 0x0000023E System.Void Manager_Scripts.OtherManager.FoodSpotManager::CheckPlayer()
extern void FoodSpotManager_CheckPlayer_m69DDFBA135C950A93A9A3C993B46D03B64B45819 (void);
// 0x0000023F System.Void Manager_Scripts.OtherManager.FoodSpotManager::ClickFeed()
extern void FoodSpotManager_ClickFeed_mC957B5DBA352AE4A7BC8B35C1A8BE9F7EE737A76 (void);
// 0x00000240 System.Void Manager_Scripts.OtherManager.FoodSpotManager::ClickSelect()
extern void FoodSpotManager_ClickSelect_m3DAF48E2B6F7AE3CB953A3642847B72A2116BF6D (void);
// 0x00000241 System.Void Manager_Scripts.OtherManager.FoodSpotManager::OnTriggerExit2D(UnityEngine.Collider2D)
extern void FoodSpotManager_OnTriggerExit2D_mB833B98D271312EF5169EBE4258752FAFB5A5145 (void);
// 0x00000242 System.Void Manager_Scripts.OtherManager.FoodSpotManager::.ctor()
extern void FoodSpotManager__ctor_mB717E24FA39B3D947C265E03D3ECBB1072BD45D7 (void);
// 0x00000243 System.Void Manager_Scripts.OtherManager.GameManager::Start()
extern void GameManager_Start_m0EF119BD17C6D75A16D3F807D719064FB9F8243F (void);
// 0x00000244 System.Void Manager_Scripts.OtherManager.GameManager::Update()
extern void GameManager_Update_m99B56B63EDAD1B6A16C6AE463A4F96D6FA58C95B (void);
// 0x00000245 System.Void Manager_Scripts.OtherManager.GameManager::SpawnPlayer(UnityEngine.Vector3)
extern void GameManager_SpawnPlayer_m8F255DBF77242C4053255B8082009B55A6E6280C (void);
// 0x00000246 System.Void Manager_Scripts.OtherManager.GameManager::SpawnPlayerStart()
extern void GameManager_SpawnPlayerStart_mDDC1193874A5A6F8AFD289572BCB6F503FFF167F (void);
// 0x00000247 System.Void Manager_Scripts.OtherManager.GameManager::SetNameAndAdd(System.String)
extern void GameManager_SetNameAndAdd_mB8CC00F2FDE1A211FA604D236084EBCADC78BF16 (void);
// 0x00000248 System.Int32 Manager_Scripts.OtherManager.GameManager::ListThePlayer()
extern void GameManager_ListThePlayer_mF715B32CFAE5347785DFC8AE782A7865420CF553 (void);
// 0x00000249 System.Void Manager_Scripts.OtherManager.GameManager::DestroyPlayer(System.String)
extern void GameManager_DestroyPlayer_m952CC84AC6C358ECB93170CDAE18F6D8A12F9116 (void);
// 0x0000024A System.Void Manager_Scripts.OtherManager.GameManager::SpawnBase()
extern void GameManager_SpawnBase_m31B9245A18F46872F9BA5B096E908A2987E04406 (void);
// 0x0000024B System.Void Manager_Scripts.OtherManager.GameManager::GameWin()
extern void GameManager_GameWin_m44BB3C674FFE1B8905EF202D0C241DF015ADF2D3 (void);
// 0x0000024C System.Void Manager_Scripts.OtherManager.GameManager::GameLose()
extern void GameManager_GameLose_m14F7832835F2728EBAE88A4089A5E08C13073208 (void);
// 0x0000024D System.Void Manager_Scripts.OtherManager.GameManager::Score(System.Int32)
extern void GameManager_Score_mFAEC4974B09BD5373A560F9C8A1921643E8183FF (void);
// 0x0000024E System.Void Manager_Scripts.OtherManager.GameManager::SetScore(System.Int32)
extern void GameManager_SetScore_m7850D63048E55226A62B5ADFEF46CEF7DE8B7C34 (void);
// 0x0000024F System.Int32 Manager_Scripts.OtherManager.GameManager::ShowScore()
extern void GameManager_ShowScore_mEFBB4EE3811C279754B54FA4B0AE48F1AD479984 (void);
// 0x00000250 System.Void Manager_Scripts.OtherManager.GameManager::GameExit()
extern void GameManager_GameExit_mDDCA18AA4321ADC5199A0ABB0228C29DF819677E (void);
// 0x00000251 System.Void Manager_Scripts.OtherManager.GameManager::.ctor()
extern void GameManager__ctor_mA39016FC4FFB429D0F123D1582917989C89E2517 (void);
// 0x00000252 System.Void Manager_Scripts.OtherManager.HealStation::Start()
extern void HealStation_Start_m8A939344A1E54F06288FDFC8629D8F77771A5E9B (void);
// 0x00000253 System.Void Manager_Scripts.OtherManager.HealStation::Update()
extern void HealStation_Update_mA140D8D8F0EA8127E1F5322B02B133BE37E33509 (void);
// 0x00000254 System.Void Manager_Scripts.OtherManager.HealStation::CheckPlayer()
extern void HealStation_CheckPlayer_mD547B7885D7BBD5201A74ACB4A6F5AF55F5317A0 (void);
// 0x00000255 System.Void Manager_Scripts.OtherManager.HealStation::OnTriggerExit2D(UnityEngine.Collider2D)
extern void HealStation_OnTriggerExit2D_mE7F9E024E159F624A2145EE98C84BAAED4768506 (void);
// 0x00000256 System.Void Manager_Scripts.OtherManager.HealStation::ClickSelect()
extern void HealStation_ClickSelect_mF07937350E253FD6E7A46EEBB6D54308A15A2C04 (void);
// 0x00000257 System.Void Manager_Scripts.OtherManager.HealStation::ClickStart()
extern void HealStation_ClickStart_mFDFA9CAF205B4744E947205A24886ABEE97CCD82 (void);
// 0x00000258 System.Void Manager_Scripts.OtherManager.HealStation::ClickCancel()
extern void HealStation_ClickCancel_m3967E354EC5CE11713D1F35DDFE2953C89CA5F76 (void);
// 0x00000259 System.Void Manager_Scripts.OtherManager.HealStation::SetUi(System.Boolean)
extern void HealStation_SetUi_m90BA08BF988F6D06B8C5B16DF27FEC4EEF20C614 (void);
// 0x0000025A System.Void Manager_Scripts.OtherManager.HealStation::.ctor()
extern void HealStation__ctor_m7AFDC11DF9D47B97ACD82B2FC2B3FE0D808AEBD0 (void);
// 0x0000025B System.Void Manager_Scripts.OtherManager.InventoryManager::Update()
extern void InventoryManager_Update_m8BC45ABCAB101C5323C104F155C8412301BBF029 (void);
// 0x0000025C System.Void Manager_Scripts.OtherManager.InventoryManager::ChekAndAdd(System.String,System.Int32)
extern void InventoryManager_ChekAndAdd_m66A9147DE4EE1E3BD67734C1B84188E4DCFF696F (void);
// 0x0000025D System.Void Manager_Scripts.OtherManager.InventoryManager::RemoveMaterial(System.String)
extern void InventoryManager_RemoveMaterial_mB90923819917D89E180064016F25EFEC1A54F780 (void);
// 0x0000025E System.Void Manager_Scripts.OtherManager.InventoryManager::CheckAndRemove()
extern void InventoryManager_CheckAndRemove_mF56221EC3148961173D4015C75B2E609CA083D6C (void);
// 0x0000025F System.Void Manager_Scripts.OtherManager.InventoryManager::.ctor()
extern void InventoryManager__ctor_m6A4BFF1ADFEA179EECB250DD71FAAE6E1069A1B8 (void);
// 0x00000260 System.Void Manager_Scripts.OtherManager.LightManager::Start()
extern void LightManager_Start_m99EB45F9427E91EEEE67A9056873CC9E4021DB4D (void);
// 0x00000261 System.Void Manager_Scripts.OtherManager.LightManager::Update()
extern void LightManager_Update_m3A1B813FF2D68D9FCA3F3A44C89A6927E507F448 (void);
// 0x00000262 System.Void Manager_Scripts.OtherManager.LightManager::SetLight()
extern void LightManager_SetLight_m1A497DBB32555195FA6DA50EFCF49C995987E2A4 (void);
// 0x00000263 System.Void Manager_Scripts.OtherManager.LightManager::.ctor()
extern void LightManager__ctor_m886D2DCF596AAF9F3E24AB77F2AF52D811361C3F (void);
// 0x00000264 System.Void Manager_Scripts.OtherManager.LvTutorial::Update()
extern void LvTutorial_Update_mAA51DB51AB924BE49C3804C3ED889F83A85750C3 (void);
// 0x00000265 System.Void Manager_Scripts.OtherManager.LvTutorial::CheckPlayer()
extern void LvTutorial_CheckPlayer_mC2DB70C4CE2FBB4F7DC5DC705F66293B7488B40C (void);
// 0x00000266 System.Void Manager_Scripts.OtherManager.LvTutorial::.ctor()
extern void LvTutorial__ctor_m72E7D375C0D41BB08F42A96D5DFDDB9EAA81AC7C (void);
// 0x00000267 System.Void Manager_Scripts.OtherManager.LvUpManager::Start()
extern void LvUpManager_Start_mDB4E4585B9A5D86C40D4DE0432DD57A6B761C239 (void);
// 0x00000268 System.Void Manager_Scripts.OtherManager.LvUpManager::Update()
extern void LvUpManager_Update_mA776B6905A04C3D355847504A0B8FEABE7DFCBDC (void);
// 0x00000269 System.Void Manager_Scripts.OtherManager.LvUpManager::CheckAndSetPlayerLv()
extern void LvUpManager_CheckAndSetPlayerLv_mB03A40080A983D61C67EB926D71DBC9FB315F70B (void);
// 0x0000026A System.Void Manager_Scripts.OtherManager.LvUpManager::SelectAndCondition(System.Int32)
extern void LvUpManager_SelectAndCondition_m9F8BFF42EC2666AF280DC13F9C7643CE13A1EF05 (void);
// 0x0000026B System.Void Manager_Scripts.OtherManager.LvUpManager::UiSetActive(System.Boolean)
extern void LvUpManager_UiSetActive_m75614EC89D171BAE3D7EB9F2F5FC7A55AED42564 (void);
// 0x0000026C System.Void Manager_Scripts.OtherManager.LvUpManager::Setmaterial1(System.String)
extern void LvUpManager_Setmaterial1_mF7BD1E13044E349263832E658D4CA636A53096A9 (void);
// 0x0000026D System.Void Manager_Scripts.OtherManager.LvUpManager::Setmaterial2(System.String)
extern void LvUpManager_Setmaterial2_m8A482BB59A7BE047C69DCD5EF31041A16975330A (void);
// 0x0000026E System.Void Manager_Scripts.OtherManager.LvUpManager::Setmaterial3(System.String)
extern void LvUpManager_Setmaterial3_m308E34A4E538C6EF7C649330051CDE002FAD8D7A (void);
// 0x0000026F System.Void Manager_Scripts.OtherManager.LvUpManager::SetRqM1(System.Int32)
extern void LvUpManager_SetRqM1_m9DAA7CB8E46F3978CC83A309F95D7124DDCBED69 (void);
// 0x00000270 System.Void Manager_Scripts.OtherManager.LvUpManager::SetRqM2(System.Int32)
extern void LvUpManager_SetRqM2_mF2D1B69BD726888F0B6C69D4C7E0C78376790025 (void);
// 0x00000271 System.Void Manager_Scripts.OtherManager.LvUpManager::SetRqM3(System.Int32)
extern void LvUpManager_SetRqM3_mD9E32CA0A809D6285EF0318AE795D1E73EBFAF9B (void);
// 0x00000272 System.Void Manager_Scripts.OtherManager.LvUpManager::OnTriggerExit2D(UnityEngine.Collider2D)
extern void LvUpManager_OnTriggerExit2D_mE20FEC0DF9A6B1DB4D3EF8855C83158163D4C492 (void);
// 0x00000273 System.Void Manager_Scripts.OtherManager.LvUpManager::.ctor()
extern void LvUpManager__ctor_mA1E9E27F30D3E24DBA7C717AC1B8DFD8061F64DC (void);
// 0x00000274 System.Void Manager_Scripts.OtherManager.MoveCamera::Start()
extern void MoveCamera_Start_m64AEDA3BD1EBD98F2C09AD6C1581A934D55014F9 (void);
// 0x00000275 System.Void Manager_Scripts.OtherManager.MoveCamera::Update()
extern void MoveCamera_Update_m0A5C860EA49C17A849C51BB4B2D141FC0082336F (void);
// 0x00000276 System.Void Manager_Scripts.OtherManager.MoveCamera::ResetCamera()
extern void MoveCamera_ResetCamera_mB8088C05267F8729298C02CE33947F5C2E4B3DFE (void);
// 0x00000277 System.Void Manager_Scripts.OtherManager.MoveCamera::.ctor()
extern void MoveCamera__ctor_m9D1449774B647BD35259A1E157455EC268D22DEB (void);
// 0x00000278 System.Void Manager_Scripts.OtherManager.ReinforcementManager::Start()
extern void ReinforcementManager_Start_m704287AAAA1094E3AF11112CEB44F39268A2A05E (void);
// 0x00000279 System.Void Manager_Scripts.OtherManager.ReinforcementManager::Update()
extern void ReinforcementManager_Update_m6E4CD309856354AD68CB6C59EF628C5F0110880C (void);
// 0x0000027A System.Void Manager_Scripts.OtherManager.ReinforcementManager::RfPlayer()
extern void ReinforcementManager_RfPlayer_mA38EB3C81087BA7A901F7231551570906442D39A (void);
// 0x0000027B System.Void Manager_Scripts.OtherManager.ReinforcementManager::OnTriggerExit2D(UnityEngine.Collider2D)
extern void ReinforcementManager_OnTriggerExit2D_m1DD77D3D007AC3902EBA5E4973195D2539F980AC (void);
// 0x0000027C System.Void Manager_Scripts.OtherManager.ReinforcementManager::ClickRf(System.Int32)
extern void ReinforcementManager_ClickRf_m9CBD544BA204DBBA4EA402C46122EBA090EB731D (void);
// 0x0000027D System.Void Manager_Scripts.OtherManager.ReinforcementManager::.ctor()
extern void ReinforcementManager__ctor_mA43BE5D9AA0AD3B272EB6611627044E07861B9E6 (void);
// 0x0000027E System.Void Manager_Scripts.OtherManager.Tutorial::Start()
extern void Tutorial_Start_m3CBAA1BEDC8CB2E4FFB90AB956834087D2A6C667 (void);
// 0x0000027F System.Void Manager_Scripts.OtherManager.Tutorial::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Tutorial_OnTriggerEnter2D_m35ECA137580629EAD929A46DC23F67BEB501D469 (void);
// 0x00000280 System.Void Manager_Scripts.OtherManager.Tutorial::OnTriggerExit2D(UnityEngine.Collider2D)
extern void Tutorial_OnTriggerExit2D_mA98DB3014BBFBB986704D62F71C8E95F574B4C74 (void);
// 0x00000281 System.Void Manager_Scripts.OtherManager.Tutorial::.ctor()
extern void Tutorial__ctor_m87A8838748CCCE8002A2154285057CD40F2F4551 (void);
// 0x00000282 System.Void Manager_Scripts.OtherManager.TutorialUIManager::Start()
extern void TutorialUIManager_Start_mA37C7F0E2A98F1689BECDC0D41DFE38A6F8C6D05 (void);
// 0x00000283 System.Void Manager_Scripts.OtherManager.TutorialUIManager::Update()
extern void TutorialUIManager_Update_m8FEC2C84EED0F0816D8A30E236715A4D78C5C68B (void);
// 0x00000284 System.Void Manager_Scripts.OtherManager.TutorialUIManager::.ctor()
extern void TutorialUIManager__ctor_m7355C66656A355CA245D9998BD4C8913F99B740C (void);
// 0x00000285 System.Void Manager_Scripts.Material.MaterialManager::Start()
extern void MaterialManager_Start_m59AFCADB11F369FB8A2666DDB7398220369110AD (void);
// 0x00000286 System.Void Manager_Scripts.Material.MaterialManager::Update()
extern void MaterialManager_Update_mF5A3C0E8EC8875FAF86EF86B2E32E066A66CD796 (void);
// 0x00000287 System.Void Manager_Scripts.Material.MaterialManager::CheckPlayer()
extern void MaterialManager_CheckPlayer_m3B5F10970DDD4C4C4629CEB666378608ED216412 (void);
// 0x00000288 System.Void Manager_Scripts.Material.MaterialManager::ClickKeep()
extern void MaterialManager_ClickKeep_m7C6ED7F4CF265A1FB762D6B81EEC646FD2B83816 (void);
// 0x00000289 System.Void Manager_Scripts.Material.MaterialManager::ClickCancel()
extern void MaterialManager_ClickCancel_m89CF04C9FB1191B2DD82A9AD03A9864146DC5DA4 (void);
// 0x0000028A System.Boolean Manager_Scripts.Material.MaterialManager::IKeep()
extern void MaterialManager_IKeep_m971961B10D1A4E7BBA457F9061F157362C564698 (void);
// 0x0000028B System.Void Manager_Scripts.Material.MaterialManager::MaterialSetSo()
extern void MaterialManager_MaterialSetSo_m6F654CC1DD6A4794F6DE03F6C50EE7A3355536E4 (void);
// 0x0000028C System.Void Manager_Scripts.Material.MaterialManager::Damage(System.Int32)
extern void MaterialManager_Damage_m33813A56A3B321106EC99BDEDC66697363A0473A (void);
// 0x0000028D System.Void Manager_Scripts.Material.MaterialManager::.ctor()
extern void MaterialManager__ctor_mD99481FBF92105EC70D2FFBF6001A8067CE9AAF3 (void);
// 0x0000028E System.Void Manager_Scripts.Enemy.Scripe.EnemyMainGame::Start()
extern void EnemyMainGame_Start_m84E14F2AF2CD654A99E7C6430E7CF89AED4D0C02 (void);
// 0x0000028F System.Void Manager_Scripts.Enemy.Scripe.EnemyMainGame::Update()
extern void EnemyMainGame_Update_m54DDFF9CE3A5FA4DA7A66DB02D077F29DE71BA34 (void);
// 0x00000290 System.Void Manager_Scripts.Enemy.Scripe.EnemyMainGame::Walking()
extern void EnemyMainGame_Walking_mAC8FB21362B773F550A64A79DB899D3935D9F2C5 (void);
// 0x00000291 System.Void Manager_Scripts.Enemy.Scripe.EnemyMainGame::Attack()
extern void EnemyMainGame_Attack_m999AAF5DEC46BB17A1F6D1B43877CA0560610376 (void);
// 0x00000292 System.Void Manager_Scripts.Enemy.Scripe.EnemyMainGame::Damage(System.Int32)
extern void EnemyMainGame_Damage_mB2D9D081745B4342622C57758AA89807DAAB766E (void);
// 0x00000293 System.Void Manager_Scripts.Enemy.Scripe.EnemyMainGame::HorderAttack()
extern void EnemyMainGame_HorderAttack_m8F56A546B60E03B1A9BF8CA90B011796D6A9EB36 (void);
// 0x00000294 System.Void Manager_Scripts.Enemy.Scripe.EnemyMainGame::EnemySetSo()
extern void EnemyMainGame_EnemySetSo_m0BCDB9FA42CD7DABB441709FB5C59D80C6BC5529 (void);
// 0x00000295 System.Void Manager_Scripts.Enemy.Scripe.EnemyMainGame::.ctor()
extern void EnemyMainGame__ctor_m8D656B27E3250BD6D2502550423CAA2EAAA53774 (void);
// 0x00000296 System.Void Scripe.EnemyFrom::.ctor()
extern void EnemyFrom__ctor_mE3E743282D65CC63958C536EF55D32449F6554C5 (void);
// 0x00000297 System.Void Interface.IBoolCheck::IsRotate180()
// 0x00000298 System.Void Interface.IBoolCheck::IsRotate0()
// 0x00000299 System.Void Interface.IDamage::Damage(System.Int32)
// 0x0000029A System.Void Interface.IHungry::HungryPlus(System.Single)
// 0x0000029B System.Void Interface.IHungry::HpPlus(System.Int32)
// 0x0000029C System.Boolean Interface.INameMaterial::IKeep()
// 0x0000029D System.Void Interface.IPlayerLv::SetPlayerLv(System.Int32)
// 0x0000029E System.Int32 Interface.IPlayerLv::PlayerLv()
// 0x0000029F System.Boolean Interface.ISPlayerDoWork::IsDoingWork()
// 0x000002A0 System.Boolean Interface.ISPlayerDoWork::IsDoingWorkSet(System.Boolean)
// 0x000002A1 System.Boolean Interface.ISPlayerDoWork::IsGenerate()
// 0x000002A2 System.Boolean Interface.ISPlayerDoWork::IsGenerateSet(System.Boolean)
// 0x000002A3 System.Void Firebase.LeaderBoard::UpdateData()
extern void LeaderBoard_UpdateData_mF378FEB2ED5E0FB959E2A2702219ABBFB0CFF9CF (void);
// 0x000002A4 System.Void Firebase.LeaderBoard::.ctor()
extern void LeaderBoard__ctor_mA72A7EF68FDA8A1486F87F04CEAFD147B0C9783E (void);
// 0x000002A5 System.Void Firebase.PlayerData::.ctor(System.String,System.Int32,System.Int32)
extern void PlayerData__ctor_m6F8EA950FBCA164DEEB2D2BDB3C042BD0445A686 (void);
// 0x000002A6 System.Void FileSO.SpawnSO.SpawnSo::.ctor()
extern void SpawnSo__ctor_mF7D614AEB481DD7162111CBA81B7B5C3782576D6 (void);
// 0x000002A7 System.Void FileSO.Player.PlayerSo::.ctor()
extern void PlayerSo__ctor_mC99B8B485B534C95B7ADCA7CEB273F12C65F4E1B (void);
// 0x000002A8 System.Void FileSO.Material.MaterialSo::.ctor()
extern void MaterialSo__ctor_mACD4972C54650A369CAF39878D31370872C3753A (void);
// 0x000002A9 System.Void FileSO.Factory.FactorySo::.ctor()
extern void FactorySo__ctor_mC8C502A4511DEEBBEF38C9A56846A1B77071D252 (void);
// 0x000002AA System.Void FileSO.Barricade.BarricadeFileSo::.ctor()
extern void BarricadeFileSo__ctor_m2DA7256EBE6376FB349568B49434115884E44CAA (void);
// 0x000002AB System.Void Building.BuildingWall::Start()
extern void BuildingWall_Start_mFB4DE8008CB378C079D3D36B2C7AA69990477B31 (void);
// 0x000002AC System.Void Building.BuildingWall::Update()
extern void BuildingWall_Update_m5AA91A2C37B92B9159C48F0586B5BA4FDD6B6BAF (void);
// 0x000002AD System.Void Building.BuildingWall::Damage(System.Int32)
extern void BuildingWall_Damage_mE5596CA50EDA9F948888C92B56FE4EE188D9C0AA (void);
// 0x000002AE System.Void Building.BuildingWall::SetBuildingSo()
extern void BuildingWall_SetBuildingSo_m457CD46D87C2D6254021F313B064370955E37085 (void);
// 0x000002AF System.Void Building.BuildingWall::SetMtSo(System.Int32)
extern void BuildingWall_SetMtSo_mFA6AE20480CCB4D55FAB8C59A734797A2F0FFDB3 (void);
// 0x000002B0 System.Void Building.BuildingWall::Fire()
extern void BuildingWall_Fire_m701B74EDFC98FAD7FFD79B42612E0DAF418FFC3A (void);
// 0x000002B1 System.Void Building.BuildingWall::CheckPlayer()
extern void BuildingWall_CheckPlayer_mB0CAACCBC1F0C76B58637CD5C9FD448DEDEFC9F5 (void);
// 0x000002B2 System.Void Building.BuildingWall::SelectType1()
extern void BuildingWall_SelectType1_m189A03A10C63D57950EE186D63724CA8E64FC8E8 (void);
// 0x000002B3 System.Void Building.BuildingWall::SelectType2()
extern void BuildingWall_SelectType2_mD539BD501E737F4D9B7D8C053B626BD885CCD359 (void);
// 0x000002B4 System.Void Building.BuildingWall::SelectType0()
extern void BuildingWall_SelectType0_m17AC622BAA963E33A4471B6DFE8B0F2C6CBCE7E9 (void);
// 0x000002B5 System.Void Building.BuildingWall::.ctor()
extern void BuildingWall__ctor_mC2DBA4C89C22F3D9DF460949675B7D6AB033A99B (void);
// 0x000002B6 System.Void Building.Climb::.ctor()
extern void Climb__ctor_m233AD2F2D7BD21041C6D493D7AB64188EB3C021F (void);
// 0x000002B7 System.Int32 RSG.IPromise`1::get_Id()
// 0x000002B8 RSG.IPromise`1<PromisedT> RSG.IPromise`1::WithName(System.String)
// 0x000002B9 System.Void RSG.IPromise`1::Done(System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x000002BA System.Void RSG.IPromise`1::Done(System.Action`1<PromisedT>)
// 0x000002BB System.Void RSG.IPromise`1::Done()
// 0x000002BC RSG.IPromise RSG.IPromise`1::Catch(System.Action`1<System.Exception>)
// 0x000002BD RSG.IPromise`1<PromisedT> RSG.IPromise`1::Catch(System.Func`2<System.Exception,PromisedT>)
// 0x000002BE RSG.IPromise`1<ConvertedT> RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>)
// 0x000002BF RSG.IPromise RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise>)
// 0x000002C0 RSG.IPromise RSG.IPromise`1::Then(System.Action`1<PromisedT>)
// 0x000002C1 RSG.IPromise`1<ConvertedT> RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>)
// 0x000002C2 RSG.IPromise RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise>,System.Action`1<System.Exception>)
// 0x000002C3 RSG.IPromise RSG.IPromise`1::Then(System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x000002C4 RSG.IPromise`1<ConvertedT> RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>,System.Action`1<System.Single>)
// 0x000002C5 RSG.IPromise RSG.IPromise`1::Then(System.Func`2<PromisedT,RSG.IPromise>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x000002C6 RSG.IPromise RSG.IPromise`1::Then(System.Action`1<PromisedT>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x000002C7 RSG.IPromise`1<ConvertedT> RSG.IPromise`1::Then(System.Func`2<PromisedT,ConvertedT>)
// 0x000002C8 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.IPromise`1::ThenAll(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x000002C9 RSG.IPromise RSG.IPromise`1::ThenAll(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x000002CA RSG.IPromise`1<ConvertedT> RSG.IPromise`1::ThenRace(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x000002CB RSG.IPromise RSG.IPromise`1::ThenRace(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x000002CC RSG.IPromise`1<PromisedT> RSG.IPromise`1::Finally(System.Action)
// 0x000002CD RSG.IPromise RSG.IPromise`1::ContinueWith(System.Func`1<RSG.IPromise>)
// 0x000002CE RSG.IPromise`1<ConvertedT> RSG.IPromise`1::ContinueWith(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x000002CF RSG.IPromise`1<PromisedT> RSG.IPromise`1::Progress(System.Action`1<System.Single>)
// 0x000002D0 System.Void RSG.IRejectable::Reject(System.Exception)
// 0x000002D1 System.Int32 RSG.IPendingPromise`1::get_Id()
// 0x000002D2 System.Void RSG.IPendingPromise`1::Resolve(PromisedT)
// 0x000002D3 System.Void RSG.IPendingPromise`1::ReportProgress(System.Single)
// 0x000002D4 System.Int32 RSG.Promise`1::get_Id()
// 0x000002D5 System.String RSG.Promise`1::get_Name()
// 0x000002D6 System.Void RSG.Promise`1::set_Name(System.String)
// 0x000002D7 RSG.PromiseState RSG.Promise`1::get_CurState()
// 0x000002D8 System.Void RSG.Promise`1::set_CurState(RSG.PromiseState)
// 0x000002D9 System.Void RSG.Promise`1::.ctor()
// 0x000002DA System.Void RSG.Promise`1::.ctor(System.Action`2<System.Action`1<PromisedT>,System.Action`1<System.Exception>>)
// 0x000002DB System.Void RSG.Promise`1::AddRejectHandler(System.Action`1<System.Exception>,RSG.IRejectable)
// 0x000002DC System.Void RSG.Promise`1::AddResolveHandler(System.Action`1<PromisedT>,RSG.IRejectable)
// 0x000002DD System.Void RSG.Promise`1::AddProgressHandler(System.Action`1<System.Single>,RSG.IRejectable)
// 0x000002DE System.Void RSG.Promise`1::InvokeHandler(System.Action`1<T>,RSG.IRejectable,T)
// 0x000002DF System.Void RSG.Promise`1::ClearHandlers()
// 0x000002E0 System.Void RSG.Promise`1::InvokeRejectHandlers(System.Exception)
// 0x000002E1 System.Void RSG.Promise`1::InvokeResolveHandlers(PromisedT)
// 0x000002E2 System.Void RSG.Promise`1::InvokeProgressHandlers(System.Single)
// 0x000002E3 System.Void RSG.Promise`1::Reject(System.Exception)
// 0x000002E4 System.Void RSG.Promise`1::Resolve(PromisedT)
// 0x000002E5 System.Void RSG.Promise`1::ReportProgress(System.Single)
// 0x000002E6 System.Void RSG.Promise`1::Done(System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x000002E7 System.Void RSG.Promise`1::Done(System.Action`1<PromisedT>)
// 0x000002E8 System.Void RSG.Promise`1::Done()
// 0x000002E9 RSG.IPromise`1<PromisedT> RSG.Promise`1::WithName(System.String)
// 0x000002EA RSG.IPromise RSG.Promise`1::Catch(System.Action`1<System.Exception>)
// 0x000002EB RSG.IPromise`1<PromisedT> RSG.Promise`1::Catch(System.Func`2<System.Exception,PromisedT>)
// 0x000002EC RSG.IPromise`1<ConvertedT> RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>)
// 0x000002ED RSG.IPromise RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise>)
// 0x000002EE RSG.IPromise RSG.Promise`1::Then(System.Action`1<PromisedT>)
// 0x000002EF RSG.IPromise`1<ConvertedT> RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>)
// 0x000002F0 RSG.IPromise RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise>,System.Action`1<System.Exception>)
// 0x000002F1 RSG.IPromise RSG.Promise`1::Then(System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x000002F2 RSG.IPromise`1<ConvertedT> RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>,System.Action`1<System.Single>)
// 0x000002F3 RSG.IPromise RSG.Promise`1::Then(System.Func`2<PromisedT,RSG.IPromise>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x000002F4 RSG.IPromise RSG.Promise`1::Then(System.Action`1<PromisedT>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x000002F5 RSG.IPromise`1<ConvertedT> RSG.Promise`1::Then(System.Func`2<PromisedT,ConvertedT>)
// 0x000002F6 System.Void RSG.Promise`1::ActionHandlers(RSG.IRejectable,System.Action`1<PromisedT>,System.Action`1<System.Exception>)
// 0x000002F7 System.Void RSG.Promise`1::ProgressHandlers(RSG.IRejectable,System.Action`1<System.Single>)
// 0x000002F8 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.Promise`1::ThenAll(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x000002F9 RSG.IPromise RSG.Promise`1::ThenAll(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x000002FA RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<PromisedT>> RSG.Promise`1::All(RSG.IPromise`1<PromisedT>[])
// 0x000002FB RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<PromisedT>> RSG.Promise`1::All(System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<PromisedT>>)
// 0x000002FC RSG.IPromise`1<ConvertedT> RSG.Promise`1::ThenRace(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x000002FD RSG.IPromise RSG.Promise`1::ThenRace(System.Func`2<PromisedT,System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x000002FE RSG.IPromise`1<PromisedT> RSG.Promise`1::Race(RSG.IPromise`1<PromisedT>[])
// 0x000002FF RSG.IPromise`1<PromisedT> RSG.Promise`1::Race(System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<PromisedT>>)
// 0x00000300 RSG.IPromise`1<PromisedT> RSG.Promise`1::Resolved(PromisedT)
// 0x00000301 RSG.IPromise`1<PromisedT> RSG.Promise`1::Rejected(System.Exception)
// 0x00000302 RSG.IPromise`1<PromisedT> RSG.Promise`1::Finally(System.Action)
// 0x00000303 RSG.IPromise RSG.Promise`1::ContinueWith(System.Func`1<RSG.IPromise>)
// 0x00000304 RSG.IPromise`1<ConvertedT> RSG.Promise`1::ContinueWith(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x00000305 RSG.IPromise`1<PromisedT> RSG.Promise`1::Progress(System.Action`1<System.Single>)
// 0x00000306 System.Void RSG.Promise`1::<Done>b__30_0(System.Exception)
// 0x00000307 System.Void RSG.Promise`1::<Done>b__31_0(System.Exception)
// 0x00000308 System.Void RSG.Promise`1::<Done>b__32_0(System.Exception)
// 0x00000309 System.Void RSG.Promise`1/<>c__DisplayClass24_0::.ctor()
// 0x0000030A System.Void RSG.Promise`1/<>c__DisplayClass24_0::<InvokeRejectHandlers>b__0(RSG.RejectHandler)
// 0x0000030B System.Void RSG.Promise`1/<>c__DisplayClass26_0::.ctor()
// 0x0000030C System.Void RSG.Promise`1/<>c__DisplayClass26_0::<InvokeProgressHandlers>b__0(RSG.ProgressHandler)
// 0x0000030D System.Void RSG.Promise`1/<>c__DisplayClass34_0::.ctor()
// 0x0000030E System.Void RSG.Promise`1/<>c__DisplayClass34_0::<Catch>b__0(PromisedT)
// 0x0000030F System.Void RSG.Promise`1/<>c__DisplayClass34_0::<Catch>b__1(System.Exception)
// 0x00000310 System.Void RSG.Promise`1/<>c__DisplayClass34_0::<Catch>b__2(System.Single)
// 0x00000311 System.Void RSG.Promise`1/<>c__DisplayClass35_0::.ctor()
// 0x00000312 System.Void RSG.Promise`1/<>c__DisplayClass35_0::<Catch>b__0(PromisedT)
// 0x00000313 System.Void RSG.Promise`1/<>c__DisplayClass35_0::<Catch>b__1(System.Exception)
// 0x00000314 System.Void RSG.Promise`1/<>c__DisplayClass35_0::<Catch>b__2(System.Single)
// 0x00000315 System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::.ctor()
// 0x00000316 System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__0(PromisedT)
// 0x00000317 System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__2(System.Single)
// 0x00000318 System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__3(ConvertedT)
// 0x00000319 System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__4(System.Exception)
// 0x0000031A System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__1(System.Exception)
// 0x0000031B System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__5(ConvertedT)
// 0x0000031C System.Void RSG.Promise`1/<>c__DisplayClass42_0`1::<Then>b__6(System.Exception)
// 0x0000031D System.Void RSG.Promise`1/<>c__DisplayClass43_0::.ctor()
// 0x0000031E System.Void RSG.Promise`1/<>c__DisplayClass43_0::<Then>b__0(PromisedT)
// 0x0000031F System.Void RSG.Promise`1/<>c__DisplayClass43_0::<Then>b__2(System.Single)
// 0x00000320 System.Void RSG.Promise`1/<>c__DisplayClass43_0::<Then>b__3()
// 0x00000321 System.Void RSG.Promise`1/<>c__DisplayClass43_0::<Then>b__4(System.Exception)
// 0x00000322 System.Void RSG.Promise`1/<>c__DisplayClass43_0::<Then>b__1(System.Exception)
// 0x00000323 System.Void RSG.Promise`1/<>c__DisplayClass44_0::.ctor()
// 0x00000324 System.Void RSG.Promise`1/<>c__DisplayClass44_0::<Then>b__0(PromisedT)
// 0x00000325 System.Void RSG.Promise`1/<>c__DisplayClass44_0::<Then>b__1(System.Exception)
// 0x00000326 System.Void RSG.Promise`1/<>c__DisplayClass45_0`1::.ctor()
// 0x00000327 RSG.IPromise`1<ConvertedT> RSG.Promise`1/<>c__DisplayClass45_0`1::<Then>b__0(PromisedT)
// 0x00000328 System.Void RSG.Promise`1/<>c__DisplayClass48_0`1::.ctor()
// 0x00000329 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.Promise`1/<>c__DisplayClass48_0`1::<ThenAll>b__0(PromisedT)
// 0x0000032A System.Void RSG.Promise`1/<>c__DisplayClass49_0::.ctor()
// 0x0000032B RSG.IPromise RSG.Promise`1/<>c__DisplayClass49_0::<ThenAll>b__0(PromisedT)
// 0x0000032C System.Void RSG.Promise`1/<>c__DisplayClass51_0::.ctor()
// 0x0000032D System.Void RSG.Promise`1/<>c__DisplayClass51_0::<All>b__0(RSG.IPromise`1<PromisedT>,System.Int32)
// 0x0000032E System.Void RSG.Promise`1/<>c__DisplayClass51_0::<All>b__3(System.Exception)
// 0x0000032F System.Void RSG.Promise`1/<>c__DisplayClass51_1::.ctor()
// 0x00000330 System.Void RSG.Promise`1/<>c__DisplayClass51_1::<All>b__1(System.Single)
// 0x00000331 System.Void RSG.Promise`1/<>c__DisplayClass51_1::<All>b__2(PromisedT)
// 0x00000332 System.Void RSG.Promise`1/<>c__DisplayClass52_0`1::.ctor()
// 0x00000333 RSG.IPromise`1<ConvertedT> RSG.Promise`1/<>c__DisplayClass52_0`1::<ThenRace>b__0(PromisedT)
// 0x00000334 System.Void RSG.Promise`1/<>c__DisplayClass53_0::.ctor()
// 0x00000335 RSG.IPromise RSG.Promise`1/<>c__DisplayClass53_0::<ThenRace>b__0(PromisedT)
// 0x00000336 System.Void RSG.Promise`1/<>c__DisplayClass55_0::.ctor()
// 0x00000337 System.Void RSG.Promise`1/<>c__DisplayClass55_0::<Race>b__0(RSG.IPromise`1<PromisedT>,System.Int32)
// 0x00000338 System.Void RSG.Promise`1/<>c__DisplayClass55_0::<Race>b__2(PromisedT)
// 0x00000339 System.Void RSG.Promise`1/<>c__DisplayClass55_0::<Race>b__3(System.Exception)
// 0x0000033A System.Void RSG.Promise`1/<>c__DisplayClass55_1::.ctor()
// 0x0000033B System.Void RSG.Promise`1/<>c__DisplayClass55_1::<Race>b__1(System.Single)
// 0x0000033C System.Void RSG.Promise`1/<>c__DisplayClass58_0::.ctor()
// 0x0000033D System.Void RSG.Promise`1/<>c__DisplayClass58_0::<Finally>b__0(PromisedT)
// 0x0000033E System.Void RSG.Promise`1/<>c__DisplayClass58_0::<Finally>b__1(System.Exception)
// 0x0000033F PromisedT RSG.Promise`1/<>c__DisplayClass58_0::<Finally>b__2(PromisedT)
// 0x00000340 System.Void RSG.Promise`1/<>c__DisplayClass59_0::.ctor()
// 0x00000341 System.Void RSG.Promise`1/<>c__DisplayClass59_0::<ContinueWith>b__0(PromisedT)
// 0x00000342 System.Void RSG.Promise`1/<>c__DisplayClass59_0::<ContinueWith>b__1(System.Exception)
// 0x00000343 System.Void RSG.Promise`1/<>c__DisplayClass60_0`1::.ctor()
// 0x00000344 System.Void RSG.Promise`1/<>c__DisplayClass60_0`1::<ContinueWith>b__0(PromisedT)
// 0x00000345 System.Void RSG.Promise`1/<>c__DisplayClass60_0`1::<ContinueWith>b__1(System.Exception)
// 0x00000346 RSG.IPromise`1<RSG.Tuple`2<T1,T2>> RSG.PromiseHelpers::All(RSG.IPromise`1<T1>,RSG.IPromise`1<T2>)
// 0x00000347 RSG.IPromise`1<RSG.Tuple`3<T1,T2,T3>> RSG.PromiseHelpers::All(RSG.IPromise`1<T1>,RSG.IPromise`1<T2>,RSG.IPromise`1<T3>)
// 0x00000348 RSG.IPromise`1<RSG.Tuple`4<T1,T2,T3,T4>> RSG.PromiseHelpers::All(RSG.IPromise`1<T1>,RSG.IPromise`1<T2>,RSG.IPromise`1<T3>,RSG.IPromise`1<T4>)
// 0x00000349 System.Void RSG.PromiseHelpers/<>c__DisplayClass0_0`2::.ctor()
// 0x0000034A System.Void RSG.PromiseHelpers/<>c__DisplayClass0_0`2::<All>b__0(T1)
// 0x0000034B System.Void RSG.PromiseHelpers/<>c__DisplayClass0_0`2::<All>b__1(System.Exception)
// 0x0000034C System.Void RSG.PromiseHelpers/<>c__DisplayClass0_0`2::<All>b__2(T2)
// 0x0000034D System.Void RSG.PromiseHelpers/<>c__DisplayClass0_0`2::<All>b__3(System.Exception)
// 0x0000034E System.Void RSG.PromiseHelpers/<>c__1`3::.cctor()
// 0x0000034F System.Void RSG.PromiseHelpers/<>c__1`3::.ctor()
// 0x00000350 RSG.Tuple`3<T1,T2,T3> RSG.PromiseHelpers/<>c__1`3::<All>b__1_0(RSG.Tuple`2<RSG.Tuple`2<T1,T2>,T3>)
// 0x00000351 System.Void RSG.PromiseHelpers/<>c__2`4::.cctor()
// 0x00000352 System.Void RSG.PromiseHelpers/<>c__2`4::.ctor()
// 0x00000353 RSG.Tuple`4<T1,T2,T3,T4> RSG.PromiseHelpers/<>c__2`4::<All>b__2_0(RSG.Tuple`2<RSG.Tuple`2<T1,T2>,RSG.Tuple`2<T3,T4>>)
// 0x00000354 System.Void RSG.PromiseCancelledException::.ctor()
extern void PromiseCancelledException__ctor_mDA55F2F9C6DF87917C0A40BC149A437C750C66FD (void);
// 0x00000355 System.Void RSG.PromiseCancelledException::.ctor(System.String)
extern void PromiseCancelledException__ctor_m0733E2CEAEE71A6A1A22E96FFF62D85627437BF1 (void);
// 0x00000356 System.Void RSG.PredicateWait::.ctor()
extern void PredicateWait__ctor_mCD6942E554ED82AF36B3F07828C4E5BCA7F0D318 (void);
// 0x00000357 RSG.IPromise RSG.IPromiseTimer::WaitFor(System.Single)
// 0x00000358 RSG.IPromise RSG.IPromiseTimer::WaitUntil(System.Func`2<RSG.TimeData,System.Boolean>)
// 0x00000359 RSG.IPromise RSG.IPromiseTimer::WaitWhile(System.Func`2<RSG.TimeData,System.Boolean>)
// 0x0000035A System.Void RSG.IPromiseTimer::Update(System.Single)
// 0x0000035B System.Boolean RSG.IPromiseTimer::Cancel(RSG.IPromise)
// 0x0000035C RSG.IPromise RSG.PromiseTimer::WaitFor(System.Single)
extern void PromiseTimer_WaitFor_m99B243125D5686DC36844FA69D796651E12C9B76 (void);
// 0x0000035D RSG.IPromise RSG.PromiseTimer::WaitWhile(System.Func`2<RSG.TimeData,System.Boolean>)
extern void PromiseTimer_WaitWhile_m8EF7C405289D3A2AE7F7185AE8E4E901BF504EC9 (void);
// 0x0000035E RSG.IPromise RSG.PromiseTimer::WaitUntil(System.Func`2<RSG.TimeData,System.Boolean>)
extern void PromiseTimer_WaitUntil_m7AC7AE2AFC6CB5E43A789DE5A6E683D62D23FBFC (void);
// 0x0000035F System.Boolean RSG.PromiseTimer::Cancel(RSG.IPromise)
extern void PromiseTimer_Cancel_m87F8524626DDC28FB1ECA1968F73A2D18540704E (void);
// 0x00000360 System.Collections.Generic.LinkedListNode`1<RSG.PredicateWait> RSG.PromiseTimer::FindInWaiting(RSG.IPromise)
extern void PromiseTimer_FindInWaiting_mA1660E092E5065B151FB000488B2C87FF4C6C8F1 (void);
// 0x00000361 System.Void RSG.PromiseTimer::Update(System.Single)
extern void PromiseTimer_Update_m9C84E7E7B30B14A7C13510CC22AB6032C7C0874F (void);
// 0x00000362 System.Collections.Generic.LinkedListNode`1<RSG.PredicateWait> RSG.PromiseTimer::RemoveNode(System.Collections.Generic.LinkedListNode`1<RSG.PredicateWait>)
extern void PromiseTimer_RemoveNode_mF32CC3AE04AE1AFBCEF4B8DA7DFD9E2AF02A6E89 (void);
// 0x00000363 System.Void RSG.PromiseTimer::.ctor()
extern void PromiseTimer__ctor_m3F1298B21CD5644C3AE646911AD13B6FCCD19BE6 (void);
// 0x00000364 System.Void RSG.PromiseTimer/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mC51BCEA4436030F27897F20585D501ABF744C78E (void);
// 0x00000365 System.Boolean RSG.PromiseTimer/<>c__DisplayClass3_0::<WaitFor>b__0(RSG.TimeData)
extern void U3CU3Ec__DisplayClass3_0_U3CWaitForU3Eb__0_m7EEE8458FA739B7F87A8EF705E05039C9EFB483D (void);
// 0x00000366 System.Void RSG.PromiseTimer/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m8884F549B84F64B7E7D4B61E036A398416091313 (void);
// 0x00000367 System.Boolean RSG.PromiseTimer/<>c__DisplayClass4_0::<WaitWhile>b__0(RSG.TimeData)
extern void U3CU3Ec__DisplayClass4_0_U3CWaitWhileU3Eb__0_m8DF4AC7205D6CD8FF4E8AE640F92F6AB4A9B59F7 (void);
// 0x00000368 System.Int32 RSG.IPromise::get_Id()
// 0x00000369 RSG.IPromise RSG.IPromise::WithName(System.String)
// 0x0000036A System.Void RSG.IPromise::Done(System.Action,System.Action`1<System.Exception>)
// 0x0000036B System.Void RSG.IPromise::Done(System.Action)
// 0x0000036C System.Void RSG.IPromise::Done()
// 0x0000036D RSG.IPromise RSG.IPromise::Catch(System.Action`1<System.Exception>)
// 0x0000036E RSG.IPromise`1<ConvertedT> RSG.IPromise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x0000036F RSG.IPromise RSG.IPromise::Then(System.Func`1<RSG.IPromise>)
// 0x00000370 RSG.IPromise RSG.IPromise::Then(System.Action)
// 0x00000371 RSG.IPromise`1<ConvertedT> RSG.IPromise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>)
// 0x00000372 RSG.IPromise RSG.IPromise::Then(System.Func`1<RSG.IPromise>,System.Action`1<System.Exception>)
// 0x00000373 RSG.IPromise RSG.IPromise::Then(System.Action,System.Action`1<System.Exception>)
// 0x00000374 RSG.IPromise`1<ConvertedT> RSG.IPromise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>,System.Action`1<System.Single>)
// 0x00000375 RSG.IPromise RSG.IPromise::Then(System.Func`1<RSG.IPromise>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x00000376 RSG.IPromise RSG.IPromise::Then(System.Action,System.Action`1<System.Exception>,System.Action`1<System.Single>)
// 0x00000377 RSG.IPromise RSG.IPromise::ThenAll(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x00000378 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.IPromise::ThenAll(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x00000379 RSG.IPromise RSG.IPromise::ThenSequence(System.Func`1<System.Collections.Generic.IEnumerable`1<System.Func`1<RSG.IPromise>>>)
// 0x0000037A RSG.IPromise RSG.IPromise::ThenRace(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
// 0x0000037B RSG.IPromise`1<ConvertedT> RSG.IPromise::ThenRace(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x0000037C RSG.IPromise RSG.IPromise::Finally(System.Action)
// 0x0000037D RSG.IPromise RSG.IPromise::ContinueWith(System.Func`1<RSG.IPromise>)
// 0x0000037E RSG.IPromise`1<ConvertedT> RSG.IPromise::ContinueWith(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x0000037F RSG.IPromise RSG.IPromise::Progress(System.Action`1<System.Single>)
// 0x00000380 System.Int32 RSG.IPendingPromise::get_Id()
// 0x00000381 System.Void RSG.IPendingPromise::Resolve()
// 0x00000382 System.Void RSG.IPendingPromise::ReportProgress(System.Single)
// 0x00000383 System.Int32 RSG.IPromiseInfo::get_Id()
// 0x00000384 System.String RSG.IPromiseInfo::get_Name()
// 0x00000385 System.Void RSG.ExceptionEventArgs::.ctor(System.Exception)
extern void ExceptionEventArgs__ctor_m7023DA6E99C0B23D96D0AAB0500F16B19345A30C (void);
// 0x00000386 System.Exception RSG.ExceptionEventArgs::get_Exception()
extern void ExceptionEventArgs_get_Exception_m79958EAB2CAA0E6C91E10902A3ABCC242012745A (void);
// 0x00000387 System.Void RSG.ExceptionEventArgs::set_Exception(System.Exception)
extern void ExceptionEventArgs_set_Exception_mBEFA8B8A6990A708691413D5E188FD73D6036A88 (void);
// 0x00000388 System.Void RSG.Promise::add_UnhandledException(System.EventHandler`1<RSG.ExceptionEventArgs>)
extern void Promise_add_UnhandledException_m501ED8710D326733D0A4B095516E79AD93771A17 (void);
// 0x00000389 System.Void RSG.Promise::remove_UnhandledException(System.EventHandler`1<RSG.ExceptionEventArgs>)
extern void Promise_remove_UnhandledException_m0A7255398B93C672C3F4AD9AE8CC1260E6BDC493 (void);
// 0x0000038A System.Collections.Generic.IEnumerable`1<RSG.IPromiseInfo> RSG.Promise::GetPendingPromises()
extern void Promise_GetPendingPromises_mCD78AA3FD69C8809D95E0FBBC6DC24DB648CD1E4 (void);
// 0x0000038B System.Int32 RSG.Promise::get_Id()
extern void Promise_get_Id_m2A159B9C83B813983859B9F8497B92A636D26198 (void);
// 0x0000038C System.String RSG.Promise::get_Name()
extern void Promise_get_Name_m2681487A6A6188C86D6B1FEE6F4E42F0817F0AF1 (void);
// 0x0000038D System.Void RSG.Promise::set_Name(System.String)
extern void Promise_set_Name_m04BD79E80089945F37642A501B2E0C621CFAADFF (void);
// 0x0000038E RSG.PromiseState RSG.Promise::get_CurState()
extern void Promise_get_CurState_mBE60E772D62CB2C8449BF67F3A2106C0E0FA720D (void);
// 0x0000038F System.Void RSG.Promise::set_CurState(RSG.PromiseState)
extern void Promise_set_CurState_mC9014B9835DB4BDFE7B3E1AFC0B61A249DB0B148 (void);
// 0x00000390 System.Void RSG.Promise::.ctor()
extern void Promise__ctor_m72891E8C449A8F0285F2BBEE6596EEE91D1B69D0 (void);
// 0x00000391 System.Void RSG.Promise::.ctor(System.Action`2<System.Action,System.Action`1<System.Exception>>)
extern void Promise__ctor_m0F170C315BB66058A0B8DB59D2FB2E269460DB4A (void);
// 0x00000392 System.Int32 RSG.Promise::NextId()
extern void Promise_NextId_m91D574BF971140F5BCA824858A1F23785E625936 (void);
// 0x00000393 System.Void RSG.Promise::AddRejectHandler(System.Action`1<System.Exception>,RSG.IRejectable)
extern void Promise_AddRejectHandler_m8A5C912F1C8D8FC752EAAFCC3397992F84DE2C83 (void);
// 0x00000394 System.Void RSG.Promise::AddResolveHandler(System.Action,RSG.IRejectable)
extern void Promise_AddResolveHandler_m39C28AC656C57C1865250657901C41569B49B87A (void);
// 0x00000395 System.Void RSG.Promise::AddProgressHandler(System.Action`1<System.Single>,RSG.IRejectable)
extern void Promise_AddProgressHandler_mC3147E564182B45C04B4AD18F55200ADE09494A9 (void);
// 0x00000396 System.Void RSG.Promise::InvokeRejectHandler(System.Action`1<System.Exception>,RSG.IRejectable,System.Exception)
extern void Promise_InvokeRejectHandler_mD002929AA239DA2F2D3CBA6B828BCC190E2FA010 (void);
// 0x00000397 System.Void RSG.Promise::InvokeResolveHandler(System.Action,RSG.IRejectable)
extern void Promise_InvokeResolveHandler_m8253411D16B16952735214A8D4840B69CBC19367 (void);
// 0x00000398 System.Void RSG.Promise::InvokeProgressHandler(System.Action`1<System.Single>,RSG.IRejectable,System.Single)
extern void Promise_InvokeProgressHandler_m2767B570D7BE8E457C295823CFD48F4DF079AD1F (void);
// 0x00000399 System.Void RSG.Promise::ClearHandlers()
extern void Promise_ClearHandlers_m77595F54867BFFB97E19A6354940D3C574E8AF30 (void);
// 0x0000039A System.Void RSG.Promise::InvokeRejectHandlers(System.Exception)
extern void Promise_InvokeRejectHandlers_m78C4A77BB7F1728BCF63373111A992940FD73FBA (void);
// 0x0000039B System.Void RSG.Promise::InvokeResolveHandlers()
extern void Promise_InvokeResolveHandlers_m071F1B5884447CEE1F7D19154AB430522BE6C208 (void);
// 0x0000039C System.Void RSG.Promise::InvokeProgressHandlers(System.Single)
extern void Promise_InvokeProgressHandlers_m33BBE047F8B32BDC15B943203CF4D653C10F5F3D (void);
// 0x0000039D System.Void RSG.Promise::Reject(System.Exception)
extern void Promise_Reject_mA334C811DFA609A9294A15B8C4FE3D06CFE59E4B (void);
// 0x0000039E System.Void RSG.Promise::Resolve()
extern void Promise_Resolve_mB395CBF764AD56F64C953F240F5525C4F24AA176 (void);
// 0x0000039F System.Void RSG.Promise::ReportProgress(System.Single)
extern void Promise_ReportProgress_m7496AD24A305E551C317FAF83A339D50555EC2F0 (void);
// 0x000003A0 System.Void RSG.Promise::Done(System.Action,System.Action`1<System.Exception>)
extern void Promise_Done_mBF4D40ECA5734EFE1213BD2AD11124841BCA6C63 (void);
// 0x000003A1 System.Void RSG.Promise::Done(System.Action)
extern void Promise_Done_mA20B58D299DFC458330AB673679F52760076ED6D (void);
// 0x000003A2 System.Void RSG.Promise::Done()
extern void Promise_Done_mB94F090BD0EDC421C98291DF2437788619416322 (void);
// 0x000003A3 RSG.IPromise RSG.Promise::WithName(System.String)
extern void Promise_WithName_m9A8610E5066D79F98133FBDE0C427032A9EF04DB (void);
// 0x000003A4 RSG.IPromise RSG.Promise::Catch(System.Action`1<System.Exception>)
extern void Promise_Catch_m34A1A6F483209E344CEE20CAECA131319E25A35F (void);
// 0x000003A5 RSG.IPromise`1<ConvertedT> RSG.Promise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x000003A6 RSG.IPromise RSG.Promise::Then(System.Func`1<RSG.IPromise>)
extern void Promise_Then_m660D492D7CB977D2658FDC71CA16C5F1C7C80BFB (void);
// 0x000003A7 RSG.IPromise RSG.Promise::Then(System.Action)
extern void Promise_Then_mE35145837C7EBB840D64C24A92FFEC013AA4E52F (void);
// 0x000003A8 RSG.IPromise`1<ConvertedT> RSG.Promise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>)
// 0x000003A9 RSG.IPromise RSG.Promise::Then(System.Func`1<RSG.IPromise>,System.Action`1<System.Exception>)
extern void Promise_Then_m9C5987E6126BE35855402986BE5878DDDA5DDD2F (void);
// 0x000003AA RSG.IPromise RSG.Promise::Then(System.Action,System.Action`1<System.Exception>)
extern void Promise_Then_m1F2DA120D93DB9D1801A4410BACE242992BFDE5D (void);
// 0x000003AB RSG.IPromise`1<ConvertedT> RSG.Promise::Then(System.Func`1<RSG.IPromise`1<ConvertedT>>,System.Func`2<System.Exception,RSG.IPromise`1<ConvertedT>>,System.Action`1<System.Single>)
// 0x000003AC RSG.IPromise RSG.Promise::Then(System.Func`1<RSG.IPromise>,System.Action`1<System.Exception>,System.Action`1<System.Single>)
extern void Promise_Then_mE03C61503CE58EA2981EF96852E6F252B6F29736 (void);
// 0x000003AD RSG.IPromise RSG.Promise::Then(System.Action,System.Action`1<System.Exception>,System.Action`1<System.Single>)
extern void Promise_Then_m6693CA7F4467B53237CE92F7F44C36CD44FABE0D (void);
// 0x000003AE System.Void RSG.Promise::ActionHandlers(RSG.IRejectable,System.Action,System.Action`1<System.Exception>)
extern void Promise_ActionHandlers_m226D1CB18140648C8EC32E3E2FA7DF0730478F26 (void);
// 0x000003AF System.Void RSG.Promise::ProgressHandlers(RSG.IRejectable,System.Action`1<System.Single>)
extern void Promise_ProgressHandlers_m21C8B94D38E0FBF1F14B24FCFFFEC214D9D80912 (void);
// 0x000003B0 RSG.IPromise RSG.Promise::ThenAll(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
extern void Promise_ThenAll_m444DBB4B6489921A44CDCF98AB8D3F2708372D66 (void);
// 0x000003B1 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.Promise::ThenAll(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x000003B2 RSG.IPromise RSG.Promise::All(RSG.IPromise[])
extern void Promise_All_m4021CBAF737AB75139BE836A21E02B28B238AD21 (void);
// 0x000003B3 RSG.IPromise RSG.Promise::All(System.Collections.Generic.IEnumerable`1<RSG.IPromise>)
extern void Promise_All_mD80850322247A01D346893F8962617690B9D2606 (void);
// 0x000003B4 RSG.IPromise RSG.Promise::ThenSequence(System.Func`1<System.Collections.Generic.IEnumerable`1<System.Func`1<RSG.IPromise>>>)
extern void Promise_ThenSequence_mE42D6CD848060EB59C10345B5A14B16724D35F9B (void);
// 0x000003B5 RSG.IPromise RSG.Promise::Sequence(System.Func`1<RSG.IPromise>[])
extern void Promise_Sequence_m9F9C078AA9DB0404DAB83831A3EF769379E58906 (void);
// 0x000003B6 RSG.IPromise RSG.Promise::Sequence(System.Collections.Generic.IEnumerable`1<System.Func`1<RSG.IPromise>>)
extern void Promise_Sequence_m646E63C1583234FE0B9EC2F642D5B1EB7A830E52 (void);
// 0x000003B7 RSG.IPromise RSG.Promise::ThenRace(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise>>)
extern void Promise_ThenRace_mF04FB2EDFB9FF1589EF714F409994945E123EE7D (void);
// 0x000003B8 RSG.IPromise`1<ConvertedT> RSG.Promise::ThenRace(System.Func`1<System.Collections.Generic.IEnumerable`1<RSG.IPromise`1<ConvertedT>>>)
// 0x000003B9 RSG.IPromise RSG.Promise::Race(RSG.IPromise[])
extern void Promise_Race_m7A61FE78FC907244940AE23BE119699E657860BB (void);
// 0x000003BA RSG.IPromise RSG.Promise::Race(System.Collections.Generic.IEnumerable`1<RSG.IPromise>)
extern void Promise_Race_m422B8D03C310B70EC818A1A5F7A8E12C836FD877 (void);
// 0x000003BB RSG.IPromise RSG.Promise::Resolved()
extern void Promise_Resolved_m27D7349EFBB518D89093ADB573BD7D7F836B24DE (void);
// 0x000003BC RSG.IPromise RSG.Promise::Rejected(System.Exception)
extern void Promise_Rejected_mF6CA3689BE449406F16A5D145B1DD4FD13C41890 (void);
// 0x000003BD RSG.IPromise RSG.Promise::Finally(System.Action)
extern void Promise_Finally_m762D2CD8F1F48788A1B94FB46CB338D69083230B (void);
// 0x000003BE RSG.IPromise RSG.Promise::ContinueWith(System.Func`1<RSG.IPromise>)
extern void Promise_ContinueWith_m1A53BD142DC421621617AE8E0BE6DA2053D96D8A (void);
// 0x000003BF RSG.IPromise`1<ConvertedT> RSG.Promise::ContinueWith(System.Func`1<RSG.IPromise`1<ConvertedT>>)
// 0x000003C0 RSG.IPromise RSG.Promise::Progress(System.Action`1<System.Single>)
extern void Promise_Progress_m53E64BDA8764C5E88057601FAB54BF75FD0B368A (void);
// 0x000003C1 System.Void RSG.Promise::PropagateUnhandledException(System.Object,System.Exception)
extern void Promise_PropagateUnhandledException_m30B4923F2598FA6B4F7CD5964E00760B5D9F10B7 (void);
// 0x000003C2 System.Void RSG.Promise::.cctor()
extern void Promise__cctor_mB03C9B64D5DF3D552E96C4FCFA942B128173413D (void);
// 0x000003C3 System.Void RSG.Promise::<InvokeResolveHandlers>b__35_0(RSG.Promise/ResolveHandler)
extern void Promise_U3CInvokeResolveHandlersU3Eb__35_0_mE4D9BB0CB11B33C2E7F12BFE44103CC546FD2E2A (void);
// 0x000003C4 System.Void RSG.Promise::<Done>b__40_0(System.Exception)
extern void Promise_U3CDoneU3Eb__40_0_m03D038B6097EEEF2B736CCE4A1D9154518BE3E01 (void);
// 0x000003C5 System.Void RSG.Promise::<Done>b__41_0(System.Exception)
extern void Promise_U3CDoneU3Eb__41_0_mDE5A60CCBCC7F17ADD74A395C92F553EAAA52817 (void);
// 0x000003C6 System.Void RSG.Promise::<Done>b__42_0(System.Exception)
extern void Promise_U3CDoneU3Eb__42_0_m10D5F4FC16A8A05494ED431EA28AA288DB13AE3B (void);
// 0x000003C7 System.Void RSG.Promise/<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_mF81EF54B39A864EB5E463DA04D72B5F8A0CF4DFC (void);
// 0x000003C8 System.Void RSG.Promise/<>c__DisplayClass34_0::<InvokeRejectHandlers>b__0(RSG.RejectHandler)
extern void U3CU3Ec__DisplayClass34_0_U3CInvokeRejectHandlersU3Eb__0_mC2CB7940F03A8D527C6C9BA7793BA6C6A07E13FC (void);
// 0x000003C9 System.Void RSG.Promise/<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_m18834E008AB1CCA3258C2388F1158B37AB6AFAC1 (void);
// 0x000003CA System.Void RSG.Promise/<>c__DisplayClass36_0::<InvokeProgressHandlers>b__0(RSG.ProgressHandler)
extern void U3CU3Ec__DisplayClass36_0_U3CInvokeProgressHandlersU3Eb__0_mB466D8A8500579A62E0EF36D3766DB1A59E89F41 (void);
// 0x000003CB System.Void RSG.Promise/<>c__DisplayClass44_0::.ctor()
extern void U3CU3Ec__DisplayClass44_0__ctor_mCB6B49A87238E9E939A3FB884973D5B8085A1805 (void);
// 0x000003CC System.Void RSG.Promise/<>c__DisplayClass44_0::<Catch>b__0()
extern void U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__0_m59D9BB020E9BAA19BF3A55E1A66B4BBF0115A4F3 (void);
// 0x000003CD System.Void RSG.Promise/<>c__DisplayClass44_0::<Catch>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__1_mC2F86C6B2CC1463D07838F8B6E71CF95A00D6B1C (void);
// 0x000003CE System.Void RSG.Promise/<>c__DisplayClass44_0::<Catch>b__2(System.Single)
extern void U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__2_m1C82A7D09BAFC515977C21BBAD3E8EE6E6C8B665 (void);
// 0x000003CF System.Void RSG.Promise/<>c__DisplayClass51_0`1::.ctor()
// 0x000003D0 System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__0()
// 0x000003D1 System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__2(System.Single)
// 0x000003D2 System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__3(ConvertedT)
// 0x000003D3 System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__4(System.Exception)
// 0x000003D4 System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__1(System.Exception)
// 0x000003D5 System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__5(ConvertedT)
// 0x000003D6 System.Void RSG.Promise/<>c__DisplayClass51_0`1::<Then>b__6(System.Exception)
// 0x000003D7 System.Void RSG.Promise/<>c__DisplayClass52_0::.ctor()
extern void U3CU3Ec__DisplayClass52_0__ctor_mB894C314363743106885723C9A014E58D79394DE (void);
// 0x000003D8 System.Void RSG.Promise/<>c__DisplayClass52_0::<Then>b__0()
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__0_mFAB16B8D05F5B6D1B0820FEAECBB318B28D6F95F (void);
// 0x000003D9 System.Void RSG.Promise/<>c__DisplayClass52_0::<Then>b__2(System.Single)
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__2_m68317E06CF2A6CB4E45914984E5E7B1C8E250B3A (void);
// 0x000003DA System.Void RSG.Promise/<>c__DisplayClass52_0::<Then>b__3()
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__3_m1DD830DC1CA6AEAB53B32028949E3349E37146EE (void);
// 0x000003DB System.Void RSG.Promise/<>c__DisplayClass52_0::<Then>b__4(System.Exception)
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__4_m6F52D5A374717AC944B68D544EF57498A6CF0C12 (void);
// 0x000003DC System.Void RSG.Promise/<>c__DisplayClass52_0::<Then>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__1_mE79481476F323A2E8C64CF6853F48919B454B238 (void);
// 0x000003DD System.Void RSG.Promise/<>c__DisplayClass53_0::.ctor()
extern void U3CU3Ec__DisplayClass53_0__ctor_m8AD8D6610D93664501D710EB55F2D75DE59813DC (void);
// 0x000003DE System.Void RSG.Promise/<>c__DisplayClass53_0::<Then>b__0()
extern void U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__0_m089AF19F35A75DF89A101BC920FA8F3CBF856F03 (void);
// 0x000003DF System.Void RSG.Promise/<>c__DisplayClass53_0::<Then>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__1_m6BF2C7A1D0A0D3699E5339E92DFF00CA7B60BBE2 (void);
// 0x000003E0 System.Void RSG.Promise/<>c__DisplayClass56_0::.ctor()
extern void U3CU3Ec__DisplayClass56_0__ctor_mA16A09395360FDB88DAEB794DAF2A6BAF4CAC8AE (void);
// 0x000003E1 RSG.IPromise RSG.Promise/<>c__DisplayClass56_0::<ThenAll>b__0()
extern void U3CU3Ec__DisplayClass56_0_U3CThenAllU3Eb__0_m76809680DE80AD5F5596FA06F41BA163AC6D1C30 (void);
// 0x000003E2 System.Void RSG.Promise/<>c__DisplayClass57_0`1::.ctor()
// 0x000003E3 RSG.IPromise`1<System.Collections.Generic.IEnumerable`1<ConvertedT>> RSG.Promise/<>c__DisplayClass57_0`1::<ThenAll>b__0()
// 0x000003E4 System.Void RSG.Promise/<>c__DisplayClass59_0::.ctor()
extern void U3CU3Ec__DisplayClass59_0__ctor_mBA66AC86289688F1C468B96C50BE5358F31A082F (void);
// 0x000003E5 System.Void RSG.Promise/<>c__DisplayClass59_0::<All>b__0(RSG.IPromise,System.Int32)
extern void U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__0_mA4CF65F285BDA1A6D803D917E9FD265051D6BEB9 (void);
// 0x000003E6 System.Void RSG.Promise/<>c__DisplayClass59_0::<All>b__3(System.Exception)
extern void U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__3_m8253EC136EA78D646B69616B15FA235271A992AA (void);
// 0x000003E7 System.Void RSG.Promise/<>c__DisplayClass59_1::.ctor()
extern void U3CU3Ec__DisplayClass59_1__ctor_m526DF9DF8BBEBFA28914FB4F54E3A1AF03B414BB (void);
// 0x000003E8 System.Void RSG.Promise/<>c__DisplayClass59_1::<All>b__1(System.Single)
extern void U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__1_m593233FA219F817C7E2E48E4B57F1967B608EDAE (void);
// 0x000003E9 System.Void RSG.Promise/<>c__DisplayClass59_1::<All>b__2()
extern void U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__2_m5AC61534D5AE0A52E1F555A299AD9A673E2DFD45 (void);
// 0x000003EA System.Void RSG.Promise/<>c__DisplayClass60_0::.ctor()
extern void U3CU3Ec__DisplayClass60_0__ctor_m99C72D8D0D1E5F3744F901AF2CE1D01B10074CD3 (void);
// 0x000003EB RSG.IPromise RSG.Promise/<>c__DisplayClass60_0::<ThenSequence>b__0()
extern void U3CU3Ec__DisplayClass60_0_U3CThenSequenceU3Eb__0_m6FC48A06C04839828FBCC417B074F35B1849CBF1 (void);
// 0x000003EC System.Void RSG.Promise/<>c__DisplayClass62_0::.ctor()
extern void U3CU3Ec__DisplayClass62_0__ctor_m4E600EF746E7384F9FF81A106144CCC5CA295EB6 (void);
// 0x000003ED RSG.IPromise RSG.Promise/<>c__DisplayClass62_0::<Sequence>b__0(RSG.IPromise,System.Func`1<RSG.IPromise>)
extern void U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__0_m3BD1BDD0E7EEF8F00E0C7DE8CD8AA502555164A8 (void);
// 0x000003EE System.Void RSG.Promise/<>c__DisplayClass62_0::<Sequence>b__1()
extern void U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__1_m030831A862679BA67E0AFFF2A793684303FDC197 (void);
// 0x000003EF System.Void RSG.Promise/<>c__DisplayClass62_1::.ctor()
extern void U3CU3Ec__DisplayClass62_1__ctor_m4E3A09413376A37B8429009826FB5E58FB5918D7 (void);
// 0x000003F0 RSG.IPromise RSG.Promise/<>c__DisplayClass62_1::<Sequence>b__2()
extern void U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__2_m38F09C7AB72B5AAAEE2B1F47D8B8E46A8B244D76 (void);
// 0x000003F1 System.Void RSG.Promise/<>c__DisplayClass62_1::<Sequence>b__3(System.Single)
extern void U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__3_m45E00529CA4F236640E37AD28227E06C03260F75 (void);
// 0x000003F2 System.Void RSG.Promise/<>c__DisplayClass63_0::.ctor()
extern void U3CU3Ec__DisplayClass63_0__ctor_mA3AE9325599E7C269CF28464DFD338E2FB36D87B (void);
// 0x000003F3 RSG.IPromise RSG.Promise/<>c__DisplayClass63_0::<ThenRace>b__0()
extern void U3CU3Ec__DisplayClass63_0_U3CThenRaceU3Eb__0_m5B8C751B1FAD053AE58DE02DF3FEEA12AE43EC44 (void);
// 0x000003F4 System.Void RSG.Promise/<>c__DisplayClass64_0`1::.ctor()
// 0x000003F5 RSG.IPromise`1<ConvertedT> RSG.Promise/<>c__DisplayClass64_0`1::<ThenRace>b__0()
// 0x000003F6 System.Void RSG.Promise/<>c__DisplayClass66_0::.ctor()
extern void U3CU3Ec__DisplayClass66_0__ctor_m20042739F9915BDA4DCECD38A6FAEC9A32FC9FD4 (void);
// 0x000003F7 System.Void RSG.Promise/<>c__DisplayClass66_0::<Race>b__0(RSG.IPromise,System.Int32)
extern void U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__0_m3C1812F10C134DFFDE20266483FB8CEE43E00D28 (void);
// 0x000003F8 System.Void RSG.Promise/<>c__DisplayClass66_0::<Race>b__2(System.Exception)
extern void U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__2_mD8A7C646663CD58D24E0B9E5AD99A223042BCC7B (void);
// 0x000003F9 System.Void RSG.Promise/<>c__DisplayClass66_0::<Race>b__3()
extern void U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__3_m5925431199B068785646FFD433C27ACF0DA6C7D2 (void);
// 0x000003FA System.Void RSG.Promise/<>c__DisplayClass66_1::.ctor()
extern void U3CU3Ec__DisplayClass66_1__ctor_m864691E480DAEE7AEEAD46F8FB882828DB249E77 (void);
// 0x000003FB System.Void RSG.Promise/<>c__DisplayClass66_1::<Race>b__1(System.Single)
extern void U3CU3Ec__DisplayClass66_1_U3CRaceU3Eb__1_mAA09AC32AAC0FB42ED3E102028E9F62480131D48 (void);
// 0x000003FC System.Void RSG.Promise/<>c__DisplayClass69_0::.ctor()
extern void U3CU3Ec__DisplayClass69_0__ctor_m1076FE9DB847E9C246A4B77B4A9AD68DA316DAFC (void);
// 0x000003FD System.Void RSG.Promise/<>c__DisplayClass69_0::<Finally>b__0()
extern void U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__0_m05B68401734C79A8332FD12FBF32C5C1DB2FCF61 (void);
// 0x000003FE System.Void RSG.Promise/<>c__DisplayClass69_0::<Finally>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__1_m805874267E687B76A8017DEFD8A11A96DA9FA8AD (void);
// 0x000003FF System.Void RSG.Promise/<>c__DisplayClass70_0::.ctor()
extern void U3CU3Ec__DisplayClass70_0__ctor_mD191E17C3CB17FB74E6636B739DFFE3332CF62FD (void);
// 0x00000400 System.Void RSG.Promise/<>c__DisplayClass70_0::<ContinueWith>b__0()
extern void U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__0_m251D75318366BEDFA9CC05A71A3B108CBEBDE4D6 (void);
// 0x00000401 System.Void RSG.Promise/<>c__DisplayClass70_0::<ContinueWith>b__1(System.Exception)
extern void U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__1_m8EF480C7A95C97BAE71BB581C32C4B1A14B98178 (void);
// 0x00000402 System.Void RSG.Promise/<>c__DisplayClass71_0`1::.ctor()
// 0x00000403 System.Void RSG.Promise/<>c__DisplayClass71_0`1::<ContinueWith>b__0()
// 0x00000404 System.Void RSG.Promise/<>c__DisplayClass71_0`1::<ContinueWith>b__1(System.Exception)
// 0x00000405 RSG.Tuple`2<T1,T2> RSG.Tuple::Create(T1,T2)
// 0x00000406 RSG.Tuple`3<T1,T2,T3> RSG.Tuple::Create(T1,T2,T3)
// 0x00000407 RSG.Tuple`4<T1,T2,T3,T4> RSG.Tuple::Create(T1,T2,T3,T4)
// 0x00000408 System.Void RSG.Tuple::.ctor()
extern void Tuple__ctor_mB90BC7455CD38661E7B174497948846C3717B84A (void);
// 0x00000409 System.Void RSG.Tuple`2::.ctor(T1,T2)
// 0x0000040A T1 RSG.Tuple`2::get_Item1()
// 0x0000040B System.Void RSG.Tuple`2::set_Item1(T1)
// 0x0000040C T2 RSG.Tuple`2::get_Item2()
// 0x0000040D System.Void RSG.Tuple`2::set_Item2(T2)
// 0x0000040E System.Void RSG.Tuple`3::.ctor(T1,T2,T3)
// 0x0000040F T1 RSG.Tuple`3::get_Item1()
// 0x00000410 System.Void RSG.Tuple`3::set_Item1(T1)
// 0x00000411 T2 RSG.Tuple`3::get_Item2()
// 0x00000412 System.Void RSG.Tuple`3::set_Item2(T2)
// 0x00000413 T3 RSG.Tuple`3::get_Item3()
// 0x00000414 System.Void RSG.Tuple`3::set_Item3(T3)
// 0x00000415 System.Void RSG.Tuple`4::.ctor(T1,T2,T3,T4)
// 0x00000416 T1 RSG.Tuple`4::get_Item1()
// 0x00000417 System.Void RSG.Tuple`4::set_Item1(T1)
// 0x00000418 T2 RSG.Tuple`4::get_Item2()
// 0x00000419 System.Void RSG.Tuple`4::set_Item2(T2)
// 0x0000041A T3 RSG.Tuple`4::get_Item3()
// 0x0000041B System.Void RSG.Tuple`4::set_Item3(T3)
// 0x0000041C T4 RSG.Tuple`4::get_Item4()
// 0x0000041D System.Void RSG.Tuple`4::set_Item4(T4)
// 0x0000041E System.Void RSG.Exceptions.PromiseException::.ctor()
extern void PromiseException__ctor_mC1C353F7C09B485B8EFCF3B176412385745A37B2 (void);
// 0x0000041F System.Void RSG.Exceptions.PromiseException::.ctor(System.String)
extern void PromiseException__ctor_m25FEF871F4EADD0813603091A36151F62E072F34 (void);
// 0x00000420 System.Void RSG.Exceptions.PromiseException::.ctor(System.String,System.Exception)
extern void PromiseException__ctor_m378D7284D56C1662B14AFD057C60ACBBABCF8895 (void);
// 0x00000421 System.Void RSG.Exceptions.PromiseStateException::.ctor()
extern void PromiseStateException__ctor_m0841D9ED95CB98A821651EB934050C7F99325539 (void);
// 0x00000422 System.Void RSG.Exceptions.PromiseStateException::.ctor(System.String)
extern void PromiseStateException__ctor_mB10D86B4B0D696B724F6A344C9DA8BA582785E8A (void);
// 0x00000423 System.Void RSG.Exceptions.PromiseStateException::.ctor(System.String,System.Exception)
extern void PromiseStateException__ctor_m09D7E625AC5B60FBAA33F53C52F2C5B9784C3AA9 (void);
// 0x00000424 System.Void RSG.Promises.EnumerableExt::Each(System.Collections.Generic.IEnumerable`1<T>,System.Action`1<T>)
// 0x00000425 System.Void RSG.Promises.EnumerableExt::Each(System.Collections.Generic.IEnumerable`1<T>,System.Action`2<T,System.Int32>)
// 0x00000426 System.Collections.Generic.IEnumerable`1<T> RSG.Promises.EnumerableExt::FromItems(T[])
// 0x00000427 System.Void RSG.Promises.EnumerableExt/<FromItems>d__2`1::.ctor(System.Int32)
// 0x00000428 System.Void RSG.Promises.EnumerableExt/<FromItems>d__2`1::System.IDisposable.Dispose()
// 0x00000429 System.Boolean RSG.Promises.EnumerableExt/<FromItems>d__2`1::MoveNext()
// 0x0000042A T RSG.Promises.EnumerableExt/<FromItems>d__2`1::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x0000042B System.Void RSG.Promises.EnumerableExt/<FromItems>d__2`1::System.Collections.IEnumerator.Reset()
// 0x0000042C System.Object RSG.Promises.EnumerableExt/<FromItems>d__2`1::System.Collections.IEnumerator.get_Current()
// 0x0000042D System.Collections.Generic.IEnumerator`1<T> RSG.Promises.EnumerableExt/<FromItems>d__2`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x0000042E System.Collections.IEnumerator RSG.Promises.EnumerableExt/<FromItems>d__2`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000042F System.Collections.IEnumerator Proyecto26.HttpBase::CreateRequestAndRetry(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void HttpBase_CreateRequestAndRetry_m6DC921EB7E83F0FB058D9C9889B15D489092D059 (void);
// 0x00000430 UnityEngine.Networking.UnityWebRequest Proyecto26.HttpBase::CreateRequest(Proyecto26.RequestHelper)
extern void HttpBase_CreateRequest_m395D82BBC11CA3C9D8FA5DCA55BC4E5AFE6AC451 (void);
// 0x00000431 Proyecto26.RequestException Proyecto26.HttpBase::CreateException(Proyecto26.RequestHelper,UnityEngine.Networking.UnityWebRequest)
extern void HttpBase_CreateException_m4F00611C1B083E175A53008F96FCDAEED39E094E (void);
// 0x00000432 System.Void Proyecto26.HttpBase::DebugLog(System.Boolean,System.Object,System.Boolean)
extern void HttpBase_DebugLog_mB403310A43B2C0F73923BE8135DBA43E5FECF8C3 (void);
// 0x00000433 System.Collections.IEnumerator Proyecto26.HttpBase::DefaultUnityWebRequest(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void HttpBase_DefaultUnityWebRequest_mA2CA028D057F027E7D8283E63643F0AF2974B069 (void);
// 0x00000434 System.Collections.IEnumerator Proyecto26.HttpBase::DefaultUnityWebRequest(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,TResponse>)
// 0x00000435 System.Collections.IEnumerator Proyecto26.HttpBase::DefaultUnityWebRequest(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,TResponse[]>)
// 0x00000436 System.Void Proyecto26.HttpBase/<CreateRequestAndRetry>d__0::.ctor(System.Int32)
extern void U3CCreateRequestAndRetryU3Ed__0__ctor_mA038A5FC9BE11F7D821C2EB89C903BD81DD6673F (void);
// 0x00000437 System.Void Proyecto26.HttpBase/<CreateRequestAndRetry>d__0::System.IDisposable.Dispose()
extern void U3CCreateRequestAndRetryU3Ed__0_System_IDisposable_Dispose_m0F0654EB7C2275B8DDFC1B070E8B145B2C5FBCAC (void);
// 0x00000438 System.Boolean Proyecto26.HttpBase/<CreateRequestAndRetry>d__0::MoveNext()
extern void U3CCreateRequestAndRetryU3Ed__0_MoveNext_mC05783571C265F2A6348BF44253BDF379AD516DD (void);
// 0x00000439 System.Void Proyecto26.HttpBase/<CreateRequestAndRetry>d__0::<>m__Finally1()
extern void U3CCreateRequestAndRetryU3Ed__0_U3CU3Em__Finally1_m65EAC36497FC4D9964288E99CD0E428ABC806C46 (void);
// 0x0000043A System.Object Proyecto26.HttpBase/<CreateRequestAndRetry>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateRequestAndRetryU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m585D7434321501EABFADD21A314DCBB231EC3252 (void);
// 0x0000043B System.Void Proyecto26.HttpBase/<CreateRequestAndRetry>d__0::System.Collections.IEnumerator.Reset()
extern void U3CCreateRequestAndRetryU3Ed__0_System_Collections_IEnumerator_Reset_m09C357E4FCCD4430591B46B1960B5DC7D4BFE376 (void);
// 0x0000043C System.Object Proyecto26.HttpBase/<CreateRequestAndRetry>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CCreateRequestAndRetryU3Ed__0_System_Collections_IEnumerator_get_Current_m4D5CD62D3F82FDDE9C67A9FD2E2894857D7746A6 (void);
// 0x0000043D System.Void Proyecto26.HttpBase/<>c__DisplayClass5_0`1::.ctor()
// 0x0000043E System.Void Proyecto26.HttpBase/<>c__DisplayClass5_0`1::<DefaultUnityWebRequest>b__0(Proyecto26.RequestException,Proyecto26.ResponseHelper)
// 0x0000043F System.Void Proyecto26.HttpBase/<>c__DisplayClass6_0`1::.ctor()
// 0x00000440 System.Void Proyecto26.HttpBase/<>c__DisplayClass6_0`1::<DefaultUnityWebRequest>b__0(Proyecto26.RequestException,Proyecto26.ResponseHelper)
// 0x00000441 T[] Proyecto26.JsonHelper::ArrayFromJson(System.String)
// 0x00000442 T[] Proyecto26.JsonHelper::FromJsonString(System.String)
// 0x00000443 System.String Proyecto26.JsonHelper::ArrayToJsonString(T[])
// 0x00000444 System.String Proyecto26.JsonHelper::ArrayToJsonString(T[],System.Boolean)
// 0x00000445 System.Void Proyecto26.JsonHelper/Wrapper`1::.ctor()
// 0x00000446 System.Boolean Proyecto26.RequestException::get_IsHttpError()
extern void RequestException_get_IsHttpError_mF1D87818843F7F4BFA1BB1B1F1EAD6925497CF30 (void);
// 0x00000447 System.Void Proyecto26.RequestException::set_IsHttpError(System.Boolean)
extern void RequestException_set_IsHttpError_mF311DBD704CCC61C23A270F8BCD1E7E8E5F17469 (void);
// 0x00000448 System.Boolean Proyecto26.RequestException::get_IsNetworkError()
extern void RequestException_get_IsNetworkError_mF6C31F24DCF7B5F649A4CABF455EBB03CB53434C (void);
// 0x00000449 System.Void Proyecto26.RequestException::set_IsNetworkError(System.Boolean)
extern void RequestException_set_IsNetworkError_mB4D9148437DD8A59B2969764E067E35E085ABB15 (void);
// 0x0000044A System.Int64 Proyecto26.RequestException::get_StatusCode()
extern void RequestException_get_StatusCode_m786C170A63F36EF40CF6F4347F5EACC85925E1F1 (void);
// 0x0000044B System.Void Proyecto26.RequestException::set_StatusCode(System.Int64)
extern void RequestException_set_StatusCode_mFD7034224DB7D4C27984CE0DA365D02173432309 (void);
// 0x0000044C System.String Proyecto26.RequestException::get_ServerMessage()
extern void RequestException_get_ServerMessage_m14FEBDED7437643B057376759428572897C03597 (void);
// 0x0000044D System.Void Proyecto26.RequestException::set_ServerMessage(System.String)
extern void RequestException_set_ServerMessage_m3DE596FD331A4F694B415FA3C8886FDC5F861DE9 (void);
// 0x0000044E System.String Proyecto26.RequestException::get_Response()
extern void RequestException_get_Response_m4FE8480DACD2F2347FDE3BC8F0843885E10912E5 (void);
// 0x0000044F System.Void Proyecto26.RequestException::set_Response(System.String)
extern void RequestException_set_Response_mEF4EB83001EB93F345CD9FB6214237F3888EDA25 (void);
// 0x00000450 System.Void Proyecto26.RequestException::.ctor()
extern void RequestException__ctor_m4255A573E554424A1B5C9F6E2D58590C50877C85 (void);
// 0x00000451 System.Void Proyecto26.RequestException::.ctor(System.String)
extern void RequestException__ctor_m2A12EC5D24756FA04C4D080FFD4DB9264B01753B (void);
// 0x00000452 System.Void Proyecto26.RequestException::.ctor(System.String,System.Object[])
extern void RequestException__ctor_m230AC0532C60E770A87DB74A57630F5464269D23 (void);
// 0x00000453 System.Void Proyecto26.RequestException::.ctor(System.String,System.Boolean,System.Boolean,System.Int64,System.String)
extern void RequestException__ctor_mD2293F4AA83F9A3A3EA0E48219894E91DF485FCC (void);
// 0x00000454 System.String Proyecto26.RequestHelper::get_Uri()
extern void RequestHelper_get_Uri_m579870F497E54040498F01E0F03D8E385B75682B (void);
// 0x00000455 System.Void Proyecto26.RequestHelper::set_Uri(System.String)
extern void RequestHelper_set_Uri_mED832E7E6B02422B23573A3BE154D41312A32BE3 (void);
// 0x00000456 System.String Proyecto26.RequestHelper::get_Method()
extern void RequestHelper_get_Method_m3CAF08A8E99D378C7693940ED9BBC576130C5558 (void);
// 0x00000457 System.Void Proyecto26.RequestHelper::set_Method(System.String)
extern void RequestHelper_set_Method_m93E8A5F66E37E3459E8F1689E272C9EA2303EEA2 (void);
// 0x00000458 System.Object Proyecto26.RequestHelper::get_Body()
extern void RequestHelper_get_Body_mE96291B55EA0C31805FDF261E1A6E0E0A04B9B4D (void);
// 0x00000459 System.Void Proyecto26.RequestHelper::set_Body(System.Object)
extern void RequestHelper_set_Body_m6C34BB52EE69E8A7D5B2FC14CE8478A4CCCB223A (void);
// 0x0000045A System.String Proyecto26.RequestHelper::get_BodyString()
extern void RequestHelper_get_BodyString_m583C6B16055049B23992E90B9BE9240D2494C872 (void);
// 0x0000045B System.Void Proyecto26.RequestHelper::set_BodyString(System.String)
extern void RequestHelper_set_BodyString_m264B491596B1E5D59DEBEDFA6D2C07EC37F6AF25 (void);
// 0x0000045C System.Byte[] Proyecto26.RequestHelper::get_BodyRaw()
extern void RequestHelper_get_BodyRaw_m0567B940B2FD769D548A63A3937EDAB8AF8BEB07 (void);
// 0x0000045D System.Void Proyecto26.RequestHelper::set_BodyRaw(System.Byte[])
extern void RequestHelper_set_BodyRaw_m4BB1B19C3109ED8297827657F5855DCAF7EF315E (void);
// 0x0000045E System.Nullable`1<System.Int32> Proyecto26.RequestHelper::get_Timeout()
extern void RequestHelper_get_Timeout_m5EB987EDD3D72C0AFC56600A506DED4393B31D03 (void);
// 0x0000045F System.Void Proyecto26.RequestHelper::set_Timeout(System.Nullable`1<System.Int32>)
extern void RequestHelper_set_Timeout_m8769C12B49481D0B8BC11B3227F802FDF049707F (void);
// 0x00000460 System.String Proyecto26.RequestHelper::get_ContentType()
extern void RequestHelper_get_ContentType_mF1D15920E2C975B8788B61B57B6A0EC713C5EB97 (void);
// 0x00000461 System.Void Proyecto26.RequestHelper::set_ContentType(System.String)
extern void RequestHelper_set_ContentType_m53EBFC2111ABCD757D3B613E943FED0F2A916B1F (void);
// 0x00000462 System.Int32 Proyecto26.RequestHelper::get_Retries()
extern void RequestHelper_get_Retries_mFE6F51F6EE991FEE0982128A8305F013668B7D5B (void);
// 0x00000463 System.Void Proyecto26.RequestHelper::set_Retries(System.Int32)
extern void RequestHelper_set_Retries_m8E91AB08F2D651BE88B57973D472A87CFE403AE1 (void);
// 0x00000464 System.Single Proyecto26.RequestHelper::get_RetrySecondsDelay()
extern void RequestHelper_get_RetrySecondsDelay_mF461806AF11B3F89D745861D3EE818A9548705F5 (void);
// 0x00000465 System.Void Proyecto26.RequestHelper::set_RetrySecondsDelay(System.Single)
extern void RequestHelper_set_RetrySecondsDelay_mF554D9A11BAD76DF31F07A5FA92B67E34FF7D6F8 (void);
// 0x00000466 System.Action`2<Proyecto26.RequestException,System.Int32> Proyecto26.RequestHelper::get_RetryCallback()
extern void RequestHelper_get_RetryCallback_m8487D96D1889FAC85CC99D59CE9AAD81AF975858 (void);
// 0x00000467 System.Void Proyecto26.RequestHelper::set_RetryCallback(System.Action`2<Proyecto26.RequestException,System.Int32>)
extern void RequestHelper_set_RetryCallback_m8B02AFF7AB55609093C48182D79501B34C2F2646 (void);
// 0x00000468 System.Boolean Proyecto26.RequestHelper::get_EnableDebug()
extern void RequestHelper_get_EnableDebug_m031DA45E2494D29141E793FBFE2BEF047DCF7472 (void);
// 0x00000469 System.Void Proyecto26.RequestHelper::set_EnableDebug(System.Boolean)
extern void RequestHelper_set_EnableDebug_m7DEAE27887D92A686E341EB14010DE5A4B3E4DF9 (void);
// 0x0000046A System.Nullable`1<System.Boolean> Proyecto26.RequestHelper::get_UseHttpContinue()
extern void RequestHelper_get_UseHttpContinue_m64713A0BA2260B82D1587517ADF0671DB60B8D66 (void);
// 0x0000046B System.Void Proyecto26.RequestHelper::set_UseHttpContinue(System.Nullable`1<System.Boolean>)
extern void RequestHelper_set_UseHttpContinue_m454427FB941A608ABD370EC1E64F0C5166183E48 (void);
// 0x0000046C System.Nullable`1<System.Int32> Proyecto26.RequestHelper::get_RedirectLimit()
extern void RequestHelper_get_RedirectLimit_m7628CFD271A1A06DBF264C289268CE52DA66DB82 (void);
// 0x0000046D System.Void Proyecto26.RequestHelper::set_RedirectLimit(System.Nullable`1<System.Int32>)
extern void RequestHelper_set_RedirectLimit_m32E15A1AE611D405855B008D58576C8839929DC3 (void);
// 0x0000046E System.Boolean Proyecto26.RequestHelper::get_IgnoreHttpException()
extern void RequestHelper_get_IgnoreHttpException_m0BA6B17AACCF9C74FEFD730806A64B67E4505FEA (void);
// 0x0000046F System.Void Proyecto26.RequestHelper::set_IgnoreHttpException(System.Boolean)
extern void RequestHelper_set_IgnoreHttpException_m6653B079C6C70E478895D1EF4BFAC9FE5D5D2209 (void);
// 0x00000470 UnityEngine.WWWForm Proyecto26.RequestHelper::get_FormData()
extern void RequestHelper_get_FormData_m1CE699C5BC879C7AEFBB1084CF0A1928CD02B931 (void);
// 0x00000471 System.Void Proyecto26.RequestHelper::set_FormData(UnityEngine.WWWForm)
extern void RequestHelper_set_FormData_mD33B2EEF5749F7898DB81BD2ACEB0F7EA9E97EC4 (void);
// 0x00000472 System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.RequestHelper::get_SimpleForm()
extern void RequestHelper_get_SimpleForm_mE158581E8CAC46C434A072324A6290CBE4878C78 (void);
// 0x00000473 System.Void Proyecto26.RequestHelper::set_SimpleForm(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void RequestHelper_set_SimpleForm_m885368EFE7D8FF37C0E9CBC36A8B551562042165 (void);
// 0x00000474 System.Collections.Generic.List`1<UnityEngine.Networking.IMultipartFormSection> Proyecto26.RequestHelper::get_FormSections()
extern void RequestHelper_get_FormSections_m4157D4255B827B864F882669136AB30BA93316F0 (void);
// 0x00000475 System.Void Proyecto26.RequestHelper::set_FormSections(System.Collections.Generic.List`1<UnityEngine.Networking.IMultipartFormSection>)
extern void RequestHelper_set_FormSections_m955F901BFE0A3D2FFE42F223BB2005BA2028337E (void);
// 0x00000476 UnityEngine.Networking.CertificateHandler Proyecto26.RequestHelper::get_CertificateHandler()
extern void RequestHelper_get_CertificateHandler_mBF95069255782425F21F491C5A51F3BBCEE956B1 (void);
// 0x00000477 System.Void Proyecto26.RequestHelper::set_CertificateHandler(UnityEngine.Networking.CertificateHandler)
extern void RequestHelper_set_CertificateHandler_m1B5296D19D58F16EF358706D8AC3817F0ED0B1C5 (void);
// 0x00000478 UnityEngine.Networking.UploadHandler Proyecto26.RequestHelper::get_UploadHandler()
extern void RequestHelper_get_UploadHandler_m5C72A7EF49F678C8CB02FA0620C0B41D1CF8AE78 (void);
// 0x00000479 System.Void Proyecto26.RequestHelper::set_UploadHandler(UnityEngine.Networking.UploadHandler)
extern void RequestHelper_set_UploadHandler_mDA3C9BCAE240C2A5185B88FE2FE7BF198B6467ED (void);
// 0x0000047A UnityEngine.Networking.DownloadHandler Proyecto26.RequestHelper::get_DownloadHandler()
extern void RequestHelper_get_DownloadHandler_m3CC95DFC6C81ABD8508947C1808BC8FB7095BDA3 (void);
// 0x0000047B System.Void Proyecto26.RequestHelper::set_DownloadHandler(UnityEngine.Networking.DownloadHandler)
extern void RequestHelper_set_DownloadHandler_mE7FC2C6C38C5418A915D4007DCFF2A9CF5C6DC74 (void);
// 0x0000047C System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.RequestHelper::get_Headers()
extern void RequestHelper_get_Headers_m09842B9228E6B35E7254DFAC41A5D2C98DBF0BE9 (void);
// 0x0000047D System.Void Proyecto26.RequestHelper::set_Headers(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void RequestHelper_set_Headers_mE4B41FA1E24D02AD057068E2D0A697C129921A29 (void);
// 0x0000047E System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.RequestHelper::get_Params()
extern void RequestHelper_get_Params_mDB495A9CB92B43BB1BD3526C99880EFBDC40E033 (void);
// 0x0000047F System.Void Proyecto26.RequestHelper::set_Params(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void RequestHelper_set_Params_m887AAEE89701BE57B2875DD482A584C58EF7C400 (void);
// 0x00000480 System.Boolean Proyecto26.RequestHelper::get_ParseResponseBody()
extern void RequestHelper_get_ParseResponseBody_m8352D39E0EBACC6FB532FBE05B5D3EC709EA43BD (void);
// 0x00000481 System.Void Proyecto26.RequestHelper::set_ParseResponseBody(System.Boolean)
extern void RequestHelper_set_ParseResponseBody_m1149B5FC0628F6BCDAFA95D21516A9CAE3F4031D (void);
// 0x00000482 UnityEngine.Networking.UnityWebRequest Proyecto26.RequestHelper::get_Request()
extern void RequestHelper_get_Request_mB3DD0BDA5D264948DE92BA5E060545D8D3CCBD63 (void);
// 0x00000483 System.Void Proyecto26.RequestHelper::set_Request(UnityEngine.Networking.UnityWebRequest)
extern void RequestHelper_set_Request_m4C49567052095FC092A24AD821EF7957CCB6F4BF (void);
// 0x00000484 System.Single Proyecto26.RequestHelper::get_UploadProgress()
extern void RequestHelper_get_UploadProgress_m4F7D8BCA66AE1D13F1C59D91EE10E656D48DE91B (void);
// 0x00000485 System.UInt64 Proyecto26.RequestHelper::get_UploadedBytes()
extern void RequestHelper_get_UploadedBytes_m7D3445EE67E794E91D82A28171224EED03460533 (void);
// 0x00000486 System.Single Proyecto26.RequestHelper::get_DownloadProgress()
extern void RequestHelper_get_DownloadProgress_m0ADF466E7C45A9D043506B099C6B3E7A08C5BE33 (void);
// 0x00000487 System.UInt64 Proyecto26.RequestHelper::get_DownloadedBytes()
extern void RequestHelper_get_DownloadedBytes_m5201AEFCAF699F4C054CBA3C63004950C1E32ED7 (void);
// 0x00000488 System.String Proyecto26.RequestHelper::GetHeader(System.String)
extern void RequestHelper_GetHeader_m0EAA6AEED4B885553B4CD97B2C4CBF81A32EF468 (void);
// 0x00000489 System.Boolean Proyecto26.RequestHelper::get_IsAborted()
extern void RequestHelper_get_IsAborted_mED2B0C2E2FA7DA65574ED4141818116D5407825B (void);
// 0x0000048A System.Void Proyecto26.RequestHelper::set_IsAborted(System.Boolean)
extern void RequestHelper_set_IsAborted_mF37D6A6B93B70D7B4AC2B579FDA59345D0D51DDF (void);
// 0x0000048B System.Boolean Proyecto26.RequestHelper::get_DefaultContentType()
extern void RequestHelper_get_DefaultContentType_mF52E8AD322DC2F3A0A0444FA1A60D2C62B42113F (void);
// 0x0000048C System.Void Proyecto26.RequestHelper::set_DefaultContentType(System.Boolean)
extern void RequestHelper_set_DefaultContentType_m0098EE480F8E776673979AFB751C76CD61AB5365 (void);
// 0x0000048D System.Void Proyecto26.RequestHelper::Abort()
extern void RequestHelper_Abort_m7CE069D799170B7D0336E7DB358C78B1B00DA8F3 (void);
// 0x0000048E System.Void Proyecto26.RequestHelper::.ctor()
extern void RequestHelper__ctor_m76D5CE4B07BAD0EFE344E9A8C4D1BFE895922C13 (void);
// 0x0000048F UnityEngine.Networking.UnityWebRequest Proyecto26.ResponseHelper::get_Request()
extern void ResponseHelper_get_Request_mE2DF5B066E83E4216167EEE1A28A79A09A372843 (void);
// 0x00000490 System.Void Proyecto26.ResponseHelper::set_Request(UnityEngine.Networking.UnityWebRequest)
extern void ResponseHelper_set_Request_m9743967B9D789F748E8205E910F339114CB7D522 (void);
// 0x00000491 System.Void Proyecto26.ResponseHelper::.ctor(UnityEngine.Networking.UnityWebRequest)
extern void ResponseHelper__ctor_m0F45F428ADF231F62EC7C6E5621058DA806A1850 (void);
// 0x00000492 System.Int64 Proyecto26.ResponseHelper::get_StatusCode()
extern void ResponseHelper_get_StatusCode_mAC67167172472DC6804430CA8FDEDDBF1FED2D4F (void);
// 0x00000493 System.Byte[] Proyecto26.ResponseHelper::get_Data()
extern void ResponseHelper_get_Data_mDFB63D2691666575C126F59C928580F359646E44 (void);
// 0x00000494 System.String Proyecto26.ResponseHelper::get_Text()
extern void ResponseHelper_get_Text_m4C6CE813DC4FA5B0659BF20EC4B5EE2504C618AF (void);
// 0x00000495 System.String Proyecto26.ResponseHelper::get_Error()
extern void ResponseHelper_get_Error_mAF99FD6F00AF225E4D2EF620266C6A428199F6E7 (void);
// 0x00000496 System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.ResponseHelper::get_Headers()
extern void ResponseHelper_get_Headers_m2990A8F894F71B2503103388796B0717EEF9D9CB (void);
// 0x00000497 System.String Proyecto26.ResponseHelper::GetHeader(System.String)
extern void ResponseHelper_GetHeader_m9E39EF8000022EA04DF66B43FCCB31AA01AB0D52 (void);
// 0x00000498 System.String Proyecto26.ResponseHelper::ToString()
extern void ResponseHelper_ToString_m3DD8554605B5BDE7A6412218369E317C804B0572 (void);
// 0x00000499 Proyecto26.StaticCoroutine/CoroutineHolder Proyecto26.StaticCoroutine::get_Runner()
extern void StaticCoroutine_get_Runner_mC13BCB3C4C6F548812E35E5555D3455AEB2700DF (void);
// 0x0000049A UnityEngine.Coroutine Proyecto26.StaticCoroutine::StartCoroutine(System.Collections.IEnumerator)
extern void StaticCoroutine_StartCoroutine_m46187D9942A6387EAD6EEE4771C5C54027547C58 (void);
// 0x0000049B System.Void Proyecto26.StaticCoroutine/CoroutineHolder::.ctor()
extern void CoroutineHolder__ctor_mC1424305AB5B1F2E631B7121BBB184B1B8BBBCF6 (void);
// 0x0000049C System.Version Proyecto26.RestClient::get_Version()
extern void RestClient_get_Version_m5701043CF8E7F883ACF65A25CADCE6AB4AFC8EB0 (void);
// 0x0000049D System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.RestClient::get_DefaultRequestParams()
extern void RestClient_get_DefaultRequestParams_mE23F55E6B8003CFEE7FF60C7483232FE7E247A3D (void);
// 0x0000049E System.Void Proyecto26.RestClient::set_DefaultRequestParams(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void RestClient_set_DefaultRequestParams_mA826F5ADE53A384196CC73D37D26E72E5B7CAA9B (void);
// 0x0000049F System.Void Proyecto26.RestClient::ClearDefaultParams()
extern void RestClient_ClearDefaultParams_mCBFF0C9C7DA720E8F0F9D54D0BDDE3D1C421CA71 (void);
// 0x000004A0 System.Collections.Generic.Dictionary`2<System.String,System.String> Proyecto26.RestClient::get_DefaultRequestHeaders()
extern void RestClient_get_DefaultRequestHeaders_m7CFC695866D2FFFAE6C2C79B38564917EFFE37AA (void);
// 0x000004A1 System.Void Proyecto26.RestClient::set_DefaultRequestHeaders(System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void RestClient_set_DefaultRequestHeaders_m8ED47C4D90B6F05DB8ED523A5A49C2B0B1B7C2B5 (void);
// 0x000004A2 System.Void Proyecto26.RestClient::ClearDefaultHeaders()
extern void RestClient_ClearDefaultHeaders_mFADDD96FA3D43A3CFC44B9A6A641B666F000C09F (void);
// 0x000004A3 System.Void Proyecto26.RestClient::Request(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Request_m6D0EEC74B2073F8006977A737FEB5A26FF8B2BE4 (void);
// 0x000004A4 System.Void Proyecto26.RestClient::Request(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x000004A5 System.Void Proyecto26.RestClient::Get(System.String,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Get_mD3CD57C7B7809173BB0841F05883D4EAB667C4E4 (void);
// 0x000004A6 System.Void Proyecto26.RestClient::Get(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Get_m62DDD6AA1F3E2D93DC5AF9378090C97D67EE7D54 (void);
// 0x000004A7 System.Void Proyecto26.RestClient::Get(System.String,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x000004A8 System.Void Proyecto26.RestClient::Get(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x000004A9 System.Void Proyecto26.RestClient::GetArray(System.String,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T[]>)
// 0x000004AA System.Void Proyecto26.RestClient::GetArray(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T[]>)
// 0x000004AB System.Void Proyecto26.RestClient::Post(System.String,System.Object,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Post_m4B07264A4B85D23B2F3013C227AB599B04F18E7A (void);
// 0x000004AC System.Void Proyecto26.RestClient::Post(System.String,System.String,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Post_mAD4114F33CCE7E913400D3AA42D2494C1C71D27A (void);
// 0x000004AD System.Void Proyecto26.RestClient::Post(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Post_mB43880A95D1AD7E76C1878CB288DC4E6B3453651 (void);
// 0x000004AE System.Void Proyecto26.RestClient::Post(System.String,System.Object,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x000004AF System.Void Proyecto26.RestClient::Post(System.String,System.String,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x000004B0 System.Void Proyecto26.RestClient::Post(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x000004B1 System.Void Proyecto26.RestClient::PostArray(System.String,System.Object,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T[]>)
// 0x000004B2 System.Void Proyecto26.RestClient::PostArray(System.String,System.String,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T[]>)
// 0x000004B3 System.Void Proyecto26.RestClient::PostArray(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T[]>)
// 0x000004B4 System.Void Proyecto26.RestClient::Put(System.String,System.Object,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Put_m955E298F1437E5DDB4391BE3BF2A58B02232AA4E (void);
// 0x000004B5 System.Void Proyecto26.RestClient::Put(System.String,System.String,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Put_m8AF7486C0FD795F8D4CCEBE663A56980E9397A02 (void);
// 0x000004B6 System.Void Proyecto26.RestClient::Put(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Put_m08D2C2C9C8B6F73A7531BF8BD2AC15C9C5E13680 (void);
// 0x000004B7 System.Void Proyecto26.RestClient::Put(System.String,System.Object,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x000004B8 System.Void Proyecto26.RestClient::Put(System.String,System.String,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x000004B9 System.Void Proyecto26.RestClient::Put(Proyecto26.RequestHelper,System.Action`3<Proyecto26.RequestException,Proyecto26.ResponseHelper,T>)
// 0x000004BA System.Void Proyecto26.RestClient::Delete(System.String,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Delete_m7701A6040B30FF21656087D1FFBE7BCAFA782E16 (void);
// 0x000004BB System.Void Proyecto26.RestClient::Delete(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Delete_m1C6AA27C4494A101CED7B1B6BFE9A3B38247FB60 (void);
// 0x000004BC System.Void Proyecto26.RestClient::Head(System.String,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Head_m01F7D5AEF77C54D5AE2948C1F579C1FD856AB0A8 (void);
// 0x000004BD System.Void Proyecto26.RestClient::Head(Proyecto26.RequestHelper,System.Action`2<Proyecto26.RequestException,Proyecto26.ResponseHelper>)
extern void RestClient_Head_mFEFB7B87CFC87642E95B09D1A4AB76F63ABA3E14 (void);
// 0x000004BE RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Request(Proyecto26.RequestHelper)
extern void RestClient_Request_mA130CF629F30CB634AA89655F61C5BD37F729409 (void);
// 0x000004BF RSG.IPromise`1<T> Proyecto26.RestClient::Request(Proyecto26.RequestHelper)
// 0x000004C0 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Get(System.String)
extern void RestClient_Get_m7E5DEB235C456E5FAFCA828BF688ECFD630E86F7 (void);
// 0x000004C1 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Get(Proyecto26.RequestHelper)
extern void RestClient_Get_mDEA2483857BD9E64941E12CB42063D74EEF50FE6 (void);
// 0x000004C2 RSG.IPromise`1<T> Proyecto26.RestClient::Get(System.String)
// 0x000004C3 RSG.IPromise`1<T> Proyecto26.RestClient::Get(Proyecto26.RequestHelper)
// 0x000004C4 RSG.IPromise`1<T[]> Proyecto26.RestClient::GetArray(System.String)
// 0x000004C5 RSG.IPromise`1<T[]> Proyecto26.RestClient::GetArray(Proyecto26.RequestHelper)
// 0x000004C6 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Post(System.String,System.Object)
extern void RestClient_Post_m4763A3817990302191F3A5B069A64B02419523BC (void);
// 0x000004C7 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Post(System.String,System.String)
extern void RestClient_Post_mD59FE057DB71BF372315AB100BD5E4339FFFFE07 (void);
// 0x000004C8 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Post(Proyecto26.RequestHelper)
extern void RestClient_Post_m27A12B1AAD73F990A53E62B578D01BF4EA933509 (void);
// 0x000004C9 RSG.IPromise`1<T> Proyecto26.RestClient::Post(System.String,System.Object)
// 0x000004CA RSG.IPromise`1<T> Proyecto26.RestClient::Post(System.String,System.String)
// 0x000004CB RSG.IPromise`1<T> Proyecto26.RestClient::Post(Proyecto26.RequestHelper)
// 0x000004CC RSG.IPromise`1<T[]> Proyecto26.RestClient::PostArray(System.String,System.Object)
// 0x000004CD RSG.IPromise`1<T[]> Proyecto26.RestClient::PostArray(System.String,System.String)
// 0x000004CE RSG.IPromise`1<T[]> Proyecto26.RestClient::PostArray(Proyecto26.RequestHelper)
// 0x000004CF RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Put(System.String,System.Object)
extern void RestClient_Put_mB84F593B9E21B27814D3EBAA2920B67D1699FD26 (void);
// 0x000004D0 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Put(System.String,System.String)
extern void RestClient_Put_m3E06B35E9CFAE4AF81DCF12A8F30A178E1CC5ABA (void);
// 0x000004D1 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Put(Proyecto26.RequestHelper)
extern void RestClient_Put_m5E7F007136807CD127CA4FD4D6A986B3979391F4 (void);
// 0x000004D2 RSG.IPromise`1<T> Proyecto26.RestClient::Put(System.String,System.Object)
// 0x000004D3 RSG.IPromise`1<T> Proyecto26.RestClient::Put(System.String,System.String)
// 0x000004D4 RSG.IPromise`1<T> Proyecto26.RestClient::Put(Proyecto26.RequestHelper)
// 0x000004D5 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Delete(System.String)
extern void RestClient_Delete_m7CACD86724C07540430FDA0E5D759F6E57BBF75C (void);
// 0x000004D6 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Delete(Proyecto26.RequestHelper)
extern void RestClient_Delete_m33B84121232197C62DB7D3BAFB4E5F39CEF4C474 (void);
// 0x000004D7 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Head(System.String)
extern void RestClient_Head_m6CA11D53CD3CD3724C345EA2DCEBCF665E31D101 (void);
// 0x000004D8 RSG.IPromise`1<Proyecto26.ResponseHelper> Proyecto26.RestClient::Head(Proyecto26.RequestHelper)
extern void RestClient_Head_mFE1399A7241D6D868C0AB0B65C774812167D9D43 (void);
// 0x000004D9 System.Void Proyecto26.RestClient::Promisify(RSG.Promise`1<T>,Proyecto26.RequestException,T)
// 0x000004DA System.Void Proyecto26.RestClient::Promisify(RSG.Promise`1<T>,Proyecto26.RequestException,Proyecto26.ResponseHelper,T)
// 0x000004DB System.String Proyecto26.Common.Common::GetFormSectionsContentType(System.Byte[]&,Proyecto26.RequestHelper)
extern void Common_GetFormSectionsContentType_m19E34A4A4613B617EB59187AE573639031CD6B93 (void);
// 0x000004DC System.Void Proyecto26.Common.Common::ConfigureWebRequestWithOptions(UnityEngine.Networking.UnityWebRequest,System.Byte[],System.String,Proyecto26.RequestHelper)
extern void Common_ConfigureWebRequestWithOptions_mF5668E71E221566BFD09C871A2B0667206901F78 (void);
// 0x000004DD System.Collections.IEnumerator Proyecto26.Common.Common::SendWebRequestWithOptions(UnityEngine.Networking.UnityWebRequest,Proyecto26.RequestHelper)
extern void Common_SendWebRequestWithOptions_mA6DA8F8469D1F337E3CB043E57F2191D8D95EE1B (void);
// 0x000004DE System.Void Proyecto26.Common.Common/<SendWebRequestWithOptions>d__4::.ctor(System.Int32)
extern void U3CSendWebRequestWithOptionsU3Ed__4__ctor_m47FB1CC913CF34CE244286B13D6600F97F2C7006 (void);
// 0x000004DF System.Void Proyecto26.Common.Common/<SendWebRequestWithOptions>d__4::System.IDisposable.Dispose()
extern void U3CSendWebRequestWithOptionsU3Ed__4_System_IDisposable_Dispose_m5F26553216BF51BE2953AFD9118938F28D511A34 (void);
// 0x000004E0 System.Boolean Proyecto26.Common.Common/<SendWebRequestWithOptions>d__4::MoveNext()
extern void U3CSendWebRequestWithOptionsU3Ed__4_MoveNext_mB5E4CFE2AAE6A9BCE0D76C72C378282AFCB56C68 (void);
// 0x000004E1 System.Object Proyecto26.Common.Common/<SendWebRequestWithOptions>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSendWebRequestWithOptionsU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF49002192678DCE4CE3D8D78E1E922C730FED67C (void);
// 0x000004E2 System.Void Proyecto26.Common.Common/<SendWebRequestWithOptions>d__4::System.Collections.IEnumerator.Reset()
extern void U3CSendWebRequestWithOptionsU3Ed__4_System_Collections_IEnumerator_Reset_m82497AA2724FEA6918E2C0900364DA889677B45E (void);
// 0x000004E3 System.Object Proyecto26.Common.Common/<SendWebRequestWithOptions>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CSendWebRequestWithOptionsU3Ed__4_System_Collections_IEnumerator_get_Current_mD15322133CA601A9884E3FCBDE8BD76FFFB1558D (void);
// 0x000004E4 Proyecto26.ResponseHelper Proyecto26.Common.Extensions::CreateWebResponse(UnityEngine.Networking.UnityWebRequest)
extern void Extensions_CreateWebResponse_m85A8BFBB1916DE1247943658A0758B372807A7F1 (void);
// 0x000004E5 System.Boolean Proyecto26.Common.Extensions::IsValidRequest(UnityEngine.Networking.UnityWebRequest,Proyecto26.RequestHelper)
extern void Extensions_IsValidRequest_m95295E8977C94FB722EE97C5047C3975DE5EA65A (void);
// 0x000004E6 System.String Proyecto26.Common.Extensions::EscapeURL(System.String)
extern void Extensions_EscapeURL_mAAD51E7CBC83CB8CE79715D8A6086CB153919CF7 (void);
// 0x000004E7 System.String Proyecto26.Common.Extensions::BuildUrl(System.String,System.Collections.Generic.Dictionary`2<System.String,System.String>)
extern void Extensions_BuildUrl_mE2D5E2B005BCD677EED820AE36D5F28F01E57CA0 (void);
// 0x000004E8 System.Void Proyecto26.Common.Extensions/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mDEE443CFFB4553720586AE36F8109D07F7E08B87 (void);
// 0x000004E9 System.Boolean Proyecto26.Common.Extensions/<>c__DisplayClass3_0::<BuildUrl>b__0(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern void U3CU3Ec__DisplayClass3_0_U3CBuildUrlU3Eb__0_mDFB7EBC5B80A07C2899A8B10EE18130CC2AE4157 (void);
// 0x000004EA System.Void Proyecto26.Common.Extensions/<>c::.cctor()
extern void U3CU3Ec__cctor_m51C966458A7B400E929B365351E9FA572508174F (void);
// 0x000004EB System.Void Proyecto26.Common.Extensions/<>c::.ctor()
extern void U3CU3Ec__ctor_m3AF7EC6F7B8017905D17E0078A8E8219711B52B7 (void);
// 0x000004EC System.String Proyecto26.Common.Extensions/<>c::<BuildUrl>b__3_1(System.Collections.Generic.KeyValuePair`2<System.String,System.String>)
extern void U3CU3Ec_U3CBuildUrlU3Eb__3_1_m3F16B42B55E86F33694E97ABD64E3ADB7B6EDD03 (void);
// 0x000004ED System.String Models.Photo::ToString()
extern void Photo_ToString_mC7549402494D8D3A4192988B7A75C3F43BB81C7D (void);
// 0x000004EE System.Void Models.Photo::.ctor()
extern void Photo__ctor_mFB7B355DA04B8883B308CC03347EF777898F6846 (void);
// 0x000004EF System.String Models.Post::ToString()
extern void Post_ToString_m4AD8D8586CF507C7C435254089148F3AD32A855A (void);
// 0x000004F0 System.Void Models.Post::.ctor()
extern void Post__ctor_mAD9D421E1716FEB8D8B297565CE8F10FFB42CC79 (void);
// 0x000004F1 System.String Models.Todo::ToString()
extern void Todo_ToString_mAD1AD0938A238807A2D0EA2888893C11CBE105CF (void);
// 0x000004F2 System.Void Models.Todo::.ctor()
extern void Todo__ctor_m3DBCDF59C636313C811C93C83ED5ABC292A19AA4 (void);
// 0x000004F3 System.String Models.User::ToString()
extern void User_ToString_mD57533E879F6F6B77BD1A38883A0DAED53D2FAF2 (void);
// 0x000004F4 System.Void Models.User::.ctor()
extern void User__ctor_m179E4458F95BA48DD9D0F7E1E4DF8AC26DB05C0F (void);
// 0x000004F5 SimpleJSON.JSONNodeType SimpleJSON.JSONNode::get_Tag()
// 0x000004F6 SimpleJSON.JSONNode SimpleJSON.JSONNode::get_Item(System.Int32)
extern void JSONNode_get_Item_m9B4DBE85D7742250EF918F4450E3CE020AF7C807 (void);
// 0x000004F7 System.Void SimpleJSON.JSONNode::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONNode_set_Item_m62DFE58028E55800BF80EFE6E0CE4CDE6DF00CA7 (void);
// 0x000004F8 SimpleJSON.JSONNode SimpleJSON.JSONNode::get_Item(System.String)
extern void JSONNode_get_Item_mB39E17FF10DD6C0D91FCF1EF485A73F434B34F1D (void);
// 0x000004F9 System.Void SimpleJSON.JSONNode::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONNode_set_Item_m9F8D28C00CA46F8C0BD5E43500AEAFC3A1999E07 (void);
// 0x000004FA System.String SimpleJSON.JSONNode::get_Value()
extern void JSONNode_get_Value_m8F31142225AAE4DDEC7ACB74B7FFB30A6DD672F1 (void);
// 0x000004FB System.Void SimpleJSON.JSONNode::set_Value(System.String)
extern void JSONNode_set_Value_mA046548FC6B7DCBDA4964DB05303EBA0CF2A29CA (void);
// 0x000004FC System.Int32 SimpleJSON.JSONNode::get_Count()
extern void JSONNode_get_Count_m6FD5676FECC6B26A6384D8294C213C4B032EA269 (void);
// 0x000004FD System.Boolean SimpleJSON.JSONNode::get_IsNumber()
extern void JSONNode_get_IsNumber_m682272D381670DCFA66BD9EE3C0CE3CB161E3AAD (void);
// 0x000004FE System.Boolean SimpleJSON.JSONNode::get_IsString()
extern void JSONNode_get_IsString_mE0D6737C47977364E9F1620CEA98454EAFAEB196 (void);
// 0x000004FF System.Boolean SimpleJSON.JSONNode::get_IsBoolean()
extern void JSONNode_get_IsBoolean_mA575C54AD05577A9F45E0F55355C0B40F60BFA24 (void);
// 0x00000500 System.Boolean SimpleJSON.JSONNode::get_IsNull()
extern void JSONNode_get_IsNull_m85D1B73ABCCE95314768A4E690D941B82D08CD58 (void);
// 0x00000501 System.Boolean SimpleJSON.JSONNode::get_IsArray()
extern void JSONNode_get_IsArray_m54719E46991451D5C8750D8265D0C6F91EEE97D7 (void);
// 0x00000502 System.Boolean SimpleJSON.JSONNode::get_IsObject()
extern void JSONNode_get_IsObject_mC9E7B94CB90443629157D7C2AB5ED83510DD0BD1 (void);
// 0x00000503 System.Boolean SimpleJSON.JSONNode::get_Inline()
extern void JSONNode_get_Inline_m6791C62A74ACCEA90470A7EC177FA3F12F744F09 (void);
// 0x00000504 System.Void SimpleJSON.JSONNode::set_Inline(System.Boolean)
extern void JSONNode_set_Inline_m5C5C9E182CE521A831EBD2D708568297601AD835 (void);
// 0x00000505 System.Void SimpleJSON.JSONNode::Add(System.String,SimpleJSON.JSONNode)
extern void JSONNode_Add_m10F03DC3FD017B22B831388D39FF735D5AFE96A4 (void);
// 0x00000506 System.Void SimpleJSON.JSONNode::Add(SimpleJSON.JSONNode)
extern void JSONNode_Add_m7DCFC7266181DE46B90ECC7C5DD7D5B7618F9DE2 (void);
// 0x00000507 SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(System.String)
extern void JSONNode_Remove_m49D627BAE273E902C8EB8345BDD7B33E945C2E47 (void);
// 0x00000508 SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(System.Int32)
extern void JSONNode_Remove_m3B3259E69D33121E4974FE1ACFAEBED4B034C053 (void);
// 0x00000509 SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(SimpleJSON.JSONNode)
extern void JSONNode_Remove_mD1FE270ABF87BCD7D09AAA22286AB3C315BAA0EA (void);
// 0x0000050A System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode::get_Children()
extern void JSONNode_get_Children_mB9A68101166B1C5176311D2EC1ABCC832248FDFF (void);
// 0x0000050B System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode::get_DeepChildren()
extern void JSONNode_get_DeepChildren_m3E3CE542A4856D0BD557B7E121B4CAA999C8110C (void);
// 0x0000050C System.String SimpleJSON.JSONNode::ToString()
extern void JSONNode_ToString_m9E23A359C8B7209DEED6B81DA27C8D0619F9CE98 (void);
// 0x0000050D System.String SimpleJSON.JSONNode::ToString(System.Int32)
extern void JSONNode_ToString_m9B8D0351B9B14D65ABF1090E6D07E0C69DFB84C3 (void);
// 0x0000050E System.Void SimpleJSON.JSONNode::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
// 0x0000050F SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONNode::GetEnumerator()
// 0x00000510 System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>> SimpleJSON.JSONNode::get_Linq()
extern void JSONNode_get_Linq_m568F8D242CB58C62C9D8230A0C1F684D70573268 (void);
// 0x00000511 SimpleJSON.JSONNode/KeyEnumerator SimpleJSON.JSONNode::get_Keys()
extern void JSONNode_get_Keys_m6C1FC7F6C35656362327B486F0426273234584E2 (void);
// 0x00000512 SimpleJSON.JSONNode/ValueEnumerator SimpleJSON.JSONNode::get_Values()
extern void JSONNode_get_Values_m1097622D87EFAC54681A9A9796E563DA142D7677 (void);
// 0x00000513 System.Double SimpleJSON.JSONNode::get_AsDouble()
extern void JSONNode_get_AsDouble_m0DE7D5E7712FB59B32C1C570EBBF85D597B98656 (void);
// 0x00000514 System.Void SimpleJSON.JSONNode::set_AsDouble(System.Double)
extern void JSONNode_set_AsDouble_mBA8F81DF080D29E60DDE8235663BAFFA688736D5 (void);
// 0x00000515 System.Int32 SimpleJSON.JSONNode::get_AsInt()
extern void JSONNode_get_AsInt_m743FC99FEF2DA80A8C6CB71137A5FFD28084E0FA (void);
// 0x00000516 System.Void SimpleJSON.JSONNode::set_AsInt(System.Int32)
extern void JSONNode_set_AsInt_m7CA6313AD31E9E08FEB0B6F8B92AD9A3882D9B75 (void);
// 0x00000517 System.Single SimpleJSON.JSONNode::get_AsFloat()
extern void JSONNode_get_AsFloat_m4B2A24C67F4FBF872DEA6360719D854AE1AD1FBB (void);
// 0x00000518 System.Void SimpleJSON.JSONNode::set_AsFloat(System.Single)
extern void JSONNode_set_AsFloat_m624BDD6CAF17D1709BACFDF3554F2AE11FDD22D1 (void);
// 0x00000519 System.Boolean SimpleJSON.JSONNode::get_AsBool()
extern void JSONNode_get_AsBool_m15160B79EBEA51E7A2C7C7A23B3540A60434B36D (void);
// 0x0000051A System.Void SimpleJSON.JSONNode::set_AsBool(System.Boolean)
extern void JSONNode_set_AsBool_m0878AF783E25077E17850DD1B4522B17FF08770F (void);
// 0x0000051B SimpleJSON.JSONArray SimpleJSON.JSONNode::get_AsArray()
extern void JSONNode_get_AsArray_m10801C5609C0C024480B49DFA03A4FB16A4E6829 (void);
// 0x0000051C SimpleJSON.JSONObject SimpleJSON.JSONNode::get_AsObject()
extern void JSONNode_get_AsObject_mDE74F42234B130BA44AD17DF9FFC64A2D8DFCD46 (void);
// 0x0000051D SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.String)
extern void JSONNode_op_Implicit_m3938223B519495895A5B4E53D60789BB2D4620F2 (void);
// 0x0000051E System.String SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m110AF33FB2CEF68FF905E93F656AF02222554668 (void);
// 0x0000051F SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Double)
extern void JSONNode_op_Implicit_m66EA9B62BBFD5A306C2B95703443F1960338BA01 (void);
// 0x00000520 System.Double SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m98778303F0AD762A7C4FE928D82352A1F8276EBE (void);
// 0x00000521 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Single)
extern void JSONNode_op_Implicit_m3780006769998A253BE0F8068E68BD88AC692112 (void);
// 0x00000522 System.Single SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mA90C42C0D957F5F284857AE91907D69477331166 (void);
// 0x00000523 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Int32)
extern void JSONNode_op_Implicit_mDAC018F58F31AB6333A9852BD0B038152063B3F2 (void);
// 0x00000524 System.Int32 SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m10A6B0627DE9BC247B0B39554FEA1CD37CA7988C (void);
// 0x00000525 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Boolean)
extern void JSONNode_op_Implicit_mAF077674B97BAF8FD4EB941A63B84D26F6E08EDC (void);
// 0x00000526 System.Boolean SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mE4078D38F919268986EF017D4A9EA55FD8CBA826 (void);
// 0x00000527 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>)
extern void JSONNode_op_Implicit_m92A46C2D66615E97C21041238C09386EDCF0A266 (void);
// 0x00000528 System.Boolean SimpleJSON.JSONNode::op_Equality(SimpleJSON.JSONNode,System.Object)
extern void JSONNode_op_Equality_mA320F56360F44724C465167318C8FC38F0DCA38F (void);
// 0x00000529 System.Boolean SimpleJSON.JSONNode::op_Inequality(SimpleJSON.JSONNode,System.Object)
extern void JSONNode_op_Inequality_m00AE17CECA317ECC98958F027D0545C831DA24FE (void);
// 0x0000052A System.Boolean SimpleJSON.JSONNode::Equals(System.Object)
extern void JSONNode_Equals_mC1598F5265D88ECAA3040C537A28FC2DC878BBFF (void);
// 0x0000052B System.Int32 SimpleJSON.JSONNode::GetHashCode()
extern void JSONNode_GetHashCode_mF900BB7E50D965906CDBC7210B9097CDE6B89834 (void);
// 0x0000052C System.Text.StringBuilder SimpleJSON.JSONNode::get_EscapeBuilder()
extern void JSONNode_get_EscapeBuilder_m87ACD2EE75FD74E9E7B67BBB66FCDB0643749628 (void);
// 0x0000052D System.String SimpleJSON.JSONNode::Escape(System.String)
extern void JSONNode_Escape_m5C3A578A1281224A7BDF595470FA706CBC597A68 (void);
// 0x0000052E System.Void SimpleJSON.JSONNode::ParseElement(SimpleJSON.JSONNode,System.String,System.String,System.Boolean)
extern void JSONNode_ParseElement_mC27D1FD482D97FE1550AAFC625379A7BD8DB1895 (void);
// 0x0000052F SimpleJSON.JSONNode SimpleJSON.JSONNode::Parse(System.String)
extern void JSONNode_Parse_m747C7ACB3E8F42CB32651EAB1506047F02BF4076 (void);
// 0x00000530 System.Void SimpleJSON.JSONNode::SerializeBinary(System.IO.BinaryWriter)
// 0x00000531 System.Void SimpleJSON.JSONNode::SaveToBinaryStream(System.IO.Stream)
extern void JSONNode_SaveToBinaryStream_m5CD58A850F95A069843BC02BED9B7190D4CE6F73 (void);
// 0x00000532 System.Void SimpleJSON.JSONNode::SaveToCompressedStream(System.IO.Stream)
extern void JSONNode_SaveToCompressedStream_m939EB2E9267698F54AD64849D96E4ADFCCF5A00A (void);
// 0x00000533 System.Void SimpleJSON.JSONNode::SaveToCompressedFile(System.String)
extern void JSONNode_SaveToCompressedFile_m66A1234959DDCEE44D9000255F86A8FD32EC87BC (void);
// 0x00000534 System.String SimpleJSON.JSONNode::SaveToCompressedBase64()
extern void JSONNode_SaveToCompressedBase64_mF1BB008CE575D288A986676FD23806E62C98D539 (void);
// 0x00000535 System.Void SimpleJSON.JSONNode::SaveToBinaryFile(System.String)
extern void JSONNode_SaveToBinaryFile_m4C3B67B465C870818ED92E4FC14F9B9527734EC1 (void);
// 0x00000536 System.String SimpleJSON.JSONNode::SaveToBinaryBase64()
extern void JSONNode_SaveToBinaryBase64_m31110029F0029D7416604433C849EBF33A7F12B5 (void);
// 0x00000537 SimpleJSON.JSONNode SimpleJSON.JSONNode::DeserializeBinary(System.IO.BinaryReader)
extern void JSONNode_DeserializeBinary_mE398A9C5CB421A0CCC20A18486D83964E36DAE45 (void);
// 0x00000538 SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromCompressedFile(System.String)
extern void JSONNode_LoadFromCompressedFile_m50BB9E1D951C7444EFE1FE570894E5A92BA35275 (void);
// 0x00000539 SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromCompressedStream(System.IO.Stream)
extern void JSONNode_LoadFromCompressedStream_m6D59AB50712DF46BE7D37DB5EDB8EDD7C0B6D04D (void);
// 0x0000053A SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromCompressedBase64(System.String)
extern void JSONNode_LoadFromCompressedBase64_mBC74C1DE9C65EF47BA738413E2140F1370A9F433 (void);
// 0x0000053B SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromBinaryStream(System.IO.Stream)
extern void JSONNode_LoadFromBinaryStream_m540612C537938B1403281538E5B33BCFAFF29B46 (void);
// 0x0000053C SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromBinaryFile(System.String)
extern void JSONNode_LoadFromBinaryFile_m47D1B956E15231FD6537A96C09B317F821374B1E (void);
// 0x0000053D SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromBinaryBase64(System.String)
extern void JSONNode_LoadFromBinaryBase64_m91E331970C76EE0AE16B3FFFE8535321EADE65E0 (void);
// 0x0000053E SimpleJSON.JSONNode SimpleJSON.JSONNode::GetContainer(SimpleJSON.JSONContainerType)
extern void JSONNode_GetContainer_m8A2362DAE7D92FC4A7977415F5618EBE892DE819 (void);
// 0x0000053F SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(UnityEngine.Vector2)
extern void JSONNode_op_Implicit_m7F01CA65EDCA916BD0BFB9BFEC73444FB26D59FD (void);
// 0x00000540 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(UnityEngine.Vector3)
extern void JSONNode_op_Implicit_mDD9DF895CC3317F2C6D8E325E6133C1FE0954864 (void);
// 0x00000541 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(UnityEngine.Vector4)
extern void JSONNode_op_Implicit_mDF1EE24EF1759B2C134B0804A3497B2BFAF0A348 (void);
// 0x00000542 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(UnityEngine.Quaternion)
extern void JSONNode_op_Implicit_m3B87FAAC3847B6EDAF525A9240A6AAD7489219B3 (void);
// 0x00000543 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(UnityEngine.Rect)
extern void JSONNode_op_Implicit_m4B12356D2C135933B13ACB20AAA0BCA93221A308 (void);
// 0x00000544 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(UnityEngine.RectOffset)
extern void JSONNode_op_Implicit_m880C05AE8E8F125C0FEC37BE1C32F5D2A8D32B70 (void);
// 0x00000545 UnityEngine.Vector2 SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m50E48009BD81DB50555DADF99F5C424EAC0007FB (void);
// 0x00000546 UnityEngine.Vector3 SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m94957CEB32B45139355EE8C3D85B4C9DDA107484 (void);
// 0x00000547 UnityEngine.Vector4 SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m01E7C3C12602B1EFB9EDA7A4812536D44C1CC27D (void);
// 0x00000548 UnityEngine.Quaternion SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m39284C070FA8F77D4A75A67E44ECFDC033719CBA (void);
// 0x00000549 UnityEngine.Rect SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m1F0EDFBF473C04FAF2A76125C40D27AFBF427E56 (void);
// 0x0000054A UnityEngine.RectOffset SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m4F12BB544F8D9E6DC387FC276BAE60269715326C (void);
// 0x0000054B UnityEngine.Vector2 SimpleJSON.JSONNode::ReadVector2(UnityEngine.Vector2)
extern void JSONNode_ReadVector2_mB5168EB80BA8FE7D38324BB66580CDE962B5BE7B (void);
// 0x0000054C UnityEngine.Vector2 SimpleJSON.JSONNode::ReadVector2(System.String,System.String)
extern void JSONNode_ReadVector2_mE9045E685C7EA3CFE8546153F99152CCBE61A18A (void);
// 0x0000054D UnityEngine.Vector2 SimpleJSON.JSONNode::ReadVector2()
extern void JSONNode_ReadVector2_m80B259CF2BDAFA309EAF5B6B2C78AED2EB528A15 (void);
// 0x0000054E SimpleJSON.JSONNode SimpleJSON.JSONNode::WriteVector2(UnityEngine.Vector2,System.String,System.String)
extern void JSONNode_WriteVector2_m78FD594CD3963CDAC2B1BF2DF26220692FD54DAC (void);
// 0x0000054F UnityEngine.Vector3 SimpleJSON.JSONNode::ReadVector3(UnityEngine.Vector3)
extern void JSONNode_ReadVector3_mC7A091BF1F3DE71655260268551A9E449EDAA1B6 (void);
// 0x00000550 UnityEngine.Vector3 SimpleJSON.JSONNode::ReadVector3(System.String,System.String,System.String)
extern void JSONNode_ReadVector3_m1F04E19E177F89BB76CA022C70A975C08061F725 (void);
// 0x00000551 UnityEngine.Vector3 SimpleJSON.JSONNode::ReadVector3()
extern void JSONNode_ReadVector3_mF41BE2C41613AFEA70CAB539AFA7D0A6F573AB9C (void);
// 0x00000552 SimpleJSON.JSONNode SimpleJSON.JSONNode::WriteVector3(UnityEngine.Vector3,System.String,System.String,System.String)
extern void JSONNode_WriteVector3_mAE65146631E220583C90A007F05DE374A7E3D067 (void);
// 0x00000553 UnityEngine.Vector4 SimpleJSON.JSONNode::ReadVector4(UnityEngine.Vector4)
extern void JSONNode_ReadVector4_m2B894C5C5513E9454511DDAAFE7C3A416470907D (void);
// 0x00000554 UnityEngine.Vector4 SimpleJSON.JSONNode::ReadVector4()
extern void JSONNode_ReadVector4_m13FF00E78429AC6F1E1284539C759AB69BDF1CD6 (void);
// 0x00000555 SimpleJSON.JSONNode SimpleJSON.JSONNode::WriteVector4(UnityEngine.Vector4)
extern void JSONNode_WriteVector4_m1210A9104B4EFE99047D7AFE6AB10DD01A73D27B (void);
// 0x00000556 UnityEngine.Quaternion SimpleJSON.JSONNode::ReadQuaternion(UnityEngine.Quaternion)
extern void JSONNode_ReadQuaternion_m6EEE1B3ADE85662B1363029EE602CA1EC02F8DCF (void);
// 0x00000557 UnityEngine.Quaternion SimpleJSON.JSONNode::ReadQuaternion()
extern void JSONNode_ReadQuaternion_m6147D59E720DA900144A556FCB4365FCC2A71A32 (void);
// 0x00000558 SimpleJSON.JSONNode SimpleJSON.JSONNode::WriteQuaternion(UnityEngine.Quaternion)
extern void JSONNode_WriteQuaternion_m2A11E75564526030F3EF8D276E578F39AA18CCA1 (void);
// 0x00000559 UnityEngine.Rect SimpleJSON.JSONNode::ReadRect(UnityEngine.Rect)
extern void JSONNode_ReadRect_m831CAA59B68FD465C7739CE3DE1C297D8A23B023 (void);
// 0x0000055A UnityEngine.Rect SimpleJSON.JSONNode::ReadRect()
extern void JSONNode_ReadRect_m5DA61EE22CAA874BD2457B060F6286A7FB1DBFB4 (void);
// 0x0000055B SimpleJSON.JSONNode SimpleJSON.JSONNode::WriteRect(UnityEngine.Rect)
extern void JSONNode_WriteRect_m3614416241A91C0EDAB54E5645D7A5D9B7AADB55 (void);
// 0x0000055C UnityEngine.RectOffset SimpleJSON.JSONNode::ReadRectOffset(UnityEngine.RectOffset)
extern void JSONNode_ReadRectOffset_m830101CD2863A9DAA92A65983D5F7E44439522E0 (void);
// 0x0000055D UnityEngine.RectOffset SimpleJSON.JSONNode::ReadRectOffset()
extern void JSONNode_ReadRectOffset_mA8D64472E6F8F695EED27E33E13B204BD87194C8 (void);
// 0x0000055E SimpleJSON.JSONNode SimpleJSON.JSONNode::WriteRectOffset(UnityEngine.RectOffset)
extern void JSONNode_WriteRectOffset_m1146232B54BE25B8E9AC363C776E4B8A40C8A789 (void);
// 0x0000055F UnityEngine.Matrix4x4 SimpleJSON.JSONNode::ReadMatrix()
extern void JSONNode_ReadMatrix_mCE78E3CAC6BE0132EBF82990DB741A12E1B3030C (void);
// 0x00000560 SimpleJSON.JSONNode SimpleJSON.JSONNode::WriteMatrix(UnityEngine.Matrix4x4)
extern void JSONNode_WriteMatrix_m7F72D94C9EDC6B9E590231A27FFE6C52F200EA23 (void);
// 0x00000561 System.Void SimpleJSON.JSONNode::.ctor()
extern void JSONNode__ctor_mF0692814A0795056AE052B05EF63D6B216B5619E (void);
// 0x00000562 System.Void SimpleJSON.JSONNode::.cctor()
extern void JSONNode__cctor_m0F95CA9754AE155CEC873DEFB28A83E8AAC25990 (void);
// 0x00000563 System.Boolean SimpleJSON.JSONNode/Enumerator::get_IsValid()
extern void Enumerator_get_IsValid_m078841FA11B23FAA03C8C0F0F5CFE1C7A33CC267 (void);
// 0x00000564 System.Void SimpleJSON.JSONNode/Enumerator::.ctor(System.Collections.Generic.List`1/Enumerator<SimpleJSON.JSONNode>)
extern void Enumerator__ctor_m0A9EDBF3F33AA5FAE87C204FA885D965D16E8A44 (void);
// 0x00000565 System.Void SimpleJSON.JSONNode/Enumerator::.ctor(System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJSON.JSONNode>)
extern void Enumerator__ctor_mDE6D3B06DF10240F0C637131119C8B09CE9EC4DC (void);
// 0x00000566 System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONNode/Enumerator::get_Current()
extern void Enumerator_get_Current_m6C700A8C86DAB32F9FE845C3BE1E35D17757C65C (void);
// 0x00000567 System.Boolean SimpleJSON.JSONNode/Enumerator::MoveNext()
extern void Enumerator_MoveNext_m095925BE3D881DF1E015997DBEAFE0851722E53E (void);
// 0x00000568 System.Void SimpleJSON.JSONNode/ValueEnumerator::.ctor(System.Collections.Generic.List`1/Enumerator<SimpleJSON.JSONNode>)
extern void ValueEnumerator__ctor_m1FF9C4191C706E48C3F9A191D0CE02F9BECDCE45 (void);
// 0x00000569 System.Void SimpleJSON.JSONNode/ValueEnumerator::.ctor(System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJSON.JSONNode>)
extern void ValueEnumerator__ctor_m8143FFB5AD9AB3936DF1A59E3492C2CF81793AF5 (void);
// 0x0000056A System.Void SimpleJSON.JSONNode/ValueEnumerator::.ctor(SimpleJSON.JSONNode/Enumerator)
extern void ValueEnumerator__ctor_mE92051B3948C6A45D4F9265EA6D8329D70649D06 (void);
// 0x0000056B SimpleJSON.JSONNode SimpleJSON.JSONNode/ValueEnumerator::get_Current()
extern void ValueEnumerator_get_Current_mAB38FFFD4C1293C61EE73C825D85B7258B61961C (void);
// 0x0000056C System.Boolean SimpleJSON.JSONNode/ValueEnumerator::MoveNext()
extern void ValueEnumerator_MoveNext_mE68E69AFB9D6FA2B41F55B066F3295490EED5589 (void);
// 0x0000056D SimpleJSON.JSONNode/ValueEnumerator SimpleJSON.JSONNode/ValueEnumerator::GetEnumerator()
extern void ValueEnumerator_GetEnumerator_mF841522782073D8D5F9782A37B2487BEA63E5E09 (void);
// 0x0000056E System.Void SimpleJSON.JSONNode/KeyEnumerator::.ctor(System.Collections.Generic.List`1/Enumerator<SimpleJSON.JSONNode>)
extern void KeyEnumerator__ctor_m6E59374D6FF2E994DFB9BFCA3759CC335CA48D23 (void);
// 0x0000056F System.Void SimpleJSON.JSONNode/KeyEnumerator::.ctor(System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJSON.JSONNode>)
extern void KeyEnumerator__ctor_mB67A74FB33429333A208FA269F011122C1AB6395 (void);
// 0x00000570 System.Void SimpleJSON.JSONNode/KeyEnumerator::.ctor(SimpleJSON.JSONNode/Enumerator)
extern void KeyEnumerator__ctor_m5F83F3F252DE00DB6AC2AE59FD501F921C56F224 (void);
// 0x00000571 SimpleJSON.JSONNode SimpleJSON.JSONNode/KeyEnumerator::get_Current()
extern void KeyEnumerator_get_Current_mAC153090E0D57451FE61CA3651CF32A4CE21477C (void);
// 0x00000572 System.Boolean SimpleJSON.JSONNode/KeyEnumerator::MoveNext()
extern void KeyEnumerator_MoveNext_mFF5FB4ECB623B58733E9B85846A296853462F2F4 (void);
// 0x00000573 SimpleJSON.JSONNode/KeyEnumerator SimpleJSON.JSONNode/KeyEnumerator::GetEnumerator()
extern void KeyEnumerator_GetEnumerator_m4534071ECBA3E7165BB0DC642EA9E827A29F5A87 (void);
// 0x00000574 System.Void SimpleJSON.JSONNode/LinqEnumerator::.ctor(SimpleJSON.JSONNode)
extern void LinqEnumerator__ctor_m711F6A5F82CC6093E5245A51BFEF95B0355E2FAC (void);
// 0x00000575 System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONNode/LinqEnumerator::get_Current()
extern void LinqEnumerator_get_Current_m7F293F41FED328A040E5BC71C7F4560C3B8C295C (void);
// 0x00000576 System.Object SimpleJSON.JSONNode/LinqEnumerator::System.Collections.IEnumerator.get_Current()
extern void LinqEnumerator_System_Collections_IEnumerator_get_Current_m36203F845AA95DEB756D145E03469EF077B79BF1 (void);
// 0x00000577 System.Boolean SimpleJSON.JSONNode/LinqEnumerator::MoveNext()
extern void LinqEnumerator_MoveNext_m547BA7FD679ED14C3E698763EBEC58333194B9EE (void);
// 0x00000578 System.Void SimpleJSON.JSONNode/LinqEnumerator::Dispose()
extern void LinqEnumerator_Dispose_mF5270BF1DEAB954E8FC65435DF6D6FD5DBE1C21F (void);
// 0x00000579 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>> SimpleJSON.JSONNode/LinqEnumerator::GetEnumerator()
extern void LinqEnumerator_GetEnumerator_m356D1E4E1EC2D215962F677F3C6AE538E3CF9A20 (void);
// 0x0000057A System.Void SimpleJSON.JSONNode/LinqEnumerator::Reset()
extern void LinqEnumerator_Reset_mCC3E0E49D5343E81C63A2B4A28A7268F628A2522 (void);
// 0x0000057B System.Collections.IEnumerator SimpleJSON.JSONNode/LinqEnumerator::System.Collections.IEnumerable.GetEnumerator()
extern void LinqEnumerator_System_Collections_IEnumerable_GetEnumerator_m713D8655544E9FC2911762CD17D215B5763AE735 (void);
// 0x0000057C System.Void SimpleJSON.JSONNode/<get_Children>d__39::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__39__ctor_m0BF33D6A9ACF56C849BB64AB175B43D7692499A8 (void);
// 0x0000057D System.Void SimpleJSON.JSONNode/<get_Children>d__39::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__39_System_IDisposable_Dispose_mFD004DCB3358CE2D7764E776A031A077B21BEB29 (void);
// 0x0000057E System.Boolean SimpleJSON.JSONNode/<get_Children>d__39::MoveNext()
extern void U3Cget_ChildrenU3Ed__39_MoveNext_m858F052BAF7771FE605FCE68EC05F97FA5143538 (void);
// 0x0000057F SimpleJSON.JSONNode SimpleJSON.JSONNode/<get_Children>d__39::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__39_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mB88A038112706EAD24B23A14267B794021C761C2 (void);
// 0x00000580 System.Void SimpleJSON.JSONNode/<get_Children>d__39::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerator_Reset_m716ACA15194120CF8192937025FD3D4D7F8C710F (void);
// 0x00000581 System.Object SimpleJSON.JSONNode/<get_Children>d__39::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerator_get_Current_m831C58F860DBA03146F00BB7DC77DD1852943881 (void);
// 0x00000582 System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode/<get_Children>d__39::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__39_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mC22210E7538A1A259E64227EEAC5440FE868ED60 (void);
// 0x00000583 System.Collections.IEnumerator SimpleJSON.JSONNode/<get_Children>d__39::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerable_GetEnumerator_mDB57DC3D1B30B06919F6F3985E3B3564974C1CAA (void);
// 0x00000584 System.Void SimpleJSON.JSONNode/<get_DeepChildren>d__41::.ctor(System.Int32)
extern void U3Cget_DeepChildrenU3Ed__41__ctor_mAF92DE35182876CDDB7F9E2831AA0078B111A5F9 (void);
// 0x00000585 System.Void SimpleJSON.JSONNode/<get_DeepChildren>d__41::System.IDisposable.Dispose()
extern void U3Cget_DeepChildrenU3Ed__41_System_IDisposable_Dispose_m0C1243483A3118059E0FB871E404B606DD55CBBD (void);
// 0x00000586 System.Boolean SimpleJSON.JSONNode/<get_DeepChildren>d__41::MoveNext()
extern void U3Cget_DeepChildrenU3Ed__41_MoveNext_mBC10790834A0F6E35D8D274754EF1C77A320E391 (void);
// 0x00000587 System.Void SimpleJSON.JSONNode/<get_DeepChildren>d__41::<>m__Finally1()
extern void U3Cget_DeepChildrenU3Ed__41_U3CU3Em__Finally1_m90DA0BA712B5459B1157239B9995E01820163928 (void);
// 0x00000588 System.Void SimpleJSON.JSONNode/<get_DeepChildren>d__41::<>m__Finally2()
extern void U3Cget_DeepChildrenU3Ed__41_U3CU3Em__Finally2_mB8A69D17B39DA453A0811F66B86B5426EF3C74DA (void);
// 0x00000589 SimpleJSON.JSONNode SimpleJSON.JSONNode/<get_DeepChildren>d__41::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_DeepChildrenU3Ed__41_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mF12A4534E7D4122F67B420801B2522BC9F839868 (void);
// 0x0000058A System.Void SimpleJSON.JSONNode/<get_DeepChildren>d__41::System.Collections.IEnumerator.Reset()
extern void U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerator_Reset_m3F120F44D1BEB26137AF14E11165003EF8E18190 (void);
// 0x0000058B System.Object SimpleJSON.JSONNode/<get_DeepChildren>d__41::System.Collections.IEnumerator.get_Current()
extern void U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerator_get_Current_mF43CD05D3BF0CD3E76A265F0A684D43EFA4F22EC (void);
// 0x0000058C System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode/<get_DeepChildren>d__41::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_DeepChildrenU3Ed__41_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mAD435B3462420F0485567562B0FF3F1DB60B6AF8 (void);
// 0x0000058D System.Collections.IEnumerator SimpleJSON.JSONNode/<get_DeepChildren>d__41::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerable_GetEnumerator_m554A68924D4655591E09B01C0EFF3A9204E5E117 (void);
// 0x0000058E System.Boolean SimpleJSON.JSONArray::get_Inline()
extern void JSONArray_get_Inline_mCCA0FAE052593C420DF7266D9030F7CBCFF5D43A (void);
// 0x0000058F System.Void SimpleJSON.JSONArray::set_Inline(System.Boolean)
extern void JSONArray_set_Inline_mCDD35E23A81DC4DF48DB54FA164B2D05CA8CDD5C (void);
// 0x00000590 SimpleJSON.JSONNodeType SimpleJSON.JSONArray::get_Tag()
extern void JSONArray_get_Tag_mA6475F4E3B27AE262DFF53A510E5DD9CA9A82212 (void);
// 0x00000591 System.Boolean SimpleJSON.JSONArray::get_IsArray()
extern void JSONArray_get_IsArray_m015BCD163D49042D8827D8B5880EAC8720B93DDD (void);
// 0x00000592 SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONArray::GetEnumerator()
extern void JSONArray_GetEnumerator_m107028482740A363BC3AAC4348F729F49094B8D7 (void);
// 0x00000593 SimpleJSON.JSONNode SimpleJSON.JSONArray::get_Item(System.Int32)
extern void JSONArray_get_Item_mB56A68792D63083DAE88CF576C4E5C9C6D0E1AEC (void);
// 0x00000594 System.Void SimpleJSON.JSONArray::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONArray_set_Item_m8D70249BB8D717AD878AFE87F9F86345CD5DE9EF (void);
// 0x00000595 SimpleJSON.JSONNode SimpleJSON.JSONArray::get_Item(System.String)
extern void JSONArray_get_Item_m5E864AF954B22AD5841E8CEB58AF68DED45C4C8B (void);
// 0x00000596 System.Void SimpleJSON.JSONArray::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONArray_set_Item_mD6DB253A66DC2D34179D9773A1ADF25A11FE7DDE (void);
// 0x00000597 System.Int32 SimpleJSON.JSONArray::get_Count()
extern void JSONArray_get_Count_m8649900F00FBEDB004834FB9D814F848E6C7D72B (void);
// 0x00000598 System.Void SimpleJSON.JSONArray::Add(System.String,SimpleJSON.JSONNode)
extern void JSONArray_Add_m14182F062E171457670DB45BE811A46A78B2D685 (void);
// 0x00000599 SimpleJSON.JSONNode SimpleJSON.JSONArray::Remove(System.Int32)
extern void JSONArray_Remove_m65505E642765169F35A9F21760C7EDC39407B9F9 (void);
// 0x0000059A SimpleJSON.JSONNode SimpleJSON.JSONArray::Remove(SimpleJSON.JSONNode)
extern void JSONArray_Remove_mA79C09C43B22EE2B4965C1D8B3794BAE1957037E (void);
// 0x0000059B System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONArray::get_Children()
extern void JSONArray_get_Children_m56D8A696E09F5EEFF1FF9F8BE06011CE5EAFA197 (void);
// 0x0000059C System.Void SimpleJSON.JSONArray::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONArray_WriteToStringBuilder_m64C54C1D36684A7CE5EAB86E3350AE4B811D40C9 (void);
// 0x0000059D System.Void SimpleJSON.JSONArray::SerializeBinary(System.IO.BinaryWriter)
extern void JSONArray_SerializeBinary_m8BB39836ADBAC72EB4DD67C37A3CFC44FFB8333C (void);
// 0x0000059E System.Void SimpleJSON.JSONArray::.ctor()
extern void JSONArray__ctor_m2B6B15254CC3578F4B78A9E5CA656C4D25119512 (void);
// 0x0000059F System.Void SimpleJSON.JSONArray/<get_Children>d__22::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__22__ctor_m95EBCC8BC9794B6D9F4C6B5885704BF181FC9DBF (void);
// 0x000005A0 System.Void SimpleJSON.JSONArray/<get_Children>d__22::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__22_System_IDisposable_Dispose_mCBFE04EF85659884667F66453D36323A90F4A37E (void);
// 0x000005A1 System.Boolean SimpleJSON.JSONArray/<get_Children>d__22::MoveNext()
extern void U3Cget_ChildrenU3Ed__22_MoveNext_m465E0B8D3164E27BE47D02A0ACC56B5B795DBB95 (void);
// 0x000005A2 System.Void SimpleJSON.JSONArray/<get_Children>d__22::<>m__Finally1()
extern void U3Cget_ChildrenU3Ed__22_U3CU3Em__Finally1_mCA65F3342A396F12C190A1654CC8019248A0BAB4 (void);
// 0x000005A3 SimpleJSON.JSONNode SimpleJSON.JSONArray/<get_Children>d__22::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mA7F43EFC69B364F48AC29BF1C37A5001990511FE (void);
// 0x000005A4 System.Void SimpleJSON.JSONArray/<get_Children>d__22::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_Reset_m531EF5FB0BD16A29769CBDE11FDF26AEC2D55F5E (void);
// 0x000005A5 System.Object SimpleJSON.JSONArray/<get_Children>d__22::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_get_Current_m9C20E2FAE0940D92F51C5E1C9D804E406BF2F34F (void);
// 0x000005A6 System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONArray/<get_Children>d__22::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m7F40779C5E4EF43C9898EFDECF0FA816A7E5DCFA (void);
// 0x000005A7 System.Collections.IEnumerator SimpleJSON.JSONArray/<get_Children>d__22::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerable_GetEnumerator_mCC77091552DD207B63E4B7C2B33693B4467ED68D (void);
// 0x000005A8 System.Boolean SimpleJSON.JSONObject::get_Inline()
extern void JSONObject_get_Inline_mEBB1EA4AB1EE32D80F2DB4C5C4240DD13593750A (void);
// 0x000005A9 System.Void SimpleJSON.JSONObject::set_Inline(System.Boolean)
extern void JSONObject_set_Inline_mDDC84E63241E060414987E3B8301F7F1AB2967B4 (void);
// 0x000005AA SimpleJSON.JSONNodeType SimpleJSON.JSONObject::get_Tag()
extern void JSONObject_get_Tag_m0F8454A3B161D9954B2C1B49945DE0E9041907B6 (void);
// 0x000005AB System.Boolean SimpleJSON.JSONObject::get_IsObject()
extern void JSONObject_get_IsObject_m6E6B9273D50B23643A6DFBD17FBEA6BD6143E12C (void);
// 0x000005AC SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONObject::GetEnumerator()
extern void JSONObject_GetEnumerator_mC0AAAB9C27211FED4A7DA97BB1D2DEA1777CABD6 (void);
// 0x000005AD SimpleJSON.JSONNode SimpleJSON.JSONObject::get_Item(System.String)
extern void JSONObject_get_Item_mF7EF352F2BA035D7EFC5902F2DEB12168E586CBD (void);
// 0x000005AE System.Void SimpleJSON.JSONObject::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONObject_set_Item_m84B17410261AD3D4C06D551BB169126C42331DD8 (void);
// 0x000005AF SimpleJSON.JSONNode SimpleJSON.JSONObject::get_Item(System.Int32)
extern void JSONObject_get_Item_m28A01147850D0D538204316C35A6E1A823602C0E (void);
// 0x000005B0 System.Void SimpleJSON.JSONObject::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONObject_set_Item_m17A462CA68BCC6AB9A077BD2D5354C2CD9BAA448 (void);
// 0x000005B1 System.Int32 SimpleJSON.JSONObject::get_Count()
extern void JSONObject_get_Count_m5271E2DB59F0A774A71E1313A43FC47D209DE4BD (void);
// 0x000005B2 System.Void SimpleJSON.JSONObject::Add(System.String,SimpleJSON.JSONNode)
extern void JSONObject_Add_m89A24748B1E3A3BC627C355AD3F946A5541D0D47 (void);
// 0x000005B3 SimpleJSON.JSONNode SimpleJSON.JSONObject::Remove(System.String)
extern void JSONObject_Remove_m20C910052474109A5D0B8C63DA98648296C1E846 (void);
// 0x000005B4 SimpleJSON.JSONNode SimpleJSON.JSONObject::Remove(System.Int32)
extern void JSONObject_Remove_m6A66604EDD3CBED40F44D85BAD15342B133A2B21 (void);
// 0x000005B5 SimpleJSON.JSONNode SimpleJSON.JSONObject::Remove(SimpleJSON.JSONNode)
extern void JSONObject_Remove_m2000AEFD68838D8F04D250DDFF4E5B2B52D2661A (void);
// 0x000005B6 System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONObject::get_Children()
extern void JSONObject_get_Children_mDC37B11CC04E072B9E1DFE56A3B176A1E22F8E6D (void);
// 0x000005B7 System.Void SimpleJSON.JSONObject::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONObject_WriteToStringBuilder_m8B9F8C4C58F878A4992C39D21E75CF735DB65F61 (void);
// 0x000005B8 System.Void SimpleJSON.JSONObject::SerializeBinary(System.IO.BinaryWriter)
extern void JSONObject_SerializeBinary_mD11FD4D432B7BA08AC16D504396247E871FBCDEA (void);
// 0x000005B9 System.Void SimpleJSON.JSONObject::.ctor()
extern void JSONObject__ctor_m159A8A4A6AD38E7E4786F8EA8FFB9948DF520F92 (void);
// 0x000005BA System.Void SimpleJSON.JSONObject/<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_mC0EC999DE1ACB8335F31D6D652C0379FF2D065A6 (void);
// 0x000005BB System.Boolean SimpleJSON.JSONObject/<>c__DisplayClass21_0::<Remove>b__0(System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>)
extern void U3CU3Ec__DisplayClass21_0_U3CRemoveU3Eb__0_m4DAF41E69ADC8210F10A0E9353204C4C9BB593F0 (void);
// 0x000005BC System.Void SimpleJSON.JSONObject/<get_Children>d__23::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__23__ctor_m24E00E4270F8E59C9ECF5B85CDB1A34683FC1141 (void);
// 0x000005BD System.Void SimpleJSON.JSONObject/<get_Children>d__23::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__23_System_IDisposable_Dispose_m413B24FB7D7B0156DFC7E240DA8391C859440847 (void);
// 0x000005BE System.Boolean SimpleJSON.JSONObject/<get_Children>d__23::MoveNext()
extern void U3Cget_ChildrenU3Ed__23_MoveNext_m3AD86370936CBDF4689019E0369B72B54B4B4BE0 (void);
// 0x000005BF System.Void SimpleJSON.JSONObject/<get_Children>d__23::<>m__Finally1()
extern void U3Cget_ChildrenU3Ed__23_U3CU3Em__Finally1_m86405C8AACDCB84C101AB4D4812392161C83D82C (void);
// 0x000005C0 SimpleJSON.JSONNode SimpleJSON.JSONObject/<get_Children>d__23::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m226D4CC6A995605411240005C904F701A7E7D224 (void);
// 0x000005C1 System.Void SimpleJSON.JSONObject/<get_Children>d__23::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_Reset_m60B145CE869947C90DB4665A23EDDF48AE63AC25 (void);
// 0x000005C2 System.Object SimpleJSON.JSONObject/<get_Children>d__23::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_get_Current_m51D1A00926BFE5BDA287FE9A878C6E949F9D0DBB (void);
// 0x000005C3 System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONObject/<get_Children>d__23::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mBE8B0928518FD2517C5F344791F32EA64F13C45C (void);
// 0x000005C4 System.Collections.IEnumerator SimpleJSON.JSONObject/<get_Children>d__23::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerable_GetEnumerator_m19CBB01715DEE09E679303048771E8ABC2A2D4C0 (void);
// 0x000005C5 SimpleJSON.JSONNodeType SimpleJSON.JSONString::get_Tag()
extern void JSONString_get_Tag_m8CD40E0F8FEA6E68B02D661527AAB26E581AD925 (void);
// 0x000005C6 System.Boolean SimpleJSON.JSONString::get_IsString()
extern void JSONString_get_IsString_mE62F3299F2BDCFA5704FEFBB6D6174C7D18EFEA0 (void);
// 0x000005C7 SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONString::GetEnumerator()
extern void JSONString_GetEnumerator_m2DB5AD3DA3FA00B24522A9083E0EC92952D28C14 (void);
// 0x000005C8 System.String SimpleJSON.JSONString::get_Value()
extern void JSONString_get_Value_m0A72F580BA1EBD98A7592810069BDF215BC737F0 (void);
// 0x000005C9 System.Void SimpleJSON.JSONString::set_Value(System.String)
extern void JSONString_set_Value_m9B7FCFDF82ED801A434AC208F52582A2B0DF2164 (void);
// 0x000005CA System.Void SimpleJSON.JSONString::.ctor(System.String)
extern void JSONString__ctor_mE09DCD65C60DE37D1F0F79BD6902E464EE1960A3 (void);
// 0x000005CB System.Void SimpleJSON.JSONString::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONString_WriteToStringBuilder_mB338AAA2644A34B03F6DBE6620F8370C64D15627 (void);
// 0x000005CC System.Boolean SimpleJSON.JSONString::Equals(System.Object)
extern void JSONString_Equals_m9709B63863E6B7E1000EF9E0318B2C499D325B92 (void);
// 0x000005CD System.Int32 SimpleJSON.JSONString::GetHashCode()
extern void JSONString_GetHashCode_m573FB034DD33C5A300F358A318A0BCA761E35AB1 (void);
// 0x000005CE System.Void SimpleJSON.JSONString::SerializeBinary(System.IO.BinaryWriter)
extern void JSONString_SerializeBinary_m155A6E6D4045E9859823E7811A1434C0A0006654 (void);
// 0x000005CF SimpleJSON.JSONNodeType SimpleJSON.JSONNumber::get_Tag()
extern void JSONNumber_get_Tag_mC3886860EBAEBED98DF4B58A2E5D2DA1C86D0DF0 (void);
// 0x000005D0 System.Boolean SimpleJSON.JSONNumber::get_IsNumber()
extern void JSONNumber_get_IsNumber_m55E3251DC2B157F6A06A78047C8B7F6A8710CFC1 (void);
// 0x000005D1 SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONNumber::GetEnumerator()
extern void JSONNumber_GetEnumerator_m48371CA028B151ED79247AB9B49B7B120C172F78 (void);
// 0x000005D2 System.String SimpleJSON.JSONNumber::get_Value()
extern void JSONNumber_get_Value_m02C36CBFEEDCE75834055B25A6F0F9845B661B98 (void);
// 0x000005D3 System.Void SimpleJSON.JSONNumber::set_Value(System.String)
extern void JSONNumber_set_Value_m437CA8FBE33C4A5C6CA4E2951767974987142A01 (void);
// 0x000005D4 System.Double SimpleJSON.JSONNumber::get_AsDouble()
extern void JSONNumber_get_AsDouble_m181BFF83082F0A4A380023EDE5D1EA55B0EC6427 (void);
// 0x000005D5 System.Void SimpleJSON.JSONNumber::set_AsDouble(System.Double)
extern void JSONNumber_set_AsDouble_m48E57B49311F9720098069A63A056AF5B44AECBE (void);
// 0x000005D6 System.Void SimpleJSON.JSONNumber::.ctor(System.Double)
extern void JSONNumber__ctor_mD0AF1324557B9FC7B1D63842D2CA65EC2D1A643A (void);
// 0x000005D7 System.Void SimpleJSON.JSONNumber::.ctor(System.String)
extern void JSONNumber__ctor_m03A803840EF273185C5C63642FAA70FAF9DFE40E (void);
// 0x000005D8 System.Void SimpleJSON.JSONNumber::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONNumber_WriteToStringBuilder_m48A94BA46F195E37F78A7AA167655AE1E870231A (void);
// 0x000005D9 System.Boolean SimpleJSON.JSONNumber::IsNumeric(System.Object)
extern void JSONNumber_IsNumeric_mEA4ED6C1587A4D94F5D6F30B12FA284AB56F11EB (void);
// 0x000005DA System.Boolean SimpleJSON.JSONNumber::Equals(System.Object)
extern void JSONNumber_Equals_m0D80E8BA86AA78FBB0482F9F053A0BACB65D8D97 (void);
// 0x000005DB System.Int32 SimpleJSON.JSONNumber::GetHashCode()
extern void JSONNumber_GetHashCode_mB10776EF12594532AB12FA617D1BF18FC53C790C (void);
// 0x000005DC System.Void SimpleJSON.JSONNumber::SerializeBinary(System.IO.BinaryWriter)
extern void JSONNumber_SerializeBinary_m0250E8C465049F13DF32A2553DC76191A2012C7A (void);
// 0x000005DD SimpleJSON.JSONNodeType SimpleJSON.JSONBool::get_Tag()
extern void JSONBool_get_Tag_m43F4216FE45F0043979093D7BE5FD16C0F462A62 (void);
// 0x000005DE System.Boolean SimpleJSON.JSONBool::get_IsBoolean()
extern void JSONBool_get_IsBoolean_mE70A4F82605CAB1E14A70413FC42EF66ADBAB727 (void);
// 0x000005DF SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONBool::GetEnumerator()
extern void JSONBool_GetEnumerator_mA15666FFF18AADF12912689A521087C98B5B2D1C (void);
// 0x000005E0 System.String SimpleJSON.JSONBool::get_Value()
extern void JSONBool_get_Value_m11D2863303B2003F337549581C8A583A6A3B4D74 (void);
// 0x000005E1 System.Void SimpleJSON.JSONBool::set_Value(System.String)
extern void JSONBool_set_Value_mA7FB197B21177B947BC3C2940A63724FE532E373 (void);
// 0x000005E2 System.Boolean SimpleJSON.JSONBool::get_AsBool()
extern void JSONBool_get_AsBool_mA5617A7BC781E111FBD24A7E53FB34C7DE6F62B6 (void);
// 0x000005E3 System.Void SimpleJSON.JSONBool::set_AsBool(System.Boolean)
extern void JSONBool_set_AsBool_mDDD0D1CA7B06DAD37AA13987F46B95C6A1AEF489 (void);
// 0x000005E4 System.Void SimpleJSON.JSONBool::.ctor(System.Boolean)
extern void JSONBool__ctor_m7593DD0B3E9628BA9BB4DE4A0E904BBA6BF151C1 (void);
// 0x000005E5 System.Void SimpleJSON.JSONBool::.ctor(System.String)
extern void JSONBool__ctor_m8E5E6A23827EE2B5DB5E7C86377805ADD8A771B2 (void);
// 0x000005E6 System.Void SimpleJSON.JSONBool::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONBool_WriteToStringBuilder_m7EA9750436CEE42357B6A109B83870F4FC2C403E (void);
// 0x000005E7 System.Boolean SimpleJSON.JSONBool::Equals(System.Object)
extern void JSONBool_Equals_m57A524B4FEBF04285341E4FC42EDEDE7A524F4BB (void);
// 0x000005E8 System.Int32 SimpleJSON.JSONBool::GetHashCode()
extern void JSONBool_GetHashCode_m49A9C417E05DFB795FAE0F38737C7629A3A69A22 (void);
// 0x000005E9 System.Void SimpleJSON.JSONBool::SerializeBinary(System.IO.BinaryWriter)
extern void JSONBool_SerializeBinary_m58E3D67CAB665202AF9AF699A0E2C267BE4A778A (void);
// 0x000005EA SimpleJSON.JSONNull SimpleJSON.JSONNull::CreateOrGet()
extern void JSONNull_CreateOrGet_m96465EECF543329FFBBC26B552EDB6E43D8DBC90 (void);
// 0x000005EB System.Void SimpleJSON.JSONNull::.ctor()
extern void JSONNull__ctor_mA4FF0FB3001766DFB7A8D8C23063101AC377F277 (void);
// 0x000005EC SimpleJSON.JSONNodeType SimpleJSON.JSONNull::get_Tag()
extern void JSONNull_get_Tag_m2A705402C448944F6395AAC88DA5DE7ADF99C4D1 (void);
// 0x000005ED System.Boolean SimpleJSON.JSONNull::get_IsNull()
extern void JSONNull_get_IsNull_m1964A7B7E9C0FD17CDE2E4DFDF6345FE03EBA59B (void);
// 0x000005EE SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONNull::GetEnumerator()
extern void JSONNull_GetEnumerator_m7B4ADD53B56B6663A0832AC611D7B47FA98D1A35 (void);
// 0x000005EF System.String SimpleJSON.JSONNull::get_Value()
extern void JSONNull_get_Value_m76B2E412BD13C31AD68C272CA9AF2E6063F165D3 (void);
// 0x000005F0 System.Void SimpleJSON.JSONNull::set_Value(System.String)
extern void JSONNull_set_Value_m713260989D4BA62C8B8CD716893585A1AA88ABE9 (void);
// 0x000005F1 System.Boolean SimpleJSON.JSONNull::get_AsBool()
extern void JSONNull_get_AsBool_m5E619608D71A63CBD234CA282B3BB2A7EFF385D5 (void);
// 0x000005F2 System.Void SimpleJSON.JSONNull::set_AsBool(System.Boolean)
extern void JSONNull_set_AsBool_m267D1AA7D6003F371DC10DB65EB4E80643BB99F1 (void);
// 0x000005F3 System.Boolean SimpleJSON.JSONNull::Equals(System.Object)
extern void JSONNull_Equals_m3063912429EF72D2F9AFAEDF0D0303EB5D60A893 (void);
// 0x000005F4 System.Int32 SimpleJSON.JSONNull::GetHashCode()
extern void JSONNull_GetHashCode_m899F43B83E13ABFDE999C3781C45C6C03CC21077 (void);
// 0x000005F5 System.Void SimpleJSON.JSONNull::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONNull_WriteToStringBuilder_m66FC8C85A593D6198406753B845BFECA59117B46 (void);
// 0x000005F6 System.Void SimpleJSON.JSONNull::SerializeBinary(System.IO.BinaryWriter)
extern void JSONNull_SerializeBinary_m0D96E78D3AC9372DFC0EEF6F260367E9FCB4D939 (void);
// 0x000005F7 System.Void SimpleJSON.JSONNull::.cctor()
extern void JSONNull__cctor_mF5CFD47108FE35E808A8BA6514AA8283BBFD3193 (void);
// 0x000005F8 SimpleJSON.JSONNodeType SimpleJSON.JSONLazyCreator::get_Tag()
extern void JSONLazyCreator_get_Tag_m492C25FDFDC35D2EFB17711E1C259976A38BB46E (void);
// 0x000005F9 SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONLazyCreator::GetEnumerator()
extern void JSONLazyCreator_GetEnumerator_m6B2038B21158F725FC5B607544FB590A0E313AEC (void);
// 0x000005FA System.Void SimpleJSON.JSONLazyCreator::.ctor(SimpleJSON.JSONNode)
extern void JSONLazyCreator__ctor_m790C925B9AD99D9F574C556397F3C2D868D2F7EE (void);
// 0x000005FB System.Void SimpleJSON.JSONLazyCreator::.ctor(SimpleJSON.JSONNode,System.String)
extern void JSONLazyCreator__ctor_mE9B31AC731EAF8B853DA1671147CD5B9B5C6F10A (void);
// 0x000005FC System.Void SimpleJSON.JSONLazyCreator::Set(SimpleJSON.JSONNode)
extern void JSONLazyCreator_Set_m9C40ED5996DFD3D796BFAFBC25EEAB76F29F871F (void);
// 0x000005FD SimpleJSON.JSONNode SimpleJSON.JSONLazyCreator::get_Item(System.Int32)
extern void JSONLazyCreator_get_Item_m0A09379FDB2D2CB95B781D1E3A03855661A0F446 (void);
// 0x000005FE System.Void SimpleJSON.JSONLazyCreator::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONLazyCreator_set_Item_mE77E182C7C150529FBA7BD013A03F95FDCA25E9E (void);
// 0x000005FF SimpleJSON.JSONNode SimpleJSON.JSONLazyCreator::get_Item(System.String)
extern void JSONLazyCreator_get_Item_m55D3BD8AF905099931604EACC6A907C17AACA753 (void);
// 0x00000600 System.Void SimpleJSON.JSONLazyCreator::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONLazyCreator_set_Item_m8E286CF72FF8622427E69587C30C91BE0D242FCF (void);
// 0x00000601 System.Void SimpleJSON.JSONLazyCreator::Add(SimpleJSON.JSONNode)
extern void JSONLazyCreator_Add_m7393F681C0F1D9F798D3FEAE8C3A37C9F73E0E1B (void);
// 0x00000602 System.Void SimpleJSON.JSONLazyCreator::Add(System.String,SimpleJSON.JSONNode)
extern void JSONLazyCreator_Add_m8762C532A8EBAFF5AE213C3B2A9AC983653C8D37 (void);
// 0x00000603 System.Boolean SimpleJSON.JSONLazyCreator::op_Equality(SimpleJSON.JSONLazyCreator,System.Object)
extern void JSONLazyCreator_op_Equality_mF472937FE1ABFEBCEB8A717561AB198CCB0F75E8 (void);
// 0x00000604 System.Boolean SimpleJSON.JSONLazyCreator::op_Inequality(SimpleJSON.JSONLazyCreator,System.Object)
extern void JSONLazyCreator_op_Inequality_m8C2EB320847B968D52F5E5D96599F734FEEE8ACA (void);
// 0x00000605 System.Boolean SimpleJSON.JSONLazyCreator::Equals(System.Object)
extern void JSONLazyCreator_Equals_m94A17EF99F288D175F1A40DC7B6569643CA84DD9 (void);
// 0x00000606 System.Int32 SimpleJSON.JSONLazyCreator::GetHashCode()
extern void JSONLazyCreator_GetHashCode_m85E38D69ED1F0970E4EE3583196A4E9CD37C9DAF (void);
// 0x00000607 System.Int32 SimpleJSON.JSONLazyCreator::get_AsInt()
extern void JSONLazyCreator_get_AsInt_m8C8C09CC03C6E229A680D004C070734C424DA012 (void);
// 0x00000608 System.Void SimpleJSON.JSONLazyCreator::set_AsInt(System.Int32)
extern void JSONLazyCreator_set_AsInt_m9CEB176958FE16A9213B7828EEAF442F54E70587 (void);
// 0x00000609 System.Single SimpleJSON.JSONLazyCreator::get_AsFloat()
extern void JSONLazyCreator_get_AsFloat_m1EC025D56616385B163C548183783BA8E4FD67E4 (void);
// 0x0000060A System.Void SimpleJSON.JSONLazyCreator::set_AsFloat(System.Single)
extern void JSONLazyCreator_set_AsFloat_mAECCEF9C5598A5EA2204CDD1730D19B073050956 (void);
// 0x0000060B System.Double SimpleJSON.JSONLazyCreator::get_AsDouble()
extern void JSONLazyCreator_get_AsDouble_m019E3ACC054CCE18601A8731E772C86C62C09545 (void);
// 0x0000060C System.Void SimpleJSON.JSONLazyCreator::set_AsDouble(System.Double)
extern void JSONLazyCreator_set_AsDouble_mBFEC4A9E9A946716829F584BE00CFA35D8DBA1E2 (void);
// 0x0000060D System.Boolean SimpleJSON.JSONLazyCreator::get_AsBool()
extern void JSONLazyCreator_get_AsBool_mE7E32A21C51AFA46EC9BE4F78E96AE20986E7E00 (void);
// 0x0000060E System.Void SimpleJSON.JSONLazyCreator::set_AsBool(System.Boolean)
extern void JSONLazyCreator_set_AsBool_m700796091E7C1940E21834CE4BB1A8FD80017094 (void);
// 0x0000060F SimpleJSON.JSONArray SimpleJSON.JSONLazyCreator::get_AsArray()
extern void JSONLazyCreator_get_AsArray_mF737B0C7548A4C304278366931569BD173D4C2C6 (void);
// 0x00000610 SimpleJSON.JSONObject SimpleJSON.JSONLazyCreator::get_AsObject()
extern void JSONLazyCreator_get_AsObject_m0678D4A1725598B36657E07D1626B8387D3B4C88 (void);
// 0x00000611 System.Void SimpleJSON.JSONLazyCreator::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONLazyCreator_WriteToStringBuilder_m20163F20F4F24E7E82C30AAA1878AE7DE301D0E9 (void);
// 0x00000612 System.Void SimpleJSON.JSONLazyCreator::SerializeBinary(System.IO.BinaryWriter)
extern void JSONLazyCreator_SerializeBinary_m2E7AFA0F831BF1CBB93F6BC72BEB67A531A31FD5 (void);
// 0x00000613 SimpleJSON.JSONNode SimpleJSON.JSON::Parse(System.String)
extern void JSON_Parse_mEAB5874EF687F205543F8469A9FAE7DB3C134421 (void);
static Il2CppMethodPointer s_methodPointers[1555] = 
{
	DemoScriptUpload_Start_mEA0B6F65135C7190FA7CF15E2FDD0B601219FD97,
	DemoScriptUpload_SetSSScore_m38CB33DC46259C1C7B17049939FDBABFE4D53DF9,
	DemoScriptUpload_DownloadScores_m4DCE1F5EC0F4880AAA8AD5BC57F42E08A54A3B88,
	DemoScriptUpload_OnUsernameChaged_m6E1BC99BAB8530F37B95C4558804EE5856FE91A0,
	DemoScriptUpload_OnLeaderboardUpdated_mD9D4380D0CFAF6ECADFC9BF315A4C7E40B74BA62,
	DemoScriptUpload_OkButtonTouched_mF7A02AEF54563644C5A81B0D47586D93E0E5EDC5,
	DemoScriptUpload_OnDestroy_mC08808A3709B44CC61F2C7B7FD5E19BC28C0B670,
	DemoScriptUpload_USERNAME_mDB4B629ECC5D00A4B905A347688F46EC3B8800E2,
	DemoScriptUpload__ctor_mF21648CBC6C501CB6B8C43ECF49B1C1936171AD1,
	U3CDownloadScoresU3Ed__6__ctor_m458B4DBDDBAEB5065623F0C54F9EF89B8F020E62,
	U3CDownloadScoresU3Ed__6_System_IDisposable_Dispose_m8A74ABA9B83DA29A1360699BBDF8DB1121D2AC6E,
	U3CDownloadScoresU3Ed__6_MoveNext_m76311778CF8934A78555B6FF9ACB85EE691750DF,
	U3CDownloadScoresU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF1616BD5BE626F4FB23ACF7427B45246F98982FE,
	U3CDownloadScoresU3Ed__6_System_Collections_IEnumerator_Reset_m33DB0A4DEB52998E9A02C5461074496DE74116CD,
	U3CDownloadScoresU3Ed__6_System_Collections_IEnumerator_get_Current_m77E46FD77E265BFDB234B1ACFBFF25935F8A41CE,
	LeaderBoardEntrySizeFitter_Start_m773E209A1FA87ED6580D6A4ADCA26AC1B823E513,
	LeaderBoardEntrySizeFitter_OnRectTransformDimensionsChange_m78FF00DFD8E28D715AEED1713E713B35964E6589,
	LeaderBoardEntrySizeFitter_Update_mDB5CF453EF87999C91159C368A2DB99AAA97F5CB,
	LeaderBoardEntrySizeFitter_UpdateWidth_m25BC960E5AE9DBB5BC33A56DA22DA07B79F579AF,
	LeaderBoardEntrySizeFitter__ctor_mFC64905BCF9E16DA766D014AB128877B345F13FB,
	UIDemoLeaderboardEntry_Start_m859529BD9C882A8D2C79F3B126221183A6A05147,
	UIDemoLeaderboardEntry_Setup_m677B2F556C06F065FDE7CDD3D855E8771813EA19,
	UIDemoLeaderboardEntry_SetupLayout_mCEF15D13A90378EB65FBB01ED65E4A42992E2759,
	UIDemoLeaderboardEntry__ctor_m26A64DD8D4EC07872E596247D1FA9D32DA753DFF,
	U3CSetupLayoutU3Ed__6__ctor_mD78FF193B3045CEE87B93F49B5C9BF7B3E9C4C16,
	U3CSetupLayoutU3Ed__6_System_IDisposable_Dispose_m3E4CB9082D3557CF3B6DDC4F85D90A5E7E4A4679,
	U3CSetupLayoutU3Ed__6_MoveNext_m2184DA523B03F8562B7D626151BAFB369CF0397B,
	U3CSetupLayoutU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA57BB452742FD0576D602BF4F1B61442AA6D29C1,
	U3CSetupLayoutU3Ed__6_System_Collections_IEnumerator_Reset_mEB9B0829A05E26C36FAABCCB2CA48934B53B989F,
	U3CSetupLayoutU3Ed__6_System_Collections_IEnumerator_get_Current_mC290A24F1EFCF9F793DE4E9CB92B74C1B13F16B4,
	UI_DemoLeaderboard_OnEnable_mBF732224E4001D6CE288ACB720F3FA62969CBD9A,
	UI_DemoLeaderboard_BackbutttonTouched_m7281FC30CF0EB760C02F6AD9702944CE998DD7FF,
	UI_DemoLeaderboard_OnDisable_mA19A7691256994B39A1FC6025424229D85E1AF86,
	UI_DemoLeaderboard_RemoveAllUIEntries_mC8BD26AA5C8694C08B34F3DCC73AF1AC45BA5EFF,
	UI_DemoLeaderboard_OnLeaderboardUpdated_m2C9479A3484AFC01DAEF6E89775ABE07D62DE6B9,
	UI_DemoLeaderboard_SetupRank_mDD168BDF24ED66BC9FD9DA78709AA8F82A863A84,
	UI_DemoLeaderboard__ctor_m482BD3932FF81D180E8A1935721B053106C9D9AC,
	LB_ChangeUsername_Awake_mA61790D7B93F799604D685476909DBCFD91816C6,
	LB_ChangeUsername_ChangeUsername_m4CF717A903A65E8EE94D96F6413E72CC215658F6,
	LB_ChangeUsername_WaitForRequest_m3892185548E537D0ACE6C7AFB7245DC7676324D2,
	LB_ChangeUsername_Queue_mB548F5326B96D3307CC0D184F7DF8D236F3E26C0,
	LB_ChangeUsername__ctor_mBCE805B655C310CB2BF2B515F142ED22A2FA330B,
	OnChangeUsernameFinishedDelegate__ctor_m321935EA9898632CC84FE823E3F19B741FF586C4,
	OnChangeUsernameFinishedDelegate_Invoke_m7053FF7EA4E3E1DD5EFE252236EA1C0FF52D8E17,
	OnChangeUsernameFinishedDelegate_BeginInvoke_m1BF4B394E834836650E7C7FD0A47E985B0E63066,
	OnChangeUsernameFinishedDelegate_EndInvoke_m57C17139125B84F1B190F89169D7D4377F45E719,
	U3CWaitForRequestU3Ed__8__ctor_mC00B9BF46A96F886C582B70A8DD1B565DB61A516,
	U3CWaitForRequestU3Ed__8_System_IDisposable_Dispose_mFE69ADB3A97C8493CB73F87FDAC3380347DA52A2,
	U3CWaitForRequestU3Ed__8_MoveNext_m6CA3F1D353718CF5D1F4A3DE04D7B026A9BC133F,
	U3CWaitForRequestU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m29C97B6571BF10FE1B6D5627EF92BDC0E437DEB3,
	U3CWaitForRequestU3Ed__8_System_Collections_IEnumerator_Reset_mB2A138469B68697C56C0BB7963D0789E51C07E6A,
	U3CWaitForRequestU3Ed__8_System_Collections_IEnumerator_get_Current_m7188E281D35498BE04AE17D62C000FC55C59DB48,
	U3CQueueU3Ed__9__ctor_m8AAF21ADBBFA6EF0231E388774456F2093A1D2B1,
	U3CQueueU3Ed__9_System_IDisposable_Dispose_m70CCC49884F45C7E412E5ACAC7E67CD6DC95CD2F,
	U3CQueueU3Ed__9_MoveNext_m052D2C2A82F20D41BCB1914D7171D94607A2A4D5,
	U3CQueueU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6FDE843FE9C4F7718E1F56B0F45DD72A49BDEC7D,
	U3CQueueU3Ed__9_System_Collections_IEnumerator_Reset_mE4A8DCB3E020101E430C43AD68A26999C8AD9DFE,
	U3CQueueU3Ed__9_System_Collections_IEnumerator_get_Current_mD835F4F4F7170E1807829EA642A169C3AE0A3974,
	LB_Controller_Awake_m357722092F27230636F3209D48D966BCF8A9584D,
	LB_Controller_Start_mC89F9407D6FCF7EF7AC29720CA5419461713377F,
	LB_Controller_StoreScore_m08384F34856FABD7CBC2127B575E54391D0C35F8,
	LB_Controller_ChangeUsername_m19EABE0E568E6540C749CA9A3432A30F3A81A83E,
	LB_Controller_OnChangeUsernameFinished_m9AA6FEDD4ACD23D8008F0C1A092938EEEF8BECF9,
	LB_Controller_ReloadLeaderboard_mB971EA354F6A01006E275DE507C173FE8DD43FB2,
	LB_Controller_OnRequestFinished_m09C76FB9B7652D67438A79E214ACFC264328748D,
	LB_Controller_GetRankForUser_mC4481C593C1433AF9BB848046274A12B60579F11,
	LB_Controller_Entries_m08C48EB98BA13A6293CF6908CE0CCA85A739AA73,
	LB_Controller_OnDestroy_m76FDCBEA9E2A889E0C0E70B1E61E20DB79E001DD,
	LB_Controller__ctor_mFF8FF45A36B9AB0D70EFB9C34E3C8793DB2DDFD2,
	OnAllScoresUpdated__ctor_m241AA2B78CDC9863E57393622AF1FD5A5383F760,
	OnAllScoresUpdated_Invoke_m6C67385D54030029DB5054800F3C5D012181539A,
	OnAllScoresUpdated_BeginInvoke_m1EFF01980AC81F8C72181199A833E8AA7748EB76,
	OnAllScoresUpdated_EndInvoke_mBCA1CC8E757C612B5EE3FDCBD9F3CB322216C832,
	OnUsernameChanged__ctor_m449ACFA6CAB11172F0967BAAB39C2A49D19394C6,
	OnUsernameChanged_Invoke_mF48A1C0EDD88DC1C8FD19146114D2A8E1D9E6FDD,
	OnUsernameChanged_BeginInvoke_m17B348337FCCAFE7D8C49B2E1735FF73E3F9DBC2,
	OnUsernameChanged_EndInvoke_mE4F5B23728C224683BAA3270DFE72D8C4D7BB79A,
	Status__ctor_m0FC8D4BFEED8FB685CC8024A574EB1CCAB4F415E,
	RequestStatus__ctor_m924EEBC8D13A01BC270C34D3B4E1A31FF0241F03,
	LB_Entry__ctor_mC7B35C70ED11911969BDF78757D6DA38DDF59B32,
	LeaderboardResult__ctor_m64DE2CA9B65BF635684B3C92E27631B9468A9A2E,
	LB_GetAllScores_Awake_m896166A9C96587431EB6EEA4576B35B3BAA357D0,
	LB_GetAllScores_GetAllScores_m4B44D580958771F4B6267A686ABF40CA173EDA75,
	LB_GetAllScores_WaitForRequest_m0D5EF9D675EE95A203DA964047FA41839FAEE6AB,
	LB_GetAllScores_Queue_m4A5049608C05575844BC43966116496547349A5D,
	LB_GetAllScores__ctor_mADC5CD83C0E28C0DFB4A9082B9AA1D7D8F224DCC,
	OnGetAllScoresFinishedDelegate__ctor_m9519F4612572A54A9E707B89546D8B52EF0768BE,
	OnGetAllScoresFinishedDelegate_Invoke_m29B4448B8EA0C36CCC86C410630966A182C4DE75,
	OnGetAllScoresFinishedDelegate_BeginInvoke_m086D2E2D98632432267CBDF219081A1FE445469C,
	OnGetAllScoresFinishedDelegate_EndInvoke_mC829E772F3DFB0C33A0497DB807C1DC14E1022C0,
	U3CWaitForRequestU3Ed__6__ctor_m97BB7708C2EB3A70F5C94182372AF9E32FC7CD47,
	U3CWaitForRequestU3Ed__6_System_IDisposable_Dispose_m7EE7540A570B1E2434619CDA4358814C30FB86A4,
	U3CWaitForRequestU3Ed__6_MoveNext_m49F31127E708112D8AEEC026608B46AF6CD0E84E,
	U3CWaitForRequestU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF12E25E1BEE99CAF799E12150915679EA217C41B,
	U3CWaitForRequestU3Ed__6_System_Collections_IEnumerator_Reset_mD25372C19CEFCCB9AA996890BBD729FBB9540D9E,
	U3CWaitForRequestU3Ed__6_System_Collections_IEnumerator_get_Current_m9C7C1B996CB4FA711EAF40E109A2514D210DE5B3,
	U3CQueueU3Ed__7__ctor_m61B5858BFB2402452D6C13C8E84B4D85D88AFEA3,
	U3CQueueU3Ed__7_System_IDisposable_Dispose_m4F87B63F9383B2160E165D1FC8836C0AA460099D,
	U3CQueueU3Ed__7_MoveNext_m77D9B22933517A79019F7068E4E6FC65835F1546,
	U3CQueueU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC868CCF7E7C3E8225901D74C22CBF6F5CB55FC7C,
	U3CQueueU3Ed__7_System_Collections_IEnumerator_Reset_mD1460AC868EBBC5EF1151FAE7B95C77AB7711F69,
	U3CQueueU3Ed__7_System_Collections_IEnumerator_get_Current_m799D1D4AFD057C4A1D779F6891F5DAD2104225A1,
	LB_StoreScore_Awake_m955A76945B2FD44355B68DACA00252BFD7182A3E,
	LB_StoreScore_StoreScore_m241D6C1F495DD3B2236001278901C3DC8846183B,
	LB_StoreScore_WaitForRequest_m960777347A3E737EB979B99E98792C91F19C6B0D,
	LB_StoreScore_Queue_m43344EBE7F6233651E8F82495D1C195773F9ED70,
	LB_StoreScore__ctor_mC0D86244E986EC67F432EA0177B07229D5E6C076,
	U3CWaitForRequestU3Ed__6__ctor_mF766FA099094CD3144F44A84514A1200680C3AFA,
	U3CWaitForRequestU3Ed__6_System_IDisposable_Dispose_mC5111309F5B49F24C7C35D0CBDB7940979057C41,
	U3CWaitForRequestU3Ed__6_MoveNext_m96A93D387A6BBC777125737DEA2EDEC154D948F4,
	U3CWaitForRequestU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE6ACA16FE5EB0E47CB665A9A42D8D4AF9ABDBED1,
	U3CWaitForRequestU3Ed__6_System_Collections_IEnumerator_Reset_mAD39B16DB0BEAC45E03C87EE94FE87EE0414331A,
	U3CWaitForRequestU3Ed__6_System_Collections_IEnumerator_get_Current_m440BAE92717F6F07FD2056DE956D6A0C76F68261,
	U3CQueueU3Ed__7__ctor_m46A68EFAF2FEE8EFC70B275F322103151F6C0129,
	U3CQueueU3Ed__7_System_IDisposable_Dispose_mDADD194443E921593D77B71AAEDFE003D0A0BF38,
	U3CQueueU3Ed__7_MoveNext_mE15D5F8AEF59D919C469D337C305D9AAFD594CCC,
	U3CQueueU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2A7707DE7731C252B928CC1C3CD2295F60F1A978,
	U3CQueueU3Ed__7_System_Collections_IEnumerator_Reset_m99C2E6B33286E36FE375FFC48B3CAADF620744EE,
	U3CQueueU3Ed__7_System_Collections_IEnumerator_get_Current_m1DBEF398E21DCFD6B46024014F6CEA2D23CC0E0D,
	MainScript_LogMessage_m7953CB447ABFC4D0E286219425CF26684DB6F6B0,
	MainScript_Get_m3BD5249433D12EE1562CB113FF95FD302207B2A0,
	MainScript_Post_mF7C21819A39C80CBEF1E5471FF7A505A6CF9B687,
	MainScript_Put_m2E077AE7D135014788A9F219D4FA32B618BED552,
	MainScript_Delete_m3CEEF47B740A70E766D3CF74E6C1D37E13C0A8C8,
	MainScript_AbortRequest_mDCEC8008FD946FB21C66D8DE04AA652FD091AC5E,
	MainScript_DownloadFile_mF54C07D06969AD450838E27A885FB450BDA6971C,
	MainScript__ctor_mDE68DE7EC111F10F50287CE15AC2100BAC26DF23,
	MainScript_U3CPostU3Eb__4_0_m8B0202AA78218D3787871313668C76F260A52D42,
	MainScript_U3CPostU3Eb__4_1_m2C8C662A8A4EE9A219C715C38E88E9D156CC0777,
	MainScript_U3CPutU3Eb__5_0_m556845DFA7BB10FCFF4A3A04B17EF6BC8AA77CAE,
	MainScript_U3CDeleteU3Eb__6_0_m3092FBC0D31C4B83C0C7E537BFD404FD28F0BB8E,
	MainScript_U3CDownloadFileU3Eb__8_0_m8D61A6C39FFBB97884CF0935DA9BF297B5DE8393,
	MainScript_U3CDownloadFileU3Eb__8_1_m99B4D340A42D8D8B784669EDB99B721CAD853FB6,
	U3CU3Ec__DisplayClass3_0__ctor_m46C50E0A5C12FA151CC55E0B928E27838925B4FC,
	U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__0_m1E95C1F0B2D8E5BC302A5ADCA1ABAE0293AEA82D,
	U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__1_mB5A9CA352F06FB80FDC47E6FE114495BA4687AC1,
	U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__2_mB93ACE707409C85F0542FD7665B0BF499473D98D,
	U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__3_m381C9E851E8FB988B02A1076B749138ABC954BDB,
	U3CU3Ec__DisplayClass3_0_U3CGetU3Eb__4_m44FBEB10E4BAEB006FC92154DB40AB3CB7BC4DE0,
	U3CU3Ec__cctor_m4FA3DF2408BEA55F36632D2A36DDEA9E1475BBA3,
	U3CU3Ec__ctor_m6B5FCC705564205B21810FFBA043CB8DA659F8E1,
	U3CU3Ec_U3CPutU3Eb__5_1_mC12F48C710EDD364D74E0411BCE9CB0F55326D2F,
	AdsManager_Start_m561A6D94B810C2CBBAD6235533FEF81D3FCB93BA,
	AdsManager_Update_m84B68116B4140F28E1729396720F8FCD9F790646,
	AdsManager_ShowAd_m37205D6D03ADA57C5DC2B7890B09CDDCFB87C6DC,
	AdsManager_ShowAds_m8F51286E3D51611E15459A5635394278B8DB192D,
	AdsManager_OnUnityAdsReady_mA76941ECD72FD15E03D0B7DBF10365BF5AD72764,
	AdsManager_OnUnityAdsDidError_m1908BC190CA0D84743BF3480F0D10B4202F9E207,
	AdsManager_OnUnityAdsDidStart_mF0AB782E7E55745E4D5A5A1EDFDB13CF64F73B15,
	AdsManager_OnUnityAdsDidFinish_m73319AF105DBF4B961201FC670FDE62BE190E01F,
	AdsManager__ctor_m1324EE3D9841045940C44D5E0379750DBE0C0A53,
	FirebaseLeaderBoard_Start_m2A8606E3CB9A1B2C1679AB3CEA2ABBAE82EDAF19,
	FirebaseLeaderBoard_Getdata_m4C48E7B72E7E7C7D2120D93037E2EB619D94E7DF,
	FirebaseLeaderBoard_SetData_mD4B3213998D0FB7115813C65D30AA885CE07785B,
	FirebaseLeaderBoard__ctor_m1FAF10D9488E553684210B591B25772D8E144EF6,
	User__ctor_mADEC54352092BAE9268BB4C08BDFA1044CFA7B5F,
	UserDataaa__ctor_m9E49CA15AB81912B51F77B0354896A50F74495A4,
	U3CU3Ec__DisplayClass5_0__ctor_mCC77BBBA53D919BDE33512665E573EF1DB596230,
	U3CU3Ec__DisplayClass5_0_U3CGetdataU3Eb__0_mE072A040B7AB8851F8813648BA075A4FF0BCB9EE,
	U3CU3Ec__cctor_mE98FA4BC4AF7D98FDEF99BCB24D6A5AAB2E4B5D2,
	U3CU3Ec__ctor_m1EA501D17E3E06CDD519EF188BDF8290240D7E44,
	U3CU3Ec_U3CGetdataU3Eb__5_1_mA3A399D9EB7F52C3B85CC5AAB12B4241CDE01885,
	U3CU3Ec_U3CSetDataU3Eb__6_2_mDE271C3889E38C35DD4E13884087AB860F477075,
	U3CU3Ec_U3CSetDataU3Eb__6_3_m3A3BBB790869403499086B12DDBFE46EEB57E118,
	U3CU3Ec_U3CSetDataU3Eb__6_1_mB823AA97FFC1975F9E35B3FEFE4CB2B2C1AB49D1,
	U3CU3Ec__DisplayClass6_0__ctor_m67BFE4E5C73A36DD5EB78B620CB1A626133396D6,
	U3CU3Ec__DisplayClass6_0_U3CSetDataU3Eb__0_m1EF9F29E4C954A82EA8A318ABF931A34E2CFC786,
	RankManger_Start_mC0E8A40BDD26934BA9F22FD2DF1B2D0A5217433A,
	RankManger_CreatRankData_mB9223C0DC71C1F401546AF3110A8893E72A008E8,
	RankManger__ctor_m277636F4F694280DC5E884A5A40D2B80A90BE665,
	SceneManagere_nextScene_mC0A2B5AE808B0DDA8257E6012639B94F02CFB0E8,
	SceneManagere__ctor_m9EE09D20C8AEF419239B33A2B635CCFA60092E0F,
	SafeAreaSetter_Start_m83780A11588277507500E26788A2EA69E09FF392,
	SafeAreaSetter_ApplySafeArea_m4F4F38103E2C3689B92117BF87F0E7559B48CB25,
	SafeAreaSetter_Update_mF64C75E3889BDD924F5886A3AFF934BC395A5E01,
	SafeAreaSetter__ctor_mA93E55DED3B5E17188E4D3CC093573F5E1BC696B,
	ScreenCapture_Start_mE754AB7DB07B1B52F9528011C8394B374ACF6CE5,
	ScreenCapture_CreatFileName_mBC0FF46D1C6D2C3167E22CAB2F5D4F863CA559FB,
	ScreenCapture_CaptureScreen_m075969365E6FBE7A5902917C1CE1BA98BE589632,
	ScreenCapture_ShowImage_m9219EEEC540FBD07E04C20A067BAD7F855423823,
	ScreenCapture_TakeScreen_m46600B2F2EEFBB4002BE5326D6511884E56AF201,
	ScreenCapture_Update_m1F10F112088C1AD9570A869177A6D9AD5FD2F4F5,
	ScreenCapture__ctor_mF1A1D405C141990DEFD5166FA5370FD91EE8F6D4,
	U3CU3Ec__DisplayClass15_0__ctor_mFAB3648391D7F7415C8099820E6687D05184B688,
	U3CU3Ec__DisplayClass15_0_U3CCaptureScreenU3Eb__0_mFFF45C5919C398F496FF3917DF70929D77D5EEEF,
	U3CShowImageU3Ed__16__ctor_mCC0CEF23681A7B9048820FBE923839D9B68B87AD,
	U3CShowImageU3Ed__16_System_IDisposable_Dispose_m87BB0FC2D94FEFAFF97161D322FBB0CE5898DC65,
	U3CShowImageU3Ed__16_MoveNext_m7DDEBE1874282C4F66CAD5BAF0ADD1798C33E7D0,
	U3CShowImageU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m598FC1EC637E2B1CFAEF2530048EDF317FC78FF7,
	U3CShowImageU3Ed__16_System_Collections_IEnumerator_Reset_mEE4AEDDD2B6E7A84E2AE04E3B8EA2DEFC538C3AE,
	U3CShowImageU3Ed__16_System_Collections_IEnumerator_get_Current_m762F9432D7D55A302AD5EB8D65BF6C58167D4262,
	ChatController_OnEnable_mBC3BFC05A1D47069DFA58A4F0E39CFF8FAD44FF2,
	ChatController_OnDisable_m268A7488A3FDEB850FC3BC91A0BBDA767D42876B,
	ChatController_AddToChatOutput_m43856EEA133E04C24701A2616E7F10D7FFA68371,
	ChatController__ctor_m3B66A5F749B457D865E8BDA1DE481C8CF1158026,
	DropdownSample_OnButtonClick_m97641451A2EAC4FC6682897A948A37FDD6BF9EA6,
	DropdownSample__ctor_m87664E3DC75A1A9AA19FEE6ED6B993AB1E50E2B9,
	EnvMapAnimator_Awake_mFFC04BC5320F83CA8C45FF36A11BF43AC17C6B93,
	EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE,
	EnvMapAnimator__ctor_mC151D673A394E2E7CEA8774C4004985028C5C3EC,
	U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23,
	U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B,
	U3CStartU3Ed__4_MoveNext_m2F1A8053A32AD86DA80F86391EC32EDC1C396AEE,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF,
	TMP_DigitValidator_Validate_m5D303EB8CD6E9E7D526D633BA1021884051FE226,
	TMP_DigitValidator__ctor_m1E838567EF38170662F1BAF52A847CC2C258333E,
	TMP_PhoneNumberValidator_Validate_mF0E90A277E9E91BC213DD02DC60088D03C9436B1,
	TMP_PhoneNumberValidator__ctor_mB3C36CAAE3B52554C44A1D19194F0176B5A8EED3,
	TMP_TextEventHandler_get_onCharacterSelection_m90C39320C726E8E542D91F4BBD690697349F5385,
	TMP_TextEventHandler_set_onCharacterSelection_m5ED7658EB101C6740A921FA150DE18C443BDA0C4,
	TMP_TextEventHandler_get_onSpriteSelection_m0E645AE1DFE19B011A3319474D0CF0DA612C8B7B,
	TMP_TextEventHandler_set_onSpriteSelection_m4AFC6772C3357218956A5D33B3CD19F3AAF39788,
	TMP_TextEventHandler_get_onWordSelection_mA42B89A37810FB659FCFA8539339A3BB8037203A,
	TMP_TextEventHandler_set_onWordSelection_m4A839FAFFABAFECD073B82BA8826E1CD033C0076,
	TMP_TextEventHandler_get_onLineSelection_mB701B6C713AD4EFC61E1B30A564EE54ADE31F58D,
	TMP_TextEventHandler_set_onLineSelection_m0B2337598350E51D0A17B8FCB3AAA533F312F3DA,
	TMP_TextEventHandler_get_onLinkSelection_mF5C3875D661F5B1E3712566FE16D332EA37D15BB,
	TMP_TextEventHandler_set_onLinkSelection_m200566EDEB2C9299647F3EEAC588B51818D360A5,
	TMP_TextEventHandler_Awake_m43EB03A4A6776A624F79457EC49E78E7B5BA1C70,
	TMP_TextEventHandler_LateUpdate_mE1D989C40DA8E54E116E3C60217DFCAADD6FDE11,
	TMP_TextEventHandler_OnPointerEnter_m8FC88F25858B24CE68BE80C727A3F0227A8EE5AC,
	TMP_TextEventHandler_OnPointerExit_mBB2A74D55741F631A678A5D6997D40247FE75D44,
	TMP_TextEventHandler_SendOnCharacterSelection_m78983D3590F1B0C242BEAB0A11FDBDABDD1814EC,
	TMP_TextEventHandler_SendOnSpriteSelection_m10C257A74F121B95E7077F7E488FBC52380A6C53,
	TMP_TextEventHandler_SendOnWordSelection_m95A4E5A00E339E5B8BA9AA63B98DB3E81C8B8F66,
	TMP_TextEventHandler_SendOnLineSelection_mE85BA6ECE1188B666FD36839B8970C3E0130BC43,
	TMP_TextEventHandler_SendOnLinkSelection_m299E6620DFD835C8258A3F48B4EA076C304E9B77,
	TMP_TextEventHandler__ctor_m6345DC1CEA2E4209E928AD1E61C3ACA4227DD9B8,
	CharacterSelectionEvent__ctor_m06BC183AF31BA4A2055A44514BC3FF0539DD04C7,
	SpriteSelectionEvent__ctor_m0F760052E9A5AF44A7AF7AC006CB4B24809590F2,
	WordSelectionEvent__ctor_m106CDEB17C520C9D20CE7120DE6BBBDEDB48886C,
	LineSelectionEvent__ctor_m5D735FDA9B71B9147C6F791B331498F145D75018,
	LinkSelectionEvent__ctor_m7AB7977D0D0F8883C5A98DD6BB2D390BC3CAB8E0,
	Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C,
	Benchmark01__ctor_mB92568AA7A9E13B92315B6270FCA23584A7D0F7F,
	U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73,
	U3CStartU3Ed__10_MoveNext_mF3B1D38CD37FD5187C3192141DB380747C66AD3C,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E,
	Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4,
	Benchmark01_UGUI__ctor_mACFE8D997EAB50FDBD7F671C1A25A892D9F78376,
	U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651,
	U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561,
	U3CStartU3Ed__10_MoveNext_mF9B2C8D290035BC9A79C4347772B5B7B9D404DE1,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2,
	Benchmark02_Start_m315C0DF3A9AC66B4F5802FDAED056E7E5F3D2046,
	Benchmark02__ctor_m526B38D3B7E75905208E4E57B168D8AC1900F4E5,
	Benchmark03_Awake_mD23429C1EE428EEF4ED9A753385BF194BAA41A77,
	Benchmark03_Start_m19E638DED29CB7F96362F3C346DC47217C4AF516,
	Benchmark03__ctor_m89F9102259BE1694EC418E9023DC563F3999B0BE,
	Benchmark04_Start_mEABD467C12547066E33359FDC433E8B5BAD43DB9,
	Benchmark04__ctor_m5015375E0C8D19060CB5DE14CBF4AC02DDD70E7C,
	CameraController_Awake_m420393892377B9703EC97764B34E32890FC5283E,
	CameraController_Start_m32D90EE7232BE6DE08D224F824F8EB9571655610,
	CameraController_LateUpdate_m6D81DEBA4E8B443CF2AD8288F0E76E3A6B3B5373,
	CameraController_GetPlayerInput_m6695FE20CFC691585A6AC279EDB338EC9DD13FE3,
	CameraController__ctor_m09187FB27B590118043D4DC7B89B93164124C124,
	ObjectSpin_Awake_mC1EB9630B6D3BAE645D3DD79C264F71F7B18A1AA,
	ObjectSpin_Update_mD39DCBA0789DC0116037C442F0BA1EE6752E36D3,
	ObjectSpin__ctor_m1827B9648659746252026432DFED907AEC6007FC,
	ShaderPropAnimator_Awake_mE04C66A41CA53AB73733E7D2CCD21B30A360C5A8,
	ShaderPropAnimator_Start_mC5AC59C59AF2F71E0CF65F11ACD787488140EDD2,
	ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469,
	ShaderPropAnimator__ctor_mAA626BC8AEEB00C5AE362FE8690D3F2200CE6E64,
	U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A,
	U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B,
	U3CAnimatePropertiesU3Ed__6_MoveNext_m8A58CCFDAE59F55AB1BB2C103800886E62C807CF,
	U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362,
	SimpleScript_Start_m75CD9CEDCAFA9A991753478D7C28407C7329FB8F,
	SimpleScript_Update_mE8C3930DCC1767C2DF59B622FABD188E6FB91BE5,
	SimpleScript__ctor_mEB370A69544227EF04C48C604A28502ADAA73697,
	SkewTextExample_Awake_m6FBA7E7DC9AFDD3099F0D0B9CDE91574551105DB,
	SkewTextExample_Start_m536F82F3A5D229332694688C59F396E07F88153F,
	SkewTextExample_CopyAnimationCurve_m555177255F5828DBC7E677ED06F7EFFC052886B3,
	SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3,
	SkewTextExample__ctor_m945059906734DD38CF860CD32B3E07635D0E1F86,
	U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F,
	U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF,
	U3CWarpTextU3Ed__7_MoveNext_mB832E4A3DFDDFECC460A510BBC664F218B3D7FCF,
	U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A,
	TMP_ExampleScript_01_Awake_m381EF5B50E1D012B8CA1883DEFF604EEAE6D95F4,
	TMP_ExampleScript_01_Update_m31611F22A608784F164A24444195B33B714567FB,
	TMP_ExampleScript_01__ctor_m3641F2E0D25B6666CE77773A4A493C4C4D3D3A12,
	TMP_FrameRateCounter_Awake_m958B668086DB4A38D9C56E3F8C2DCCB6FF11FAA3,
	TMP_FrameRateCounter_Start_mC642CA714D0FB8482D2AC6192D976DA179EE3720,
	TMP_FrameRateCounter_Update_m5E41B55573D86C3D345BFE11C489B00229BCEE21,
	TMP_FrameRateCounter_Set_FrameCounter_Position_mA72ECDE464E3290E4F533491FC8113B8D4068BE1,
	TMP_FrameRateCounter__ctor_mC79D5BF3FAE2BB7D22D9955A75E3483725BA619C,
	TMP_TextEventCheck_OnEnable_mE6D5125EEE0720E6F414978A496066E7CF1592DF,
	TMP_TextEventCheck_OnDisable_m94F087C259890C48D4F5464ABA4CD1D0E5863A9A,
	TMP_TextEventCheck_OnCharacterSelection_mDA044F0808D61A99CDA374075943CEB1C92C253D,
	TMP_TextEventCheck_OnSpriteSelection_mE8FFA550F38F5447CA37205698A30A41ADE901E0,
	TMP_TextEventCheck_OnWordSelection_m6037166D18A938678A2B06F28A5DCB3E09FAC61B,
	TMP_TextEventCheck_OnLineSelection_mD2BAA6C8ABD61F0442A40C5448FA7774D782C363,
	TMP_TextEventCheck_OnLinkSelection_m184B11D5E35AC4EA7CDCE3340AB9FEE3C14BF41C,
	TMP_TextEventCheck__ctor_mA5E82EE7CE8F8836FC6CF3823CBC7356005B898B,
	TMP_TextInfoDebugTool__ctor_mF6AA30660FBD4CE708B6147833853498993CB9EE,
	TMP_TextSelector_A_Awake_mD7252A5075E30E3BF9BEF4353F7AA2A9203FC943,
	TMP_TextSelector_A_LateUpdate_mDBBC09726332EDDEAF7C30AB6C08FB33261F79FD,
	TMP_TextSelector_A_OnPointerEnter_mC19B85FB5B4E6EB47832C39F0115A513B85060D0,
	TMP_TextSelector_A_OnPointerExit_mE51C850F54B18A5C96E3BE015DFBB0F079E5AE66,
	TMP_TextSelector_A__ctor_mEB83D3952B32CEE9A871EC60E3AE9B79048302BF,
	TMP_TextSelector_B_Awake_m7E413A43C54DF9E0FE70C4E69FC682391B47205A,
	TMP_TextSelector_B_OnEnable_m0828D13E2D407B90038442228D54FB0D7B3D29FB,
	TMP_TextSelector_B_OnDisable_m2B559A85B52C8CAFC7350CC7B4F8E5BC773EF781,
	TMP_TextSelector_B_ON_TEXT_CHANGED_m1597DBE7C7EBE7CFA4DC395897A4779387B59910,
	TMP_TextSelector_B_LateUpdate_m8BA10E368C7F3483E9EC05589BBE45D7EFC75691,
	TMP_TextSelector_B_OnPointerEnter_m0A0064632E4C0E0ADCD4358AD5BC168BFB74AC4D,
	TMP_TextSelector_B_OnPointerExit_m667659102B5B17FBAF56593B7034E5EC1C48D43F,
	TMP_TextSelector_B_OnPointerClick_m8DB2D22EE2F4D3965115791C81F985D82021469F,
	TMP_TextSelector_B_OnPointerUp_m303500BE309B56F3ADCE8B461CEC50C8D5ED70BC,
	TMP_TextSelector_B_RestoreCachedVertexAttributes_m3D637CF2C6CA663922EBE56FFD672BE582E9F1F2,
	TMP_TextSelector_B__ctor_mE654F9F1570570C4BACDA79640B8DB3033D91C33,
	TMP_UiFrameRateCounter_Awake_m83DBE22B6CC551BE5E385048B92067679716179E,
	TMP_UiFrameRateCounter_Start_m59DA342A492C9EF8AD5C4512753211BF3796C944,
	TMP_UiFrameRateCounter_Update_m28FC233C475AA15A3BE399BF9345977089997749,
	TMP_UiFrameRateCounter_Set_FrameCounter_Position_m2025933902F312C0EC4B6A864196A8BA8D545647,
	TMP_UiFrameRateCounter__ctor_m5774BF4B9389770FC34991095557B24417600F84,
	TMPro_InstructionOverlay_Awake_m2444EA8749D72BCEA4DC30A328E3745AB19062EC,
	TMPro_InstructionOverlay_Set_FrameCounter_Position_m62B897E4DB2D6B1936833EC54D9C246D68D50EEB,
	TMPro_InstructionOverlay__ctor_m7AB5B851B4BFB07547E460D6B7B9D969DEB4A7CF,
	TeleType_Awake_m099FCB202F2A8299B133DC56ECB9D01A16C08DFE,
	TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7,
	TeleType__ctor_m12A79B34F66CBFDCA8F582F0689D1731586B90AF,
	U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9,
	U3CStartU3Ed__4_MoveNext_mEF7A3215376BDFB52C3DA9D26FF0459074E89715,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261,
	TextConsoleSimulator_Awake_mA51EE182A528CCA5CAEA8DBE8AD30E43FDFAE7B0,
	TextConsoleSimulator_Start_m429701BE4C9A52AA6B257172A4D85D58E091E7DA,
	TextConsoleSimulator_OnEnable_m3397FBCDA8D9DA264D2149288BE4DCF63368AB50,
	TextConsoleSimulator_OnDisable_m43DF869E864789E215873192ECFCDC51ABA79712,
	TextConsoleSimulator_ON_TEXT_CHANGED_m51F06EE5DD9B32FEFB529743F209C6E6C41BE3B8,
	TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534,
	TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4,
	TextConsoleSimulator__ctor_m4719FB9D4D89F37234757D93876DF0193E8E2848,
	U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D,
	U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C,
	U3CRevealCharactersU3Ed__7_MoveNext_mA9A6555E50889A7AA73F20749F25989165023DE3,
	U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F,
	U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0,
	U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8,
	U3CRevealWordsU3Ed__8_MoveNext_m3811753E2384D4CBBAD6BD712EBA4FAF00D73210,
	U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27,
	TextMeshProFloatingText_Awake_m679E597FF18E192C1FBD263D0E5ECEA392412046,
	TextMeshProFloatingText_Start_m68E511DEEDA883FE0F0999B329451F3A8A7269B1,
	TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7,
	TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF,
	TextMeshProFloatingText__ctor_m968E2691E21A93C010CF8205BC3666ADF712457E,
	TextMeshProFloatingText__cctor_m5FF3949FBDB0FD13908FB7349F64DDB3012512B8,
	U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_m3F7F9561735FF8403090A93E708E781B3997B81B,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m6F24E1DC5777D4682375D8742BC9272EE6D4EA2C,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m04B1BB9074CBA76BA7F63849721F4CAEA1045A28,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14A7C9DEBD0FF0C7A6979AFEC78B0BEE9E4C3963,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mC0D5548A92F57550F60EE76E17B58125B57B94A8,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mD3CBFD6F8558EF09F2B4A3D8D1B76BD7FDB3A98B,
	U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m0466586E44A01852547C6CECCA91C14673B98C20,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m8730A3A2EFBF759A51F0E5330C3BBF8EFC1736A3,
	U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m93E19CF96C4EBC57CC4E03E2D3D0B56B3E92A36A,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4076BE73343A473EFE46E73ABA5DE66CC797598,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_mC33B355BB392C5A52DCA0AAF2588E1E78AF6D3A5,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m28CCE2E51295AD5F368C504BADDDFF008C2CC0A6,
	TextMeshSpawner_Awake_mDF8CCC9C6B7380D6FA027263CDC3BC00EF75AEFB,
	TextMeshSpawner_Start_m7CC21883CA786A846A44921D2E37C201C25439BA,
	TextMeshSpawner__ctor_m1255777673BE591F62B4FC55EB999DBD7A7CB5A1,
	VertexColorCycler_Awake_m84B4548078500DA811F3ADFF66186373BE8EDABC,
	VertexColorCycler_Start_mC3FF90808EDD6A02F08375E909254778D5268B66,
	VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42,
	VertexColorCycler__ctor_m09990A8066C8A957A96CDBEBDA980399283B45E9,
	U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F,
	U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007,
	U3CAnimateVertexColorsU3Ed__3_MoveNext_m2842AF5B12AFC17112D1AE75E46AB1B12776D2A6,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52,
	VertexJitter_Awake_m9C5586A35BD9C928455D6479C93C1CE095447D8F,
	VertexJitter_OnEnable_m97ED60DBD350C72D1436ADFB8009A6F33A78C825,
	VertexJitter_OnDisable_mF8D32D6E02E41A73C3E958FB6EE5D1D659D2A846,
	VertexJitter_Start_m8A0FED7ED16F90DBF287E09BFCBD7B26E07DBF97,
	VertexJitter_ON_TEXT_CHANGED_mC7AB0113F6823D4FF415A096314B55D9C1BE9549,
	VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16,
	VertexJitter__ctor_m550C9169D6FCD6F60D6AABCB8B4955DF58A12DCE,
	U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_m169A75C7E2147372CB933520B670AF77907C1C6B,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF,
	VertexShakeA_Awake_m005C6F9EE8A8FD816CD3A736D6AF199CD2B615A2,
	VertexShakeA_OnEnable_m9F60F3951A7D0DF4201A0409F0ADC05243D324EA,
	VertexShakeA_OnDisable_m59B86895C03B278B2E634A7C68CC942344A8D2D2,
	VertexShakeA_Start_m3E623C28F873F54F4AC7579433C7C50B2A3319DE,
	VertexShakeA_ON_TEXT_CHANGED_m7AD31F3239351E0916E4D02148F46A80DC148D7E,
	VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599,
	VertexShakeA__ctor_m7E6FD1700BD08616532AF22B3B489A18B88DFB62,
	U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mCEEDE03667D329386BB4AE7B8252B7A9B54F443F,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E,
	VertexShakeB_Awake_m99C9A0474DBFE462DC88C898F958C1805376F9D5,
	VertexShakeB_OnEnable_m64299258797D745CDFE7F3CBEC4708BDBC7C3971,
	VertexShakeB_OnDisable_m07D520A8D7BCD8D188CE9F5CC7845F47D5AD6EF4,
	VertexShakeB_Start_m9642281210FA5F701A324B78850331E4638B2DD1,
	VertexShakeB_ON_TEXT_CHANGED_m5650D69D258D0EEF35AF390D6D5086B327B308A5,
	VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87,
	VertexShakeB__ctor_m605A778C36B506B5763A1AE17B01F8DCCBCD51EC,
	U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m0534E436514E663BF024364E524EE5716FF15C8E,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD,
	VertexZoom_Awake_m1B5D386B98CF2EB05A8155B238D6F6E8275D181C,
	VertexZoom_OnEnable_m7F980FC038FC2534C428A5FD33E3E13AEAEB4EEC,
	VertexZoom_OnDisable_m880C38B62B4BB05AF49F12F75B83FFA2F0519884,
	VertexZoom_Start_m10A182DCEF8D430DADAFBFFA3F04171F8E60C84C,
	VertexZoom_ON_TEXT_CHANGED_mB696E443C61D2212AC93A7615AA58106DD25250F,
	VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED,
	VertexZoom__ctor_mE7B36F2D3EC8EF397AB0AD7A19E988C06927A3B2,
	U3CU3Ec__DisplayClass10_0__ctor_m4EC5F8042B5AC645EA7D0913E50FD22144DBD348,
	U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m959A7E1D6162B5AAF28B953AFB04D7943BCAB107,
	U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m8340EFEB2B2CF6EA1545CFB6967968CA171FEE66,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F,
	WarpTextExample_Awake_mEDB072A485F3F37270FC736E1A201624D696B4B0,
	WarpTextExample_Start_m36448AEA494658D7C129C3B71BF3A64CC4DFEDC4,
	WarpTextExample_CopyAnimationCurve_m744026E638662DA48AC81ED21E0589E3E4E05FAB,
	WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2,
	WarpTextExample__ctor_m62299DFDB704027267321007E16DB87D93D0F099,
	U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49,
	U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60,
	U3CWarpTextU3Ed__8_MoveNext_m9C83C8455FF8ADFBAA8D05958C3048612A96EB84,
	U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25,
	Soundmanager_Update_m74DF9C012C81AA801A44F4FDC8AC74DE9EC297AC,
	Soundmanager_Frige_m96C7E9177EAD0F9F183D6A409EA8D2B9D9BEF817,
	Soundmanager_BED_mF1315719DC7550F3F765551DE7E1718D31ED34F1,
	Soundmanager_Radio_m189672A2157ACE9F2F835040E6708EF4339AD68E,
	Soundmanager_Craft_m5DFB6F8FB5415A24BF4240E63BA36A0D87F9087C,
	Soundmanager_LV_UP_m5721FA3F9529787E6706A8AAB92BD85E50B657A2,
	Soundmanager__ctor_m5551364FC678ADE0EA31E2BB68C8F60F29F1A2C9,
	Sound_Update_mE12B8CD3A20B004DC36BC45E9BC9C25E645E6D61,
	Sound__ctor_mC4DC1A033BFAF868C25B9BE0C5EF88746FC409E5,
	BuildingManager__ctor_m7E966CE862DD79F8D88433097A7037EE516521C0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PlatfromManager_Start_m8FE9C105B2EA16FE51891CA6B5B078E93281BF08,
	PlatfromManager_IsRotate180_mF43E0B55F21A7B5CB11D28FAF6287271029E70EF,
	PlatfromManager_IsRotate0_m20F1688A43BB64494A487B84BB80AFBEBAC36536,
	PlatfromManager__ctor_mD28BEC470527210C7F9F68690AABF0CA70B7A9E1,
	SpawnManager_SetSo_m8EC7ED1F7FD378F297290C75BC2A30FACF6D5E1B,
	SpawnManager_SetCAndF_m788401AA369A7B68BEA6BF12417DA251370FDD05,
	SpawnManager_Start_m2914D0E400BF8251790B98C978F1892EC16D5D5F,
	SpawnManager_SetDo_mD3290533A25967749AEE5E58E8F3B7AA677BB3FD,
	SpawnManager_Spawn_m17DF3984B2CB551FBF02408BDB46185BBB63867C,
	SpawnManager_SetSpawn_mAAEBA95EABD7CC29B5BE361B29F8A213467F3987,
	SpawnManager_Spawner_m7B820BDCD1BB3955B04D1F27E5AFB6CFAB502812,
	SpawnManager_Update_mA35DCE24CE5FCEFEEF0910123C5664BABD2240F9,
	SpawnManager__ctor_mE5AB370502C4322E2E8AC79E6052CB98068D6A49,
	UIManager_Start_m30D1D9B7AE6D78B5422B9F91B698F76AF09A743B,
	UIManager_Update_mD58EC6793FABAFC42ECABA3EAE82FE1836D6EF5B,
	UIManager_ClickOther_mF6E3D08C01D79CFCF9B1E9895D9839E08731555B,
	UIManager_OnClick_m8FC4AD0B85F91D0458887FEA359F86DED6492581,
	UIManager_OnClickUIPlay_m96BACFBAA2726D19573D833B882001AC877D9B7F,
	UIManager_Pause_m2D383414219C4487F2791FC5ABC3BEBC5DBEB082,
	UIManager_Continue_m69402401EDC6567B9BEFCB9901CB32D949BB00FC,
	UIManager_ClickPlay_m9ACBFAD72FA41316EFED8DB4C034D55ADEB049C5,
	UIManager_ClickQuit_mF285C011A906DE445B31B991031C5C176FF67198,
	UIManager_GameWin_mF2AB94D893A28F3983AB149BB9EBBE939065ADBE,
	UIManager_GameLose_m5288C91533E2354065396B2985E4F32AD86CAC40,
	UIManager_GameQuit_mF590928DD6B3D2D344C2349BD8789CAADDE18D5E,
	UIManager_CheckAndShowText_mA26C841C7A8B02AD3F0E2FCD860B5BCBD74610FF,
	UIManager_CheckBigWaveDay_m429C6672B6233081A96D796A4A60EC26757BC65C,
	UIManager__ctor_m8ECCADCA9247F60A1D29333FBADC9494CCCB9312,
	UIManager__cctor_m14BF6A4B635F3569EFA4C551189459DA4F19A393,
	CharacterMove_Start_m257966B887DD11B33BDF48671CC666080BAB1FD8,
	CharacterMove_Update_m8B3187A4F87D10523AF6B106A039B9A24E291BC3,
	CharacterMove_FixedUpdate_m2D6D6D80D202FF6167F0F277929735CEC79EFED3,
	CharacterMove_Selection_m4B218325D01D3B356362FEAE7B76221B2FF3DE0E,
	CharacterMove_Move_m4CE32DFD9EF7D2FAE06446121B01E1EF42C012AE,
	CharacterMove_SetPosition_m61F6019D17C2B5529AF270243F3B682757892B01,
	CharacterMove_Rotation_mFED0F8E423A048BA0EF6F5F4F5564E6C95C5B340,
	CharacterMove_Climbing_m82C4900C2A36EBC624755883C3AA23DC1E0B1A64,
	CharacterMove_OnTriggerStay2D_m644439F00D9100C83ED94748620755B3D6C4CCBB,
	CharacterMove_OnTriggerEnter2D_m4632E9E168E170350B961DA639C83216E3522A9E,
	CharacterMove_OnTriggerExit2D_mF85BA188A925E601FB5EE8A42FE7EEB04C514E06,
	CharacterMove_FindLadderRight_m1EF56287B091B70D96202218663CAF720AFAD812,
	CharacterMove_FindLadderLeft_mA650CACBFFAE61A93F30EDCEFFC1A97F9ECE4170,
	CharacterMove_CheckToLadder_m00725F34E489F87D20EC59EE7FD8C329D90CF523,
	CharacterMove_MoveToLadder_m817AE798764995AB84F82547B04A1C7531367145,
	CharacterMove__ctor_mCE2847ADCE505966C47F1AC3F79577F74EC21592,
	Direction_OnInstant_m6CF5592DFC35586EC1B1F1542465FE11BD6135EF,
	Direction_Update_m6B21A4EE3452A662C24CFD59AAAA80F32B8C5230,
	Direction_DestroyDirecUI_mDBABA1FEFB3E2925EBD49F38416D0D1DF38317FD,
	Direction__ctor_m43FB67A7571F853129C6513828383B7486DF7B2F,
	FindMaterial_Update_mDAF5067442621C41B7545C79C5846D6CFD3FC801,
	FindMaterial_Find_mFB51ECD5976932B95B1959794082078FF499F2DD,
	FindMaterial__ctor_mB886E1806B1D378E6DCCDCD96D2BBCD9B9156AE1,
	FireManager_Update_m2D43DAA0463D61E8B1286BF788BE6BB204D1570F,
	FireManager_Shoot1Enemy_mADC43B16FB7289CD4E4AB64D9302E18D788A813C,
	FireManager_ShootThru_m1298F8F648155BCDDC4229E468E5D951D825D48C,
	FireManager__ctor_m03A6E01A8F00323994D01B0F8B5979089A0CF141,
	PlayerManager_Start_mEAE82B2C4E0B77F8ECEE948BCAD04F6D77E3FDB1,
	PlayerManager_Update_mADC80049A80052CD88405D1B7AF948060D04795F,
	PlayerManager_SetSlider_mB61C81B0ABD3C0D6FF8C745F97B9DAC844AB783E,
	PlayerManager_HungryReduce_m120A192980D51E77F3B69769048BDB0F4747900B,
	PlayerManager_HungryPlus_m0B962F1072BA7AC0522BE88BB86C2478CC77D7B5,
	PlayerManager_HpPlus_m19F7F266EF1245E1E3FBFC6B4FD491CD9D8B5C0E,
	PlayerManager_Damage_mD1B821A822502051EFBEC098179AF8DD34109D89,
	PlayerManager_SetPlayerSo_mA923DBC7D40253550FBF92F1BA125EA99EEACFBD,
	PlayerManager_IsDoingWork_m9593617A98175F4F06D9A8A1FCB8FCE62204B311,
	PlayerManager_IsDoingWorkSet_m59EED3EBDB1CB6CE814644A675C5A0801211D8F1,
	PlayerManager_IsGenerate_m6A68B960D78F1D6E43D8E9B482FACB9A52B35ACE,
	PlayerManager_IsGenerateSet_m13AD1480C2DD02B3AA73AE4E02A2FA3CAEAE45A4,
	PlayerManager_SetPlayerLv_mA70DF5F7E92687CBFAD0719326AC90D36A377541,
	PlayerManager_PlayerLv_m257275C2189D591A977B140947C4DC7158EDCC47,
	PlayerManager_Shot_m8745F07EC76BAC781C5AB1D1475FA49C2735388D,
	PlayerManager__ctor_mF0285F069DD6A97E5F8A83E9640C66B7B547F5C8,
	BaseDamage_Damage_mCE308A5737937691F2E37FAB572C867943E4F955,
	BaseDamage__ctor_m1CB85B501E977C53BFFE2BAC9CFB5FA6490A0A0D,
	BaseManager_Start_m405C00D0986B0FC9FDB6BB7B9C9FE8411053D05D,
	BaseManager_Update_m6CE8D6CE1B813015EBA57179B3DB452134B8D741,
	BaseManager_CheckPlayer_mEF5A4B2D8B3C3BB496A345DE687157ABDEA5E268,
	BaseManager_Damage_mFE5B61A207017F62528FE738F379F95646B1EA7D,
	BaseManager_ClickFix_m04AB72EA57E4C8080D2B169E98A58D0634D39EC0,
	BaseManager_ClickCancel_m95135BFFEB2494D36FA6B0C30CE7AA0835F96932,
	BaseManager_SetText_mB8EA1C8CE7DC41611D1A23A601921666644E7F81,
	BaseManager_FixBase_m51722C365224C79E334804B0A99929076C42601B,
	BaseManager__ctor_m3EE267AD02BF980FE1881223D0E8D04CC6FA5C6B,
	CheckPlayer_Update_mF44444F1811599ECA58A4A8FDE1854CE59189B86,
	CheckPlayer_CheckPlayerInScene_mF43A9C299A4E174E946021EC0C8843F8F1BCF760,
	CheckPlayer__ctor_m34EC14FD61F650BB671A7BE314C636937C602179,
	FactoryManager_Start_m50DBD9106DCC92BDC55908992BCA6722E28D1D2C,
	FactoryManager_Update_mC4B194FE946C209877469FBFA5DA677D23FB5B19,
	FactoryManager_SetUIAll_m679C98D812EE509A4A85C56AE7400A38EC653A7B,
	FactoryManager_GenerateMaterial_m5F2202E6790EB864087C21A1FA3A10BD8D001759,
	FactoryManager_SelectFactory_mB444C11EB33B91D2564BA5102582A20F975CFBAA,
	FactoryManager_SetSo_m62C2C8D42B30D452E7BE522A866493C6C86B2853,
	FactoryManager_SetMaterial_m5208FC67DDDCBBBAB28838943FA312FCB8B105E9,
	FactoryManager_ClickGenerate_mB810E824C10B7ECCF0E2759AE5F287EC04296212,
	FactoryManager_ClickCancelGenerate_m23DA6EF240B21976DFFB341D546F47CBC7542592,
	FactoryManager_ClickSelectPlayerToFactory_mEC96C811A4EDCFC74BCB4810A84969AF93463510,
	FactoryManager_CheckPlayer_mA1A8E2F57F1785A48E250C3EE3B5267B7AE73B8F,
	FactoryManager_OnTriggerExit2D_mF0D1C08AA05E4B4A65DFA27ACC59A8D8EA4E9ADA,
	FactoryManager__ctor_m9C5FFFF05A299EA7D5FE98C30D177DA6114F8555,
	FoodSpotManager_Start_m485950875F8CF186646AF14E6EC55906D9E3F235,
	FoodSpotManager_Update_m20645A54F4518FCAF727A20FB11AE677D0ACB88B,
	FoodSpotManager_CheckPlayer_m69DDFBA135C950A93A9A3C993B46D03B64B45819,
	FoodSpotManager_ClickFeed_mC957B5DBA352AE4A7BC8B35C1A8BE9F7EE737A76,
	FoodSpotManager_ClickSelect_m3DAF48E2B6F7AE3CB953A3642847B72A2116BF6D,
	FoodSpotManager_OnTriggerExit2D_mB833B98D271312EF5169EBE4258752FAFB5A5145,
	FoodSpotManager__ctor_mB717E24FA39B3D947C265E03D3ECBB1072BD45D7,
	GameManager_Start_m0EF119BD17C6D75A16D3F807D719064FB9F8243F,
	GameManager_Update_m99B56B63EDAD1B6A16C6AE463A4F96D6FA58C95B,
	GameManager_SpawnPlayer_m8F255DBF77242C4053255B8082009B55A6E6280C,
	GameManager_SpawnPlayerStart_mDDC1193874A5A6F8AFD289572BCB6F503FFF167F,
	GameManager_SetNameAndAdd_mB8CC00F2FDE1A211FA604D236084EBCADC78BF16,
	GameManager_ListThePlayer_mF715B32CFAE5347785DFC8AE782A7865420CF553,
	GameManager_DestroyPlayer_m952CC84AC6C358ECB93170CDAE18F6D8A12F9116,
	GameManager_SpawnBase_m31B9245A18F46872F9BA5B096E908A2987E04406,
	GameManager_GameWin_m44BB3C674FFE1B8905EF202D0C241DF015ADF2D3,
	GameManager_GameLose_m14F7832835F2728EBAE88A4089A5E08C13073208,
	GameManager_Score_mFAEC4974B09BD5373A560F9C8A1921643E8183FF,
	GameManager_SetScore_m7850D63048E55226A62B5ADFEF46CEF7DE8B7C34,
	GameManager_ShowScore_mEFBB4EE3811C279754B54FA4B0AE48F1AD479984,
	GameManager_GameExit_mDDCA18AA4321ADC5199A0ABB0228C29DF819677E,
	GameManager__ctor_mA39016FC4FFB429D0F123D1582917989C89E2517,
	HealStation_Start_m8A939344A1E54F06288FDFC8629D8F77771A5E9B,
	HealStation_Update_mA140D8D8F0EA8127E1F5322B02B133BE37E33509,
	HealStation_CheckPlayer_mD547B7885D7BBD5201A74ACB4A6F5AF55F5317A0,
	HealStation_OnTriggerExit2D_mE7F9E024E159F624A2145EE98C84BAAED4768506,
	HealStation_ClickSelect_mF07937350E253FD6E7A46EEBB6D54308A15A2C04,
	HealStation_ClickStart_mFDFA9CAF205B4744E947205A24886ABEE97CCD82,
	HealStation_ClickCancel_m3967E354EC5CE11713D1F35DDFE2953C89CA5F76,
	HealStation_SetUi_m90BA08BF988F6D06B8C5B16DF27FEC4EEF20C614,
	HealStation__ctor_m7AFDC11DF9D47B97ACD82B2FC2B3FE0D808AEBD0,
	InventoryManager_Update_m8BC45ABCAB101C5323C104F155C8412301BBF029,
	InventoryManager_ChekAndAdd_m66A9147DE4EE1E3BD67734C1B84188E4DCFF696F,
	InventoryManager_RemoveMaterial_mB90923819917D89E180064016F25EFEC1A54F780,
	InventoryManager_CheckAndRemove_mF56221EC3148961173D4015C75B2E609CA083D6C,
	InventoryManager__ctor_m6A4BFF1ADFEA179EECB250DD71FAAE6E1069A1B8,
	LightManager_Start_m99EB45F9427E91EEEE67A9056873CC9E4021DB4D,
	LightManager_Update_m3A1B813FF2D68D9FCA3F3A44C89A6927E507F448,
	LightManager_SetLight_m1A497DBB32555195FA6DA50EFCF49C995987E2A4,
	LightManager__ctor_m886D2DCF596AAF9F3E24AB77F2AF52D811361C3F,
	LvTutorial_Update_mAA51DB51AB924BE49C3804C3ED889F83A85750C3,
	LvTutorial_CheckPlayer_mC2DB70C4CE2FBB4F7DC5DC705F66293B7488B40C,
	LvTutorial__ctor_m72E7D375C0D41BB08F42A96D5DFDDB9EAA81AC7C,
	LvUpManager_Start_mDB4E4585B9A5D86C40D4DE0432DD57A6B761C239,
	LvUpManager_Update_mA776B6905A04C3D355847504A0B8FEABE7DFCBDC,
	LvUpManager_CheckAndSetPlayerLv_mB03A40080A983D61C67EB926D71DBC9FB315F70B,
	LvUpManager_SelectAndCondition_m9F8BFF42EC2666AF280DC13F9C7643CE13A1EF05,
	LvUpManager_UiSetActive_m75614EC89D171BAE3D7EB9F2F5FC7A55AED42564,
	LvUpManager_Setmaterial1_mF7BD1E13044E349263832E658D4CA636A53096A9,
	LvUpManager_Setmaterial2_m8A482BB59A7BE047C69DCD5EF31041A16975330A,
	LvUpManager_Setmaterial3_m308E34A4E538C6EF7C649330051CDE002FAD8D7A,
	LvUpManager_SetRqM1_m9DAA7CB8E46F3978CC83A309F95D7124DDCBED69,
	LvUpManager_SetRqM2_mF2D1B69BD726888F0B6C69D4C7E0C78376790025,
	LvUpManager_SetRqM3_mD9E32CA0A809D6285EF0318AE795D1E73EBFAF9B,
	LvUpManager_OnTriggerExit2D_mE20FEC0DF9A6B1DB4D3EF8855C83158163D4C492,
	LvUpManager__ctor_mA1E9E27F30D3E24DBA7C717AC1B8DFD8061F64DC,
	MoveCamera_Start_m64AEDA3BD1EBD98F2C09AD6C1581A934D55014F9,
	MoveCamera_Update_m0A5C860EA49C17A849C51BB4B2D141FC0082336F,
	MoveCamera_ResetCamera_mB8088C05267F8729298C02CE33947F5C2E4B3DFE,
	MoveCamera__ctor_m9D1449774B647BD35259A1E157455EC268D22DEB,
	ReinforcementManager_Start_m704287AAAA1094E3AF11112CEB44F39268A2A05E,
	ReinforcementManager_Update_m6E4CD309856354AD68CB6C59EF628C5F0110880C,
	ReinforcementManager_RfPlayer_mA38EB3C81087BA7A901F7231551570906442D39A,
	ReinforcementManager_OnTriggerExit2D_m1DD77D3D007AC3902EBA5E4973195D2539F980AC,
	ReinforcementManager_ClickRf_m9CBD544BA204DBBA4EA402C46122EBA090EB731D,
	ReinforcementManager__ctor_mA43BE5D9AA0AD3B272EB6611627044E07861B9E6,
	Tutorial_Start_m3CBAA1BEDC8CB2E4FFB90AB956834087D2A6C667,
	Tutorial_OnTriggerEnter2D_m35ECA137580629EAD929A46DC23F67BEB501D469,
	Tutorial_OnTriggerExit2D_mA98DB3014BBFBB986704D62F71C8E95F574B4C74,
	Tutorial__ctor_m87A8838748CCCE8002A2154285057CD40F2F4551,
	TutorialUIManager_Start_mA37C7F0E2A98F1689BECDC0D41DFE38A6F8C6D05,
	TutorialUIManager_Update_m8FEC2C84EED0F0816D8A30E236715A4D78C5C68B,
	TutorialUIManager__ctor_m7355C66656A355CA245D9998BD4C8913F99B740C,
	MaterialManager_Start_m59AFCADB11F369FB8A2666DDB7398220369110AD,
	MaterialManager_Update_mF5A3C0E8EC8875FAF86EF86B2E32E066A66CD796,
	MaterialManager_CheckPlayer_m3B5F10970DDD4C4C4629CEB666378608ED216412,
	MaterialManager_ClickKeep_m7C6ED7F4CF265A1FB762D6B81EEC646FD2B83816,
	MaterialManager_ClickCancel_m89CF04C9FB1191B2DD82A9AD03A9864146DC5DA4,
	MaterialManager_IKeep_m971961B10D1A4E7BBA457F9061F157362C564698,
	MaterialManager_MaterialSetSo_m6F654CC1DD6A4794F6DE03F6C50EE7A3355536E4,
	MaterialManager_Damage_m33813A56A3B321106EC99BDEDC66697363A0473A,
	MaterialManager__ctor_mD99481FBF92105EC70D2FFBF6001A8067CE9AAF3,
	EnemyMainGame_Start_m84E14F2AF2CD654A99E7C6430E7CF89AED4D0C02,
	EnemyMainGame_Update_m54DDFF9CE3A5FA4DA7A66DB02D077F29DE71BA34,
	EnemyMainGame_Walking_mAC8FB21362B773F550A64A79DB899D3935D9F2C5,
	EnemyMainGame_Attack_m999AAF5DEC46BB17A1F6D1B43877CA0560610376,
	EnemyMainGame_Damage_mB2D9D081745B4342622C57758AA89807DAAB766E,
	EnemyMainGame_HorderAttack_m8F56A546B60E03B1A9BF8CA90B011796D6A9EB36,
	EnemyMainGame_EnemySetSo_m0BCDB9FA42CD7DABB441709FB5C59D80C6BC5529,
	EnemyMainGame__ctor_m8D656B27E3250BD6D2502550423CAA2EAAA53774,
	EnemyFrom__ctor_mE3E743282D65CC63958C536EF55D32449F6554C5,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	LeaderBoard_UpdateData_mF378FEB2ED5E0FB959E2A2702219ABBFB0CFF9CF,
	LeaderBoard__ctor_mA72A7EF68FDA8A1486F87F04CEAFD147B0C9783E,
	PlayerData__ctor_m6F8EA950FBCA164DEEB2D2BDB3C042BD0445A686,
	SpawnSo__ctor_mF7D614AEB481DD7162111CBA81B7B5C3782576D6,
	PlayerSo__ctor_mC99B8B485B534C95B7ADCA7CEB273F12C65F4E1B,
	MaterialSo__ctor_mACD4972C54650A369CAF39878D31370872C3753A,
	FactorySo__ctor_mC8C502A4511DEEBBEF38C9A56846A1B77071D252,
	BarricadeFileSo__ctor_m2DA7256EBE6376FB349568B49434115884E44CAA,
	BuildingWall_Start_mFB4DE8008CB378C079D3D36B2C7AA69990477B31,
	BuildingWall_Update_m5AA91A2C37B92B9159C48F0586B5BA4FDD6B6BAF,
	BuildingWall_Damage_mE5596CA50EDA9F948888C92B56FE4EE188D9C0AA,
	BuildingWall_SetBuildingSo_m457CD46D87C2D6254021F313B064370955E37085,
	BuildingWall_SetMtSo_mFA6AE20480CCB4D55FAB8C59A734797A2F0FFDB3,
	BuildingWall_Fire_m701B74EDFC98FAD7FFD79B42612E0DAF418FFC3A,
	BuildingWall_CheckPlayer_mB0CAACCBC1F0C76B58637CD5C9FD448DEDEFC9F5,
	BuildingWall_SelectType1_m189A03A10C63D57950EE186D63724CA8E64FC8E8,
	BuildingWall_SelectType2_mD539BD501E737F4D9B7D8C053B626BD885CCD359,
	BuildingWall_SelectType0_m17AC622BAA963E33A4471B6DFE8B0F2C6CBCE7E9,
	BuildingWall__ctor_mC2DBA4C89C22F3D9DF460949675B7D6AB033A99B,
	Climb__ctor_m233AD2F2D7BD21041C6D493D7AB64188EB3C021F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PromiseCancelledException__ctor_mDA55F2F9C6DF87917C0A40BC149A437C750C66FD,
	PromiseCancelledException__ctor_m0733E2CEAEE71A6A1A22E96FFF62D85627437BF1,
	PredicateWait__ctor_mCD6942E554ED82AF36B3F07828C4E5BCA7F0D318,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PromiseTimer_WaitFor_m99B243125D5686DC36844FA69D796651E12C9B76,
	PromiseTimer_WaitWhile_m8EF7C405289D3A2AE7F7185AE8E4E901BF504EC9,
	PromiseTimer_WaitUntil_m7AC7AE2AFC6CB5E43A789DE5A6E683D62D23FBFC,
	PromiseTimer_Cancel_m87F8524626DDC28FB1ECA1968F73A2D18540704E,
	PromiseTimer_FindInWaiting_mA1660E092E5065B151FB000488B2C87FF4C6C8F1,
	PromiseTimer_Update_m9C84E7E7B30B14A7C13510CC22AB6032C7C0874F,
	PromiseTimer_RemoveNode_mF32CC3AE04AE1AFBCEF4B8DA7DFD9E2AF02A6E89,
	PromiseTimer__ctor_m3F1298B21CD5644C3AE646911AD13B6FCCD19BE6,
	U3CU3Ec__DisplayClass3_0__ctor_mC51BCEA4436030F27897F20585D501ABF744C78E,
	U3CU3Ec__DisplayClass3_0_U3CWaitForU3Eb__0_m7EEE8458FA739B7F87A8EF705E05039C9EFB483D,
	U3CU3Ec__DisplayClass4_0__ctor_m8884F549B84F64B7E7D4B61E036A398416091313,
	U3CU3Ec__DisplayClass4_0_U3CWaitWhileU3Eb__0_m8DF4AC7205D6CD8FF4E8AE640F92F6AB4A9B59F7,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ExceptionEventArgs__ctor_m7023DA6E99C0B23D96D0AAB0500F16B19345A30C,
	ExceptionEventArgs_get_Exception_m79958EAB2CAA0E6C91E10902A3ABCC242012745A,
	ExceptionEventArgs_set_Exception_mBEFA8B8A6990A708691413D5E188FD73D6036A88,
	Promise_add_UnhandledException_m501ED8710D326733D0A4B095516E79AD93771A17,
	Promise_remove_UnhandledException_m0A7255398B93C672C3F4AD9AE8CC1260E6BDC493,
	Promise_GetPendingPromises_mCD78AA3FD69C8809D95E0FBBC6DC24DB648CD1E4,
	Promise_get_Id_m2A159B9C83B813983859B9F8497B92A636D26198,
	Promise_get_Name_m2681487A6A6188C86D6B1FEE6F4E42F0817F0AF1,
	Promise_set_Name_m04BD79E80089945F37642A501B2E0C621CFAADFF,
	Promise_get_CurState_mBE60E772D62CB2C8449BF67F3A2106C0E0FA720D,
	Promise_set_CurState_mC9014B9835DB4BDFE7B3E1AFC0B61A249DB0B148,
	Promise__ctor_m72891E8C449A8F0285F2BBEE6596EEE91D1B69D0,
	Promise__ctor_m0F170C315BB66058A0B8DB59D2FB2E269460DB4A,
	Promise_NextId_m91D574BF971140F5BCA824858A1F23785E625936,
	Promise_AddRejectHandler_m8A5C912F1C8D8FC752EAAFCC3397992F84DE2C83,
	Promise_AddResolveHandler_m39C28AC656C57C1865250657901C41569B49B87A,
	Promise_AddProgressHandler_mC3147E564182B45C04B4AD18F55200ADE09494A9,
	Promise_InvokeRejectHandler_mD002929AA239DA2F2D3CBA6B828BCC190E2FA010,
	Promise_InvokeResolveHandler_m8253411D16B16952735214A8D4840B69CBC19367,
	Promise_InvokeProgressHandler_m2767B570D7BE8E457C295823CFD48F4DF079AD1F,
	Promise_ClearHandlers_m77595F54867BFFB97E19A6354940D3C574E8AF30,
	Promise_InvokeRejectHandlers_m78C4A77BB7F1728BCF63373111A992940FD73FBA,
	Promise_InvokeResolveHandlers_m071F1B5884447CEE1F7D19154AB430522BE6C208,
	Promise_InvokeProgressHandlers_m33BBE047F8B32BDC15B943203CF4D653C10F5F3D,
	Promise_Reject_mA334C811DFA609A9294A15B8C4FE3D06CFE59E4B,
	Promise_Resolve_mB395CBF764AD56F64C953F240F5525C4F24AA176,
	Promise_ReportProgress_m7496AD24A305E551C317FAF83A339D50555EC2F0,
	Promise_Done_mBF4D40ECA5734EFE1213BD2AD11124841BCA6C63,
	Promise_Done_mA20B58D299DFC458330AB673679F52760076ED6D,
	Promise_Done_mB94F090BD0EDC421C98291DF2437788619416322,
	Promise_WithName_m9A8610E5066D79F98133FBDE0C427032A9EF04DB,
	Promise_Catch_m34A1A6F483209E344CEE20CAECA131319E25A35F,
	NULL,
	Promise_Then_m660D492D7CB977D2658FDC71CA16C5F1C7C80BFB,
	Promise_Then_mE35145837C7EBB840D64C24A92FFEC013AA4E52F,
	NULL,
	Promise_Then_m9C5987E6126BE35855402986BE5878DDDA5DDD2F,
	Promise_Then_m1F2DA120D93DB9D1801A4410BACE242992BFDE5D,
	NULL,
	Promise_Then_mE03C61503CE58EA2981EF96852E6F252B6F29736,
	Promise_Then_m6693CA7F4467B53237CE92F7F44C36CD44FABE0D,
	Promise_ActionHandlers_m226D1CB18140648C8EC32E3E2FA7DF0730478F26,
	Promise_ProgressHandlers_m21C8B94D38E0FBF1F14B24FCFFFEC214D9D80912,
	Promise_ThenAll_m444DBB4B6489921A44CDCF98AB8D3F2708372D66,
	NULL,
	Promise_All_m4021CBAF737AB75139BE836A21E02B28B238AD21,
	Promise_All_mD80850322247A01D346893F8962617690B9D2606,
	Promise_ThenSequence_mE42D6CD848060EB59C10345B5A14B16724D35F9B,
	Promise_Sequence_m9F9C078AA9DB0404DAB83831A3EF769379E58906,
	Promise_Sequence_m646E63C1583234FE0B9EC2F642D5B1EB7A830E52,
	Promise_ThenRace_mF04FB2EDFB9FF1589EF714F409994945E123EE7D,
	NULL,
	Promise_Race_m7A61FE78FC907244940AE23BE119699E657860BB,
	Promise_Race_m422B8D03C310B70EC818A1A5F7A8E12C836FD877,
	Promise_Resolved_m27D7349EFBB518D89093ADB573BD7D7F836B24DE,
	Promise_Rejected_mF6CA3689BE449406F16A5D145B1DD4FD13C41890,
	Promise_Finally_m762D2CD8F1F48788A1B94FB46CB338D69083230B,
	Promise_ContinueWith_m1A53BD142DC421621617AE8E0BE6DA2053D96D8A,
	NULL,
	Promise_Progress_m53E64BDA8764C5E88057601FAB54BF75FD0B368A,
	Promise_PropagateUnhandledException_m30B4923F2598FA6B4F7CD5964E00760B5D9F10B7,
	Promise__cctor_mB03C9B64D5DF3D552E96C4FCFA942B128173413D,
	Promise_U3CInvokeResolveHandlersU3Eb__35_0_mE4D9BB0CB11B33C2E7F12BFE44103CC546FD2E2A,
	Promise_U3CDoneU3Eb__40_0_m03D038B6097EEEF2B736CCE4A1D9154518BE3E01,
	Promise_U3CDoneU3Eb__41_0_mDE5A60CCBCC7F17ADD74A395C92F553EAAA52817,
	Promise_U3CDoneU3Eb__42_0_m10D5F4FC16A8A05494ED431EA28AA288DB13AE3B,
	U3CU3Ec__DisplayClass34_0__ctor_mF81EF54B39A864EB5E463DA04D72B5F8A0CF4DFC,
	U3CU3Ec__DisplayClass34_0_U3CInvokeRejectHandlersU3Eb__0_mC2CB7940F03A8D527C6C9BA7793BA6C6A07E13FC,
	U3CU3Ec__DisplayClass36_0__ctor_m18834E008AB1CCA3258C2388F1158B37AB6AFAC1,
	U3CU3Ec__DisplayClass36_0_U3CInvokeProgressHandlersU3Eb__0_mB466D8A8500579A62E0EF36D3766DB1A59E89F41,
	U3CU3Ec__DisplayClass44_0__ctor_mCB6B49A87238E9E939A3FB884973D5B8085A1805,
	U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__0_m59D9BB020E9BAA19BF3A55E1A66B4BBF0115A4F3,
	U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__1_mC2F86C6B2CC1463D07838F8B6E71CF95A00D6B1C,
	U3CU3Ec__DisplayClass44_0_U3CCatchU3Eb__2_m1C82A7D09BAFC515977C21BBAD3E8EE6E6C8B665,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass52_0__ctor_mB894C314363743106885723C9A014E58D79394DE,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__0_mFAB16B8D05F5B6D1B0820FEAECBB318B28D6F95F,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__2_m68317E06CF2A6CB4E45914984E5E7B1C8E250B3A,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__3_m1DD830DC1CA6AEAB53B32028949E3349E37146EE,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__4_m6F52D5A374717AC944B68D544EF57498A6CF0C12,
	U3CU3Ec__DisplayClass52_0_U3CThenU3Eb__1_mE79481476F323A2E8C64CF6853F48919B454B238,
	U3CU3Ec__DisplayClass53_0__ctor_m8AD8D6610D93664501D710EB55F2D75DE59813DC,
	U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__0_m089AF19F35A75DF89A101BC920FA8F3CBF856F03,
	U3CU3Ec__DisplayClass53_0_U3CThenU3Eb__1_m6BF2C7A1D0A0D3699E5339E92DFF00CA7B60BBE2,
	U3CU3Ec__DisplayClass56_0__ctor_mA16A09395360FDB88DAEB794DAF2A6BAF4CAC8AE,
	U3CU3Ec__DisplayClass56_0_U3CThenAllU3Eb__0_m76809680DE80AD5F5596FA06F41BA163AC6D1C30,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass59_0__ctor_mBA66AC86289688F1C468B96C50BE5358F31A082F,
	U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__0_mA4CF65F285BDA1A6D803D917E9FD265051D6BEB9,
	U3CU3Ec__DisplayClass59_0_U3CAllU3Eb__3_m8253EC136EA78D646B69616B15FA235271A992AA,
	U3CU3Ec__DisplayClass59_1__ctor_m526DF9DF8BBEBFA28914FB4F54E3A1AF03B414BB,
	U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__1_m593233FA219F817C7E2E48E4B57F1967B608EDAE,
	U3CU3Ec__DisplayClass59_1_U3CAllU3Eb__2_m5AC61534D5AE0A52E1F555A299AD9A673E2DFD45,
	U3CU3Ec__DisplayClass60_0__ctor_m99C72D8D0D1E5F3744F901AF2CE1D01B10074CD3,
	U3CU3Ec__DisplayClass60_0_U3CThenSequenceU3Eb__0_m6FC48A06C04839828FBCC417B074F35B1849CBF1,
	U3CU3Ec__DisplayClass62_0__ctor_m4E600EF746E7384F9FF81A106144CCC5CA295EB6,
	U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__0_m3BD1BDD0E7EEF8F00E0C7DE8CD8AA502555164A8,
	U3CU3Ec__DisplayClass62_0_U3CSequenceU3Eb__1_m030831A862679BA67E0AFFF2A793684303FDC197,
	U3CU3Ec__DisplayClass62_1__ctor_m4E3A09413376A37B8429009826FB5E58FB5918D7,
	U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__2_m38F09C7AB72B5AAAEE2B1F47D8B8E46A8B244D76,
	U3CU3Ec__DisplayClass62_1_U3CSequenceU3Eb__3_m45E00529CA4F236640E37AD28227E06C03260F75,
	U3CU3Ec__DisplayClass63_0__ctor_mA3AE9325599E7C269CF28464DFD338E2FB36D87B,
	U3CU3Ec__DisplayClass63_0_U3CThenRaceU3Eb__0_m5B8C751B1FAD053AE58DE02DF3FEEA12AE43EC44,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass66_0__ctor_m20042739F9915BDA4DCECD38A6FAEC9A32FC9FD4,
	U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__0_m3C1812F10C134DFFDE20266483FB8CEE43E00D28,
	U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__2_mD8A7C646663CD58D24E0B9E5AD99A223042BCC7B,
	U3CU3Ec__DisplayClass66_0_U3CRaceU3Eb__3_m5925431199B068785646FFD433C27ACF0DA6C7D2,
	U3CU3Ec__DisplayClass66_1__ctor_m864691E480DAEE7AEEAD46F8FB882828DB249E77,
	U3CU3Ec__DisplayClass66_1_U3CRaceU3Eb__1_mAA09AC32AAC0FB42ED3E102028E9F62480131D48,
	U3CU3Ec__DisplayClass69_0__ctor_m1076FE9DB847E9C246A4B77B4A9AD68DA316DAFC,
	U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__0_m05B68401734C79A8332FD12FBF32C5C1DB2FCF61,
	U3CU3Ec__DisplayClass69_0_U3CFinallyU3Eb__1_m805874267E687B76A8017DEFD8A11A96DA9FA8AD,
	U3CU3Ec__DisplayClass70_0__ctor_mD191E17C3CB17FB74E6636B739DFFE3332CF62FD,
	U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__0_m251D75318366BEDFA9CC05A71A3B108CBEBDE4D6,
	U3CU3Ec__DisplayClass70_0_U3CContinueWithU3Eb__1_m8EF480C7A95C97BAE71BB581C32C4B1A14B98178,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Tuple__ctor_mB90BC7455CD38661E7B174497948846C3717B84A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PromiseException__ctor_mC1C353F7C09B485B8EFCF3B176412385745A37B2,
	PromiseException__ctor_m25FEF871F4EADD0813603091A36151F62E072F34,
	PromiseException__ctor_m378D7284D56C1662B14AFD057C60ACBBABCF8895,
	PromiseStateException__ctor_m0841D9ED95CB98A821651EB934050C7F99325539,
	PromiseStateException__ctor_mB10D86B4B0D696B724F6A344C9DA8BA582785E8A,
	PromiseStateException__ctor_m09D7E625AC5B60FBAA33F53C52F2C5B9784C3AA9,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	HttpBase_CreateRequestAndRetry_m6DC921EB7E83F0FB058D9C9889B15D489092D059,
	HttpBase_CreateRequest_m395D82BBC11CA3C9D8FA5DCA55BC4E5AFE6AC451,
	HttpBase_CreateException_m4F00611C1B083E175A53008F96FCDAEED39E094E,
	HttpBase_DebugLog_mB403310A43B2C0F73923BE8135DBA43E5FECF8C3,
	HttpBase_DefaultUnityWebRequest_mA2CA028D057F027E7D8283E63643F0AF2974B069,
	NULL,
	NULL,
	U3CCreateRequestAndRetryU3Ed__0__ctor_mA038A5FC9BE11F7D821C2EB89C903BD81DD6673F,
	U3CCreateRequestAndRetryU3Ed__0_System_IDisposable_Dispose_m0F0654EB7C2275B8DDFC1B070E8B145B2C5FBCAC,
	U3CCreateRequestAndRetryU3Ed__0_MoveNext_mC05783571C265F2A6348BF44253BDF379AD516DD,
	U3CCreateRequestAndRetryU3Ed__0_U3CU3Em__Finally1_m65EAC36497FC4D9964288E99CD0E428ABC806C46,
	U3CCreateRequestAndRetryU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m585D7434321501EABFADD21A314DCBB231EC3252,
	U3CCreateRequestAndRetryU3Ed__0_System_Collections_IEnumerator_Reset_m09C357E4FCCD4430591B46B1960B5DC7D4BFE376,
	U3CCreateRequestAndRetryU3Ed__0_System_Collections_IEnumerator_get_Current_m4D5CD62D3F82FDDE9C67A9FD2E2894857D7746A6,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	RequestException_get_IsHttpError_mF1D87818843F7F4BFA1BB1B1F1EAD6925497CF30,
	RequestException_set_IsHttpError_mF311DBD704CCC61C23A270F8BCD1E7E8E5F17469,
	RequestException_get_IsNetworkError_mF6C31F24DCF7B5F649A4CABF455EBB03CB53434C,
	RequestException_set_IsNetworkError_mB4D9148437DD8A59B2969764E067E35E085ABB15,
	RequestException_get_StatusCode_m786C170A63F36EF40CF6F4347F5EACC85925E1F1,
	RequestException_set_StatusCode_mFD7034224DB7D4C27984CE0DA365D02173432309,
	RequestException_get_ServerMessage_m14FEBDED7437643B057376759428572897C03597,
	RequestException_set_ServerMessage_m3DE596FD331A4F694B415FA3C8886FDC5F861DE9,
	RequestException_get_Response_m4FE8480DACD2F2347FDE3BC8F0843885E10912E5,
	RequestException_set_Response_mEF4EB83001EB93F345CD9FB6214237F3888EDA25,
	RequestException__ctor_m4255A573E554424A1B5C9F6E2D58590C50877C85,
	RequestException__ctor_m2A12EC5D24756FA04C4D080FFD4DB9264B01753B,
	RequestException__ctor_m230AC0532C60E770A87DB74A57630F5464269D23,
	RequestException__ctor_mD2293F4AA83F9A3A3EA0E48219894E91DF485FCC,
	RequestHelper_get_Uri_m579870F497E54040498F01E0F03D8E385B75682B,
	RequestHelper_set_Uri_mED832E7E6B02422B23573A3BE154D41312A32BE3,
	RequestHelper_get_Method_m3CAF08A8E99D378C7693940ED9BBC576130C5558,
	RequestHelper_set_Method_m93E8A5F66E37E3459E8F1689E272C9EA2303EEA2,
	RequestHelper_get_Body_mE96291B55EA0C31805FDF261E1A6E0E0A04B9B4D,
	RequestHelper_set_Body_m6C34BB52EE69E8A7D5B2FC14CE8478A4CCCB223A,
	RequestHelper_get_BodyString_m583C6B16055049B23992E90B9BE9240D2494C872,
	RequestHelper_set_BodyString_m264B491596B1E5D59DEBEDFA6D2C07EC37F6AF25,
	RequestHelper_get_BodyRaw_m0567B940B2FD769D548A63A3937EDAB8AF8BEB07,
	RequestHelper_set_BodyRaw_m4BB1B19C3109ED8297827657F5855DCAF7EF315E,
	RequestHelper_get_Timeout_m5EB987EDD3D72C0AFC56600A506DED4393B31D03,
	RequestHelper_set_Timeout_m8769C12B49481D0B8BC11B3227F802FDF049707F,
	RequestHelper_get_ContentType_mF1D15920E2C975B8788B61B57B6A0EC713C5EB97,
	RequestHelper_set_ContentType_m53EBFC2111ABCD757D3B613E943FED0F2A916B1F,
	RequestHelper_get_Retries_mFE6F51F6EE991FEE0982128A8305F013668B7D5B,
	RequestHelper_set_Retries_m8E91AB08F2D651BE88B57973D472A87CFE403AE1,
	RequestHelper_get_RetrySecondsDelay_mF461806AF11B3F89D745861D3EE818A9548705F5,
	RequestHelper_set_RetrySecondsDelay_mF554D9A11BAD76DF31F07A5FA92B67E34FF7D6F8,
	RequestHelper_get_RetryCallback_m8487D96D1889FAC85CC99D59CE9AAD81AF975858,
	RequestHelper_set_RetryCallback_m8B02AFF7AB55609093C48182D79501B34C2F2646,
	RequestHelper_get_EnableDebug_m031DA45E2494D29141E793FBFE2BEF047DCF7472,
	RequestHelper_set_EnableDebug_m7DEAE27887D92A686E341EB14010DE5A4B3E4DF9,
	RequestHelper_get_UseHttpContinue_m64713A0BA2260B82D1587517ADF0671DB60B8D66,
	RequestHelper_set_UseHttpContinue_m454427FB941A608ABD370EC1E64F0C5166183E48,
	RequestHelper_get_RedirectLimit_m7628CFD271A1A06DBF264C289268CE52DA66DB82,
	RequestHelper_set_RedirectLimit_m32E15A1AE611D405855B008D58576C8839929DC3,
	RequestHelper_get_IgnoreHttpException_m0BA6B17AACCF9C74FEFD730806A64B67E4505FEA,
	RequestHelper_set_IgnoreHttpException_m6653B079C6C70E478895D1EF4BFAC9FE5D5D2209,
	RequestHelper_get_FormData_m1CE699C5BC879C7AEFBB1084CF0A1928CD02B931,
	RequestHelper_set_FormData_mD33B2EEF5749F7898DB81BD2ACEB0F7EA9E97EC4,
	RequestHelper_get_SimpleForm_mE158581E8CAC46C434A072324A6290CBE4878C78,
	RequestHelper_set_SimpleForm_m885368EFE7D8FF37C0E9CBC36A8B551562042165,
	RequestHelper_get_FormSections_m4157D4255B827B864F882669136AB30BA93316F0,
	RequestHelper_set_FormSections_m955F901BFE0A3D2FFE42F223BB2005BA2028337E,
	RequestHelper_get_CertificateHandler_mBF95069255782425F21F491C5A51F3BBCEE956B1,
	RequestHelper_set_CertificateHandler_m1B5296D19D58F16EF358706D8AC3817F0ED0B1C5,
	RequestHelper_get_UploadHandler_m5C72A7EF49F678C8CB02FA0620C0B41D1CF8AE78,
	RequestHelper_set_UploadHandler_mDA3C9BCAE240C2A5185B88FE2FE7BF198B6467ED,
	RequestHelper_get_DownloadHandler_m3CC95DFC6C81ABD8508947C1808BC8FB7095BDA3,
	RequestHelper_set_DownloadHandler_mE7FC2C6C38C5418A915D4007DCFF2A9CF5C6DC74,
	RequestHelper_get_Headers_m09842B9228E6B35E7254DFAC41A5D2C98DBF0BE9,
	RequestHelper_set_Headers_mE4B41FA1E24D02AD057068E2D0A697C129921A29,
	RequestHelper_get_Params_mDB495A9CB92B43BB1BD3526C99880EFBDC40E033,
	RequestHelper_set_Params_m887AAEE89701BE57B2875DD482A584C58EF7C400,
	RequestHelper_get_ParseResponseBody_m8352D39E0EBACC6FB532FBE05B5D3EC709EA43BD,
	RequestHelper_set_ParseResponseBody_m1149B5FC0628F6BCDAFA95D21516A9CAE3F4031D,
	RequestHelper_get_Request_mB3DD0BDA5D264948DE92BA5E060545D8D3CCBD63,
	RequestHelper_set_Request_m4C49567052095FC092A24AD821EF7957CCB6F4BF,
	RequestHelper_get_UploadProgress_m4F7D8BCA66AE1D13F1C59D91EE10E656D48DE91B,
	RequestHelper_get_UploadedBytes_m7D3445EE67E794E91D82A28171224EED03460533,
	RequestHelper_get_DownloadProgress_m0ADF466E7C45A9D043506B099C6B3E7A08C5BE33,
	RequestHelper_get_DownloadedBytes_m5201AEFCAF699F4C054CBA3C63004950C1E32ED7,
	RequestHelper_GetHeader_m0EAA6AEED4B885553B4CD97B2C4CBF81A32EF468,
	RequestHelper_get_IsAborted_mED2B0C2E2FA7DA65574ED4141818116D5407825B,
	RequestHelper_set_IsAborted_mF37D6A6B93B70D7B4AC2B579FDA59345D0D51DDF,
	RequestHelper_get_DefaultContentType_mF52E8AD322DC2F3A0A0444FA1A60D2C62B42113F,
	RequestHelper_set_DefaultContentType_m0098EE480F8E776673979AFB751C76CD61AB5365,
	RequestHelper_Abort_m7CE069D799170B7D0336E7DB358C78B1B00DA8F3,
	RequestHelper__ctor_m76D5CE4B07BAD0EFE344E9A8C4D1BFE895922C13,
	ResponseHelper_get_Request_mE2DF5B066E83E4216167EEE1A28A79A09A372843,
	ResponseHelper_set_Request_m9743967B9D789F748E8205E910F339114CB7D522,
	ResponseHelper__ctor_m0F45F428ADF231F62EC7C6E5621058DA806A1850,
	ResponseHelper_get_StatusCode_mAC67167172472DC6804430CA8FDEDDBF1FED2D4F,
	ResponseHelper_get_Data_mDFB63D2691666575C126F59C928580F359646E44,
	ResponseHelper_get_Text_m4C6CE813DC4FA5B0659BF20EC4B5EE2504C618AF,
	ResponseHelper_get_Error_mAF99FD6F00AF225E4D2EF620266C6A428199F6E7,
	ResponseHelper_get_Headers_m2990A8F894F71B2503103388796B0717EEF9D9CB,
	ResponseHelper_GetHeader_m9E39EF8000022EA04DF66B43FCCB31AA01AB0D52,
	ResponseHelper_ToString_m3DD8554605B5BDE7A6412218369E317C804B0572,
	StaticCoroutine_get_Runner_mC13BCB3C4C6F548812E35E5555D3455AEB2700DF,
	StaticCoroutine_StartCoroutine_m46187D9942A6387EAD6EEE4771C5C54027547C58,
	CoroutineHolder__ctor_mC1424305AB5B1F2E631B7121BBB184B1B8BBBCF6,
	RestClient_get_Version_m5701043CF8E7F883ACF65A25CADCE6AB4AFC8EB0,
	RestClient_get_DefaultRequestParams_mE23F55E6B8003CFEE7FF60C7483232FE7E247A3D,
	RestClient_set_DefaultRequestParams_mA826F5ADE53A384196CC73D37D26E72E5B7CAA9B,
	RestClient_ClearDefaultParams_mCBFF0C9C7DA720E8F0F9D54D0BDDE3D1C421CA71,
	RestClient_get_DefaultRequestHeaders_m7CFC695866D2FFFAE6C2C79B38564917EFFE37AA,
	RestClient_set_DefaultRequestHeaders_m8ED47C4D90B6F05DB8ED523A5A49C2B0B1B7C2B5,
	RestClient_ClearDefaultHeaders_mFADDD96FA3D43A3CFC44B9A6A641B666F000C09F,
	RestClient_Request_m6D0EEC74B2073F8006977A737FEB5A26FF8B2BE4,
	NULL,
	RestClient_Get_mD3CD57C7B7809173BB0841F05883D4EAB667C4E4,
	RestClient_Get_m62DDD6AA1F3E2D93DC5AF9378090C97D67EE7D54,
	NULL,
	NULL,
	NULL,
	NULL,
	RestClient_Post_m4B07264A4B85D23B2F3013C227AB599B04F18E7A,
	RestClient_Post_mAD4114F33CCE7E913400D3AA42D2494C1C71D27A,
	RestClient_Post_mB43880A95D1AD7E76C1878CB288DC4E6B3453651,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	RestClient_Put_m955E298F1437E5DDB4391BE3BF2A58B02232AA4E,
	RestClient_Put_m8AF7486C0FD795F8D4CCEBE663A56980E9397A02,
	RestClient_Put_m08D2C2C9C8B6F73A7531BF8BD2AC15C9C5E13680,
	NULL,
	NULL,
	NULL,
	RestClient_Delete_m7701A6040B30FF21656087D1FFBE7BCAFA782E16,
	RestClient_Delete_m1C6AA27C4494A101CED7B1B6BFE9A3B38247FB60,
	RestClient_Head_m01F7D5AEF77C54D5AE2948C1F579C1FD856AB0A8,
	RestClient_Head_mFEFB7B87CFC87642E95B09D1A4AB76F63ABA3E14,
	RestClient_Request_mA130CF629F30CB634AA89655F61C5BD37F729409,
	NULL,
	RestClient_Get_m7E5DEB235C456E5FAFCA828BF688ECFD630E86F7,
	RestClient_Get_mDEA2483857BD9E64941E12CB42063D74EEF50FE6,
	NULL,
	NULL,
	NULL,
	NULL,
	RestClient_Post_m4763A3817990302191F3A5B069A64B02419523BC,
	RestClient_Post_mD59FE057DB71BF372315AB100BD5E4339FFFFE07,
	RestClient_Post_m27A12B1AAD73F990A53E62B578D01BF4EA933509,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	RestClient_Put_mB84F593B9E21B27814D3EBAA2920B67D1699FD26,
	RestClient_Put_m3E06B35E9CFAE4AF81DCF12A8F30A178E1CC5ABA,
	RestClient_Put_m5E7F007136807CD127CA4FD4D6A986B3979391F4,
	NULL,
	NULL,
	NULL,
	RestClient_Delete_m7CACD86724C07540430FDA0E5D759F6E57BBF75C,
	RestClient_Delete_m33B84121232197C62DB7D3BAFB4E5F39CEF4C474,
	RestClient_Head_m6CA11D53CD3CD3724C345EA2DCEBCF665E31D101,
	RestClient_Head_mFE1399A7241D6D868C0AB0B65C774812167D9D43,
	NULL,
	NULL,
	Common_GetFormSectionsContentType_m19E34A4A4613B617EB59187AE573639031CD6B93,
	Common_ConfigureWebRequestWithOptions_mF5668E71E221566BFD09C871A2B0667206901F78,
	Common_SendWebRequestWithOptions_mA6DA8F8469D1F337E3CB043E57F2191D8D95EE1B,
	U3CSendWebRequestWithOptionsU3Ed__4__ctor_m47FB1CC913CF34CE244286B13D6600F97F2C7006,
	U3CSendWebRequestWithOptionsU3Ed__4_System_IDisposable_Dispose_m5F26553216BF51BE2953AFD9118938F28D511A34,
	U3CSendWebRequestWithOptionsU3Ed__4_MoveNext_mB5E4CFE2AAE6A9BCE0D76C72C378282AFCB56C68,
	U3CSendWebRequestWithOptionsU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF49002192678DCE4CE3D8D78E1E922C730FED67C,
	U3CSendWebRequestWithOptionsU3Ed__4_System_Collections_IEnumerator_Reset_m82497AA2724FEA6918E2C0900364DA889677B45E,
	U3CSendWebRequestWithOptionsU3Ed__4_System_Collections_IEnumerator_get_Current_mD15322133CA601A9884E3FCBDE8BD76FFFB1558D,
	Extensions_CreateWebResponse_m85A8BFBB1916DE1247943658A0758B372807A7F1,
	Extensions_IsValidRequest_m95295E8977C94FB722EE97C5047C3975DE5EA65A,
	Extensions_EscapeURL_mAAD51E7CBC83CB8CE79715D8A6086CB153919CF7,
	Extensions_BuildUrl_mE2D5E2B005BCD677EED820AE36D5F28F01E57CA0,
	U3CU3Ec__DisplayClass3_0__ctor_mDEE443CFFB4553720586AE36F8109D07F7E08B87,
	U3CU3Ec__DisplayClass3_0_U3CBuildUrlU3Eb__0_mDFB7EBC5B80A07C2899A8B10EE18130CC2AE4157,
	U3CU3Ec__cctor_m51C966458A7B400E929B365351E9FA572508174F,
	U3CU3Ec__ctor_m3AF7EC6F7B8017905D17E0078A8E8219711B52B7,
	U3CU3Ec_U3CBuildUrlU3Eb__3_1_m3F16B42B55E86F33694E97ABD64E3ADB7B6EDD03,
	Photo_ToString_mC7549402494D8D3A4192988B7A75C3F43BB81C7D,
	Photo__ctor_mFB7B355DA04B8883B308CC03347EF777898F6846,
	Post_ToString_m4AD8D8586CF507C7C435254089148F3AD32A855A,
	Post__ctor_mAD9D421E1716FEB8D8B297565CE8F10FFB42CC79,
	Todo_ToString_mAD1AD0938A238807A2D0EA2888893C11CBE105CF,
	Todo__ctor_m3DBCDF59C636313C811C93C83ED5ABC292A19AA4,
	User_ToString_mD57533E879F6F6B77BD1A38883A0DAED53D2FAF2,
	User__ctor_m179E4458F95BA48DD9D0F7E1E4DF8AC26DB05C0F,
	NULL,
	JSONNode_get_Item_m9B4DBE85D7742250EF918F4450E3CE020AF7C807,
	JSONNode_set_Item_m62DFE58028E55800BF80EFE6E0CE4CDE6DF00CA7,
	JSONNode_get_Item_mB39E17FF10DD6C0D91FCF1EF485A73F434B34F1D,
	JSONNode_set_Item_m9F8D28C00CA46F8C0BD5E43500AEAFC3A1999E07,
	JSONNode_get_Value_m8F31142225AAE4DDEC7ACB74B7FFB30A6DD672F1,
	JSONNode_set_Value_mA046548FC6B7DCBDA4964DB05303EBA0CF2A29CA,
	JSONNode_get_Count_m6FD5676FECC6B26A6384D8294C213C4B032EA269,
	JSONNode_get_IsNumber_m682272D381670DCFA66BD9EE3C0CE3CB161E3AAD,
	JSONNode_get_IsString_mE0D6737C47977364E9F1620CEA98454EAFAEB196,
	JSONNode_get_IsBoolean_mA575C54AD05577A9F45E0F55355C0B40F60BFA24,
	JSONNode_get_IsNull_m85D1B73ABCCE95314768A4E690D941B82D08CD58,
	JSONNode_get_IsArray_m54719E46991451D5C8750D8265D0C6F91EEE97D7,
	JSONNode_get_IsObject_mC9E7B94CB90443629157D7C2AB5ED83510DD0BD1,
	JSONNode_get_Inline_m6791C62A74ACCEA90470A7EC177FA3F12F744F09,
	JSONNode_set_Inline_m5C5C9E182CE521A831EBD2D708568297601AD835,
	JSONNode_Add_m10F03DC3FD017B22B831388D39FF735D5AFE96A4,
	JSONNode_Add_m7DCFC7266181DE46B90ECC7C5DD7D5B7618F9DE2,
	JSONNode_Remove_m49D627BAE273E902C8EB8345BDD7B33E945C2E47,
	JSONNode_Remove_m3B3259E69D33121E4974FE1ACFAEBED4B034C053,
	JSONNode_Remove_mD1FE270ABF87BCD7D09AAA22286AB3C315BAA0EA,
	JSONNode_get_Children_mB9A68101166B1C5176311D2EC1ABCC832248FDFF,
	JSONNode_get_DeepChildren_m3E3CE542A4856D0BD557B7E121B4CAA999C8110C,
	JSONNode_ToString_m9E23A359C8B7209DEED6B81DA27C8D0619F9CE98,
	JSONNode_ToString_m9B8D0351B9B14D65ABF1090E6D07E0C69DFB84C3,
	NULL,
	NULL,
	JSONNode_get_Linq_m568F8D242CB58C62C9D8230A0C1F684D70573268,
	JSONNode_get_Keys_m6C1FC7F6C35656362327B486F0426273234584E2,
	JSONNode_get_Values_m1097622D87EFAC54681A9A9796E563DA142D7677,
	JSONNode_get_AsDouble_m0DE7D5E7712FB59B32C1C570EBBF85D597B98656,
	JSONNode_set_AsDouble_mBA8F81DF080D29E60DDE8235663BAFFA688736D5,
	JSONNode_get_AsInt_m743FC99FEF2DA80A8C6CB71137A5FFD28084E0FA,
	JSONNode_set_AsInt_m7CA6313AD31E9E08FEB0B6F8B92AD9A3882D9B75,
	JSONNode_get_AsFloat_m4B2A24C67F4FBF872DEA6360719D854AE1AD1FBB,
	JSONNode_set_AsFloat_m624BDD6CAF17D1709BACFDF3554F2AE11FDD22D1,
	JSONNode_get_AsBool_m15160B79EBEA51E7A2C7C7A23B3540A60434B36D,
	JSONNode_set_AsBool_m0878AF783E25077E17850DD1B4522B17FF08770F,
	JSONNode_get_AsArray_m10801C5609C0C024480B49DFA03A4FB16A4E6829,
	JSONNode_get_AsObject_mDE74F42234B130BA44AD17DF9FFC64A2D8DFCD46,
	JSONNode_op_Implicit_m3938223B519495895A5B4E53D60789BB2D4620F2,
	JSONNode_op_Implicit_m110AF33FB2CEF68FF905E93F656AF02222554668,
	JSONNode_op_Implicit_m66EA9B62BBFD5A306C2B95703443F1960338BA01,
	JSONNode_op_Implicit_m98778303F0AD762A7C4FE928D82352A1F8276EBE,
	JSONNode_op_Implicit_m3780006769998A253BE0F8068E68BD88AC692112,
	JSONNode_op_Implicit_mA90C42C0D957F5F284857AE91907D69477331166,
	JSONNode_op_Implicit_mDAC018F58F31AB6333A9852BD0B038152063B3F2,
	JSONNode_op_Implicit_m10A6B0627DE9BC247B0B39554FEA1CD37CA7988C,
	JSONNode_op_Implicit_mAF077674B97BAF8FD4EB941A63B84D26F6E08EDC,
	JSONNode_op_Implicit_mE4078D38F919268986EF017D4A9EA55FD8CBA826,
	JSONNode_op_Implicit_m92A46C2D66615E97C21041238C09386EDCF0A266,
	JSONNode_op_Equality_mA320F56360F44724C465167318C8FC38F0DCA38F,
	JSONNode_op_Inequality_m00AE17CECA317ECC98958F027D0545C831DA24FE,
	JSONNode_Equals_mC1598F5265D88ECAA3040C537A28FC2DC878BBFF,
	JSONNode_GetHashCode_mF900BB7E50D965906CDBC7210B9097CDE6B89834,
	JSONNode_get_EscapeBuilder_m87ACD2EE75FD74E9E7B67BBB66FCDB0643749628,
	JSONNode_Escape_m5C3A578A1281224A7BDF595470FA706CBC597A68,
	JSONNode_ParseElement_mC27D1FD482D97FE1550AAFC625379A7BD8DB1895,
	JSONNode_Parse_m747C7ACB3E8F42CB32651EAB1506047F02BF4076,
	NULL,
	JSONNode_SaveToBinaryStream_m5CD58A850F95A069843BC02BED9B7190D4CE6F73,
	JSONNode_SaveToCompressedStream_m939EB2E9267698F54AD64849D96E4ADFCCF5A00A,
	JSONNode_SaveToCompressedFile_m66A1234959DDCEE44D9000255F86A8FD32EC87BC,
	JSONNode_SaveToCompressedBase64_mF1BB008CE575D288A986676FD23806E62C98D539,
	JSONNode_SaveToBinaryFile_m4C3B67B465C870818ED92E4FC14F9B9527734EC1,
	JSONNode_SaveToBinaryBase64_m31110029F0029D7416604433C849EBF33A7F12B5,
	JSONNode_DeserializeBinary_mE398A9C5CB421A0CCC20A18486D83964E36DAE45,
	JSONNode_LoadFromCompressedFile_m50BB9E1D951C7444EFE1FE570894E5A92BA35275,
	JSONNode_LoadFromCompressedStream_m6D59AB50712DF46BE7D37DB5EDB8EDD7C0B6D04D,
	JSONNode_LoadFromCompressedBase64_mBC74C1DE9C65EF47BA738413E2140F1370A9F433,
	JSONNode_LoadFromBinaryStream_m540612C537938B1403281538E5B33BCFAFF29B46,
	JSONNode_LoadFromBinaryFile_m47D1B956E15231FD6537A96C09B317F821374B1E,
	JSONNode_LoadFromBinaryBase64_m91E331970C76EE0AE16B3FFFE8535321EADE65E0,
	JSONNode_GetContainer_m8A2362DAE7D92FC4A7977415F5618EBE892DE819,
	JSONNode_op_Implicit_m7F01CA65EDCA916BD0BFB9BFEC73444FB26D59FD,
	JSONNode_op_Implicit_mDD9DF895CC3317F2C6D8E325E6133C1FE0954864,
	JSONNode_op_Implicit_mDF1EE24EF1759B2C134B0804A3497B2BFAF0A348,
	JSONNode_op_Implicit_m3B87FAAC3847B6EDAF525A9240A6AAD7489219B3,
	JSONNode_op_Implicit_m4B12356D2C135933B13ACB20AAA0BCA93221A308,
	JSONNode_op_Implicit_m880C05AE8E8F125C0FEC37BE1C32F5D2A8D32B70,
	JSONNode_op_Implicit_m50E48009BD81DB50555DADF99F5C424EAC0007FB,
	JSONNode_op_Implicit_m94957CEB32B45139355EE8C3D85B4C9DDA107484,
	JSONNode_op_Implicit_m01E7C3C12602B1EFB9EDA7A4812536D44C1CC27D,
	JSONNode_op_Implicit_m39284C070FA8F77D4A75A67E44ECFDC033719CBA,
	JSONNode_op_Implicit_m1F0EDFBF473C04FAF2A76125C40D27AFBF427E56,
	JSONNode_op_Implicit_m4F12BB544F8D9E6DC387FC276BAE60269715326C,
	JSONNode_ReadVector2_mB5168EB80BA8FE7D38324BB66580CDE962B5BE7B,
	JSONNode_ReadVector2_mE9045E685C7EA3CFE8546153F99152CCBE61A18A,
	JSONNode_ReadVector2_m80B259CF2BDAFA309EAF5B6B2C78AED2EB528A15,
	JSONNode_WriteVector2_m78FD594CD3963CDAC2B1BF2DF26220692FD54DAC,
	JSONNode_ReadVector3_mC7A091BF1F3DE71655260268551A9E449EDAA1B6,
	JSONNode_ReadVector3_m1F04E19E177F89BB76CA022C70A975C08061F725,
	JSONNode_ReadVector3_mF41BE2C41613AFEA70CAB539AFA7D0A6F573AB9C,
	JSONNode_WriteVector3_mAE65146631E220583C90A007F05DE374A7E3D067,
	JSONNode_ReadVector4_m2B894C5C5513E9454511DDAAFE7C3A416470907D,
	JSONNode_ReadVector4_m13FF00E78429AC6F1E1284539C759AB69BDF1CD6,
	JSONNode_WriteVector4_m1210A9104B4EFE99047D7AFE6AB10DD01A73D27B,
	JSONNode_ReadQuaternion_m6EEE1B3ADE85662B1363029EE602CA1EC02F8DCF,
	JSONNode_ReadQuaternion_m6147D59E720DA900144A556FCB4365FCC2A71A32,
	JSONNode_WriteQuaternion_m2A11E75564526030F3EF8D276E578F39AA18CCA1,
	JSONNode_ReadRect_m831CAA59B68FD465C7739CE3DE1C297D8A23B023,
	JSONNode_ReadRect_m5DA61EE22CAA874BD2457B060F6286A7FB1DBFB4,
	JSONNode_WriteRect_m3614416241A91C0EDAB54E5645D7A5D9B7AADB55,
	JSONNode_ReadRectOffset_m830101CD2863A9DAA92A65983D5F7E44439522E0,
	JSONNode_ReadRectOffset_mA8D64472E6F8F695EED27E33E13B204BD87194C8,
	JSONNode_WriteRectOffset_m1146232B54BE25B8E9AC363C776E4B8A40C8A789,
	JSONNode_ReadMatrix_mCE78E3CAC6BE0132EBF82990DB741A12E1B3030C,
	JSONNode_WriteMatrix_m7F72D94C9EDC6B9E590231A27FFE6C52F200EA23,
	JSONNode__ctor_mF0692814A0795056AE052B05EF63D6B216B5619E,
	JSONNode__cctor_m0F95CA9754AE155CEC873DEFB28A83E8AAC25990,
	Enumerator_get_IsValid_m078841FA11B23FAA03C8C0F0F5CFE1C7A33CC267,
	Enumerator__ctor_m0A9EDBF3F33AA5FAE87C204FA885D965D16E8A44,
	Enumerator__ctor_mDE6D3B06DF10240F0C637131119C8B09CE9EC4DC,
	Enumerator_get_Current_m6C700A8C86DAB32F9FE845C3BE1E35D17757C65C,
	Enumerator_MoveNext_m095925BE3D881DF1E015997DBEAFE0851722E53E,
	ValueEnumerator__ctor_m1FF9C4191C706E48C3F9A191D0CE02F9BECDCE45,
	ValueEnumerator__ctor_m8143FFB5AD9AB3936DF1A59E3492C2CF81793AF5,
	ValueEnumerator__ctor_mE92051B3948C6A45D4F9265EA6D8329D70649D06,
	ValueEnumerator_get_Current_mAB38FFFD4C1293C61EE73C825D85B7258B61961C,
	ValueEnumerator_MoveNext_mE68E69AFB9D6FA2B41F55B066F3295490EED5589,
	ValueEnumerator_GetEnumerator_mF841522782073D8D5F9782A37B2487BEA63E5E09,
	KeyEnumerator__ctor_m6E59374D6FF2E994DFB9BFCA3759CC335CA48D23,
	KeyEnumerator__ctor_mB67A74FB33429333A208FA269F011122C1AB6395,
	KeyEnumerator__ctor_m5F83F3F252DE00DB6AC2AE59FD501F921C56F224,
	KeyEnumerator_get_Current_mAC153090E0D57451FE61CA3651CF32A4CE21477C,
	KeyEnumerator_MoveNext_mFF5FB4ECB623B58733E9B85846A296853462F2F4,
	KeyEnumerator_GetEnumerator_m4534071ECBA3E7165BB0DC642EA9E827A29F5A87,
	LinqEnumerator__ctor_m711F6A5F82CC6093E5245A51BFEF95B0355E2FAC,
	LinqEnumerator_get_Current_m7F293F41FED328A040E5BC71C7F4560C3B8C295C,
	LinqEnumerator_System_Collections_IEnumerator_get_Current_m36203F845AA95DEB756D145E03469EF077B79BF1,
	LinqEnumerator_MoveNext_m547BA7FD679ED14C3E698763EBEC58333194B9EE,
	LinqEnumerator_Dispose_mF5270BF1DEAB954E8FC65435DF6D6FD5DBE1C21F,
	LinqEnumerator_GetEnumerator_m356D1E4E1EC2D215962F677F3C6AE538E3CF9A20,
	LinqEnumerator_Reset_mCC3E0E49D5343E81C63A2B4A28A7268F628A2522,
	LinqEnumerator_System_Collections_IEnumerable_GetEnumerator_m713D8655544E9FC2911762CD17D215B5763AE735,
	U3Cget_ChildrenU3Ed__39__ctor_m0BF33D6A9ACF56C849BB64AB175B43D7692499A8,
	U3Cget_ChildrenU3Ed__39_System_IDisposable_Dispose_mFD004DCB3358CE2D7764E776A031A077B21BEB29,
	U3Cget_ChildrenU3Ed__39_MoveNext_m858F052BAF7771FE605FCE68EC05F97FA5143538,
	U3Cget_ChildrenU3Ed__39_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mB88A038112706EAD24B23A14267B794021C761C2,
	U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerator_Reset_m716ACA15194120CF8192937025FD3D4D7F8C710F,
	U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerator_get_Current_m831C58F860DBA03146F00BB7DC77DD1852943881,
	U3Cget_ChildrenU3Ed__39_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mC22210E7538A1A259E64227EEAC5440FE868ED60,
	U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerable_GetEnumerator_mDB57DC3D1B30B06919F6F3985E3B3564974C1CAA,
	U3Cget_DeepChildrenU3Ed__41__ctor_mAF92DE35182876CDDB7F9E2831AA0078B111A5F9,
	U3Cget_DeepChildrenU3Ed__41_System_IDisposable_Dispose_m0C1243483A3118059E0FB871E404B606DD55CBBD,
	U3Cget_DeepChildrenU3Ed__41_MoveNext_mBC10790834A0F6E35D8D274754EF1C77A320E391,
	U3Cget_DeepChildrenU3Ed__41_U3CU3Em__Finally1_m90DA0BA712B5459B1157239B9995E01820163928,
	U3Cget_DeepChildrenU3Ed__41_U3CU3Em__Finally2_mB8A69D17B39DA453A0811F66B86B5426EF3C74DA,
	U3Cget_DeepChildrenU3Ed__41_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mF12A4534E7D4122F67B420801B2522BC9F839868,
	U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerator_Reset_m3F120F44D1BEB26137AF14E11165003EF8E18190,
	U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerator_get_Current_mF43CD05D3BF0CD3E76A265F0A684D43EFA4F22EC,
	U3Cget_DeepChildrenU3Ed__41_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mAD435B3462420F0485567562B0FF3F1DB60B6AF8,
	U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerable_GetEnumerator_m554A68924D4655591E09B01C0EFF3A9204E5E117,
	JSONArray_get_Inline_mCCA0FAE052593C420DF7266D9030F7CBCFF5D43A,
	JSONArray_set_Inline_mCDD35E23A81DC4DF48DB54FA164B2D05CA8CDD5C,
	JSONArray_get_Tag_mA6475F4E3B27AE262DFF53A510E5DD9CA9A82212,
	JSONArray_get_IsArray_m015BCD163D49042D8827D8B5880EAC8720B93DDD,
	JSONArray_GetEnumerator_m107028482740A363BC3AAC4348F729F49094B8D7,
	JSONArray_get_Item_mB56A68792D63083DAE88CF576C4E5C9C6D0E1AEC,
	JSONArray_set_Item_m8D70249BB8D717AD878AFE87F9F86345CD5DE9EF,
	JSONArray_get_Item_m5E864AF954B22AD5841E8CEB58AF68DED45C4C8B,
	JSONArray_set_Item_mD6DB253A66DC2D34179D9773A1ADF25A11FE7DDE,
	JSONArray_get_Count_m8649900F00FBEDB004834FB9D814F848E6C7D72B,
	JSONArray_Add_m14182F062E171457670DB45BE811A46A78B2D685,
	JSONArray_Remove_m65505E642765169F35A9F21760C7EDC39407B9F9,
	JSONArray_Remove_mA79C09C43B22EE2B4965C1D8B3794BAE1957037E,
	JSONArray_get_Children_m56D8A696E09F5EEFF1FF9F8BE06011CE5EAFA197,
	JSONArray_WriteToStringBuilder_m64C54C1D36684A7CE5EAB86E3350AE4B811D40C9,
	JSONArray_SerializeBinary_m8BB39836ADBAC72EB4DD67C37A3CFC44FFB8333C,
	JSONArray__ctor_m2B6B15254CC3578F4B78A9E5CA656C4D25119512,
	U3Cget_ChildrenU3Ed__22__ctor_m95EBCC8BC9794B6D9F4C6B5885704BF181FC9DBF,
	U3Cget_ChildrenU3Ed__22_System_IDisposable_Dispose_mCBFE04EF85659884667F66453D36323A90F4A37E,
	U3Cget_ChildrenU3Ed__22_MoveNext_m465E0B8D3164E27BE47D02A0ACC56B5B795DBB95,
	U3Cget_ChildrenU3Ed__22_U3CU3Em__Finally1_mCA65F3342A396F12C190A1654CC8019248A0BAB4,
	U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mA7F43EFC69B364F48AC29BF1C37A5001990511FE,
	U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_Reset_m531EF5FB0BD16A29769CBDE11FDF26AEC2D55F5E,
	U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_get_Current_m9C20E2FAE0940D92F51C5E1C9D804E406BF2F34F,
	U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m7F40779C5E4EF43C9898EFDECF0FA816A7E5DCFA,
	U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerable_GetEnumerator_mCC77091552DD207B63E4B7C2B33693B4467ED68D,
	JSONObject_get_Inline_mEBB1EA4AB1EE32D80F2DB4C5C4240DD13593750A,
	JSONObject_set_Inline_mDDC84E63241E060414987E3B8301F7F1AB2967B4,
	JSONObject_get_Tag_m0F8454A3B161D9954B2C1B49945DE0E9041907B6,
	JSONObject_get_IsObject_m6E6B9273D50B23643A6DFBD17FBEA6BD6143E12C,
	JSONObject_GetEnumerator_mC0AAAB9C27211FED4A7DA97BB1D2DEA1777CABD6,
	JSONObject_get_Item_mF7EF352F2BA035D7EFC5902F2DEB12168E586CBD,
	JSONObject_set_Item_m84B17410261AD3D4C06D551BB169126C42331DD8,
	JSONObject_get_Item_m28A01147850D0D538204316C35A6E1A823602C0E,
	JSONObject_set_Item_m17A462CA68BCC6AB9A077BD2D5354C2CD9BAA448,
	JSONObject_get_Count_m5271E2DB59F0A774A71E1313A43FC47D209DE4BD,
	JSONObject_Add_m89A24748B1E3A3BC627C355AD3F946A5541D0D47,
	JSONObject_Remove_m20C910052474109A5D0B8C63DA98648296C1E846,
	JSONObject_Remove_m6A66604EDD3CBED40F44D85BAD15342B133A2B21,
	JSONObject_Remove_m2000AEFD68838D8F04D250DDFF4E5B2B52D2661A,
	JSONObject_get_Children_mDC37B11CC04E072B9E1DFE56A3B176A1E22F8E6D,
	JSONObject_WriteToStringBuilder_m8B9F8C4C58F878A4992C39D21E75CF735DB65F61,
	JSONObject_SerializeBinary_mD11FD4D432B7BA08AC16D504396247E871FBCDEA,
	JSONObject__ctor_m159A8A4A6AD38E7E4786F8EA8FFB9948DF520F92,
	U3CU3Ec__DisplayClass21_0__ctor_mC0EC999DE1ACB8335F31D6D652C0379FF2D065A6,
	U3CU3Ec__DisplayClass21_0_U3CRemoveU3Eb__0_m4DAF41E69ADC8210F10A0E9353204C4C9BB593F0,
	U3Cget_ChildrenU3Ed__23__ctor_m24E00E4270F8E59C9ECF5B85CDB1A34683FC1141,
	U3Cget_ChildrenU3Ed__23_System_IDisposable_Dispose_m413B24FB7D7B0156DFC7E240DA8391C859440847,
	U3Cget_ChildrenU3Ed__23_MoveNext_m3AD86370936CBDF4689019E0369B72B54B4B4BE0,
	U3Cget_ChildrenU3Ed__23_U3CU3Em__Finally1_m86405C8AACDCB84C101AB4D4812392161C83D82C,
	U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m226D4CC6A995605411240005C904F701A7E7D224,
	U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_Reset_m60B145CE869947C90DB4665A23EDDF48AE63AC25,
	U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_get_Current_m51D1A00926BFE5BDA287FE9A878C6E949F9D0DBB,
	U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mBE8B0928518FD2517C5F344791F32EA64F13C45C,
	U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerable_GetEnumerator_m19CBB01715DEE09E679303048771E8ABC2A2D4C0,
	JSONString_get_Tag_m8CD40E0F8FEA6E68B02D661527AAB26E581AD925,
	JSONString_get_IsString_mE62F3299F2BDCFA5704FEFBB6D6174C7D18EFEA0,
	JSONString_GetEnumerator_m2DB5AD3DA3FA00B24522A9083E0EC92952D28C14,
	JSONString_get_Value_m0A72F580BA1EBD98A7592810069BDF215BC737F0,
	JSONString_set_Value_m9B7FCFDF82ED801A434AC208F52582A2B0DF2164,
	JSONString__ctor_mE09DCD65C60DE37D1F0F79BD6902E464EE1960A3,
	JSONString_WriteToStringBuilder_mB338AAA2644A34B03F6DBE6620F8370C64D15627,
	JSONString_Equals_m9709B63863E6B7E1000EF9E0318B2C499D325B92,
	JSONString_GetHashCode_m573FB034DD33C5A300F358A318A0BCA761E35AB1,
	JSONString_SerializeBinary_m155A6E6D4045E9859823E7811A1434C0A0006654,
	JSONNumber_get_Tag_mC3886860EBAEBED98DF4B58A2E5D2DA1C86D0DF0,
	JSONNumber_get_IsNumber_m55E3251DC2B157F6A06A78047C8B7F6A8710CFC1,
	JSONNumber_GetEnumerator_m48371CA028B151ED79247AB9B49B7B120C172F78,
	JSONNumber_get_Value_m02C36CBFEEDCE75834055B25A6F0F9845B661B98,
	JSONNumber_set_Value_m437CA8FBE33C4A5C6CA4E2951767974987142A01,
	JSONNumber_get_AsDouble_m181BFF83082F0A4A380023EDE5D1EA55B0EC6427,
	JSONNumber_set_AsDouble_m48E57B49311F9720098069A63A056AF5B44AECBE,
	JSONNumber__ctor_mD0AF1324557B9FC7B1D63842D2CA65EC2D1A643A,
	JSONNumber__ctor_m03A803840EF273185C5C63642FAA70FAF9DFE40E,
	JSONNumber_WriteToStringBuilder_m48A94BA46F195E37F78A7AA167655AE1E870231A,
	JSONNumber_IsNumeric_mEA4ED6C1587A4D94F5D6F30B12FA284AB56F11EB,
	JSONNumber_Equals_m0D80E8BA86AA78FBB0482F9F053A0BACB65D8D97,
	JSONNumber_GetHashCode_mB10776EF12594532AB12FA617D1BF18FC53C790C,
	JSONNumber_SerializeBinary_m0250E8C465049F13DF32A2553DC76191A2012C7A,
	JSONBool_get_Tag_m43F4216FE45F0043979093D7BE5FD16C0F462A62,
	JSONBool_get_IsBoolean_mE70A4F82605CAB1E14A70413FC42EF66ADBAB727,
	JSONBool_GetEnumerator_mA15666FFF18AADF12912689A521087C98B5B2D1C,
	JSONBool_get_Value_m11D2863303B2003F337549581C8A583A6A3B4D74,
	JSONBool_set_Value_mA7FB197B21177B947BC3C2940A63724FE532E373,
	JSONBool_get_AsBool_mA5617A7BC781E111FBD24A7E53FB34C7DE6F62B6,
	JSONBool_set_AsBool_mDDD0D1CA7B06DAD37AA13987F46B95C6A1AEF489,
	JSONBool__ctor_m7593DD0B3E9628BA9BB4DE4A0E904BBA6BF151C1,
	JSONBool__ctor_m8E5E6A23827EE2B5DB5E7C86377805ADD8A771B2,
	JSONBool_WriteToStringBuilder_m7EA9750436CEE42357B6A109B83870F4FC2C403E,
	JSONBool_Equals_m57A524B4FEBF04285341E4FC42EDEDE7A524F4BB,
	JSONBool_GetHashCode_m49A9C417E05DFB795FAE0F38737C7629A3A69A22,
	JSONBool_SerializeBinary_m58E3D67CAB665202AF9AF699A0E2C267BE4A778A,
	JSONNull_CreateOrGet_m96465EECF543329FFBBC26B552EDB6E43D8DBC90,
	JSONNull__ctor_mA4FF0FB3001766DFB7A8D8C23063101AC377F277,
	JSONNull_get_Tag_m2A705402C448944F6395AAC88DA5DE7ADF99C4D1,
	JSONNull_get_IsNull_m1964A7B7E9C0FD17CDE2E4DFDF6345FE03EBA59B,
	JSONNull_GetEnumerator_m7B4ADD53B56B6663A0832AC611D7B47FA98D1A35,
	JSONNull_get_Value_m76B2E412BD13C31AD68C272CA9AF2E6063F165D3,
	JSONNull_set_Value_m713260989D4BA62C8B8CD716893585A1AA88ABE9,
	JSONNull_get_AsBool_m5E619608D71A63CBD234CA282B3BB2A7EFF385D5,
	JSONNull_set_AsBool_m267D1AA7D6003F371DC10DB65EB4E80643BB99F1,
	JSONNull_Equals_m3063912429EF72D2F9AFAEDF0D0303EB5D60A893,
	JSONNull_GetHashCode_m899F43B83E13ABFDE999C3781C45C6C03CC21077,
	JSONNull_WriteToStringBuilder_m66FC8C85A593D6198406753B845BFECA59117B46,
	JSONNull_SerializeBinary_m0D96E78D3AC9372DFC0EEF6F260367E9FCB4D939,
	JSONNull__cctor_mF5CFD47108FE35E808A8BA6514AA8283BBFD3193,
	JSONLazyCreator_get_Tag_m492C25FDFDC35D2EFB17711E1C259976A38BB46E,
	JSONLazyCreator_GetEnumerator_m6B2038B21158F725FC5B607544FB590A0E313AEC,
	JSONLazyCreator__ctor_m790C925B9AD99D9F574C556397F3C2D868D2F7EE,
	JSONLazyCreator__ctor_mE9B31AC731EAF8B853DA1671147CD5B9B5C6F10A,
	JSONLazyCreator_Set_m9C40ED5996DFD3D796BFAFBC25EEAB76F29F871F,
	JSONLazyCreator_get_Item_m0A09379FDB2D2CB95B781D1E3A03855661A0F446,
	JSONLazyCreator_set_Item_mE77E182C7C150529FBA7BD013A03F95FDCA25E9E,
	JSONLazyCreator_get_Item_m55D3BD8AF905099931604EACC6A907C17AACA753,
	JSONLazyCreator_set_Item_m8E286CF72FF8622427E69587C30C91BE0D242FCF,
	JSONLazyCreator_Add_m7393F681C0F1D9F798D3FEAE8C3A37C9F73E0E1B,
	JSONLazyCreator_Add_m8762C532A8EBAFF5AE213C3B2A9AC983653C8D37,
	JSONLazyCreator_op_Equality_mF472937FE1ABFEBCEB8A717561AB198CCB0F75E8,
	JSONLazyCreator_op_Inequality_m8C2EB320847B968D52F5E5D96599F734FEEE8ACA,
	JSONLazyCreator_Equals_m94A17EF99F288D175F1A40DC7B6569643CA84DD9,
	JSONLazyCreator_GetHashCode_m85E38D69ED1F0970E4EE3583196A4E9CD37C9DAF,
	JSONLazyCreator_get_AsInt_m8C8C09CC03C6E229A680D004C070734C424DA012,
	JSONLazyCreator_set_AsInt_m9CEB176958FE16A9213B7828EEAF442F54E70587,
	JSONLazyCreator_get_AsFloat_m1EC025D56616385B163C548183783BA8E4FD67E4,
	JSONLazyCreator_set_AsFloat_mAECCEF9C5598A5EA2204CDD1730D19B073050956,
	JSONLazyCreator_get_AsDouble_m019E3ACC054CCE18601A8731E772C86C62C09545,
	JSONLazyCreator_set_AsDouble_mBFEC4A9E9A946716829F584BE00CFA35D8DBA1E2,
	JSONLazyCreator_get_AsBool_mE7E32A21C51AFA46EC9BE4F78E96AE20986E7E00,
	JSONLazyCreator_set_AsBool_m700796091E7C1940E21834CE4BB1A8FD80017094,
	JSONLazyCreator_get_AsArray_mF737B0C7548A4C304278366931569BD173D4C2C6,
	JSONLazyCreator_get_AsObject_m0678D4A1725598B36657E07D1626B8387D3B4C88,
	JSONLazyCreator_WriteToStringBuilder_m20163F20F4F24E7E82C30AAA1878AE7DE301D0E9,
	JSONLazyCreator_SerializeBinary_m2E7AFA0F831BF1CBB93F6BC72BEB67A531A31FD5,
	JSON_Parse_mEAB5874EF687F205543F8469A9FAE7DB3C134421,
};
extern void Enumerator_get_IsValid_m078841FA11B23FAA03C8C0F0F5CFE1C7A33CC267_AdjustorThunk (void);
extern void Enumerator__ctor_m0A9EDBF3F33AA5FAE87C204FA885D965D16E8A44_AdjustorThunk (void);
extern void Enumerator__ctor_mDE6D3B06DF10240F0C637131119C8B09CE9EC4DC_AdjustorThunk (void);
extern void Enumerator_get_Current_m6C700A8C86DAB32F9FE845C3BE1E35D17757C65C_AdjustorThunk (void);
extern void Enumerator_MoveNext_m095925BE3D881DF1E015997DBEAFE0851722E53E_AdjustorThunk (void);
extern void ValueEnumerator__ctor_m1FF9C4191C706E48C3F9A191D0CE02F9BECDCE45_AdjustorThunk (void);
extern void ValueEnumerator__ctor_m8143FFB5AD9AB3936DF1A59E3492C2CF81793AF5_AdjustorThunk (void);
extern void ValueEnumerator__ctor_mE92051B3948C6A45D4F9265EA6D8329D70649D06_AdjustorThunk (void);
extern void ValueEnumerator_get_Current_mAB38FFFD4C1293C61EE73C825D85B7258B61961C_AdjustorThunk (void);
extern void ValueEnumerator_MoveNext_mE68E69AFB9D6FA2B41F55B066F3295490EED5589_AdjustorThunk (void);
extern void ValueEnumerator_GetEnumerator_mF841522782073D8D5F9782A37B2487BEA63E5E09_AdjustorThunk (void);
extern void KeyEnumerator__ctor_m6E59374D6FF2E994DFB9BFCA3759CC335CA48D23_AdjustorThunk (void);
extern void KeyEnumerator__ctor_mB67A74FB33429333A208FA269F011122C1AB6395_AdjustorThunk (void);
extern void KeyEnumerator__ctor_m5F83F3F252DE00DB6AC2AE59FD501F921C56F224_AdjustorThunk (void);
extern void KeyEnumerator_get_Current_mAC153090E0D57451FE61CA3651CF32A4CE21477C_AdjustorThunk (void);
extern void KeyEnumerator_MoveNext_mFF5FB4ECB623B58733E9B85846A296853462F2F4_AdjustorThunk (void);
extern void KeyEnumerator_GetEnumerator_m4534071ECBA3E7165BB0DC642EA9E827A29F5A87_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[17] = 
{
	{ 0x06000563, Enumerator_get_IsValid_m078841FA11B23FAA03C8C0F0F5CFE1C7A33CC267_AdjustorThunk },
	{ 0x06000564, Enumerator__ctor_m0A9EDBF3F33AA5FAE87C204FA885D965D16E8A44_AdjustorThunk },
	{ 0x06000565, Enumerator__ctor_mDE6D3B06DF10240F0C637131119C8B09CE9EC4DC_AdjustorThunk },
	{ 0x06000566, Enumerator_get_Current_m6C700A8C86DAB32F9FE845C3BE1E35D17757C65C_AdjustorThunk },
	{ 0x06000567, Enumerator_MoveNext_m095925BE3D881DF1E015997DBEAFE0851722E53E_AdjustorThunk },
	{ 0x06000568, ValueEnumerator__ctor_m1FF9C4191C706E48C3F9A191D0CE02F9BECDCE45_AdjustorThunk },
	{ 0x06000569, ValueEnumerator__ctor_m8143FFB5AD9AB3936DF1A59E3492C2CF81793AF5_AdjustorThunk },
	{ 0x0600056A, ValueEnumerator__ctor_mE92051B3948C6A45D4F9265EA6D8329D70649D06_AdjustorThunk },
	{ 0x0600056B, ValueEnumerator_get_Current_mAB38FFFD4C1293C61EE73C825D85B7258B61961C_AdjustorThunk },
	{ 0x0600056C, ValueEnumerator_MoveNext_mE68E69AFB9D6FA2B41F55B066F3295490EED5589_AdjustorThunk },
	{ 0x0600056D, ValueEnumerator_GetEnumerator_mF841522782073D8D5F9782A37B2487BEA63E5E09_AdjustorThunk },
	{ 0x0600056E, KeyEnumerator__ctor_m6E59374D6FF2E994DFB9BFCA3759CC335CA48D23_AdjustorThunk },
	{ 0x0600056F, KeyEnumerator__ctor_mB67A74FB33429333A208FA269F011122C1AB6395_AdjustorThunk },
	{ 0x06000570, KeyEnumerator__ctor_m5F83F3F252DE00DB6AC2AE59FD501F921C56F224_AdjustorThunk },
	{ 0x06000571, KeyEnumerator_get_Current_mAC153090E0D57451FE61CA3651CF32A4CE21477C_AdjustorThunk },
	{ 0x06000572, KeyEnumerator_MoveNext_mFF5FB4ECB623B58733E9B85846A296853462F2F4_AdjustorThunk },
	{ 0x06000573, KeyEnumerator_GetEnumerator_m4534071ECBA3E7165BB0DC642EA9E827A29F5A87_AdjustorThunk },
};
static const int32_t s_InvokerIndices[1555] = 
{
	2411,
	1054,
	2347,
	1054,
	1978,
	2411,
	2411,
	2347,
	2411,
	1964,
	2411,
	2377,
	2347,
	2411,
	2347,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	1978,
	2347,
	2411,
	1964,
	2411,
	2377,
	2347,
	2411,
	2347,
	2411,
	2411,
	2411,
	2411,
	1978,
	2411,
	2411,
	2411,
	475,
	2347,
	2347,
	2411,
	1150,
	1054,
	324,
	1978,
	1964,
	2411,
	2377,
	2347,
	2411,
	2347,
	1964,
	2411,
	2377,
	2347,
	2411,
	2347,
	2411,
	2411,
	1180,
	1153,
	1054,
	2411,
	1978,
	1397,
	2347,
	2411,
	2411,
	1150,
	1978,
	573,
	1978,
	1150,
	1054,
	324,
	1978,
	2411,
	2411,
	2411,
	2411,
	2411,
	1054,
	1497,
	2347,
	2411,
	1150,
	1978,
	573,
	1978,
	1964,
	2411,
	2377,
	2347,
	2411,
	2347,
	1964,
	2411,
	2377,
	2347,
	2411,
	2347,
	2411,
	508,
	589,
	2347,
	2411,
	1964,
	2411,
	2377,
	2347,
	2411,
	2347,
	1964,
	2411,
	2377,
	2347,
	2411,
	2347,
	1153,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	1978,
	1978,
	713,
	1153,
	1978,
	1978,
	2411,
	1502,
	1502,
	1502,
	1978,
	1978,
	3874,
	2411,
	1148,
	2411,
	2411,
	2411,
	1978,
	1978,
	1978,
	1978,
	1148,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	702,
	2411,
	1978,
	3874,
	2411,
	1978,
	1978,
	1978,
	1978,
	2411,
	1978,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	844,
	2411,
	1502,
	2411,
	2411,
	2411,
	2411,
	2411,
	1964,
	2411,
	2377,
	2347,
	2411,
	2347,
	2411,
	2411,
	1978,
	2411,
	2411,
	2411,
	2411,
	2347,
	2411,
	1964,
	2411,
	2377,
	2347,
	2411,
	2347,
	524,
	2411,
	524,
	2411,
	2347,
	1978,
	2347,
	1978,
	2347,
	1978,
	2347,
	1978,
	2347,
	1978,
	2411,
	2411,
	1978,
	1978,
	969,
	969,
	702,
	702,
	711,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2347,
	2411,
	1964,
	2411,
	2377,
	2347,
	2411,
	2347,
	2347,
	2411,
	1964,
	2411,
	2377,
	2347,
	2411,
	2347,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2347,
	2411,
	1964,
	2411,
	2377,
	2347,
	2411,
	2347,
	2411,
	2411,
	2411,
	2411,
	2411,
	1502,
	2347,
	2411,
	1964,
	2411,
	2377,
	2347,
	2411,
	2347,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	1964,
	2411,
	2411,
	2411,
	969,
	969,
	702,
	702,
	711,
	2411,
	2411,
	2411,
	2411,
	1978,
	1978,
	2411,
	2411,
	2411,
	2411,
	1978,
	2411,
	1978,
	1978,
	1978,
	1978,
	1964,
	2411,
	2411,
	2411,
	2411,
	1964,
	2411,
	2411,
	1964,
	2411,
	2411,
	2347,
	2411,
	1964,
	2411,
	2377,
	2347,
	2411,
	2347,
	2411,
	2411,
	2411,
	2411,
	1978,
	1502,
	1502,
	2411,
	1964,
	2411,
	2377,
	2347,
	2411,
	2347,
	1964,
	2411,
	2377,
	2347,
	2411,
	2347,
	2411,
	2411,
	2347,
	2347,
	2411,
	3874,
	1964,
	2411,
	2377,
	2347,
	2411,
	2347,
	1964,
	2411,
	2377,
	2347,
	2411,
	2347,
	2411,
	2411,
	2411,
	2411,
	2411,
	2347,
	2411,
	1964,
	2411,
	2377,
	2347,
	2411,
	2347,
	2411,
	2411,
	2411,
	2411,
	1978,
	2347,
	2411,
	1964,
	2411,
	2377,
	2347,
	2411,
	2347,
	2411,
	2411,
	2411,
	2411,
	1978,
	2347,
	2411,
	1964,
	2411,
	2377,
	2347,
	2411,
	2347,
	2411,
	2411,
	2411,
	2411,
	1978,
	2347,
	2411,
	1964,
	2411,
	2377,
	2347,
	2411,
	2347,
	2411,
	2411,
	2411,
	2411,
	1978,
	2347,
	2411,
	2411,
	790,
	1964,
	2411,
	2377,
	2347,
	2411,
	2347,
	2411,
	2411,
	1502,
	2347,
	2411,
	1964,
	2411,
	2377,
	2347,
	2411,
	2347,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2411,
	2411,
	2411,
	2411,
	1964,
	2411,
	2411,
	2006,
	2411,
	1153,
	1964,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	3874,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2038,
	2411,
	1978,
	1978,
	1978,
	2411,
	2411,
	2411,
	2411,
	2411,
	2038,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2013,
	2013,
	1964,
	1964,
	2411,
	2377,
	1730,
	2377,
	1730,
	1964,
	2332,
	2411,
	2411,
	1964,
	2411,
	2411,
	2411,
	2411,
	1964,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2006,
	706,
	1964,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	1978,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	1978,
	2411,
	2411,
	2411,
	2038,
	2411,
	1978,
	2332,
	1978,
	2411,
	2411,
	2411,
	1964,
	1964,
	2332,
	2411,
	2411,
	2411,
	2411,
	2411,
	1978,
	2411,
	2411,
	2411,
	2006,
	2411,
	2411,
	1148,
	1978,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	1964,
	2006,
	1978,
	1978,
	1978,
	1964,
	1964,
	1964,
	1978,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	1978,
	1964,
	2411,
	2411,
	1978,
	1978,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2377,
	2411,
	1964,
	2411,
	2411,
	2411,
	2411,
	2411,
	1964,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	1964,
	2013,
	1964,
	2377,
	1964,
	2332,
	2377,
	1730,
	2377,
	1730,
	2411,
	2411,
	702,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	1964,
	2411,
	1964,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	2411,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1978,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2411,
	1978,
	2411,
	1507,
	1502,
	1502,
	2013,
	1700,
	1507,
	1502,
	1502,
	1700,
	1502,
	2013,
	1502,
	2411,
	2411,
	1756,
	2411,
	1756,
	2332,
	1502,
	1153,
	1978,
	2411,
	1502,
	-1,
	1502,
	1502,
	-1,
	851,
	851,
	-1,
	573,
	573,
	1502,
	-1,
	1502,
	1502,
	-1,
	1502,
	1502,
	-1,
	1502,
	2332,
	2411,
	2013,
	2332,
	2347,
	1978,
	2347,
	1978,
	3810,
	3810,
	3849,
	2332,
	2347,
	1978,
	2332,
	1964,
	2411,
	1978,
	3843,
	1153,
	1153,
	1153,
	713,
	1153,
	715,
	2411,
	1978,
	2411,
	2013,
	1978,
	2411,
	2013,
	1153,
	1978,
	2411,
	1502,
	1502,
	-1,
	1502,
	1502,
	-1,
	851,
	851,
	-1,
	573,
	573,
	713,
	1153,
	1502,
	-1,
	3723,
	3723,
	1502,
	3723,
	3723,
	1502,
	-1,
	3723,
	3723,
	3849,
	3723,
	1502,
	1502,
	-1,
	1502,
	3547,
	3874,
	2062,
	1978,
	1978,
	1978,
	2411,
	1996,
	2411,
	1988,
	2411,
	2411,
	1978,
	2013,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2411,
	2411,
	2013,
	2411,
	1978,
	1978,
	2411,
	2411,
	1978,
	2411,
	2347,
	-1,
	-1,
	2411,
	1148,
	1978,
	2411,
	2013,
	2411,
	2411,
	2347,
	2411,
	851,
	2411,
	2411,
	2347,
	2013,
	2411,
	2347,
	-1,
	-1,
	2411,
	1148,
	1978,
	2411,
	2411,
	2013,
	2411,
	2411,
	1978,
	2411,
	2411,
	1978,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2411,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2411,
	1978,
	1153,
	2411,
	1978,
	1153,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3350,
	3723,
	3350,
	3234,
	3350,
	-1,
	-1,
	1964,
	2411,
	2377,
	2411,
	2347,
	2411,
	2347,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2377,
	2006,
	2377,
	2006,
	2333,
	1965,
	2347,
	1978,
	2347,
	1978,
	2411,
	1978,
	1153,
	241,
	2347,
	1978,
	2347,
	1978,
	2347,
	1978,
	2347,
	1978,
	2347,
	1978,
	2273,
	1902,
	2347,
	1978,
	2332,
	1964,
	2384,
	2013,
	2347,
	1978,
	2377,
	2006,
	2272,
	1901,
	2273,
	1902,
	2377,
	2006,
	2347,
	1978,
	2347,
	1978,
	2347,
	1978,
	2347,
	1978,
	2347,
	1978,
	2347,
	1978,
	2347,
	1978,
	2347,
	1978,
	2377,
	2006,
	2347,
	1978,
	2384,
	2333,
	2384,
	2333,
	1502,
	2377,
	2006,
	2377,
	2006,
	2411,
	2411,
	2347,
	1978,
	1978,
	2333,
	2347,
	2347,
	2347,
	2347,
	1502,
	2347,
	3849,
	3723,
	2411,
	3849,
	3849,
	3810,
	3874,
	3849,
	3810,
	3874,
	3547,
	-1,
	3547,
	3547,
	-1,
	-1,
	-1,
	-1,
	3222,
	3222,
	3547,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3222,
	3222,
	3547,
	-1,
	-1,
	-1,
	3547,
	3547,
	3547,
	3547,
	3723,
	-1,
	3723,
	3723,
	-1,
	-1,
	-1,
	-1,
	3350,
	3350,
	3723,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3350,
	3350,
	3723,
	-1,
	-1,
	-1,
	3723,
	3723,
	3723,
	3723,
	-1,
	-1,
	3334,
	2973,
	3350,
	1964,
	2411,
	2377,
	2347,
	2411,
	2347,
	3723,
	3426,
	3723,
	3350,
	2411,
	1575,
	3874,
	2411,
	1489,
	2347,
	2411,
	2347,
	2411,
	2347,
	2411,
	2347,
	2411,
	2332,
	1497,
	1054,
	1502,
	1153,
	2347,
	1978,
	2332,
	2377,
	2377,
	2377,
	2377,
	2377,
	2377,
	2377,
	2006,
	1153,
	1978,
	1502,
	1497,
	1502,
	2347,
	2347,
	2347,
	1497,
	459,
	2427,
	2347,
	2428,
	2429,
	2314,
	1944,
	2332,
	1964,
	2384,
	2013,
	2377,
	2006,
	2347,
	2347,
	3723,
	3723,
	3715,
	3643,
	3733,
	3780,
	3720,
	3667,
	3732,
	3764,
	3703,
	3426,
	3426,
	1700,
	2332,
	3849,
	3723,
	2974,
	3723,
	1978,
	1978,
	1978,
	1978,
	2347,
	1978,
	2347,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3720,
	3735,
	3736,
	3737,
	3724,
	3726,
	3723,
	3790,
	3794,
	3801,
	3742,
	3747,
	3723,
	1848,
	945,
	2404,
	599,
	1856,
	655,
	2406,
	370,
	1860,
	2407,
	1510,
	1522,
	2356,
	1503,
	1530,
	2362,
	1504,
	1502,
	2347,
	1502,
	2344,
	1501,
	2411,
	3874,
	2377,
	1878,
	1879,
	2265,
	2377,
	1878,
	1879,
	2058,
	2347,
	2377,
	2429,
	1878,
	1879,
	2058,
	2347,
	2377,
	2428,
	1978,
	2265,
	2347,
	2377,
	2411,
	2347,
	2411,
	2347,
	1964,
	2411,
	2377,
	2347,
	2411,
	2347,
	2347,
	2347,
	1964,
	2411,
	2377,
	2411,
	2411,
	2347,
	2411,
	2347,
	2347,
	2347,
	2377,
	2006,
	2332,
	2377,
	2427,
	1497,
	1054,
	1502,
	1153,
	2332,
	1153,
	1497,
	1502,
	2347,
	459,
	1978,
	2411,
	1964,
	2411,
	2377,
	2411,
	2347,
	2411,
	2347,
	2347,
	2347,
	2377,
	2006,
	2332,
	2377,
	2427,
	1502,
	1153,
	1497,
	1054,
	2332,
	1153,
	1502,
	1497,
	1502,
	2347,
	459,
	1978,
	2411,
	2411,
	1574,
	1964,
	2411,
	2377,
	2411,
	2347,
	2411,
	2347,
	2347,
	2347,
	2332,
	2377,
	2427,
	2347,
	1978,
	1978,
	459,
	1700,
	2332,
	1978,
	2332,
	2377,
	2427,
	2347,
	1978,
	2314,
	1944,
	1944,
	1978,
	459,
	3764,
	1700,
	2332,
	1978,
	2332,
	2377,
	2427,
	2347,
	1978,
	2377,
	2006,
	2006,
	1978,
	459,
	1700,
	2332,
	1978,
	3849,
	2411,
	2332,
	2377,
	2427,
	2347,
	1978,
	2377,
	2006,
	1700,
	2332,
	459,
	1978,
	3874,
	2332,
	2427,
	1978,
	1153,
	1978,
	1497,
	1054,
	1502,
	1153,
	1978,
	1153,
	3426,
	3426,
	1700,
	2332,
	2332,
	1964,
	2384,
	2013,
	2314,
	1944,
	2377,
	2006,
	2347,
	2347,
	459,
	1978,
	3723,
};
static const Il2CppTokenRangePair s_rgctxIndices[89] = 
{
	{ 0x0200006F, { 0, 4 } },
	{ 0x0200009D, { 4, 101 } },
	{ 0x0200009E, { 138, 1 } },
	{ 0x0200009F, { 139, 1 } },
	{ 0x020000A1, { 140, 4 } },
	{ 0x020000A2, { 144, 13 } },
	{ 0x020000A3, { 157, 4 } },
	{ 0x020000A4, { 161, 1 } },
	{ 0x020000A5, { 162, 3 } },
	{ 0x020000A6, { 165, 3 } },
	{ 0x020000A7, { 168, 1 } },
	{ 0x020000A8, { 169, 10 } },
	{ 0x020000A9, { 179, 3 } },
	{ 0x020000AA, { 182, 3 } },
	{ 0x020000AB, { 185, 1 } },
	{ 0x020000AC, { 186, 11 } },
	{ 0x020000AD, { 197, 2 } },
	{ 0x020000AE, { 199, 2 } },
	{ 0x020000B2, { 230, 3 } },
	{ 0x020000B3, { 233, 8 } },
	{ 0x020000B4, { 241, 10 } },
	{ 0x020000C7, { 277, 13 } },
	{ 0x020000CB, { 290, 3 } },
	{ 0x020000D2, { 293, 3 } },
	{ 0x020000D9, { 302, 2 } },
	{ 0x020000DA, { 304, 3 } },
	{ 0x020000DB, { 307, 4 } },
	{ 0x020000DF, { 319, 4 } },
	{ 0x020000E2, { 329, 2 } },
	{ 0x020000E3, { 331, 2 } },
	{ 0x060002DE, { 105, 1 } },
	{ 0x060002EC, { 106, 1 } },
	{ 0x060002EF, { 107, 1 } },
	{ 0x060002F2, { 108, 7 } },
	{ 0x060002F5, { 115, 6 } },
	{ 0x060002F8, { 121, 6 } },
	{ 0x060002FC, { 127, 6 } },
	{ 0x06000304, { 133, 5 } },
	{ 0x06000346, { 201, 14 } },
	{ 0x06000347, { 215, 7 } },
	{ 0x06000348, { 222, 8 } },
	{ 0x060003A5, { 251, 1 } },
	{ 0x060003A8, { 252, 1 } },
	{ 0x060003AB, { 253, 7 } },
	{ 0x060003B1, { 260, 6 } },
	{ 0x060003B8, { 266, 6 } },
	{ 0x060003BF, { 272, 5 } },
	{ 0x06000405, { 296, 2 } },
	{ 0x06000406, { 298, 2 } },
	{ 0x06000407, { 300, 2 } },
	{ 0x06000424, { 311, 3 } },
	{ 0x06000425, { 314, 3 } },
	{ 0x06000426, { 317, 2 } },
	{ 0x06000434, { 323, 3 } },
	{ 0x06000435, { 326, 3 } },
	{ 0x06000441, { 333, 1 } },
	{ 0x06000442, { 334, 1 } },
	{ 0x06000443, { 335, 2 } },
	{ 0x06000444, { 337, 2 } },
	{ 0x060004A4, { 339, 1 } },
	{ 0x060004A7, { 340, 1 } },
	{ 0x060004A8, { 341, 1 } },
	{ 0x060004A9, { 342, 1 } },
	{ 0x060004AA, { 343, 1 } },
	{ 0x060004AE, { 344, 1 } },
	{ 0x060004AF, { 345, 1 } },
	{ 0x060004B0, { 346, 1 } },
	{ 0x060004B1, { 347, 1 } },
	{ 0x060004B2, { 348, 1 } },
	{ 0x060004B3, { 349, 1 } },
	{ 0x060004B7, { 350, 1 } },
	{ 0x060004B8, { 351, 1 } },
	{ 0x060004B9, { 352, 1 } },
	{ 0x060004BF, { 353, 6 } },
	{ 0x060004C2, { 359, 1 } },
	{ 0x060004C3, { 360, 6 } },
	{ 0x060004C4, { 366, 1 } },
	{ 0x060004C5, { 367, 6 } },
	{ 0x060004C9, { 373, 1 } },
	{ 0x060004CA, { 374, 1 } },
	{ 0x060004CB, { 375, 6 } },
	{ 0x060004CC, { 381, 1 } },
	{ 0x060004CD, { 382, 1 } },
	{ 0x060004CE, { 383, 6 } },
	{ 0x060004D2, { 389, 1 } },
	{ 0x060004D3, { 390, 1 } },
	{ 0x060004D4, { 391, 6 } },
	{ 0x060004D9, { 397, 2 } },
	{ 0x060004DA, { 399, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[400] = 
{
	{ (Il2CppRGCTXDataType)2, 2725 },
	{ (Il2CppRGCTXDataType)1, 427 },
	{ (Il2CppRGCTXDataType)2, 427 },
	{ (Il2CppRGCTXDataType)3, 17122 },
	{ (Il2CppRGCTXDataType)3, 11938 },
	{ (Il2CppRGCTXDataType)3, 11929 },
	{ (Il2CppRGCTXDataType)2, 852 },
	{ (Il2CppRGCTXDataType)3, 365 },
	{ (Il2CppRGCTXDataType)3, 11928 },
	{ (Il2CppRGCTXDataType)3, 576 },
	{ (Il2CppRGCTXDataType)2, 2450 },
	{ (Il2CppRGCTXDataType)3, 7996 },
	{ (Il2CppRGCTXDataType)3, 7997 },
	{ (Il2CppRGCTXDataType)2, 804 },
	{ (Il2CppRGCTXDataType)3, 76 },
	{ (Il2CppRGCTXDataType)3, 77 },
	{ (Il2CppRGCTXDataType)3, 11922 },
	{ (Il2CppRGCTXDataType)3, 7998 },
	{ (Il2CppRGCTXDataType)3, 7999 },
	{ (Il2CppRGCTXDataType)3, 11904 },
	{ (Il2CppRGCTXDataType)2, 805 },
	{ (Il2CppRGCTXDataType)3, 80 },
	{ (Il2CppRGCTXDataType)3, 81 },
	{ (Il2CppRGCTXDataType)3, 11936 },
	{ (Il2CppRGCTXDataType)3, 11924 },
	{ (Il2CppRGCTXDataType)3, 11925 },
	{ (Il2CppRGCTXDataType)3, 11923 },
	{ (Il2CppRGCTXDataType)3, 11932 },
	{ (Il2CppRGCTXDataType)3, 11913 },
	{ (Il2CppRGCTXDataType)3, 11930 },
	{ (Il2CppRGCTXDataType)3, 11914 },
	{ (Il2CppRGCTXDataType)3, 11915 },
	{ (Il2CppRGCTXDataType)3, 11921 },
	{ (Il2CppRGCTXDataType)3, 11939 },
	{ (Il2CppRGCTXDataType)2, 806 },
	{ (Il2CppRGCTXDataType)3, 84 },
	{ (Il2CppRGCTXDataType)3, 11937 },
	{ (Il2CppRGCTXDataType)3, 85 },
	{ (Il2CppRGCTXDataType)3, 86 },
	{ (Il2CppRGCTXDataType)3, 11916 },
	{ (Il2CppRGCTXDataType)3, 87 },
	{ (Il2CppRGCTXDataType)3, 11926 },
	{ (Il2CppRGCTXDataType)2, 807 },
	{ (Il2CppRGCTXDataType)3, 92 },
	{ (Il2CppRGCTXDataType)2, 2906 },
	{ (Il2CppRGCTXDataType)3, 11912 },
	{ (Il2CppRGCTXDataType)3, 11935 },
	{ (Il2CppRGCTXDataType)3, 93 },
	{ (Il2CppRGCTXDataType)3, 94 },
	{ (Il2CppRGCTXDataType)3, 95 },
	{ (Il2CppRGCTXDataType)3, 11934 },
	{ (Il2CppRGCTXDataType)3, 11933 },
	{ (Il2CppRGCTXDataType)2, 809 },
	{ (Il2CppRGCTXDataType)3, 116 },
	{ (Il2CppRGCTXDataType)3, 117 },
	{ (Il2CppRGCTXDataType)3, 118 },
	{ (Il2CppRGCTXDataType)2, 810 },
	{ (Il2CppRGCTXDataType)3, 128 },
	{ (Il2CppRGCTXDataType)3, 129 },
	{ (Il2CppRGCTXDataType)3, 130 },
	{ (Il2CppRGCTXDataType)3, 11905 },
	{ (Il2CppRGCTXDataType)3, 11919 },
	{ (Il2CppRGCTXDataType)3, 11918 },
	{ (Il2CppRGCTXDataType)3, 11917 },
	{ (Il2CppRGCTXDataType)2, 813 },
	{ (Il2CppRGCTXDataType)3, 142 },
	{ (Il2CppRGCTXDataType)3, 143 },
	{ (Il2CppRGCTXDataType)2, 1508 },
	{ (Il2CppRGCTXDataType)3, 5574 },
	{ (Il2CppRGCTXDataType)3, 11931 },
	{ (Il2CppRGCTXDataType)3, 11920 },
	{ (Il2CppRGCTXDataType)2, 2906 },
	{ (Il2CppRGCTXDataType)2, 814 },
	{ (Il2CppRGCTXDataType)3, 146 },
	{ (Il2CppRGCTXDataType)3, 17045 },
	{ (Il2CppRGCTXDataType)3, 17005 },
	{ (Il2CppRGCTXDataType)3, 11964 },
	{ (Il2CppRGCTXDataType)2, 2919 },
	{ (Il2CppRGCTXDataType)2, 3720 },
	{ (Il2CppRGCTXDataType)2, 2919 },
	{ (Il2CppRGCTXDataType)3, 11963 },
	{ (Il2CppRGCTXDataType)3, 11965 },
	{ (Il2CppRGCTXDataType)3, 147 },
	{ (Il2CppRGCTXDataType)2, 921 },
	{ (Il2CppRGCTXDataType)3, 578 },
	{ (Il2CppRGCTXDataType)3, 17071 },
	{ (Il2CppRGCTXDataType)2, 819 },
	{ (Il2CppRGCTXDataType)3, 178 },
	{ (Il2CppRGCTXDataType)3, 179 },
	{ (Il2CppRGCTXDataType)3, 11927 },
	{ (Il2CppRGCTXDataType)2, 820 },
	{ (Il2CppRGCTXDataType)3, 182 },
	{ (Il2CppRGCTXDataType)3, 183 },
	{ (Il2CppRGCTXDataType)2, 824 },
	{ (Il2CppRGCTXDataType)3, 198 },
	{ (Il2CppRGCTXDataType)3, 199 },
	{ (Il2CppRGCTXDataType)3, 200 },
	{ (Il2CppRGCTXDataType)3, 201 },
	{ (Il2CppRGCTXDataType)2, 1498 },
	{ (Il2CppRGCTXDataType)3, 5570 },
	{ (Il2CppRGCTXDataType)3, 11908 },
	{ (Il2CppRGCTXDataType)2, 825 },
	{ (Il2CppRGCTXDataType)3, 206 },
	{ (Il2CppRGCTXDataType)3, 207 },
	{ (Il2CppRGCTXDataType)3, 208 },
	{ (Il2CppRGCTXDataType)3, 361 },
	{ (Il2CppRGCTXDataType)3, 11910 },
	{ (Il2CppRGCTXDataType)3, 11911 },
	{ (Il2CppRGCTXDataType)2, 808 },
	{ (Il2CppRGCTXDataType)3, 100 },
	{ (Il2CppRGCTXDataType)2, 2899 },
	{ (Il2CppRGCTXDataType)3, 11896 },
	{ (Il2CppRGCTXDataType)3, 11897 },
	{ (Il2CppRGCTXDataType)3, 101 },
	{ (Il2CppRGCTXDataType)3, 102 },
	{ (Il2CppRGCTXDataType)2, 811 },
	{ (Il2CppRGCTXDataType)3, 134 },
	{ (Il2CppRGCTXDataType)3, 135 },
	{ (Il2CppRGCTXDataType)2, 1502 },
	{ (Il2CppRGCTXDataType)3, 5571 },
	{ (Il2CppRGCTXDataType)3, 11906 },
	{ (Il2CppRGCTXDataType)2, 812 },
	{ (Il2CppRGCTXDataType)3, 138 },
	{ (Il2CppRGCTXDataType)3, 139 },
	{ (Il2CppRGCTXDataType)2, 1507 },
	{ (Il2CppRGCTXDataType)3, 5573 },
	{ (Il2CppRGCTXDataType)3, 11909 },
	{ (Il2CppRGCTXDataType)2, 818 },
	{ (Il2CppRGCTXDataType)3, 174 },
	{ (Il2CppRGCTXDataType)3, 175 },
	{ (Il2CppRGCTXDataType)2, 1506 },
	{ (Il2CppRGCTXDataType)3, 5572 },
	{ (Il2CppRGCTXDataType)3, 11907 },
	{ (Il2CppRGCTXDataType)2, 827 },
	{ (Il2CppRGCTXDataType)3, 216 },
	{ (Il2CppRGCTXDataType)3, 217 },
	{ (Il2CppRGCTXDataType)3, 218 },
	{ (Il2CppRGCTXDataType)3, 17418 },
	{ (Il2CppRGCTXDataType)3, 11945 },
	{ (Il2CppRGCTXDataType)3, 11946 },
	{ (Il2CppRGCTXDataType)3, 11949 },
	{ (Il2CppRGCTXDataType)3, 5641 },
	{ (Il2CppRGCTXDataType)3, 11947 },
	{ (Il2CppRGCTXDataType)3, 11948 },
	{ (Il2CppRGCTXDataType)3, 5587 },
	{ (Il2CppRGCTXDataType)3, 103 },
	{ (Il2CppRGCTXDataType)2, 2222 },
	{ (Il2CppRGCTXDataType)3, 104 },
	{ (Il2CppRGCTXDataType)2, 862 },
	{ (Il2CppRGCTXDataType)3, 372 },
	{ (Il2CppRGCTXDataType)3, 105 },
	{ (Il2CppRGCTXDataType)3, 11958 },
	{ (Il2CppRGCTXDataType)3, 11959 },
	{ (Il2CppRGCTXDataType)3, 11957 },
	{ (Il2CppRGCTXDataType)3, 5643 },
	{ (Il2CppRGCTXDataType)3, 106 },
	{ (Il2CppRGCTXDataType)3, 107 },
	{ (Il2CppRGCTXDataType)3, 5588 },
	{ (Il2CppRGCTXDataType)3, 119 },
	{ (Il2CppRGCTXDataType)3, 120 },
	{ (Il2CppRGCTXDataType)3, 121 },
	{ (Il2CppRGCTXDataType)3, 368 },
	{ (Il2CppRGCTXDataType)3, 5589 },
	{ (Il2CppRGCTXDataType)3, 11960 },
	{ (Il2CppRGCTXDataType)2, 2916 },
	{ (Il2CppRGCTXDataType)3, 5590 },
	{ (Il2CppRGCTXDataType)3, 11961 },
	{ (Il2CppRGCTXDataType)2, 2917 },
	{ (Il2CppRGCTXDataType)3, 5591 },
	{ (Il2CppRGCTXDataType)2, 817 },
	{ (Il2CppRGCTXDataType)3, 168 },
	{ (Il2CppRGCTXDataType)3, 169 },
	{ (Il2CppRGCTXDataType)2, 2217 },
	{ (Il2CppRGCTXDataType)3, 170 },
	{ (Il2CppRGCTXDataType)2, 858 },
	{ (Il2CppRGCTXDataType)3, 369 },
	{ (Il2CppRGCTXDataType)3, 148 },
	{ (Il2CppRGCTXDataType)3, 11967 },
	{ (Il2CppRGCTXDataType)3, 11966 },
	{ (Il2CppRGCTXDataType)3, 11970 },
	{ (Il2CppRGCTXDataType)3, 11968 },
	{ (Il2CppRGCTXDataType)3, 11969 },
	{ (Il2CppRGCTXDataType)3, 5592 },
	{ (Il2CppRGCTXDataType)3, 11962 },
	{ (Il2CppRGCTXDataType)2, 2918 },
	{ (Il2CppRGCTXDataType)3, 5593 },
	{ (Il2CppRGCTXDataType)2, 822 },
	{ (Il2CppRGCTXDataType)3, 190 },
	{ (Il2CppRGCTXDataType)3, 191 },
	{ (Il2CppRGCTXDataType)2, 2218 },
	{ (Il2CppRGCTXDataType)3, 184 },
	{ (Il2CppRGCTXDataType)2, 859 },
	{ (Il2CppRGCTXDataType)3, 370 },
	{ (Il2CppRGCTXDataType)3, 185 },
	{ (Il2CppRGCTXDataType)3, 11952 },
	{ (Il2CppRGCTXDataType)3, 11951 },
	{ (Il2CppRGCTXDataType)3, 11950 },
	{ (Il2CppRGCTXDataType)3, 11954 },
	{ (Il2CppRGCTXDataType)3, 11953 },
	{ (Il2CppRGCTXDataType)3, 11956 },
	{ (Il2CppRGCTXDataType)3, 11955 },
	{ (Il2CppRGCTXDataType)2, 803 },
	{ (Il2CppRGCTXDataType)3, 66 },
	{ (Il2CppRGCTXDataType)2, 2921 },
	{ (Il2CppRGCTXDataType)3, 11971 },
	{ (Il2CppRGCTXDataType)3, 67 },
	{ (Il2CppRGCTXDataType)2, 848 },
	{ (Il2CppRGCTXDataType)3, 362 },
	{ (Il2CppRGCTXDataType)2, 2209 },
	{ (Il2CppRGCTXDataType)3, 68 },
	{ (Il2CppRGCTXDataType)3, 69 },
	{ (Il2CppRGCTXDataType)2, 861 },
	{ (Il2CppRGCTXDataType)3, 371 },
	{ (Il2CppRGCTXDataType)2, 2219 },
	{ (Il2CppRGCTXDataType)3, 70 },
	{ (Il2CppRGCTXDataType)3, 17427 },
	{ (Il2CppRGCTXDataType)3, 17430 },
	{ (Il2CppRGCTXDataType)2, 794 },
	{ (Il2CppRGCTXDataType)3, 51 },
	{ (Il2CppRGCTXDataType)2, 1557 },
	{ (Il2CppRGCTXDataType)3, 5607 },
	{ (Il2CppRGCTXDataType)3, 6134 },
	{ (Il2CppRGCTXDataType)3, 17428 },
	{ (Il2CppRGCTXDataType)3, 17429 },
	{ (Il2CppRGCTXDataType)3, 17431 },
	{ (Il2CppRGCTXDataType)2, 797 },
	{ (Il2CppRGCTXDataType)3, 56 },
	{ (Il2CppRGCTXDataType)2, 1558 },
	{ (Il2CppRGCTXDataType)3, 5608 },
	{ (Il2CppRGCTXDataType)3, 6135 },
	{ (Il2CppRGCTXDataType)3, 17679 },
	{ (Il2CppRGCTXDataType)3, 11973 },
	{ (Il2CppRGCTXDataType)3, 11972 },
	{ (Il2CppRGCTXDataType)2, 795 },
	{ (Il2CppRGCTXDataType)3, 52 },
	{ (Il2CppRGCTXDataType)2, 795 },
	{ (Il2CppRGCTXDataType)3, 13724 },
	{ (Il2CppRGCTXDataType)3, 13718 },
	{ (Il2CppRGCTXDataType)3, 13719 },
	{ (Il2CppRGCTXDataType)3, 13725 },
	{ (Il2CppRGCTXDataType)3, 17681 },
	{ (Il2CppRGCTXDataType)2, 798 },
	{ (Il2CppRGCTXDataType)3, 57 },
	{ (Il2CppRGCTXDataType)2, 798 },
	{ (Il2CppRGCTXDataType)3, 13726 },
	{ (Il2CppRGCTXDataType)3, 13720 },
	{ (Il2CppRGCTXDataType)3, 13721 },
	{ (Il2CppRGCTXDataType)3, 13727 },
	{ (Il2CppRGCTXDataType)3, 13722 },
	{ (Il2CppRGCTXDataType)3, 13723 },
	{ (Il2CppRGCTXDataType)3, 17683 },
	{ (Il2CppRGCTXDataType)3, 17422 },
	{ (Il2CppRGCTXDataType)3, 17423 },
	{ (Il2CppRGCTXDataType)2, 816 },
	{ (Il2CppRGCTXDataType)3, 152 },
	{ (Il2CppRGCTXDataType)2, 2898 },
	{ (Il2CppRGCTXDataType)3, 11894 },
	{ (Il2CppRGCTXDataType)3, 11895 },
	{ (Il2CppRGCTXDataType)3, 153 },
	{ (Il2CppRGCTXDataType)3, 154 },
	{ (Il2CppRGCTXDataType)2, 823 },
	{ (Il2CppRGCTXDataType)3, 194 },
	{ (Il2CppRGCTXDataType)3, 195 },
	{ (Il2CppRGCTXDataType)2, 1443 },
	{ (Il2CppRGCTXDataType)3, 5512 },
	{ (Il2CppRGCTXDataType)3, 17419 },
	{ (Il2CppRGCTXDataType)2, 828 },
	{ (Il2CppRGCTXDataType)3, 222 },
	{ (Il2CppRGCTXDataType)3, 223 },
	{ (Il2CppRGCTXDataType)2, 1440 },
	{ (Il2CppRGCTXDataType)3, 5510 },
	{ (Il2CppRGCTXDataType)3, 17417 },
	{ (Il2CppRGCTXDataType)2, 831 },
	{ (Il2CppRGCTXDataType)3, 244 },
	{ (Il2CppRGCTXDataType)3, 245 },
	{ (Il2CppRGCTXDataType)3, 246 },
	{ (Il2CppRGCTXDataType)3, 17416 },
	{ (Il2CppRGCTXDataType)3, 5511 },
	{ (Il2CppRGCTXDataType)3, 155 },
	{ (Il2CppRGCTXDataType)2, 2214 },
	{ (Il2CppRGCTXDataType)3, 156 },
	{ (Il2CppRGCTXDataType)2, 855 },
	{ (Il2CppRGCTXDataType)3, 367 },
	{ (Il2CppRGCTXDataType)3, 157 },
	{ (Il2CppRGCTXDataType)3, 11941 },
	{ (Il2CppRGCTXDataType)3, 11942 },
	{ (Il2CppRGCTXDataType)3, 11940 },
	{ (Il2CppRGCTXDataType)3, 5642 },
	{ (Il2CppRGCTXDataType)3, 158 },
	{ (Il2CppRGCTXDataType)3, 159 },
	{ (Il2CppRGCTXDataType)3, 5507 },
	{ (Il2CppRGCTXDataType)3, 11943 },
	{ (Il2CppRGCTXDataType)2, 2908 },
	{ (Il2CppRGCTXDataType)3, 5508 },
	{ (Il2CppRGCTXDataType)3, 11944 },
	{ (Il2CppRGCTXDataType)2, 2909 },
	{ (Il2CppRGCTXDataType)2, 3069 },
	{ (Il2CppRGCTXDataType)3, 13715 },
	{ (Il2CppRGCTXDataType)2, 3087 },
	{ (Il2CppRGCTXDataType)3, 13762 },
	{ (Il2CppRGCTXDataType)2, 3092 },
	{ (Il2CppRGCTXDataType)3, 13789 },
	{ (Il2CppRGCTXDataType)3, 13716 },
	{ (Il2CppRGCTXDataType)3, 13717 },
	{ (Il2CppRGCTXDataType)3, 13763 },
	{ (Il2CppRGCTXDataType)3, 13764 },
	{ (Il2CppRGCTXDataType)3, 13765 },
	{ (Il2CppRGCTXDataType)3, 13790 },
	{ (Il2CppRGCTXDataType)3, 13791 },
	{ (Il2CppRGCTXDataType)3, 13792 },
	{ (Il2CppRGCTXDataType)3, 13793 },
	{ (Il2CppRGCTXDataType)2, 1787 },
	{ (Il2CppRGCTXDataType)2, 1906 },
	{ (Il2CppRGCTXDataType)3, 360 },
	{ (Il2CppRGCTXDataType)2, 1788 },
	{ (Il2CppRGCTXDataType)2, 1907 },
	{ (Il2CppRGCTXDataType)3, 574 },
	{ (Il2CppRGCTXDataType)2, 837 },
	{ (Il2CppRGCTXDataType)3, 298 },
	{ (Il2CppRGCTXDataType)2, 539 },
	{ (Il2CppRGCTXDataType)2, 838 },
	{ (Il2CppRGCTXDataType)3, 299 },
	{ (Il2CppRGCTXDataType)3, 300 },
	{ (Il2CppRGCTXDataType)2, 826 },
	{ (Il2CppRGCTXDataType)3, 212 },
	{ (Il2CppRGCTXDataType)3, 213 },
	{ (Il2CppRGCTXDataType)2, 830 },
	{ (Il2CppRGCTXDataType)3, 228 },
	{ (Il2CppRGCTXDataType)3, 229 },
	{ (Il2CppRGCTXDataType)3, 17260 },
	{ (Il2CppRGCTXDataType)3, 673 },
	{ (Il2CppRGCTXDataType)3, 17252 },
	{ (Il2CppRGCTXDataType)3, 676 },
	{ (Il2CppRGCTXDataType)3, 17261 },
	{ (Il2CppRGCTXDataType)3, 17262 },
	{ (Il2CppRGCTXDataType)2, 3216 },
	{ (Il2CppRGCTXDataType)3, 14747 },
	{ (Il2CppRGCTXDataType)2, 3217 },
	{ (Il2CppRGCTXDataType)3, 14748 },
	{ (Il2CppRGCTXDataType)3, 17225 },
	{ (Il2CppRGCTXDataType)3, 17468 },
	{ (Il2CppRGCTXDataType)3, 17530 },
	{ (Il2CppRGCTXDataType)3, 17479 },
	{ (Il2CppRGCTXDataType)3, 17223 },
	{ (Il2CppRGCTXDataType)3, 17487 },
	{ (Il2CppRGCTXDataType)3, 17488 },
	{ (Il2CppRGCTXDataType)3, 17531 },
	{ (Il2CppRGCTXDataType)3, 17498 },
	{ (Il2CppRGCTXDataType)3, 17499 },
	{ (Il2CppRGCTXDataType)3, 17224 },
	{ (Il2CppRGCTXDataType)3, 17519 },
	{ (Il2CppRGCTXDataType)3, 17520 },
	{ (Il2CppRGCTXDataType)3, 17532 },
	{ (Il2CppRGCTXDataType)2, 2903 },
	{ (Il2CppRGCTXDataType)3, 11901 },
	{ (Il2CppRGCTXDataType)3, 17511 },
	{ (Il2CppRGCTXDataType)2, 948 },
	{ (Il2CppRGCTXDataType)3, 672 },
	{ (Il2CppRGCTXDataType)3, 17529 },
	{ (Il2CppRGCTXDataType)3, 17464 },
	{ (Il2CppRGCTXDataType)2, 2900 },
	{ (Il2CppRGCTXDataType)3, 11898 },
	{ (Il2CppRGCTXDataType)3, 17508 },
	{ (Il2CppRGCTXDataType)2, 945 },
	{ (Il2CppRGCTXDataType)3, 669 },
	{ (Il2CppRGCTXDataType)3, 17467 },
	{ (Il2CppRGCTXDataType)3, 17471 },
	{ (Il2CppRGCTXDataType)2, 2923 },
	{ (Il2CppRGCTXDataType)3, 11974 },
	{ (Il2CppRGCTXDataType)3, 17512 },
	{ (Il2CppRGCTXDataType)2, 960 },
	{ (Il2CppRGCTXDataType)3, 674 },
	{ (Il2CppRGCTXDataType)3, 17478 },
	{ (Il2CppRGCTXDataType)3, 17482 },
	{ (Il2CppRGCTXDataType)3, 17483 },
	{ (Il2CppRGCTXDataType)2, 2901 },
	{ (Il2CppRGCTXDataType)3, 11899 },
	{ (Il2CppRGCTXDataType)3, 17509 },
	{ (Il2CppRGCTXDataType)2, 946 },
	{ (Il2CppRGCTXDataType)3, 670 },
	{ (Il2CppRGCTXDataType)3, 17486 },
	{ (Il2CppRGCTXDataType)3, 17494 },
	{ (Il2CppRGCTXDataType)3, 17495 },
	{ (Il2CppRGCTXDataType)2, 2924 },
	{ (Il2CppRGCTXDataType)3, 11975 },
	{ (Il2CppRGCTXDataType)3, 17513 },
	{ (Il2CppRGCTXDataType)2, 961 },
	{ (Il2CppRGCTXDataType)3, 675 },
	{ (Il2CppRGCTXDataType)3, 17497 },
	{ (Il2CppRGCTXDataType)3, 17515 },
	{ (Il2CppRGCTXDataType)3, 17516 },
	{ (Il2CppRGCTXDataType)2, 2902 },
	{ (Il2CppRGCTXDataType)3, 11900 },
	{ (Il2CppRGCTXDataType)3, 17510 },
	{ (Il2CppRGCTXDataType)2, 947 },
	{ (Il2CppRGCTXDataType)3, 671 },
	{ (Il2CppRGCTXDataType)3, 17518 },
	{ (Il2CppRGCTXDataType)3, 11902 },
	{ (Il2CppRGCTXDataType)3, 11903 },
	{ (Il2CppRGCTXDataType)3, 17505 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	1555,
	s_methodPointers,
	17,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	89,
	s_rgctxIndices,
	400,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
